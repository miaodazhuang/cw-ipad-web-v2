const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const AssetsPlugin = require('assets-webpack-plugin')
const CommonsPkg = require('../bundle-conf-dev.json')
// const CommonsPkg = null;

process.env.NODE_ENV = 'development';

module.exports = {
  entry: {
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'dva',
      'dva/router',
      'rc-tree',
      'js-base64',
      'js-cookie',
      'md5',
      'keymaster',
      'id-validator',
      'classnames',
      'echarts',
      'lodash',
      'promise',
      'semantic-ui-css',
      'semantic-ui-react',
      'socket.io-client',
      'moment'

    ]
  },
  output: {
    path: path.join(__dirname, '../public/static/lib'),
    filename: 'devdll.[name].[chunkhash].js',
    library: '[name]_[chunkhash]_dev'
  },
  plugins: [
    new webpack.DllPlugin({
      // 会生成一个json文件，里面是关于dll.js的一些配置信息
      path: path.join(__dirname, '../[name]-manifest-dev.json'),
      name: '[name]_[chunkhash]_dev', // 与上面output中配置对应
      context: path.join(__dirname, '../') // 上下文环境路径（必填，为了与DllReferencePlugin存在与同一上下文中）
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development')
    }),
    new AssetsPlugin({
      filename: 'bundle-conf-dev.json',
      path: path.join(__dirname, '../')
    }),
    new CleanWebpackPlugin([CommonsPkg ? CommonsPkg.vendor.js : ""], {
      root: path.join(process.cwd(), "public/static/lib")
    })
  ]
}