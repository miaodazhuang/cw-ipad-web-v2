/**
 * 自定义插件
 * lxy
 */
const _ = require('lodash');
class HtmlWebpackHandleCssInjectPlugin {
  constructor(options = {}) {
    this.options = options;
  }

  apply(compiler) {
    // Hook into the html-webpack-plugin processing
    compiler.plugin('compilation', (compilation) => {
      compilation.plugin('html-webpack-plugin-before-html-processing', function (htmlPluginData, callback) {
        //const { lang ,version} = this.options;
        let appcss = _.last(htmlPluginData.assets.css);
        //let themecss = `/${version}/static/css/theme-${lang}.css`;
        htmlPluginData.assets.css = [];
        htmlPluginData.assets.css.push(appcss);
        //htmlPluginData.assets.css.push(themecss);
        //console.log('htmlPluginData.assets.css', htmlPluginData.assets.css);
        callback(null, htmlPluginData);
      });
    });
  }
}

module.exports = HtmlWebpackHandleCssInjectPlugin;