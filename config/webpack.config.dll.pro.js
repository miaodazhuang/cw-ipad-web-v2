const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const AssetsPlugin = require('assets-webpack-plugin')
const CommonsPkg = require('../bundle-conf-pro.json')

process.env.NODE_ENV = 'production';

module.exports = {
  entry: {
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'dva',
      'dva/router',
      'rc-tree',
      'js-base64',
      'js-cookie',
      'md5',
      'keymaster',
      'id-validator',
      'classnames',
      'echarts',
      'lodash',
      'promise',
      'semantic-ui-css',
      'semantic-ui-react',
      'socket.io-client',
      'moment'

    ]
  },
  output: {
    path: path.join(__dirname, '../public/static/lib'),
    filename: 'prodll.[name].[chunkhash].js',
    library: '[name]_[chunkhash]_pro'
  },
  plugins: [
    new webpack.DllPlugin({
      // 会生成一个json文件，里面是关于dll.js的一些配置信息
      path: path.join(__dirname, '../[name]-manifest-pro.json'),
      name: '[name]_[chunkhash]_pro', // 与上面output中配置对应
      context: path.join(__dirname, '../') // 上下文环境路径（必填，为了与DllReferencePlugin存在与同一上下文中）
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new AssetsPlugin({
      filename: 'bundle-conf-pro.json',
      path: path.join(__dirname, '../')
    }),
    new CleanWebpackPlugin([CommonsPkg ? CommonsPkg.vendor.js : ""], {
      root: path.join(process.cwd(), "public/static/lib")
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        // Disabled because of an issue with Uglify breaking seemingly valid code:
        // https://github.com/facebookincubator/create-react-app/issues/2376
        // Pending further investigation:
        // https://github.com/mishoo/UglifyJS2/issues/2011
        comparisons: false,
      },
      mangle: {
        safari10: true,
      },
      output: {
        comments: false,
        // Turned on because emoji and regex is not minified properly using default
        // https://github.com/facebookincubator/create-react-app/issues/2488
        ascii_only: true,
      },
      sourceMap: false,
    }),
  ]
}