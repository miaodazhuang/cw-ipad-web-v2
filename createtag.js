var util = require('util'),
  spawn = require('child_process').spawn;
//     // ls = spawn('git',['-m','./']);
//     var arguments = process.argv.splice(2);
//     console.log(arguments);
// process.argv.forEach(function(val,index,array){
//   console.log(index + ': ' + val);
// });

// var arguments = process.argv.splice(2);

var program = require('commander');
var fs = require('fs');
var _ = require('lodash');

program
  .version('0.1.0')
  .option('-a, --tagname [value]', 'Tag 版本号')
  .option('-m, --message [value]', 'Tag 描述信息', 'bug 版本')
  .parse(process.argv);

if (!program.tagname) {
  console.error("tag 版本号必须输入");
}
if (!program.message) {
  console.error("tag 版本描述必须输入");
}

// ls = spawn('git', ['tag', '-a', program.tagname, '-m', program.message]);
// ls = spawn('git', ['tag']);

// ls.stdout.on('data', function (data) {
//   console.log('data', data.toString());
//   console.log(program.tagname + ' ' + program.message);
// });
// ls.stderr.on('data', function (data) {
//   console.log('It\'s a miracle!');
//   console.log(data);
// });
// ls.on('close',function(code){
//   console.log('close code :'+code);
// })
// ls.on('exit', function (code) {
//   console.log('exit code :'+code);
// });

// var result = JSON.parse(fs.readFileSync('package.json'));
// console.log(result);
// result.Version = program.tagname;
// fs.writeFileSync('package.json.bk', JSON.stringify(result, null, 2));
/**
* 读取文件
*
* @param {any} filename
* @param {any} encoding
* @returns
*/
function readFile(filename, encoding) {
  return new Promise(function (resolve, reject) {
    fs.readFile(filename, encoding, function (err, res) {
      if (err) {
        return reject(err);
      }
      resolve(res);
    })
  })
}
/**
* 写入文件
*
* @param {any} filename
* @param {any} data
* @param {any} encoding
* @returns
*/
function writeFile(filename, data, encoding) {
  return new Promise(function (resolve, reject) {
    fs.writeFile(filename, data, encoding, function (err) {
      if (err) {
        return reject(err);
      }
      resolve();
    })
  })
}

/**
* 执行命令
*
* @param {any} filedata
* @param {any} resolveMsg
* @param {any} rejectMsg
* @returns
*/
function SpawnGitCommander(data, gitparameters, resolveMsg, rejectMsg) {
  return new Promise(function (resolve, reject) {
    ls = spawn('git', gitparameters);
    ls.stderr.on('data', function (data) {
      console.log(_.trim(data.toString()));
    });
    ls.on('exit', function (code) {
      if (code === 0) {
        console.log(resolveMsg);
        resolve(data);
      } else {
        console.log(rejectMsg + code);
        reject('提交失败');
      }
    });
  })
}

/**
* 创建tag方法
*
*/
function CreateTag() {
  // 读取package文件
  readFile('package.json', 'utf-8')
    // 序列化
    .then(JSON.parse)
    // 更新版本
    .then(data => {
      data.Version = program.tagname;
      return data;
    })
    // 更新package文件
    .then(data => {
      return writeFile('package.json', JSON.stringify(data, null, 2), 'utf-8');
    })
    // 提交文件
    .then(data => SpawnGitCommander(data, ['commit', '-a', '-m', '更新版本'], '版本更新成功', '版本更新失败'))
    // 推送到服务器
    .then(data => SpawnGitCommander(data, ['push'], '推送到服务器成功', '推送到服务器失败'))
    // 创建tag成功
    .then(data => SpawnGitCommander(data, ['tag', '-a', program.tagname, '-m', program.message], '创建tag成功', '创建tag失败'))
    // 推送tag到服务器
    .then(data => SpawnGitCommander(data, ['push', 'origin', program.tagname], '推送到服务器成功', '推送到服务器失败'))
    // 完成
    .then(data => console.log('创建TAG完成'))
    .catch(err => {
      console.error('err', err);
    })
}

CreateTag();