import dynamic from 'Hyutilities/dynamicload_help';
export default(app) => {
  return dynamic({
    app: app,
    title: 'baseplugin',
    isModal: true,
    component: () => import ('./component')
  });
}