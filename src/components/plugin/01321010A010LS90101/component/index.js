/**
 * 右侧通用plugin组件
 * namespace:string model空间
 * rightPluginData:array 数据源 例子
 *         rightPluginData:[
 *          {
 *             "deposit": {
                      "text": languageConfig.deposit,//"押金"
                      "subText": "Ctrl+A",
                      "className": "icon20 img63",
                      "permsCode": permsData["32400301"],
                      "authCode":  authsData["0132-2000-0010-L-A-01"],
                    },
 *          },
 *         ]
 * lxy
*/
import React from 'react';
import { connect } from 'dva';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Icon } from 'semantic-ui-react'
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';


class Plu01321010A010LS90101 extends React.Component {

  static defaultProps = {
    // 关联的命名空间名称
    namespace: ""
  }

  static propTypes = {
    // 命名空间名字为字符串
    namespace: PropTypes.string
  }


  render() {
    const rightPluginData = this.getRightPluginData();
    let com = this.getCom(rightPluginData);
    return (
      <div className={'Plugin-COM'}>
        {com}
      </div>
    );
  }


  /**
   * 整理数据
   *
   * @memberof HyRightPluginCom
   */
  getRightPluginData = () => {

    let { defaultPluginData,rightOperBottom } = this.props;
    let rightPluginData = [];
    let _temp = {};
    let _allBtnObj = {};
    // _.forEach(rightOperTop, (v, k) => {
    //   _allBtnObj[k] = {...v,"main":"true"};
    //   if (_.has(v, 'list')) {
    //     _.forEach(v.list, (v, _k) => {
    //       _allBtnObj[_k] = { ...v, "parentname": k };
    //     })
    //   }else{
    //     _allBtnObj[k]=_.omit(_allBtnObj[k],'main');
    //   }
    // });
    _.forEach(rightOperBottom, (v, k) => {
      _allBtnObj[k] = {...v,"main":"true"};
      if (_.has(v, 'list')) {
        _.forEach(v.list, (v, _k) => {
          _allBtnObj[_k] = { ...v, "parentname": k };
        })
      }else{
        _allBtnObj[k]=_.omit(_allBtnObj[k],'main');
      }
    });
    // _.forEach(rightOperBottom, (v, k) => {
    //   _allBtnObj[k] = {...v,"main":"true"};
    //   if (_.has(v, 'list')) {
    //     _.forEach(v.list, (v, _k) => {
    //       _allBtnObj[_k] = { ...v, "parentname": k };
    //     })
    //   }else{
    //     _allBtnObj[k]=_.omit(_allBtnObj[k],'main');
    //   }
    // });
    //如果当前页面根本没有按钮直接返回
    if (_.size(_allBtnObj) === 0) {
      return rightPluginData;
    }
    //如果defaultPluginData为空则直接显示当前按钮
    if (_.size(defaultPluginData) === 0 || defaultPluginData === null) {
      _temp["default"] = {};
      _.forEach(_allBtnObj, (v, k) => {
        _temp["default"] = Object.assign(_temp["default"], { [k]: { ...v } })
      })
    } else {
      _.forEach(defaultPluginData, (value, index) => {
        if (!_.has(_temp, value.shortkeygrp_cd)) {
          _temp[value.shortkeygrp_cd] = {};
        }
        _.forEach(_allBtnObj, (v, k) => {
          if (v.keyboard === value.shortkey_cd) {
            _temp[value.shortkeygrp_cd] = Object.assign(_temp[value.shortkeygrp_cd], { [k]: { ...v, subText: value.shortkey } })
          } else {
            if(!_.has(_temp[value.shortkeygrp_cd],k) && !_.has(v,'main')){
              _temp[value.shortkeygrp_cd] = Object.assign(_temp[value.shortkeygrp_cd], { [k]: { ...v } })
            }
          }
        })
      });
    }
    if (_.size(_temp) !== 0) {
      rightPluginData = _.map(_temp, (i) => {
        return i;
      })
    }
    return rightPluginData;
  }





  /**
   * 获取组件
   *
   * @memberof HyRightPluginCom
   */
  getCom = (data) => {
    let com = [];
    if (!_.isArray(data) || _.size(data) === 0) {
      return null;
    } else {
      _.forEach(data, (value, index) => {
        let coms = this._getButtonCom(value);
        com.push(<div key={`${index}`} className='PluginItem'>{coms}</div>)
      })
      return com;
    }
  }



  /**
   * 获取本组按钮
   *
   * @memberof HyRightPluginCom
   */
  _getButtonCom = (list) => {
    let coms = [];
    _.forEach(list, (value, key) => {
      const { permsCode, authCode, text, subText, className, parentname } = value;
      coms.push(
        <HyButtonCom
          name={key}
          key={`${key}`}
          title={`${text} ${subText || ''}`}
          icon
          {...{ permsCode, authCode }}
          parentname={parentname}
          onClick={this.props.parentOnClick}>
          <Icon className={className} />
          <span className='spnaText'>{text}</span>
          <span className='spnaSubText'>{subText}</span>
        </HyButtonCom>
      )
    })
    return coms;
  }






}



function mapStateToProps(state, defaultProps) {
  const { namespace = "" } = defaultProps;
  return {
    // 右侧功能按钮
    rightOperTop: state[namespace]["rightOperTop"] || {},
    rightOperCenter: state[namespace]["rightOperCenter"] || {},
    rightOperBottom: state[namespace]["rightOperBottom"] || {},
    defaultPluginData: state["SystemPluginModel"].defaultPluginData || [],
  };
}

export default connect(mapStateToProps)(Plu01321010A010LS90101);