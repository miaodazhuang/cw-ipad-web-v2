import React from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Icon } from 'semantic-ui-react';
import HyButtonCom from '../hybuttoncom';
import HyAuthCom from '../hyauthcom';
import _ from 'lodash';
import key from 'keymaster';

const _key = global.__KEYMASTER__ || key;
@BaseComponent
export default class HyRatingCom extends React.Component {
  static propTypes = {
    // 快捷键数据 ("ctrl+right") 格式参考：https://github.com/madrobby/keymaster/blob/master/test/evidence.js
    keyboard: PropTypes.string,
    // 权限代码
    permsCode: PropTypes.array,
    // 授权代码
    authCode: PropTypes.array,
    // 组件数据 [{value:text},]
    datasource: PropTypes.array,
    // 布局 水平:horizontal 垂直:vertical
    layout: PropTypes.string,
    //激活的值
    activeIndex: PropTypes.string,
    //垂直组件的标题
    title: PropTypes.string,
  }

  static defaultProps = {
    datasource: [],
    layout: 'horizontal',
    activeValue: 'keep',
    title: ''
  }



  bindKeyed = false;

  componentDidMount() {
    if (!this.bindKeyed && _.has(this.props, "keyboard")
      && _.trim(this.props.keyboard) !== ""
      && _.has(global.__SHORTKEYMAP__, this.props.keyboard)
      && _.trim(global.__SHORTKEYMAP__[this.props.keyboard]) !== ""
      && (!_.has(this.props, "permsCode") || !HyAuthCom.CheckPermsCode(this.props.permsCode))
      && (!_.has(this.props, "authCode") || !HyAuthCom.CheckAuthCode(this.props.authCode))) {
      this.bindKeyed = true;
      _key(global.__SHORTKEYMAP__[this.props.keyboard], (e) => {
        e.stopPropagation();
        e.preventDefault();
        const { children, keyboard, ...componentProps } = this.props;
        // 如果button的props是否存在click，则执行click
        if (_.has(this.props, "onClick")) {
          this.props.onClick(this, this.packageReProps(componentProps));
        }


        return false;
      })
    }
  }

  componentWillUnmount() {
    if (_.has(this.props, "keyboard")
      && _.trim(this.props.keyboard) !== ""
      && _.has(global.__SHORTKEYMAP__, this.props.keyboard)
      && _.trim(global.__SHORTKEYMAP__[this.props.keyboard]) !== "") {
      _key.unbind(global.__SHORTKEYMAP__[this.props.keyboard], "all");
    }
  }
  render() {
    const { permsCode, authCode } = this.props;
    let coms = this.props.layout === 'vertical' ? this._getVerticalCom() : this._getHorizontalCom();
    return (<HyAuthCom permsCode={permsCode} authCode={authCode}>
      {coms}
    </HyAuthCom>
    );
  }

  /**
   * 获取垂直组件
   *
   * @memberof HyIconCom
   */
  _getVerticalCom = () => {
    //没有数据源直接返回
    if (!this.props.datasource || _.size(this.props.datasource) === 0) {
      return null;
    }
    let coms = [];
    _.forEach(this.props.datasource, (item, index) => {
      let hlcss = '';
      if (index === 0) {
        hlcss = 'h-heardline';
      }
      if (index === (_.size(this.props.datasource) - 1)) {
        hlcss = 'h-lastline';
      }
      let activecss = '', activeFontColor = '';

      if (item.value === this.props.activeValue) {
        //等于index 的设置当前按钮显示
        activecss = 'Active';
        activeFontColor = 'ActiveFontColor';
      }
      let com = <div className={`v-item`} onClick={(event) => { this._onClick(event, { name: this.props.name, index: index, ...item }) }}>
        <div className={`lines ${hlcss}`}>
          <HyButtonCom icon className={`Vbtn ${activecss}`} onClick={this._onClick} {...item} index={index} name={this.props.name}  >
            <Icon className={`img335`} />
          </HyButtonCom>
        </div>
        <div className={`title-text ${activeFontColor}`}>{item.text}</div>
      </div>;
      coms.push(com);
    })
    //返回水平组件
    return (<div className='HyRatingCom-V'><div className='text-ellipsis titleHeard'>{this.props.title}</div>{coms}</div>);

  }

  /**
   * 获取水平组件
   *
   * @memberof HyIconCom
   */
  _getHorizontalCom = () => {
    //没有数据源直接返回
    if (!this.props.datasource || _.size(this.props.datasource) === 0) {
      return null;
    }
    let coms = [];
    let activeIndex=_.findIndex(this.props.datasource,(i)=>{return i.value===this.props.activeValue})
    _.forEach(this.props.datasource, (item, index) => {
      let hlcss = '';
      if (index === 0) {
        hlcss = 'heardline';
      }
      if (index === (_.size(this.props.datasource) - 1)) {
        hlcss = 'lastline';
      }
      let activecss = '', activelineColor = '', activeFontColor = '';
      //小于index 的设置线和文字为激活状态
      if (index <= activeIndex) {
        activelineColor = 'ActivelineColor';
        activeFontColor = 'ActiveFontColor';
      }
      if (item.value === this.props.activeValue) {
        //等于index 的设置当前按钮显示
        activecss = 'Active';
      }
      let com = <div className={`h-item`} onClick={(event) => { this._onClick(event, { name: this.props.name, index: index, ...item }) }}>
        <div className={`title-text ${activeFontColor}`}>{item.text}</div>
        <div className={`lines ${hlcss} ${activelineColor}`}>
          <HyButtonCom className={`Hbtn ${activecss}`} onClick={this._onClick} {...item} index={index} name={this.props.name} >
            <Icon className={`img334`} />
          </HyButtonCom>
        </div>
      </div>;
      coms.push(com);
    })
    //返回水平组件
    return (<div className='HyRatingCom-H'>{coms}</div>);

  }








  _onClick = (event, data) => {
    let authstate = HyAuthCom.CheckPermsCode(this.props.permsCode);
    if (!authstate) {
      this.props.onClick && this.props.onClick(event, data);
    }
  }
}