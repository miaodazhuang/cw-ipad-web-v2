import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ClassNames from 'classnames';
import { Card, Icon } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import { BaseComponent } from 'Hyutilities/component_helper';
import _ from 'lodash';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hyitemcontentcom';
import ja_jp from 'ja_jp/hyitemcontentcom';
import zh_cn from 'zh_cn/hyitemcontentcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
class HyItemContentCom extends Component {
  static propTypes = {
    // 示例： hiddenAddButton:false isClickAble:true isNewAddBefore:false
    // 'itemContent':{     "0": {     "className": "",     "textData": [       {
    //     "title": "name",         "value": "众荟1"       }, {         "title":
    // "name",         "value": "众荟1"       }, {         "title": "name",
    // "value": "众荟1"       }     ],     'isHandleIcon': true,     'operationType':
    // 2,     "textCBfunc": null   } }  自定义className
    customClassName: PropTypes.string,
    // 添加项显示在前还是后， true:在前，false：在后
    isNewAddBefore: PropTypes.bool,
    //是否隐藏添加项，true：隐藏，false：显示
    hiddenAddButton: PropTypes.bool,
    //按钮是否禁用
    btnDisabled: PropTypes.bool,
    //item项能否点击，true可以点击，false：不可以点击
    isClickAble: PropTypes.bool,
    //isHandleIcon:是否显示item项右上角操作按钮，true有关闭按钮，false没有关闭按钮
    isHandleIcon: PropTypes.bool,
    //自定义新建项内容
    customAddContent: PropTypes.string,
    //itemcontent数据
    itemContent: PropTypes.array,
    //operationType:item项的状态 '0':无状态 '1'：新建 '3'：编辑 '4'：删除
    operationType: PropTypes.number,
    //textCBfunc:自定义要显示的内容
    textCBfunc: PropTypes.func,
    //iconCSfunc:自定义Icon
    iconCSfunc: PropTypes.func,
    //onAddItemClick：点击新建项事件
    onAddItemClick: PropTypes.func,
    //onItemClick：点击item项事件
    onItemClick: PropTypes.func,
    //onItemCloseClick:点击item项的close事件
    onItemCloseClick: PropTypes.func,
    //onRecoverClick :点击item项的恢复按钮事件
    onRecoverClick: PropTypes.func
  }
  static defaultProps = {
    isClickAble: true,
    btnDisabled: false,
  }

  state = {
    closeIconIndex: -2
  }

  render() {
    // console.log(this.props,'===========')
    const className = ClassNames("HyItemContent", this.props.customClassName);
    return (
      <div className={className}>
        <Card.Group className='cardGroup'>
          {this._itemContent()}
        </Card.Group>
      </div>
    );
  }

  //item内容
  _itemContent = () => {
    let itemCard = [];
    let btnDisabled = this.props.btnDisabled;
    if (!this.props.hiddenAddButton && this.props.isNewAddBefore) {
      itemCard.push(
        <Card key={-1} className='add-new' onClick={(e) => this._additemClick(e)}>
          <Card.Content><Icon name='plus' />
            <span>{this.props.customAddContent || languageConfig.new}</span>
          </Card.Content>
        </Card>
      )
    }
    if (this.props.itemContent) {
      _.map(this.props.itemContent, (item, index) => {
        // console.log(item, index, '每一项')
        const cardClassName = ClassNames("cardContent", item.className, {
          "itemNew": item.operationType === '1'
        }, {
          "itemEdit": item.operationType === '3'
        }, {
          "itemdel": item.operationType === '4'
        })
        if (item.textCBfunc) {
          // debugger
          itemCard.push(
            <Card
              key={index}
              onClick={(e) => this._itemClick(e, item, index)}
              onMouseEnter={(e) => this._closeShow(e, index)}
              onMouseLeave={(e) => this._closeHide(e, index)}>
              <Card.Content className={cardClassName} key={index}>
                {item.textCBfunc(index, item, btnDisabled)}
                {this._handleIcon(item.isHandleIcon, item.operationType, index)}
              </Card.Content>
            </Card>
          )
        } else if (item.iconCSfunc) {
          itemCard.push(
            <Card
              key={index}
              onClick={(e) => this._itemClick(e, item, index)}
              onMouseEnter={(e) => this._closeShow(e, index)}
              onMouseLeave={(e) => this._closeHide(e, index)}>
              <Card.Content className={cardClassName} key={index}>
                {_.map(item.textData, (node, i) => {
                  return (
                    <Card.Description key={i}>{`${node.title}:${node.value}`}</Card.Description>
                  )
                })}
                {item.iconCSfunc(index)}
              </Card.Content>
            </Card>
          )
        } else {
          itemCard.push(
            <Card
              key={index}
              onClick={(e) => this._itemClick(e, item, index)}
              onMouseEnter={(e) => this._closeShow(e, index)}
              onMouseLeave={(e) => this._closeHide(e, index)}>
              <Card.Content className={cardClassName} key={index}>
                {_.map(item.textData, (node, i) => {
                  return (
                    <Card.Description key={i}>{`${node.title}:${node.value}`}</Card.Description>
                  )
                })}
                {this._handleIcon(item.isHandleIcon, item.operationType, index)}
              </Card.Content>
            </Card>
          )
        }
      })
    }
    if (!this.props.hiddenAddButton && !this.props.isNewAddBefore) {
      itemCard.push(
        <Card className='add-new' key={-1} onClick={(e) => this._additemClick(e)}>
          <Card.Content><Icon name='plus' />{this.props.customAddContent || languageConfig.new}</Card.Content>
        </Card>
      )
    }
    return itemCard;
  }
  //处理每一项右上角的操作项
  _handleIcon = (isHandleIcon, operationType, id) => {
    let iconElement = [];
    if (isHandleIcon) {
      if (operationType === '4') {
        iconElement.push(<Icon
          onClick={(e) => this._recoverIcon(e, id)}
          className='itemRecover icon14 img86' />)
      } else {
        iconElement.push(this.state.closeIconIndex === id
          ? <Icon
            onClick={(e) => this._itemClose(e, id)}
            className='itemClose icon14 img85' />
          : '')
      }
    }
    return iconElement
  }
  //恢复按钮点击事件
  _recoverIcon = (e, id) => {
    e.preventDefault();
    e.stopPropagation();
    // console.log(id, '恢复')
    if (this.props.onRecoverClick) {
      this
        .props
        .onRecoverClick(e, { id })
    }
  }
  //关闭按钮的显示/隐藏
  _closeShow = (e, index) => {
    this.setState({ closeIconIndex: index })
  }
  _closeHide = (e, index) => {
    this.setState({ closeIconIndex: -2 })
  }
  // 点击每一项
  _itemClick = (e, data, id) => {
    e.preventDefault();
    e.stopPropagation();
    if (this.props.isClickAble && this.props.onItemClick) {
      this
        .props
        .onItemClick(e, {
          ...data,
          id
        })
    } else {
      return
    }
  }
  //点击每一项上的关闭按钮
  _itemClose = (e, id) => {
    e.preventDefault();
    e.stopPropagation();
    // console.log(id, '关闭')
    if (this.props.onItemCloseClick) {
      this
        .props
        .onItemCloseClick(e, { id })
    } else {
      return
    }
  }
  //点击添加新项
  _additemClick = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (this.props.onAddItemClick) {
      this
        .props
        .onAddItemClick(e, { id: -1 }); //-1：点击的是新建项
    }
  }
}
export default HyItemContentCom;