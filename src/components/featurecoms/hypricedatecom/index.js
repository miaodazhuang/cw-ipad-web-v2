import React, { Component } from 'react';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import PropTypes from 'prop-types';
import classNames from "classnames";
import HyScrollCom from 'Hycomponents/featurecoms/hyscrollcom';
import moment from 'moment';
//导入日期格式化帮助方法
import { FormatMoment } from 'Hyutilities/moment_helper';
import { FormatMoney } from 'Hyutilities/money_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hydatepickercom';
import ja_jp from 'ja_jp/hydatepickercom';
import zh_cn from 'zh_cn/hydatepickercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
class HyPriceDateCom extends Component {
  static propTypes = {
    //开始时间
    strDt: PropTypes.number,
    //结束时间
    endDt: PropTypes.number,
    //数据源
    data: PropTypes.array,
    //回调函数
    Template: PropTypes.func,
    //
    className: PropTypes.string,
  }
  // Primary content.
  static defaultProps = {

  }



  componentWillMount() { }

  componentDidMount() { }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUpdate(nextProps, nextState) { }

  componentDidUpdate(prevProps, prevState) { }

  componentWillUnmount() { }

  render() {
    let className = classNames(this.props.className, "pricedate");
    let _com = this._getCon();
    return (
      <div className={className}>
        {_com}
      </div>
    );
  }

  _getCon = () => {
    let strDt = moment(this.props.strDt).toDate();;
    let endDt = moment(this.props.endDt).toDate();;
    let data = this.props.data;
    let tm_Dtdiff = endDt.getTime() - strDt.getTime();
    tm_Dtdiff = Math.floor(tm_Dtdiff / (24 * 3600 * 1000));
    if (tm_Dtdiff < 0) {
      this.props.dispatch({
        type: "SystemModel/updateMessageState",
        statecode: 2,
        message: languageConfig.pricedateErrmessage,
        messageCode: "MHyPriceDateCom",
      })
      return
    }
    return (
      <div className='total'>
        <div>
          <ul className='price'>
            <li className='lititle'>{languageConfig.Sunday}</li>
            <li className='lititle'>{languageConfig.Monday}</li>
            <li className='lititle'>{languageConfig.Tuesday}</li>
            <li className='lititle'>{languageConfig.Wednesday}</li>
            <li className='lititle'>{languageConfig.Thursday}</li>
            <li className='lititle'>{languageConfig.Friday}</li>
            <li className='lititle'>{languageConfig.Saturday}</li>
            {/* <li className='scrollwidth'></li> */}
          </ul>
        </div>
        <div className='infomation'>
          <HyScrollCom key='0' showVeriticalTrack={false} showHorizontalTrack={false}>
            <ul className='priceinfo'>{this._getCom(strDt, endDt, data)}</ul>
          </HyScrollCom>
        </div>
      </div>
    )
  }

  _getCom = (strDt, endDt, data) => {
    //取当前日期在一周内是周几
    let tm_strDt = strDt; //表格中的起始日期
    if (strDt.getDay() >= 0 && strDt.getDay() <= "6") {
      //日期减法
      tm_strDt = this.addDate(strDt, -strDt.getDay());
    }
    let tm_endDt = endDt; //表格中的终止日期
    if (endDt.getDay() >= 0 && endDt.getDay() <= "6") {
      //   //日期减法
      tm_endDt = this.addDate(endDt, (6 - endDt.getDay()));
    }
    let tm_Dtdiff = tm_endDt.getTime() - tm_strDt.getTime();

    tm_Dtdiff = Math.ceil(tm_Dtdiff / (24 * 3600 * 1000));
    let tm_day = tm_strDt;
    let dayCon = [];
    let daystyle;
    for (let i = 0; i <= tm_Dtdiff; i++) {
      if (tm_day.getDay() === 0 || tm_day.getDay() === 6) {
        daystyle = 'daycolor'
      } else {
        daystyle = ''
      }
      if (tm_day < strDt) {
        if (this.props.Calendarformat === null) {
          //dayCon.push(<li className={'empty' + ' ' + daystyle}><center>{moment(tm_day).format('MM') + '-' + moment(tm_day).format('DD')}</center></li>);
          dayCon.push(<li className={'empty' + ' ' + daystyle}><center>{moment(tm_day).format(FormatMoment('MM-DD'))}</center></li>);
        } else {
          dayCon.push(this.props.Calendarformat(moment(tm_day).unix() * 1000))
        }

      } else if (tm_day > endDt) {
        if (this.props.Calendarformat === null) {
          //dayCon.push(<li className={'empty' + ' ' + daystyle}><center>{moment(tm_day).format('MM') + '-' + moment(tm_day).format('DD')}</center></li>);
          dayCon.push(<li className={'empty' + ' ' + daystyle}><center>{moment(tm_day).format(FormatMoment('MM-DD'))}</center></li>);
        } else {
          dayCon.push(this.props.Calendarformat(moment(tm_day).unix() * 1000))
        }
      } else {
        let _dt1;
        let _Dtdiff;
        _.forEach(data, (node, index) => {
          _dt1 = moment(node.yymd).toDate();
          _Dtdiff = _dt1 - tm_day;
          if (_Dtdiff === 0) {
            if (this.props.Template === null) {
              dayCon.push(<li className={'fact' + ' ' + daystyle}>
                <center className='head'>{moment(tm_day).format(FormatMoment('MM-DD'))}</center>
                <center className='style'>{`${FormatMoney(node.amt)}`}</center>
                <center>{this.getbreak(node.breakerCnt)}</center>
              </li>);
            } else {
              dayCon.push(this.props.Template(node))
            }
          }
        });
      }
      tm_day = this.addDate(tm_day, 1);
    }
    return dayCon;
  }

  getbreak = (breakerCnt) => {
    let count;
    if (breakerCnt === 0) {
      count = languageConfig.noBreakfast;
    } else if (breakerCnt === 1) {
      count = languageConfig.noeBreakfast;
    } else if (breakerCnt === 2) {
      count = languageConfig.twoBreakfast;
    } else {
      count = languageConfig.breakfast;
    }
    return count
  }
  addDate = (date, days) => {
    let yestoday = new Date(date)
    yestoday = yestoday.valueOf()
    yestoday = yestoday + days * 24 * 60 * 60 * 1000
    yestoday = new Date(yestoday)
    return yestoday;
  }

}

export default HyPriceDateCom;