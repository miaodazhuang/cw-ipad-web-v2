import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import HyHelpCenterCom from 'Hycomponents/businesscoms/hyhelpcentercom';
import { Modal, Icon, Button } from 'semantic-ui-react';
import key from 'keymaster';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';

const _key = global.__KEYMASTER__ || key;

@BaseComponent
export default class HyModalCom extends Component {

  static propTypes = {
    customContentStyle: PropTypes.object, // 自定义弹出框行内样式
    size: PropTypes.string, // 弹框大小,主要控制宽度
    title: PropTypes.string.isRequired, // 弹框标题
    title_icon: PropTypes.string, // 弹框标题前面的图标
    isOpen: PropTypes.bool.isRequired, // 控制弹框显示
    onClose: PropTypes.func.isRequired, //关闭事件
    closeIcon: PropTypes.bool.isRequired, //是否有关闭按钮
    helpOpen: PropTypes.bool, // 是否是帮助调用
  }

  state = {
    open: this.props.isOpen
  }

  componentDidMount() {
    if (_.has(global.__SHORTKEYMAP__, "GF12") && this.props.closeIcon) {
      _key(global.__SHORTKEYMAP__["GF12"], (e) => {
        this.close(this, {});
      })
    }
  }

  componentWillUnmount() {
    _key.unbind(global.__SHORTKEYMAP__["GF12"], "all");
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      open: nextProps.isOpen
    })
  }

  render() {
    const { isOpen, title, title_icon, children, customContentStyle, closeIcon, ...childrenProps } = this.props;
    const { open } = this.state;
    return (
      <Modal open={open} onClose={this.close} eventPool="modal"  {...childrenProps} style={customContentStyle} >
        <Modal.Header className='modal_header'>
          {title_icon ? <Icon name={title_icon} /> : ''}
          <div className = 'modal_header_title'>
            <span className='title' title = {title}>{title}</span>
            <span className='help'>
              {!this.props.helpOpen && <HyAuthCom authCode={'0131-2010-0001-L-A-01'} ><HyHelpCenterCom subModal size={this.props.size} /></HyAuthCom>}
            </span>
          </div>
          {closeIcon ? <Button icon='cancel' className='cancel' onClick={this.close} /> : null}
        </Modal.Header>
        <Modal.Content className='modal_content'>
          {children}
        </Modal.Content>
      </Modal>
    )
  }

  // 关闭
  close = (event, data) => {
    this.props.onClose && this.props.onClose(event, data);
  };

  onClick = (event, data) => {
    this.setState({
      open: false
    }, () => {
      this.close(event, data);
    })
  }

}