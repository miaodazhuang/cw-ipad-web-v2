/**
 * POS桌台组件
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Icon } from 'semantic-ui-react'
import _ from 'lodash';

@BaseComponent
class HyTableItemCom extends Component {

  static propTypes = {
    //数据源
    data: PropTypes.object,
    //内容 不为空优先显示  空时显示titleArray内容
    textContent: PropTypes.strig,
    //内容数组 数组第一个元素会被用作标题显示
    titleArray: PropTypes.array,
    //需要特殊处理的titleArray索引
    specialIndex: PropTypes.number,
    //特殊处理的内容
    specialContent: PropTypes.string,
    //桌台状态 "0"空桌 "1"占用  非桌台此属性不用传
    blackStatus: PropTypes.string,
    //是否显示右上角数字
    showtopSum: PropTypes.bool,
    //右上角角标内容
    topSum: PropTypes.string,
    //自定义样式
    className: PropTypes.string,
    //是否选中
    checked: PropTypes.bool,
    // 按钮点击事件
    onClick: PropTypes.func,
    //使用不同的宽度
    usingWidth:PropTypes.bool,
  }



  componentDidMount() {
  }

  componentWillUnmount() {
  }

  render() {

    const { className, ...componentProps } = this.props;
    return (
      this._getCom()
    )
  }


  /**
 * 生成每一个元素
 *
 * @memberof HyTableItemCom
 */
  _getCom = () => {
    let com = null;
    let _classNmae = this.props.checked ? 'selected' : '';
    let _ico = (_classNmae === "selected" ? <Icon className={`icon14 img405 selectedioc`} /> : null);
    let _topSum = ((this.props.showtopSum === true && this.props.topSum !== "" && `${this.props.topSum}` !== "0") ? <span className='topSum'>{this.props.topSum || ''}</span> : null)
    let titlecoms = [];
    if (!this.props.textContent) {
      _.forEach(this.props.titleArray, (i, index) => {
        if (this.props.specialIndex === index) {
          titlecoms.push(<div className={`title-Div text-ellipsis}`}>{this.props.specialContent}</div>)
        } else {
          titlecoms.push(<div className={`title-Div text-ellipsis ${index === 0 ? 'font-head' : `item${index}`}`}>{i||""}</div>)
        }
      })
    } else {
      titlecoms.push(this.props.textContent);
    }
    let _blackStatus = "block_stus";
    if (this.props.blackStatus === '0') {
      _blackStatus = 'block_stus_0';
    } else if (this.props.blackStatus === '1') {
      _blackStatus = 'block_stus_1';
    } else if(this.props.blackStatus === '2'){
      _blackStatus = 'block_stus_2';
    }
    com = <div className={`${this.props.usingWidth ?'HyTableItemComUsing' :'HyTableItemCom'}   ${_blackStatus} ${this.props.className} ${_classNmae}`}
      onClick={(event) => { this._onClick(event, this.props.data) }}
    >{_topSum}{titlecoms}{_ico}</div>
    return com;
  }

  _onClick = (event, data) => {
    this.props.onClick && this.props.onClick(event, data);
  }

}

export default HyTableItemCom;