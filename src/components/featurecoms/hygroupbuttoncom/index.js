import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Button } from 'semantic-ui-react'
import HyButtonCom from '../hybuttoncom';
import _ from 'lodash';
@BaseComponent
class HyGroupButtonCom extends Component {

  static propTypes = {
    // 按钮点击事件
    onClick: PropTypes.func,
    //传入的button hybutton的props
    buttons:PropTypes.array,
    //激活状态的index
    activeIndex:PropTypes.number,
  }

  bindKeyed = false;

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  render() {

    const { onClick, className, buttons, activeIndex, ...componentProps } = this.props;
    const buttonGroup = this.getButtons(buttons);
   return (
        <Button.Group className = {ClassNames(className,'groupButtons')}>
          {buttonGroup}
        </Button.Group>
    )
  }

  getButtons = (buttons = []) => {
    let con = [];
    _.forEach(buttons,(item,index) => {
      let {className, value="",...itemComponentProps} = item;
      con.push(<HyButtonCom
      key={'HyButtonCom'+ index} className = {(this.props.activeIndex===index || value===this.props.selectValue)?ClassNames(className,'groupButton-active'):ClassNames(className,'groupButton')}
      {...itemComponentProps}
      onClick = {(event ,data) => this._onClick(event ,data, index, value)}/>)
    })
    return con;
  }

  _onClick = (event, data, index , value) => {
    const _props = this.packageReProps();
    let comValue = {
      ..._props,
      ...data,
      index:index,
      value:value
    }
    this.props.onClick && this.props.onClick(event, comValue);
  }

}

export default HyGroupButtonCom;