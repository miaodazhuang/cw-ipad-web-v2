/**
 * 菜单组件组件
**/
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import {Icon} from 'semantic-ui-react';
import { BaseComponent } from 'Hyutilities/component_helper';
import _ from 'lodash';
import HyButtonCom from '../hybuttoncom';
@BaseComponent
class HyRightOperCom extends Component {
  static propTypes = {
    //自定义样式
    customClassName: PropTypes.string,
    //顶部按钮 最多三个{classname：，classname：，classname:}
    topbtn: PropTypes.object,
    //中间区域data [{id:,text:,classname:,subdata:{id:,text:,classname:,parentId:''}}]
    datasource: PropTypes.arry,
    //topBtnOnClick:顶部按钮按钮点击事件
    topBtnOnClick:PropTypes.func,
    //底部按钮点击事件
    // botmbtnClick:PropTypes.func,
    //一级菜单按钮点击事件
    onClick:PropTypes.func,
    //二级菜单点击事件
    onSubClick:PropTypes.func,
  }

  static defaultProps = {

  }


  state = {
    botmbtn: [],//底部三个按钮
    subMenudata: [],//展开的二级菜单内容
    menuText: null,  //小屏幕的一级菜单要弹出的内容
    style:{position: 'absolute'}
  }


  /**
   * 组件将要渲染时
   *
   * @memberof HyRightOperCom
   */
  componentWillMount() { }

  /**
   * 组件渲染完成时
   *
   * @memberof HyRightOperCom
   */
  componentDidMount() {
    window.onclick = () => {
      if(this.state.subMenudata&&_.size(this.state.subMenudata)>0){
        this.setState({ subMenudata: [] });
      }
    }
  }

  /**
   * 组件Prop改变时
   *
   * @param {any} nextProps
   * @memberof HyRightOperCom
   */
  componentWillReceiveProps(nextProps) {

  }


  /**
   * 组件将要更新
   *
   * @param {any} nextProps
   * @param {any} nextState
   * @memberof HyRightOperCom
   */
  componentWillUpdate(nextProps, nextState) {

  }

  /**
   * 组件更新完成时
   *
   * @param {any} prevProps
   * @param {any} prevState
   * @memberof HyRightOperCom
   */
  componentDidUpdate(prevProps, prevState) {

  }

  /**
   * 组件注销时
   *
   * @memberof HyRightOperCom
   */
  componentWillUnmount() { }


  //渲染函数
  render() {
    const _className = 'rightopercom';
    let _style = _.clone(this.state.style);
    let _getChildernData = this._getChildern();
    const _trigger = this.gettigger();
    return (
      <div className={_className} ref="rigthoper">
        {_trigger}
        <div className='secondmenu' onMouseLeave = {this._onMouseLeave} style={_style}>
        {_getChildernData}
        </div>
      </div>
    );
  }

  _onOpenHandle = (e, obj) => {
    this.coords = e.currentTarget.getBoundingClientRect();
  }


  /**
  * 需要渲染的内容
  *
  * @memberof HyRightOperCom
  */
  gettigger = () => {
    return (
      <div className={global.__DOCUMENTWIDTH__ > 1280 ? 'bigrightoper':'rightoper'} >
        {this.props.topbtn ?
          <div className='topbtn'>
            {this._gettopbtn()}
          </div> : ''
        }
        <div className='midbtn'>
          <HyButtonCom className='clover' onClick={this._cloverOnClick}><Icon name='snowflake outline'/></HyButtonCom>
          {this._getmidbtn()}
        </div>
        {/* <div className='botmbtn' onMouseEnter={this._botmMouseEnter}>
          {this._getbotmbtn()}
        </div> */}
      </div>
    )
  }

   /**
  * 鑾峰彇椤堕儴鎸夐挳
  *
  * @memberof HyRightOperCom
  */
  _gettopbtn = () => {
    {/* <HyButtonCom circular className='rightoper-topbtn' onClick={this._topBtnOnClick}><Icon className={`icon14 ${item}`}/></HyButtonCom> */}
    let topbtn = this.props.topbtn;
    let ele = [];
    _.forEach(topbtn,(item,index)=>{
      ele.push(
        <Icon onClick={this._topBtnOnClick} className={`icon32 ${item}`}/>
      )
    })
    return ele;
  }

  /**
  * 顶部按钮点击事件
  *
  * @memberof HyRightOperCom
  */
  _topBtnOnClick = (event) => {
    let comValue = null;
    const _props = this.packageReProps();
    comValue = {
      ..._props,
    }
    if (this.props.topBtnOnClick) {
      this.props.topBtnOnClick(event, comValue)
    }
  }

    /**
  * 鍥涘彾鑽夌偣鍑讳簨浠?
  *
  * @memberof HyRightOperCom
  */
  _cloverOnClick = (event) => {
    let comValue = null;
    const _props = this.packageReProps();
    comValue = {
      ..._props,
    }
    if (this.props.cloverOnClick) {
      this.props.cloverOnClick(event, comValue)
    }
  }

  /**
  * 获取中间区域按钮
  *
  * @memberof HyRightOperCom
  */
  _getmidbtn = () => {
    let element = [];
    _.forEach(this.props.datasource, (item, index) => {
      if (global.__DOCUMENTWIDTH__>1280) {
        //宽屏
        if (item.subdata && _.size(item.subdata) > 0) {
          element.push(
            <HyButtonCom className='bigmidbtn' onMouseEnter={(event) => { this._onMouseOver(event, item) }} onClick={(event) => { this._onClick(event, item) }}>
            <span className='rightoperunfold'></span>
            <Icon className={`icon14 ${item.classname}`} />
            <div>{item.text}</div>
            </HyButtonCom>
          )
        } else {
          element.push(
            <HyButtonCom className='bigmidbtn' onMouseEnter={(event) => { this._onMouseOver(event, item) }} onClick={(event) => { this._onClick(event, item) }}>
              <Icon className={`icon14 ${item.classname}`} />
              <div>{item.text}</div>
            </HyButtonCom>
          )
        }

      } else {
        //窄屏
        if (item.subdata && _.size(item.subdata) > 0) {
          element.push(
            <HyButtonCom className='minmidbtn' onMouseEnter={(event) => { this._onMouseOver(event, item) }} onClick={(event) => { this._onClick(event, item) }}>
              <Icon className={`icon14 ${item.classname}`} />
              <span className='min rightoperunfold'></span>
            </HyButtonCom>
          )
        } else {
          element.push(
            <HyButtonCom className='minmidbtn' onMouseEnter={(event) => { this._onMouseOver(event, item) }} onClick={(event) => { this._onClick(event, item) }}>
              <Icon className={`icon14 ${item.classname}`} />
            </HyButtonCom>
          )
        }
      }
    })
    // if (_.size(this.props.topbtn)===0) {
    //   element.push(<div className='bigspace'></div>);
    // }
    // else if (_.size(this.props.topbtn)===1) {
    //   element.push(<div className='space'></div>);
    // }
    // else if (_.size(this.props.topbtn)===2) {
    //   element.push(<div className='minspace'></div>);
    // }
    return element
  }


  /**
    * 鼠标滑过
    *
    * @memberof HyRightOperCom
    */
  _onMouseOver = (event, data) => {
    this.coords = event.currentTarget.getBoundingClientRect();
    let style = {
      top:this.coords.top-46,
      right:this.coords.width,
     }
    if (global.__DOCUMENTWIDTH__>1280) {
      return;
      //宽屏
      // if (data.subdata) {
      //   //如果有二级菜单
      //   this.setState({ menuText: null, subMenudata: data.subdata || [] ,style:{...this.state.style,...style}});
      // }
      // else {
      //   this.setState({ subMenudata: [], menuText: null });
      // }
    } else {
      this.setState({ subMenudata: [], menuText: data || null ,style:{...this.state.style,...style}});
      //窄屏
      // if (data.subdata) {
      //   //如果有二级菜单
      //   this.setState({ subMenudata: data.subdata || [],menuText: null,style:{...this.state.style,...style} });
      // } else {
      //   this.setState({ subMenudata: [], menuText: data || null ,style:{...this.state.style,...style}});
      // }
    }
    // event.preventDefault();
    // event.stopPropagation();
  }

  _onMouseLeave = () => {
    if((global.__DOCUMENTWIDTH__ < 1280||global.__DOCUMENTWIDTH__ === 1280)&&this.state.menuText){
      this.setState({subMenudata: [], menuText: null})
    }
  }

  // /**
  //  * 获取底部按钮
  //  *
  //  * @memberof HyRightOperCom
  //  */
  // _getbotmbtn = () => {
  //   let element = [];
  //   if (_.size(this.state.botmbtn) > 0) {
  //     _.forEach(this.state.botmbtn, (item, index) => {
  //       element.push(
  //         <HyButtonCom className='rightoper-botmbtn' onClick={(event)=>{this._botmbtnClick(event,item)}}>
  //           <Icon className={`icon14 ${item.classname}`} />
  //           {document.body.clientWidth > global.__DOCUMENTWIDTH__ ? <div className='rightoper-botmbtn-text' title={ item.text }>{item.text}</div> : ''}
  //         </HyButtonCom>
  //       )
  //     })
  //   }
  //   return element;
  // }

  // /**
  //   * 底部菜单移入事件
  //   *
  //   * @memberof HyRightOperCom
  //   */
  // _botmMouseEnter=()=>{
  //   //清空
  //   this.setState({ menuText: null, subMenudata: [] });
  // }

  // /**
  //   * 底部菜单点击事件
  //   *
  //   * @memberof HyRightOperCom
  //   */
  // _botmbtnClick = (event, data) => {
  //   let comValue = null;
  //   const _props = this.packageReProps();
  //   comValue = {
  //     ..._props,
  //     id: data.id,
  //     parentId:data.parentId?data.parentId:''
  //   }
  //   if (this.props.botmbtnClick) {
  //     this.props.botmbtnClick(event, comValue)
  //   }
  // }
  /**
    * 一级菜单点击事件
    *
    * @memberof HyRightOperCom
    */
  _onClick = (event, data) => {
    //有二级菜单点击事件展开二级菜单
    this.coords = event.currentTarget.getBoundingClientRect();
    let style = {
      top:this.coords.top-46,
      right:this.coords.width,
     }
    let comValue = null;
    const _props = this.packageReProps();
    comValue = {
      ..._props,
      id: data.id,
    }
    if (data.subdata && _.size(data.subdata) > 0) {
        //如果有二级菜单
        this.setState({ menuText: null, subMenudata: data.subdata || [] ,style:{...this.state.style,...style}});
    }
    // let botmbtn = _.clone(this.state.botmbtn);
    // let _ids = [];
    // if (_.size(botmbtn) > 0) {
    //   _.forEach(botmbtn, (item, index) => {
    //     _ids.push(item.id);
    //   })
    // }
    // if (_.includes(_ids, data.id)) {
    //   this.setState({ menuText: null, subMenudata: [] });
    //   if (this.props.onClick) {
    //     this.props.onClick(event, comValue)
    //   }
    //   return
    // }
    // if (_.size(botmbtn) == 3) {
    //   botmbtn.shift();
    //   botmbtn.push(data);
    // } else {
    //   botmbtn.push(data);
    // }
    // this.setState({ botmbtn: botmbtn, menuText: null, subMenudata: [] });
    if (this.props.onClick) {
      this.props.onClick(event, comValue)
    }
    event.preventDefault();
    event.stopPropagation();
  }

  /**
  * 二级菜单
  *
  * @memberof HyRightOperCom
  */
  _getChildern = () => {
    let element = [];
    if (_.size(this.state.subMenudata) > 0) {
      //二级菜单
      _.forEach(this.state.subMenudata, (item, index) => {
        element.push(
          <HyButtonCom className={ global.__DOCUMENTWIDTH__>1280 ? 'big rightopersubmenu' : 'min rightopersubmenu'} onClick={(event) => { this._onSubClick(event, item) }}>
            <Icon className={`icon14 ${item.classname}`} />
            {global.__DOCUMENTWIDTH__ > 1280 ? <div>{item.text}</div> : <span>{item.text}</span>}
          </HyButtonCom>
        )
      })
    }
    if (this.state.menuText) {
      //text
      element.push(
        <HyButtonCom className='menutext'><div className='popupleft'></div>{this.state.menuText.text}</HyButtonCom>
      )
    }
    return element;
  }
  /**
    * 二级菜单点击事件
    *
    * @memberof HyRightOperCom
    */
  _onSubClick = (event, data) => {
    const _props = this.packageReProps();
    let comValue = null;
    comValue = {
      ..._props,
      id: data.id,
      parentId: data.parentId?data.parentId:''
    }
  //   let botmbtn=_.clone(this.state.botmbtn);
  //   let _ids=[];
  //   if(_.size(botmbtn)>0){
  //   _.forEach(botmbtn,(item,index)=>{
  //     _ids.push(item.id);
  //   })
  // }
  // if(_.includes(_ids,data.id)){
  //   this.setState({subMenudata:[],menuText:null});
  //   if (this.props.onSubClick) {
  //     this.props.onSubClick(event, comValue)
  //   }
  //    return
  // }
  //   if(_.size(botmbtn)==3){
  //     botmbtn.shift();
  //     botmbtn.push(data);
  //   }else{
  //     botmbtn.push(data);
  //   }
  //   this.setState({botmbtn:botmbtn,subMenudata:[],menuText:null});
    this.setState({subMenudata:[],menuText:null});
    if (this.props.onSubClick) {
      this.props.onSubClick(event, comValue)
    }
  }

}

export default HyRightOperCom;
