import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Popup } from 'semantic-ui-react';
import HyButtonCom from '../hybuttoncom';
import { BaseComponent } from 'Hyutilities/component_helper';
import classNames from "classnames";
@BaseComponent
class HyFeatureButtonGroupCom extends Component {
  static propTypes = {
    //数据源 例如：{btn1:{permsCode:[],text:'',name:'',id'',list:{btn1-1:{text:'',name:'',id''},btn2-2:{text:'',name:'',id''}}}}
    data: PropTypes.object,
    //打开方式
    openway: PropTypes.string,
    //position打开位置
    position: PropTypes.string,
    //是否显示按钮右侧下拉标记
    down: PropTypes.bool,
    //是否display:none渲染button list
    show: PropTypes.bool,
    // 管控字段名称
    controlFieldName: PropTypes.oneOf(["N", "I", "M", "D", "R"]),
    // 单位id
    unitId: PropTypes.string
  }
  state = {
    isopen: false
  }
  constructor(props) {
    super(props);

  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    const _className = `featurebtn ${this.props.className}`
    const _content = this._getContent();
    return (
      <div className={_className ? _className : ''}>
        {_content}
      </div>
    );
  }

  /**
 * 生成内容
 */

  _getContent = () => {
    const {
    data,
      openway,
      position,
      down,
      show,
      onClick,
      ...childrenProps
  } = this.props;
    const ele = [];
    for (let key in data) {
      if (!data[key].list) {
        ele.push(<HyButtonCom
          key={key}
          icon={data[key].icon}
          labelPosition={data[key].labelPosition}
          permsCode={data[key].permsCode}
          authCode={data[key].authCode}
          keyboard={data[key].keyboard}
          controlFieldName={data[key].controlFieldName}
          unitId={data[key].unitId}
          className={data[key].classname}
          onClick={(event) => { this.onClick(event, key) }}
          content={data[key].text}>
        </HyButtonCom>)
      } else {
        let subdata = data[key].list;
        if (key.list && _.size(key.list) === 1) {
          for (let subkey in subdata) {
            ele.push(<HyButtonCom key={key}
              keyboard={data[subkey].keyboard}
              permsCode={data[subkey].permsCode}
              authCode={data[subkey].authCode}
              controlFieldName={data[subkey].controlFieldName}
              unitId={data[subkey].unitId}
              className={subdata[subkey].classname}
              onClick={(event) => { this.onClick(event, subkey) }}>
              {subdata[subkey].text}
            </HyButtonCom>)
          }
        } else {
          let className = classNames(data[key].classname, "moreBtn");
          ele.push(
            <Popup
              key={key}
              on={openway}
              open={this.state.isopen}
              onClose={this.handleClose}
              onOpen={this.handleOpen}
              position={position}
              hoverable
              {...childrenProps}
              trigger={<HyButtonCom
                permsCode={data[key].permsCode}
                keyboard={data[key].keyboard}
                authCode={data[key].authCode}
                controlFieldName={data[key].controlFieldName}
                unitId={data[key].unitId}
                className={className}
                content={data[key].text} icon='angle down' />}>
              {show ? this._getChildren(subdata, true) : this._getChildren(subdata)}
            </Popup>
          )
          if (show) {
            ele.push(<div className='featurebtn-list-noshow'>
              {this._getChildren(subdata)}
            </div>)
          }
        }
      }
    }
    return ele;
  }


  _getChildren = (subdata, flg = false) => {
    let ele = [];
    for (let item in subdata) {
      if (flg) {
        ele.push(<HyButtonCom
          permsCode={subdata[item].permsCode}
          authCode={subdata[item].authCode}
          controlFieldName={subdata[item].controlFieldName}
          unitId={subdata[item].unitId}
          className={subdata[item].classname}
          onClick={(event) => { this.onClick(event, item) }}>
          {subdata[item].text}
        </HyButtonCom>)
      } else {
        ele.push(<HyButtonCom
          permsCode={subdata[item].permsCode}
          authCode={subdata[item].authCode}
          keyboard={subdata[item].keyboard}
          controlFieldName={subdata[item].controlFieldName}
          unitId={subdata[item].unitId}
          className={subdata[item].classname}
          onClick={(event) => { this.onClick(event, item) }}>
          {subdata[item].text}
        </HyButtonCom>)
      }
    }
    return ele;
  }

  onClick = (event, name) => {

    let comValue = null;
    const _props = this.packageReProps();
    comValue = {
      ..._props,
      name: name || ''
    };
    this.setState({ isopen: false }, () => {
      this.props.onClick && this.props.onClick(event, comValue);
    });
  }

  handleOpen = () => {
    this.setState({ isopen: true });
  }

  handleClose = () => {
    this.setState({ isopen: false })
  }
}

export default HyFeatureButtonGroupCom;