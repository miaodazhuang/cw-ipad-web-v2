/**
 * 价格日历组件
 *Start:日子开始时间。时间戳
 *Data：数据源
 *Template：渲染每一天的数据时调用的方法
 */
import React from 'react';
import { BaseComponent } from 'Hyutilities/component_helper';
import moment from 'moment';
import HyPriceDate from '../hypricedate'

@BaseComponent
export default class HyCalendar extends React.Component {
    render() {
     const _className = `HyCalendar ${this.props.CustomClassName || ''} `;
     let start = moment(this.props.Start).format('L');
     let end = moment(this.props.Start).endOf('month').format('L');
     let handle = this.props.Template ? this.props.Template : null;
     let calendarformat = this.props.Calendarformat ? this.props.Calendarformat : null
      return (
          <div className = {_className}>
             <HyPriceDate Strdt={start} Enddt={end} Data = {this.props.Data} Template ={handle} Calendarformat={calendarformat}/>
          </div>
      )
    }
}
