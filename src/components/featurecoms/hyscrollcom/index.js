
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import { trottle } from 'Hyutilities/optimize_helper';
import _ from "lodash";

const VERTICAL = 'vertial';
const HORIZONTAL = 'horizontal';

const styles = {
  main: {
    overflow: 'hidden',
    position: 'relative',
    boxSizing: 'border-box'
  },
  container: {
    position: 'absolute',
    top: '0',
    left: '0',
    right: '-10px',
    bottom: '-15px',
    overflow: 'scroll',
    boxSizing: 'border-box'
  },
  track: {
    vertical: {
      position: 'absolute',
      top: '0',
      right: '0'
    },
    verticalCustomize: {
      width: '10px',
      backgroundColor: '#FAFAFA',
      borderLeft: '1px solid #E8E8E8',
      transition: 'opacity 0.3s',
      cursor: 'pointer',
    },
    horizontal: {
      position: 'absolute',
      left: '0',
      bottom: '0',
    },
    horizontalCustomize: {
      height: '10px',
      backgroundColor: '#FAFAFA',
      borderTop: '1px solid #E8E8E8',
      transition: 'opacity 0.3s',
      cursor: 'pointer',
    }
  },
  handler: {
    vertical: {
      position: 'absolute',
    },
    verticalCustomize: {
      width: '100%',
      backgroundColor: '#C1C1C1',
      borderRadius: '5px',
      transition: 'opacity 0.3s'
    },
    horizontal: {
      position: 'absolute',
    },
    horizontalCustomize: {
      height: '100%',
      backgroundColor: '#C1C1C1',
      borderRadius: '5px',
      transition: 'opacity 0.3s'
    }
  },
  square: {
    position: 'absolute',
    width: '10px',
    height: '10px',
    right: 0,
    bottom: 0,
    backgroundColor: 'white'
  }
};

@BaseComponent
class FreeScrollbar extends Component {
  static defaultProps = {
    className: '',
    style: {
      width: '100%',
      // height: '100%'
    },
    fixed: false,
    autohideHorizontal: true,
    timeout: 2000,
    tracksize: '10px',
    start: 'top left',
    showHorizontalTrack: true,
    showVeriticalTrack: true
  }
  static propTypes = {
    showHorizontalTrack: PropTypes.bool, //是否显示横向滚动
    showVeriticalTrack: PropTypes.bool, //是否显示纵向滚动
    onScroll: PropTypes.func, //滚动事件
    isRecordPreviousVeriticalPos: PropTypes.bool,//是否记录上次滚动条垂直滚动的位置 默认true记住
    isRecordPreviousHorizontalPos: PropTypes.bool//是否记录上次滚动条水平滚动的位置 默认true记住
  }
  constructor(props) {
    super(props);

    this.state = {
      showVeriticalTrack: false,
      showHorizontalTrack: false,
      // noselect: false,
      handlerPos: {
        top: 0,
        left: 0
      },
      hideHandler: this.props.autohideHorizontal
    }

    this.el = null;
    this.lastOffsetHeight = 0;
    this.lastOffsetWidth = 0;
    this.lastScrollHeight = 0;
    this.lastScrollWidth = 0;
    this.activeHandler = null;
    this.lastMousePos = null;
    this.lastContainerScrollTop = 0;
    this.lastContainerScrollLeft = 0;
    this.handlerHider = null;
    //是否需要触发container的scroll事件
    this.isTriggerScroll = true;
    //resize时的setTimeout返回值
    this.resizeTimeHandle = null;
    this.lastScrollTop = 0;
    this.lastScrollLeft = 0;
    //是否记录上次滚动条垂直滚动的位置
    this.isRecordPreviousVeriticalPos = false;
    //是否记录上次滚动条水平滚动的位置
    this.isRecordPreviousHorizontalPos = false;
  }

  componentDidMount() {
    //鼠标移动事件，节流
    this.mouseMoveFunc = trottle(this.handleHandlerMouseMove);
    document.addEventListener('mousemove', this.mouseMoveFunc);
    document.addEventListener('mouseup', this.handleHandlerMouseUp);
    document.addEventListener('readystatechange', this.handleReadyStateChange);
    document.addEventListener('dragend', this.pauseEvent);
    document.addEventListener('dragstart', this.pauseEvent);
    // window.addEventListener('resize', this.handleResize);

    this.updateTrackVisibilities();
    this.handlerContainerScroll(null, true);
    if (this.props.start.includes('bottom')) {
      this.el.scrollTop = this.el.scrollHeight;
    }
    if (this.props.start.includes('right')) {
      this.el.scrollLeft = this.el.scrollWidth;
    }

    //初始化滚动条位置对象
    this._initScrollPosObj();
    // this.isRecordPreviousVeriticalPos = true;
    // this.isRecordPreviousHorizontalPos = true;
    this.props.isRecordPreviousVeriticalPos === false ? this.isRecordPreviousVeriticalPos = false : this.isRecordPreviousVeriticalPos = true;
    this.props.isRecordPreviousHorizontalPos === false? this.isRecordPreviousHorizontalPos = false : this.isRecordPreviousHorizontalPos = true;
    //取出上次滚动条的位置
    this.previousScrollObj = global.SCROLLPOSOBJ[this.TabIndex][this.PathName][this.Hashcode];
  }

  pauseEvent(e){
    if(e.stopPropagation) e.stopPropagation();
    if(e.preventDefault) e.preventDefault();
    e.cancelBubble=true;
    e.returnValue=false;
    return false;
   }

  componentWillReceiveProps(nextProps){
    if(this.props.showVeriticalTrack !== nextProps.showVeriticalTrack){
      this.updateTrackVisibilitiesNextProps(nextProps);
    }
  }

  componentDidUpdate() {
    if (this.el.scrollHeight !== this.lastScrollHeight || this.el.scrollWidth !== this.lastScrollWidth
      || this.el.offsetWidth !== this.lastOffsetWidth || this.el.offsetHeight !== this.lastOffsetHeight) {
      this.updateTrackVisibilities();
      this.handlerContainerScroll(null, true);
    }
    this._dealScrollPreviousPos();
  }

  componentWillUnmount() {
    document.removeEventListener('mousemove', this.mouseMoveFunc);
    document.removeEventListener('mouseup', this.handleHandlerMouseUp);
    document.removeEventListener('readystatechange', this.handleReadyStateChange);
    document.removeEventListener('dragstart', this.pauseEvent);
    document.removeEventListener('dragend', this.pauseEvent);
    // window.removeEventListener("resize", this.handleResize);
  }


  render() {
    // Dynamic styles
    let containerStyles = {
      // paddingBottom: this.props.fixed ? 0 : (this.state.showHorizontalTrack ? this.props.tracksize : 0),
      paddingBottom: 0//this.props.fixed ? 0 : (this.state.showVeriticalTrack ? this.props.tracksize : 0)
    };
    // if (this.state.noselect) {
    //   containerStyles.MozUserSelect = 'none';
    //   containerStyles.WebkitUserSelect = 'none';
    //   containerStyles.msUserSelect = 'none';
    // }
    let verticalTrackStyles = {
      bottom: this.state.showHorizontalTrack ? this.props.tracksize : '0',
      opacity: 1
      // opacity: this.state.hideHandler ? 0 : 1
    };
    let horizontalTrackStyles = {
      right: this.state.showVeriticalTrack ? this.props.tracksize : '0',
      opacity: this.state.hideHandler ? 0 : 1
    };
    let verticalHandlerStyles = {
      top: this.state.handlerPos.top + '%',
      bottom: this.state.handlerPos.bottom + '%',
      opacity: 1
      // opacity: this.state.hideHandler ? 0 : 1
    };
    let horizontalHandlerStyles = {
      left: this.state.handlerPos.left + '%',
      right: this.state.handlerPos.right + '%',
      opacity: this.state.hideHandler ? 0 : 1
    };

    const _mainStyle = { ...styles.main, ...this.props.style };
    return (
      <div className={`FreeScrollbar scroll-main ${this.props.className}`}
        style={_mainStyle}>
        <div className="FreeScrollbar-container" key="container"
          style={Object.assign(containerStyles, styles.container)}
          ref={ref => this.el = ref}
          onScroll={this._scroll}
        >
          {this.props.children}
        </div>
        {this.state.showVeriticalTrack ?
          <div key="veritical"
            className={`FreeScrollbar-vertical-track ${this.props.className ? this.props.className + '-vertical-track' : ''}`}
            style={this.props.className ? Object.assign(verticalTrackStyles, styles.track.vertical) : Object.assign(verticalTrackStyles, styles.track.vertical, styles.track.verticalCustomize, {width: this.props.tracksize})}>
            <div className={`FreeScrollbar-vertical-handler ${this.props.className ? this.props.className + '-vertical-handler' : ''}`}
              onMouseDown={this.handleVerticalHandlerMouseDown.bind(this, VERTICAL)}
              style={this.props.className ? Object.assign(verticalHandlerStyles, styles.handler.vertical) : Object.assign(verticalHandlerStyles, styles.handler.vertical, styles.handler.verticalCustomize)}></div>
          </div> : null}
        {this.state.showHorizontalTrack ?
          <div key="horizontal"
            className={`FreeScrollbar-horizontal-track ${this.props.className ? this.props.className + '-horizontal-track' : ''}`}
            style={this.props.className ? Object.assign(horizontalTrackStyles, styles.track.horizontal) : Object.assign(horizontalTrackStyles, styles.track.horizontal, styles.track.horizontalCustomize)}>
            <div className={`FreeScrollbar-horizontal-handler ${this.props.className ? this.props.className + '-horizontal-handler' : ''}`}
              onMouseDown={this.handleVerticalHandlerMouseDown.bind(this, HORIZONTAL)}
              style={this.props.className ? Object.assign(horizontalHandlerStyles, styles.handler.horizontal) : Object.assign(horizontalHandlerStyles, styles.handler.horizontal, styles.handler.horizontalCustomize)}></div>
          </div> : null}
        {this.state.showHorizontalTrack && this.state.showVeriticalTrack && !this.props.fixed ?
          <div key="track" className={`FreeScrollbar-square ${this.props.className ? this.props.className + '-square' : ''}`} style={styles.square}></div>
          : null}
      </div>
    )
  }

  /**
   * 初始化滚动条位置存储obj
   */
  _initScrollPosObj = () => {
    if (!global.SCROLLPOSOBJ) {
      global.SCROLLPOSOBJ = [];
    }
    const hashcode = this._getIdentityInView();
    let str = _.split(window.location.pathname, "/");
    this.TabIndex = str[2];
    this.PathName = _.drop(str, 3).join("/");
    this.Hashcode = hashcode;
    let initPosObj = {
      scrollTop: 0,
      scrollLeft: 0
    };
    if (!global.SCROLLPOSOBJ[this.TabIndex]) {
      global.SCROLLPOSOBJ[this.TabIndex] = {
        [this.PathName]: {
          [hashcode]: initPosObj
        }
      };
    } else if (global.SCROLLPOSOBJ[this.TabIndex][this.PathName] == undefined) {
      global.SCROLLPOSOBJ[this.TabIndex][this.PathName] = {
        [hashcode]: initPosObj
      }
    } else if (global.SCROLLPOSOBJ[this.TabIndex][this.PathName][hashcode] == undefined) {
      global.SCROLLPOSOBJ[this.TabIndex][this.PathName][hashcode] = initPosObj;
    }
  }

  /**
   * 处理滚动条上次停留的位置
   */
  _dealScrollPreviousPos = () => {
    //如果需要设置并且scrollTop大于0，并且scrollTop小于scrollHeight
    if (this.isRecordPreviousVeriticalPos && this.previousScrollObj.scrollTop > 0 && this.previousScrollObj.scrollTop < this.el.scrollHeight) {
      //如果子元素存在，再判断scrollTop小于子元素的scrollHeight
      if (this.el.children[0] && this.el.children[0].scrollHeight > this.el.clientHeight && this.previousScrollObj.scrollTop < this.el.children[0].scrollHeight) {
        this.el.scrollTop = this.previousScrollObj.scrollTop;
        this.isRecordPreviousVeriticalPos = false;
      }
    }
    //如果需要设置并且scrollLeft大于0，并且scrollLeft小于scrollWidth
    if (this.isRecordPreviousHorizontalPos && this.previousScrollObj.scrollLeft > 0 && this.previousScrollObj.scrollLeft < this.el.scrollWidth) {
      //如果子元素存在，再判断scrollLeft小于子元素的scrollWidth
      if (this.el.children[0] && this.el.children[0].scrollWidth > this.el.clientWidth && this.previousScrollObj.scrollLeft < this.el.children[0].scrollWidth) {
        this.el.scrollLeft = this.previousScrollObj.scrollLeft;
        this.isRecordPreviousHorizontalPos = false;
      }
    }
  }

  handleReadyStateChange = () => {
    if (document.readyState === 'complete') {
      this.updateTrackVisibilities();
      this.handlerContainerScroll(null, true);
      if (this.props.start.includes('bottom')) {
        this.el.scrollTop = this.el.scrollHeight;
      }
      if (this.props.start.includes('right')) {
        this.el.scrollLeft = this.el.scrollWidth;
      }
    }
  }
  /**
   * 屏幕窗口发生变更事件
   */
  // handleResize = () => {
  //   if(this.resizeTimeHandle){
  //     clearTimeout(this.resizeTimeHandle);
  //     this.resizeTimeHandle = null;
  //   }
  //   this.resizeTimeHandle = setTimeout(() => {
  //     if(this.props.name === "tableContent"){
  //       debugger;
  //     }
  //     // this.handlerContainerScroll();
  //     this.updateTrackVisibilities();

  //   }, 200);
  // }
  updateTrackVisibilities = () => {
    var el = this.el;
    var scrollHeight = el.scrollHeight; var scrollWidth = el.scrollWidth;
    var offsetWidth = el.offsetWidth; var offsetHeight = el.offsetHeight;
    if (scrollHeight === this.lastScrollHeight && scrollWidth === this.lastScrollWidth
      && offsetWidth === this.lastOffsetWidth && offsetHeight === this.lastOffsetHeight) {
      return;
    }
    if (this.props.showVeriticalTrack) {
      this.setState({
        showVeriticalTrack: scrollHeight > el.offsetHeight
      });
    }
    if (this.props.showHorizontalTrack) {
      this.setState({
        showHorizontalTrack: scrollWidth > el.offsetWidth
      });
    }
    this.lastScrollWidth = scrollWidth;
    this.lastScrollHeight = scrollHeight;
    this.lastOffsetWidth = offsetWidth;
    this.lastOffsetHeight = offsetHeight;
  }

  updateTrackVisibilitiesNextProps = (nextProps) => {
    var el = this.el;
    var scrollHeight = el.scrollHeight; var scrollWidth = el.scrollWidth;
    var offsetWidth = el.offsetWidth; var offsetHeight = el.offsetHeight;
    this.setState({
      showVeriticalTrack: nextProps.showVeriticalTrack
    });
    this.lastScrollWidth = scrollWidth;
    this.lastScrollHeight = scrollHeight;
    this.lastOffsetWidth = offsetWidth;
    this.lastOffsetHeight = offsetHeight;
  }

  handlerContainerScroll = (e, isnotNotify) => {
    if (!this.isTriggerScroll) {
      this.isTriggerScroll = true;
      return;
    }
    if (this.props.autohideHorizontal) {
      clearTimeout(this.handlerHider);
      this.setState({ hideHandler: false });
      this.handlerHider = setTimeout(() => {
        this.setState({ hideHandler: true });
      }, this.props.timeout);
    }
    var el = this.el;
    var top = el.scrollTop / (el.scrollHeight - el.offsetHeight) * (1 - el.offsetHeight / this.lastScrollHeight) * 100;
    var bottom = (1 - (el.scrollTop + el.offsetHeight) / (el.scrollHeight - el.offsetHeight) * (1 - el.offsetHeight / this.lastScrollHeight)) * 100;
    if (bottom < 0) bottom = 0;
    var left = el.scrollLeft / (el.scrollWidth - el.offsetWidth) * (1 - el.offsetWidth / this.lastScrollWidth) * 100;
    var right = (1 - (el.scrollLeft + el.offsetWidth) / (el.scrollWidth - el.offsetWidth) * (1 - el.offsetWidth / this.lastScrollWidth)) * 100;
    if (right < 0) right = 0;
    var pos = {
      top: top,
      bottom: bottom,
      left: left,
      right: right
    };
    this.setState({ handlerPos: pos });
    if (isnotNotify !== true) { //界面渲染完毕
      const isHorizontal = this.lastScrollLeft !== el.scrollLeft && this.lastScrollTop === el.scrollTop;
      const isScrollToBottom = el.scrollTop === el.scrollHeight - el.clientHeight;
      const isScrollToTop = el.scrollTop === 0;
      this.lastScrollLeft = el.scrollLeft;
      this.lastScrollTop = el.scrollTop;
      this.props.onScroll && this.props.onScroll(null, { name: this.props.name, pos, scrollTop: el.scrollTop, scrollLeft: el.scrollLeft, isHorizontal, isScrollToTop, isScrollToBottom });
      this._recordScrollPos({ scrollTop: el.scrollTop, scrollLeft: el.scrollLeft });
    }
  }

  handleVerticalHandlerMouseDown = (d, e) => {
    this.lastContainerScrollTop = this.el.scrollTop;
    this.lastContainerScrollLeft = this.el.scrollLeft;

    this.activeHandler = d;
    this.lastMousePos = {
      top: e.clientY,
      left: e.clientX
    };
    // this.setState({ noselect: true });
  }

  handleHandlerMouseMove = (e) => {
    let scroll;
    if (this.activeHandler === VERTICAL) {
      var delY = e.clientY - this.lastMousePos.top;
      this.el.scrollTop = this.lastContainerScrollTop + delY / this.el.offsetHeight * this.lastScrollHeight;
      scroll = this.el.scrollTop;
    }
    if (this.activeHandler === HORIZONTAL) {
      var delX = e.clientX - this.lastMousePos.left;
      this.el.scrollLeft = this.lastContainerScrollLeft + delX / this.el.offsetWidth * this.lastScrollWidth;
      scroll = this.el.scrollLeft;
    }

    if (this.activeHandler) {
      const isScrollToBottom = this.el.scrollTop === this.el.scrollHeight - this.el.clientHeight;
      const isScrollToTop = this.el.scrollTop === 0;
      this.props.onScroll && this.props.onScroll(e, { name: this.props.name, activeHandler: this.activeHandler, scroll: scroll, isScrollToTop, isScrollToBottom });
      this._recordScrollPos({ scrollTop: this.el.scrollTop, scrollLeft: this.el.scrollLeft });
    }
  }

  handleHandlerMouseUp = () => {
    this.lastMousePos = null;
    this.activeHandler = null;
    // this.setState({ noselect: false });
  }

  /**
   * 设置滚动条滚动位置
   * @param {*} activeHandler
   * @param {*} scroll
   */
  SetScrollPos = (obj) => {
    if (obj.activeHandler) {
      if (obj.activeHandler === VERTICAL) {
        this.el.scrollTop = obj.scroll;
      }
      if (obj.activeHandler === HORIZONTAL) {
        this.el.scrollLeft = obj.scroll;
      }
    }
    else {
      this.setState({ handlerPos: obj.pos });
      this.isTriggerScroll = false;
      if (this.el.scrollTop !== obj.scrollTop) {
        this.el.scrollTop = obj.scrollTop;
      }
      if (obj.isSetScrollLeft && this.el.scrollLeft !== obj.scrollLeft) {
        this.el.scrollLeft = obj.scrollLeft;
      }
    }
  }

  /**
   * onScroll
   */
  _scroll = (e) => {
    e.stopPropagation();
    e.preventDefault();
    requestAnimationFrame(this.handlerContainerScroll);
  }

  /**
   * 记录滚动条位置
   */
  _recordScrollPos = (data) => {
    global.SCROLLPOSOBJ[this.TabIndex][this.PathName][this.Hashcode] = data;
  }

  /**
   * 获取该滚动条在界面中的唯一标识
   * @param {*} params
   */
  _getIdentityInView() {
    var doc = document.getElementById("root").getElementsByClassName("FreeScrollbar-container");
    return _.findIndex(doc, item => item === this.el);
  }
}

export default FreeScrollbar;