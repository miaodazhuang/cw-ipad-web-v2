/**
 * 价格日历组件
 * Strdt：开始时间
 * Enddt：结束时间
 * Data：数据源
 * Template：渲染每一天的数据时调用的方法
 * Calendarformat:渲染无效时间的方法
 */
import React from 'react';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import moment from 'moment';
// import { Label, Icon } from 'semantic-ui-react';
//导入日期格式化帮助方法
import { FormatMoment } from 'Hyutilities/moment_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hypricedate';
import ja_jp from 'ja_jp/components/featurecoms/hypricedate';
import zh_cn from 'zh_cn/components/featurecoms/hypricedate';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

@BaseComponent
export default class HyPriceDate extends React.Component {
    constructor(props){
      super(props);
      // console.log(this.props,'========')
      this.state={
        show:false,
      }
    }
    // moment(this.props.Enddt).toDate();
    _getCon = ()=>{
      let strDt = moment(this.props.Strdt).toDate();;
      let endDt = moment(this.props.Enddt).toDate();;
      let data = this.props.Data;
      let tm_Dtdiff = endDt.getTime() - strDt.getTime();
      tm_Dtdiff = Math.floor(tm_Dtdiff / (24 * 3600 * 1000));
      if (tm_Dtdiff < 0) {
        alert(languageConfig.datevalidcheck);
        return false;
      }
     return(
      <div className='total'>
      <div>
         <ul className='price'>
            <li className='lititle'>{languageConfig.Sunday}</li>
            <li className='lititle'>{languageConfig.Monday}</li>
            <li className='lititle'>{languageConfig.Tuesday}</li>
            <li className='lititle'>{languageConfig.Wednesday}</li>
            <li className='lititle'>{languageConfig.Thursday}</li>
            <li className='lititle'>{languageConfig.Friday}</li>
            <li className='lititle'>{languageConfig.Saturday}</li>
         </ul>

      </div>
      <div className='infomation'><ul className='priceinfo'>{this._getCom(strDt,endDt,data)}</ul></div>
      </div>
    )
  }
  _getCom = (strDt, endDt, data) => {
    //取当前日期在一周内是周几
    let tm_strDt = strDt; //表格中的起始日期
    if (strDt.getDay() >= 0 && strDt.getDay() <= "6") {
      //日期减法
      tm_strDt =this.addDate(strDt, -strDt.getDay());
    }
    let tm_endDt = endDt; //表格中的终止日期
    if (endDt.getDay() >= 0 && endDt.getDay() <= "6") {
    //   //日期减法
      tm_endDt = this.addDate(endDt, (6 - endDt.getDay()));
    }
    let tm_Dtdiff = tm_endDt.getTime() - tm_strDt.getTime();

    tm_Dtdiff = Math.ceil(tm_Dtdiff / (24 * 3600 * 1000));
    let tm_day = tm_strDt;
    let dayCon=[];
    let daystyle;
    for (let i = 0; i <= tm_Dtdiff; i++) {
      if(tm_day.getDay() === 0 || tm_day.getDay() ===6){
         daystyle = 'daycolor'
      }else{
         daystyle = ''
      }
      if (tm_day < strDt) {
        if(this.props.Calendarformat ===null){
          dayCon.push(<li className={'empty'+' ' + daystyle}><center>{moment(tm_day).format(FormatMoment('MM-DD'))}</center></li>);
        }else{
          dayCon.push(this.props.Calendarformat(moment(tm_day).unix()*1000))
        }

      }else if (tm_day > endDt) {
        if(this.props.Calendarformat ===null){
          dayCon.push(<li className={'empty'+' '+ daystyle}><center>{moment(tm_day).format(FormatMoment('MM-DD'))}</center></li>);
        }else{
          dayCon.push(this.props.Calendarformat(moment(tm_day).unix()*1000))
        }
      }else {
        let _dt1;
        let _Dtdiff;
        _.forEach(data, (node, index)=> {
          _dt1 = moment(node.yymd).toDate();
          _Dtdiff = _dt1 - tm_day;
          if (_Dtdiff === 0) {
            if(this.props.Template === null){
              dayCon.push(<li className={'fact'+' '+ daystyle}>
                            <center className='head'>{moment(tm_day).format(FormatMoment('MM-DD'))}</center>
                            <center className ='style'>{`${node.amt}`}</center>
                            <center>{this.getbreak(node.breakerCnt)}</center>
                         </li>);

            }else{
              dayCon.push(this.props.Template(node))
            }

          }
        });
      }
      tm_day = this.addDate(tm_day, 1);
  }
  return dayCon;
}
getbreak=(breakerCnt)=>{
  let count;
   if(breakerCnt === 0){
   count = languageConfig.breakerCnt0;
   }else if(breakerCnt === 1){
    count = languageConfig.breakerCnt1;
   }else if(breakerCnt === 2){
    count = languageConfig.breakerCnt2;
   }else{
    count = `${breakerCnt}${languageConfig.breakerCnt3}`;
   }
  return count
}
  addDate= (date, days)=> {
    let yestoday = new Date(date)
    yestoday = yestoday.valueOf()
    yestoday = yestoday + days * 24 * 60 * 60 * 1000
    yestoday = new Date(yestoday)
    return yestoday;
  }

    render() {
     const _className = `HyPriceDate ${this.props.CustomClassName || ''} `
      return (
          <div className = {_className}>
              {this._getCon()}
          </div>
      )
    }
}
