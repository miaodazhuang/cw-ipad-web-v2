/*
*内容项组件   lihonglian
* -Props-
*
* CustomClassName:样式名 string
* CustomStyle:行类样式 string
* Arrangement: 排列方式 string，支持横向landscape和纵向portrait
* isEdit:是否编辑状态 bool 设置为false，不显示内容块右上角的关闭按钮,如果设置为true，默认是显示关闭按钮，需要根据isHddenCloseBtn来判断是否隐藏
* isClickAble:是否可点击，如果true则不是在编辑状态下也可以可以点击
* isHddenCloseBtn:当isEdit设置为true时，判断isHddenCloseBtn是否为true，如果为true则隐藏
* HiddenAddButton:隐藏新建按钮
* AddItemClassName:新建按钮的样式定义
* Name:当前组件名称 string
* NewWord:新建组件项上的文字 string
* ContentData: 内容数据 array[{id:idValue,className:classNameValue,itemTempFun:itemTempFun,itemData:itemDataValue},{}...]
            id:
            className:自定义样式名称 string
            itemTempFun:内容项回调函数，内容项显示的内容为该回调函数返回的内容，有回调函数时，回调函数的参数为 itemData
            itemData: array [{title:titleValue,value},{title:titleValue,value}...]
                    title:key值 string
                    value:内容 string

* IsNewAddBefore: 标记是否是新建内容项在前，默认为false bool


* ClickEvent:点击事件 function
           sender：事件对象 object
           args:参数对象 object
* CreateEvent: 新建事件 function
           sender：事件对象 object
           args:参数对象 object

* DeletEvent:删除事件 function
           sender：事件对象 object
           args:参数对象 object
 */
import React from 'react';
import HyContentitemCom from './contentitem';

//创建组件
class HyContentItemsCom extends React.Component {

  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }
  _getComms = () => {
    let comm = [];
    let _className = '';
    if (this.props.Arrangement) {
      _className = this.props.Arrangement;
      if (_className === 'portrait') {//纵向
        _className = 'orientation-pt'
      } else {//横向
        _className = 'orientation-ls'
      }
    }
    if (this.props.IsNewAddBefore && this.props.isEdit && !this.props.HiddenAddButton) {
      comm.push(<HyContentitemCom key="add-new" Name={this.props.Name} NewWord={this.props.NewWord} className={`${_className} ${this.props.AddItemClassName}`} CreateEvent={this._createEvent} isCreate={true} />);
    }
    if (this.props.ContentData) {
      this.props.ContentData.map((item, index) => {
        comm.push(
          <HyContentitemCom Name={this.props.Name} id={item.id} className={`${_className} ${item.className}`} itemTempFun={item.itemTempFun}
            itemData={item.itemData} key={index} index={index} isEdit={this.props.isEdit} isClickAble={this.props.isClickAble} isHddenCloseBtn={this.props.isHddenCloseBtn}
            isCreate={false} ClickEvent={this._itemClick} DeletEvent={this._deleteClick} />
        );
      });
    }

    if (!this.props.IsNewAddBefore && this.props.isEdit && !this.props.HiddenAddButton) {
      comm.push(<HyContentitemCom key="add-new" Name={this.props.Name} NewWord={this.props.NewWord} className={`${_className} ${this.props.AddItemClassName}`} CreateEvent={this._createEvent} isCreate={true} />);
    }

    return comm;
  }

  render() {
    return (
      <div className={`hycontentitemscom ${this.props.CustomClassName || ''}`} style={this.props.CustomStyle}>
        {this._getComms()}
      </div>
    );
  }

  _itemClick = (sender, obj) => {
    const _sender = this;
    const _obj = {
      name: this.props.Name,
      id: obj.id,
      index: obj.index
    }
    if (this.props.ClickEvent) {
      this.props.ClickEvent(_sender, _obj);
    }
  }

  _deleteClick = (sender, obj) => {
    const _sender = this;
    const _obj = {
      name: this.props.Name,
      id: obj.id,
      index: obj.index
    }
    if (this.props.DeletEvent) {
      this.props.DeletEvent(_sender, _obj);
    }

  }

  _createEvent = (sender, obj) => {
    const _sender = this;
    const _obj = {
      name: this.props.Name
    }
    if (this.props.CreateEvent) {
      this.props.CreateEvent(_sender, _obj);
    }
  }
}

export default HyContentItemsCom;
