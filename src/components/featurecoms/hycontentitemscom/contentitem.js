/**
 * 内容项组件内部的项
 * -props-
 * className:自定义样式名称 string
 * customStyle:自定义行内样式 string
 * index:当前项在内容项组件中的索引值 num
 * isEdit:是否编辑状态 bool
 * isClickAble:是否可点击，如果true则不是在编辑状态下也可以可以点击
 * isHddenCloseBtn:是否显示关闭按钮
 * isCreate:当前项是否为新建
 * NewWord:新建组件项上的文字
 * id:内容项id值
 * itemTempFun:内容项的回调方法，内容项显示的内容为回调方法返回的内容，当存在此回调方法时,itemData将不起作用
 * itemData:内容项的数据array [{title:titleValue,value},{title:titleValue,value}...]
 *          title:key值 string
 *          value:内容 string
 * ClickEvent:点击事件 function
 *         sender：事件对象 object
 *         args:参数对象 object
 * CreateEvent: 新建事件 function
 *         sender：事件对象 object
 *         args:参数对象 object
 * DeletEvent:删除事件 function
 *         sender：事件对象 object
 *         args:参数对象 object
 */

import React from 'react'
import './contentitem.css'
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hycontentitemcom';
import ja_jp from 'ja_jp/components/featurecoms/hycontentitemcom';
import zh_cn from 'zh_cn/components/featurecoms/hycontentitemcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
class HyContentitemCom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      iconClass: ""
    }
  }
  render() {
    //根据当前组件状态（是否处于编辑状态）
    if (!this.props.isCreate) {
      const _className = this.props.isEdit
        ? `hycontentitem-edit`
        : `hycontentitem`;
      let _content;
      let _overType;
      let _word;
      if (this.props.itemTempFun) {
        _word = this.props.itemTempFun(this.props.itemData,this.props.index);
        _content = <div className={`item-innerdiv`}>{_word}</div>;
      } else if (this.props.itemData && this.props.itemData.length > 0) {
        if (this.props.itemData.length === 1) {
          _overType = "item-overflow";
        } else {
          _overType = "";
        }
        _content = this
          .props
          .itemData
          .map((item, index) => {
            _word = "";
            if (item.title) {
              _word = `${item.title}:${item.value}`;
            } else {
              _word = `${item.value}`;
            }

            return (
              <div className={`item-div ${_overType}`} key={index} title={_word}>{_word}</div>
            )
          })
      } else {
        _content = <div className="item-div">{languageConfig.noData}</div>
      }

      return (
        <div className={`${this.props.className}`}>
          <div
            className={`${_className} hycontentitem-div-width`}
            style={this.props.customStyle}
            onClick={this._itemClick}>
            {this._getCloseBtn()}
            {_content}
          </div>
        </div>
      );

    } else {
      let iconClassNm = `${this.state.iconClass} fa fa-plus`;
      return (
        <div className={`hycontentitem-newmore ${this.props.className}`}>
          <div
            className="hycontentitem-new"
            onMouseOver={this._onMouseOverEvent}
            onMouseOut={this._onMouseOutEvent}
            onClick={this._createEvent}>
            <div className="item-div">
              <div className="new-addIcon">
                <HyIconCom Type="i" CustomClassName={iconClassNm}/>
              </div>
              <div className="new-addWord">
                <span>{this.props.NewWord}</span>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  _onMouseOverEvent = (event) => {
    this.setState({iconClass: "active"});
  }

  _onMouseOutEvent = (event) => {
    this.setState({iconClass: ""});
  }

  _itemClick = (event) => {
    if (this.props.isEdit||this.props.isClickAble) {
      const _sender = this;
      const _obj = {
        name: this.props.Name,
        id: this.props.id,
        index: this.props.index
      }
      if (this.props.ClickEvent) {
        this
          .props
          .ClickEvent(_sender, _obj);
      }
    }
  }

  _deleteClick = (sender, obj) => {
    const _sender = this;
    const _obj = {
      name: this.props.Name,
      id: this.props.id,
      index: this.props.index
    }
    if (this.props.DeletEvent) {
      this
        .props
        .DeletEvent(_sender, _obj);
    }
  }

  _createEvent = (event) => {
    const _sender = this;
    const _obj = {
      name: this.props.Name
    }
    if (this.props.CreateEvent) {
      this
        .props
        .CreateEvent(_sender, _obj);
    }
  }

  _getCloseBtn = () => {
    if (this.props.isEdit&&!this.props.isHddenCloseBtn) {
      return (<HyIconCom
        name="close"
        onClick={this._deleteClick}/>);
    } else {
      return null;
    }
  }
}
export default HyContentitemCom;
