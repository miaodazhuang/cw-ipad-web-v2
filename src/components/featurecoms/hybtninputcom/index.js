/**
 * 可增可减的数字框
 * -Props-
 * className:自定义样式 string
 * min: 最小值 number
 * max: 最大值 number
 * readOnly 是否只读 bool
 * datasource: 元数据
 * hiddenHandleButton 是否隐藏加减按钮
 * permsCode 调整加减按钮的权限
 * 更多属性参见HyInputCom
 *
 * -Event-
 * onChange:数字框有变化
 */
import React, { Component } from 'react';
import _ from 'lodash';

// lib
import { BaseComponent } from 'Hyutilities/component_helper';

// component tooltip
import HyInputCom from '../hyinputcom';
import HyButtonCom from '../hybuttoncom';
import HyIconCom from '../hyiconcom';

@BaseComponent
class HyBtnInputCom extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      value: Number.parseInt(this.props.value === undefined ? 1 : this.props.value),
      changeCode: ""
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props, nextProps)) {
      this.setState({ value: nextProps.value });
    }
  }

  render() {
    return (
      <div
        ref="hybtninputcom"
        className={`hybtninputcom ${this.props.className}`}>
        {!this.props.hiddenHandleButton && this._getBeforeCom()}
        <HyInputCom
          ref={(c) => { this.node = c }}
          {...this.props}
          onChange={this._onchange}
          readOnly={this.props.readOnly}
          value={this.state.value} />
        {!this.props.hiddenHandleButton && this._getafterCom()}
      </div>
    )
  }

  _getBeforeCom = () => {
    const value = this.state.value;
    const min = this.props.min;
    let disabled = false;
    if (value <= min) {
      disabled = true;
    }
    return <HyButtonCom
      className="btn-count-min"
      disabled={this.props.btnDisabled ||disabled}
      permsCode={this.props.permsCode}
      onClick={this._countMin}><HyIconCom className={disabled ? 'icon50 img28-2' : 'icon50 img28'} /></HyButtonCom>;
  }

  _getafterCom = () => {
    const value = this.state.value;
    const max = this.props.max;
    let disabled = false;
    if (value >= max) {
      disabled = true;
    }
    return <HyButtonCom
      className="btn-count-add"
      disabled={this.props.btnDisabled ||disabled}
      permsCode={this.props.permsCode}
      onClick={this._countAdd}><HyIconCom className={disabled ? 'icon50 img30-2' : 'icon50 img30'} /> </HyButtonCom>;
  }

  _countAdd = () => {
    let value = this.state.value;
    let newCount = _.add(_.parseInt(value), 1);
    this.setState({
      value: newCount
    }, () => {
      if (this.props.onChange) {
        let _obj = {
          datasource: this.props.datasource,
          value: newCount
        }
        this
          .props
          .onChange(this, _obj);
      }
    });

  }

  _countMin = () => {
    let value = this.state.value;
    let newCount = _.subtract(_.parseInt(value), 1);
    this.setState({
      value: newCount,
      changeCode: Math.random()
    }, () => {
      if (this.props.onChange) {
        let _obj = {
          datasource: this.props.datasource,
          value: newCount
        }
        this
          .props
          .onChange(this, _obj);
      }
    });
  }

  _onchange = (e, obj) => {
    //输入非数字要清空
    let newValue = "0";
    if (isNaN(obj.value) || !obj.value) {
      newValue = "0";
    } else {
      newValue = obj.value;
    }
    this.setState({
      value: newValue,
      changeCode: Math.random()
    }, () => {
      if (this.props.onChange) {
        let _obj = {
          // name: this.refs.inputcom.props.Name,
          datasource: this.props.datasource,
          value: newValue
        }
        this
          .props
          // .onChange(e, _obj, this.props);
          .onChange(this, _obj);
      }
    });

  }

}
export default HyBtnInputCom;
