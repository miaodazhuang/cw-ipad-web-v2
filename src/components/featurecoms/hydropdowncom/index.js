import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Dropdown, Checkbox, Button, Icon } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hydropdowncom';
import ja_jp from 'ja_jp/hydropdowncom';
import zh_cn from 'zh_cn/hydropdowncom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
class HyDropDownCom extends Component {

  static propTypes = {
    // 下啦菜单描述
    text: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.object,
      PropTypes.array,
      PropTypes.func
    ]),
    // 出错时调用的显示提示信息的方法
    showmessage: PropTypes.func,
    // 是否开启checkbox
    enableCheckBox: PropTypes.bool,
    // 组件是否被选中
    selected: PropTypes.bool,
    // 是否开启空值
    enableNullValue: PropTypes.bool,
    // 当开启空值时options中显示的文字信息
    enableNullText: PropTypes.string,
    // 是否触发click事件
    isTriggerClick: PropTypes.bool
  }

  static defaultProps = {
    openOnFocus: false,
    selectOnBlur: false,
    enableCheckBox: false,
    selected: false,
    noResultsMessage: "",
    enableNullText: "",
    isTriggerClick: false
  }

  state = {
    open: false
  }

  isOffset = false;

  componentDidMount() {
    const checkValue = checkDataSource(this.props);
    if (checkValue.error) {
      this.props.showmessage && this.props.showmessage(checkValue.error, checkValue.message);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.value, nextProps.value) || !_.isEqual(this.props.options, nextProps.options)) {
      const checkValue = checkDataSource(nextProps);
      nextProps.showmessage && nextProps.showmessage(checkValue.error, checkValue.message);
    }
  }

  componentDidUpdate() {
    this.dropdownCom && this.dropdownCom.setSelectedIndex(this.props.value, this.props.options);
    const postion = this.dropdownCom.ref.lastElementChild.getBoundingClientRect();
    let cssText = ""
    if (postion.width !== 0 && !this.isOffset) {
      let offsetLeft = postion.width + postion.left;
      const { clientWidth } = document.documentElement;
      if (offsetLeft > clientWidth) {
        cssText = "left:auto;right:0;"
        // this.dropdownCom.ref.lastElementChild.style.cssText = "left:auto;right:0;"
      }
    }

    if (postion.height !== 0 && !this.isOffset) {
      let offsetHeight = 193 + postion.top;
      const { clientHeight } = document.documentElement;
      if(offsetHeight > clientHeight){
        cssText += "top:auto;bottom:28px";
      }
    }

    this.dropdownCom.ref.lastElementChild.style.cssText = cssText;
  }

  render() {
    const { text, className, showmessage, enableNullValue, enableNullText, value, isMust, enableCheckBox, selected, ...childrenProps } = this.props;
    const checked = enableCheckBox && selected ? true : false;
    let options = null;
    if (_.has(childrenProps, "selection") && childrenProps.selection && !_.has(childrenProps, "options")) {
      options = [];
    } else {
      options = _.cloneDeep(childrenProps["options"]);
    }
    // 如果开启了空值，则向options属性开头添加空值选项
    if (enableNullValue && _.size(options) > 0 && options[0].key !== -1) {
      const value = this.props.multiple ? "-1" : "";
      options.unshift({ key: -1, text: enableNullText, value: value, "data-datasource": {} });
    }
    if (this.props.multiple && _.size(options) > 0 && options[0].key !== -2) {
      options.unshift({
        key: -2,
        className: "custom-header",
        text: <div><Button size='mini' floated='left' onClick={this.onAllHandle.bind(this, 1)}>{languageConfig.all}</Button><Button size='mini' floated='right' onClick={this.onAllHandle.bind(this, 0)} > {languageConfig.allNo}</Button></div>,
        value: "%#head#%"
      });
    }
    const dropdownProps = this.packageReProps({ ...childrenProps, options });
    const _className = ClassNames("ui left labeled input hydropdowncom", className, { multiple: this.props.multiple })
    const _value = this.props.multiple && _.isArray(value) ? _.map(value, item => `${item}`) : (`${_.isUndefined(value) || _.isNull(value) ? "" : value}`);
    this.packageOptions(_value, dropdownProps.options);

    const IconClassName = this.state.open ? 'trash-open' : 'trash-close';

    return (
      <div className={_className}>
        {isMust && text ? <div className='form-must'><span>*</span><div key="label" className="ui label">{text}</div></div> : <div key="label" className="ui label">{text}</div>}
        {
          enableCheckBox ? <Checkbox className="input-checkbox-div" checked={checked} onClick={this.onCheckBoxClick.bind(this)} /> : null
        }
        {this.props.multiple ? <div className="hydropdowncom-multiplediv">
          <Icon onClick={this.onAllHandle.bind(this, 0)} className={enableCheckBox ? ClassNames("icon14 img29 hydropdowncom-checkBox-trash", IconClassName) : ClassNames("icon14 img29 hydropdowncom-trash", IconClassName)} size='large'/>
          <Dropdown
            onOpen={this._onOpen}
            open={this.state.open}
            onClose={this._onClose}
            onFocus={this._onFocus}
            ref={c => this.dropdownCom = c}
            key="dropdown" {...dropdownProps}
            value={_value}
            onClick={this.onClick}
            onChange={this.onChange} />
        </div> : <Dropdown
            onOpen={this._onOpen}
            open={this.state.open}
            onClose={this._onClose}
            onFocus={this._onFocus}
            ref={c => this.dropdownCom = c}
            key="dropdown" {...dropdownProps}
            value={_value}
            onClick={this.onClick}
            onChange={this.onChange} />}

      </div>
    );
  }

  _onClose = (event, data) => {
    this.setState({
      open: false
    })
  }

  // 打开时调节位置
  _onOpen = (event, data) => {
    this.setState({
      open: true
    })
  }

  _onFocus = (event, data) => {
    // if (this.dropdownCom.ref.firstChild.tagName.toLocaleLowerCase() == 'div') {
    //   return;
    // }
    // this.dropdownCom.open(event);
    event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
    if (this.dropdownCom.ref.firstChild.tagName.toLocaleLowerCase() == 'div') {
      return;
    }
    this.dropdownCom.ref.firstChild.click();
  }

  onClick = (event, data) => {
    // event.nativeEvent.stopImmediatePropagation();
    // event.stopPropagation();
    // this.isOffset = false;
    if(this.props.isTriggerClick){
      this.props.onClick && this.props.onClick(event, data);
    }
  }

  /**
   * 点击checkbox触发
   *
   * @memberof HyInputCom
   */
  onCheckBoxClick = (event, data) => {
    if (this.props.disabled) {
      return;
    }
    this.props.onClick && this.props.onClick(event, { ...data, type: "checkbox" });
  }

  onChange = (event, data) => {
    // this.isOffset = true;
    let comValue = null;
    if (!data) {
      const _props = this.packageReProps();
      comValue = {
        ..._props,
        value: this.state.value
      }
    } else {
      let { value } = data;
      if (this.props.multiple && _.includes(value, "%#head#%")) {
        value = _.filter(value, item => item !== "%#head#%");
      }
      comValue = { ...data, value };
    }
    this.props.showmessage && this.props.showmessage(false, "");
    this.props.onChange && this.props.onChange(event, comValue);
    event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
  }

  packageOptions = (value, options) => {
    if (!_.isArray(value)) {
      return;
    }

    // 根据value去options中查找相关的键值，如果没有找到，则把该错误键值添加到错误集合中
    const errOptions = [];
    _.forEach(value, item => {
      if (_.findIndex(options, opItem => opItem.value === `${item}`) === -1) {
        options.push({ "text": `${languageConfig.errorCode} ${item}`, "key": item, "value": item });
      }
    })
  }

  /**
   * 全部处理
   *
   * type: 0-全部取消 1-全部选择
   *
   * @memberof HyDropDownCom
   */
  onAllHandle = (type, event, data) => {
    event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
    let value = [];
    if (type === 1) {
      const { options } = this.props;
      _.forEach(options, item => {
        const _value = _.trim(item.value);
        if (_value !== "" && _value !== "%#head#%") {
          value.push(_value);
        }
      })
    }
    const _props = this.packageReProps();
    this.onChange(event, { ..._props, value });
  }
}

// 校验dropdown数据源方法
export const checkDataSource = (props) => {

  if (!_.has(props, "value") || _.isNull(props.value) || props.value === "" || _.size(`${props.value}`) === 0) {
    return { error: false, message: "" };
  }

  // 如果没有数据源则直接返回错误
  if (!_.has(props, "options")) {
    return { error: true, message: languageConfig.errmessage };
  }

  if (_.isArray(props.value)) {
    let _checkValue = { error: false, message: "" };
    // const _value = _.map(props.value, item => `${item}`);
    // 如果传入的value是数组，则循环value查询数据源
    const _filterData = _.filter(props.value, item => {
      const _value = `${item}`;
      //如果值是-1，并且开启了空值，不去数据源里面找
      if(_value === "-1" && props.enableNullValue) return true;
      return _.findIndex(props.options, opt => opt.value === _value) !== -1;
    });

    if (_.size(_filterData) !== _.size(props.value)) {
      _checkValue = { error: true, message: languageConfig.errmessage };
    }

    return _checkValue;
  } else {
    // 根据传入的值在数据源中查找
    const findIndex = _.findIndex(props.options, (item, index) => {
      return `${props.value}` === `${item.value}`;
    });

    if (findIndex === -1) {
      return { error: true, message: languageConfig.errmessage };
    } else {
      return { error: false, message: "" };
    }
  }
}

HyDropDownCom.checkDataSource = checkDataSource;

export default HyDropDownCom;