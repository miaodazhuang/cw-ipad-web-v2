/**
 * @file 图片排序组件
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import uuid from 'node-uuid';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/01341010A111LT00501';
import ja_jp from 'ja_jp/01341010A111LT00501';
import zh_cn from 'zh_cn/01341010A111LT00501';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyPhotoSortCom extends Component {

  static propTypes = {
    /** dataSource 数据源
     *  {
     *    // Sting 左上角显示文字 不传不显示
     *    speWord: '',
     *    // Sting 底部设置显示文字 不传不显示
     *    setWord: '',
     *    // Bool 是否显示删除
     *    showDelete: true,
     *    // Bool 是否显示左移
     *    showLeftMove: true,
     *    // Bool 是否显示右移
     *    showRightMove: true,
     *  }
     */
    dataSource: PropTypes.array,
    // 是否隐藏添加
    hideAdd: PropTypes.bool,
  }

  // 获取内容区
  getContainer = () => {
    const { dataSource } = this.props
    return dataSource.map((d, i) => {
      const { speWord, setWord, showDelete = true, showLeftMove = true, showRightMove = true } = d
      return (
        <div className='photoItem' key={uuid.v4()}>
          <img src={d.url} />
          {speWord && (
            <span className='titleWord'>
              {speWord}
            </span>
          )}
          {showLeftMove && (
            <span className='turnLeft' onClick={(event) => this.onLeftClick(event, {...d, index: i})}>
              <Icon name='chevron left' />
            </span>
          )}
          {showRightMove && (
            <span className='turnRight' onClick={(event) => this.onRightClick(event, {...d, index: i})}>
              <Icon name='chevron right' />
            </span>
          )}
          {setWord && (
            <span className='setting' onClick={(event) => this.onSettingClick(event, {...d, index: i})}>
              {setWord}
            </span>
          )}
          {showDelete && (
            <span className='delete' onClick={(event) => this.onDeleteClick(event, {...d, index: i})}>
              <Icon name='trash alternate' />
            </span>
          )}
        </div>
      )
    })
  }

  // 新建点击事件
  onAddClick = (event) => {
    this.props.onAddClick && this.props.onAddClick(event)
  }

  // 左移事件
  onLeftClick = (event, data) => {
    this.props.onLeftClick && this.props.onLeftClick(event, data)
  }

  // 右移事件
  onRightClick = (event, data) => {
    this.props.onRightClick && this.props.onRightClick(event, data)
  }

  // 删除事件
  onDeleteClick = (event, data) => {
    this.props.onDeleteClick && this.props.onDeleteClick(event, data)
  }

  // 设置事件
  onSettingClick = (event, data) => {
    this.props.onSettingClick && this.props.onSettingClick(event, data)
  }

  render() {

    return (
      <div className='hyphotosortcom'>
        {this.getContainer()}
        {!this.props.hideAdd && (
          <div className='photoItem' name='border-dash' onClick={this.onAddClick}>
            <span className='logo'>+</span>
            <span className='word'>{languageConfig.morePhotos}</span>
          </div>
        )}
      </div>
    )

  }

}

export default HyPhotoSortCom;