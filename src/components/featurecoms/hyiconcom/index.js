import React from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Icon } from 'semantic-ui-react'
import HyAuthCom from '../hyauthcom';
import _ from 'lodash';
import key from 'keymaster';

const _key = global.__KEYMASTER__ || key;
@BaseComponent
export default class HyIconCom extends React.Component {
  static propTypes = {
    // 快捷键数据 ("ctrl+right") 格式参考：https://github.com/madrobby/keymaster/blob/master/test/evidence.js
    keyboard: PropTypes.string,
    // 权限代码
    permsCode: PropTypes.array,
    // 授权代码
    authCode: PropTypes.array
  }

  bindKeyed = false;

  componentDidMount() {
    if (!this.bindKeyed && _.has(this.props, "keyboard")
      && _.trim(this.props.keyboard) !== ""
      && _.has(global.__SHORTKEYMAP__, this.props.keyboard)
      && _.trim(global.__SHORTKEYMAP__[this.props.keyboard]) !== ""
      && (!_.has(this.props, "permsCode") || !HyAuthCom.CheckPermsCode(this.props.permsCode))
      && (!_.has(this.props, "authCode") || !HyAuthCom.CheckAuthCode(this.props.authCode))) {
      this.bindKeyed = true;
      _key(global.__SHORTKEYMAP__[this.props.keyboard], (e) => {
        e.stopPropagation();
        e.preventDefault();
        const { children, keyboard, ...componentProps } = this.props;
        // 如果button的props是否存在click，则执行click
        if (_.has(this.props, "onClick")) {
          this.props.onClick(this, this.packageReProps(componentProps));
        }


        return false;
      })
    }
  }

  componentWillUnmount() {
    if (_.has(this.props, "keyboard")
      && _.trim(this.props.keyboard) !== ""
      && _.has(global.__SHORTKEYMAP__, this.props.keyboard)
      && _.trim(global.__SHORTKEYMAP__[this.props.keyboard]) !== "") {
      _key.unbind(global.__SHORTKEYMAP__[this.props.keyboard], "all");
    }
  }
  render() {
    const { keyboard, permsCode, authCode, onClick, ...componentProps } = this.props;
    return (<HyAuthCom permsCode={permsCode} authCode={authCode}>
      <Icon {...componentProps} onClick={this._onClick} />
    </HyAuthCom>
    );
  }

  _onClick = (event, data) => {
    let authstate = HyAuthCom.CheckPermsCode(this.props.permsCode);
    if (!authstate) {
      this.props.onClick && this.props.onClick(event, data);
    }
  }
}