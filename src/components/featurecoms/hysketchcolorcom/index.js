import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Popup } from "semantic-ui-react";
import HyInputCom from '../hyinputcom';
import { SketchPicker } from 'react-color';
import _ from 'lodash';
@BaseComponent
class HySketchColorCom extends Component {
  static propTypes = {
    //未绑定,已绑定,[true,false]
    color: PropTypes.string,
    //需要回传的数据
    data: PropTypes.object,
    //选择颜色事件
    onSelectColor: PropTypes.func,
  }

  constructor(props) {
    super(props);
    this.state = {
      color: this.props.color||'#FFFFFF',
    };
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.color, nextProps.color)) {
      this.setState({ color: nextProps.color });
    }
  }


  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    const trigger = <div className={'HySketchColorCom'} >
      <HyInputCom maxlength={7} disabled={true} className={'show-input'} value={this.state.color}></HyInputCom>
      <div style={{
        width: '100px',
        height: '27px',
        marginLeft: '10px',
        background: this.state.color,
        border: '1px solid #9fa6b3',
      }}></div>
    </div>
    return (
      <Popup on="click" trigger={trigger} position="bottom center">{<SketchPicker disableAlpha={true} color={this.state.color} onChange={this.handleChange} />}</Popup>
    )
  }

  /**
   * 选择颜色
   *
   * @memberof HySketchColorCom
   */
  handleChange = (color) => {
    const _color = _.toUpper((_.has(color,'hex')&& color.hex) ?color.hex:'#FFFFFF');
    if (this.state.color !== _color) {
      setTimeout(() => {
        this.setState({ color: _color })
        if (this.props.onSelectColor) {
          this.props.onSelectColor({ color: _color, ...this.props.data })
        }
      }, 150)
    }
  };
}

export default HySketchColorCom;