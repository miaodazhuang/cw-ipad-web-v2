
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Icon } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hyinputselectcom';
import ja_jp from 'ja_jp/hyinputselectcom';
import zh_cn from 'zh_cn/hyinputselectcom';
import { relative } from 'path';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

@BaseComponent
class HyInputSelectCom extends Component {
  static propTypes = {
    //行内样式
    CustomStyle: PropTypes.object,
    //类名
    className: PropTypes.string,
    //数据源
    options: PropTypes.array,
    //是否必输
    isMust: PropTypes.bool,
    //
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    //multiple是否多选
    multiple: PropTypes.bool
  }
  static defaultProps = {
    options: []
  }

  state = {
    value: this.props.value ? this.props.value : (this.props.multiple ? [] : ""),
    isOpen: false
  }

  // componentDidMount(){
  //   //if(this.state.isOpen){
  //     document.addEventListener('click', this.onClose)
  //   //}
  // }

  // componentWillUnmount() {
  //   //if(this.state.isOpen){
  //   document.removeEventListener('click', this.onClose);
  //  // }
  // }

  componentUnDidMount

  componentWillReceiveProps(nextProps) {
    let value = (_.isNull(nextProps.value) || _.isUndefined(nextProps.value) || !_.has(nextProps, "value")) ? "" : nextProps.value;
    if (!_.isEqual(this.props.value, value)
      || !_.isEqual(this.props.checkFunction, nextProps.checkFunction)
      || !_.isEqual(this.props.isMust, nextProps.isMust)
    ) {
      // const checkValue = this.checkDataSource(this.props);
      // if (checkValue.error) {
      //   this.props.showmessage && this.props.showmessage(checkValue.error, checkValue.message);
      // }
    }
    this.setState({ value: value });
  }

  render() {

    const { isMust, className, multiple, text, options, activeClass, ...childrenProps } = this.props;
    let dataSource = [];
    const { value, isOpen } = this.state;
    _.forEach(options, item => {
      if (_.includes(value, item.value)) {
        dataSource.push(item)
      }
    })
    return (
      <div className={ClassNames("hyinputselectcom", className, 'multiple', activeClass)} style={this.props.CustomStyle} ref="inputselect">
        {isMust && text ?
          <div className='form-must'><span>*</span><div key="label" className="ui label">{text}</div></div> :
          <div key="label" className="ui label">{text}</div>}
        <div style={{ position: 'relative', width: '100%', flex: 1 }}>
          <div className='ui visible multiple selection dropdown' ref='fff' onClick={this.onClick}>
            <div className='hyinputselectcom-select'>
              {_.map(dataSource, (item, index) => {
                return <a className='ui lable' value={item.value}>
                  <div className='hyinputselectcom-select-text text-ellipsis'>{item.text}</div>
                  {this.props.multiple || !this.props.isMust ? <Icon className="delete icon" onClick={this.onChange.bind(this, item.value, '')} /> : null}
                </a>
              })}
            </div>
            {this.props.multiple && <Icon className='icon14 img29 hydropdowncom-trash' size='large' onClick={this.onChange.bind(this, value, 'all')}></Icon>}
            {this.props.multiple && <Icon className='dropdown icon' onClick={this.onOpenClick}></Icon>}
          </div>
          {(this.props.multiple && isOpen) && <div className='hyinputselectcom-select-menu'>
            {_.map(dataSource, (item, index) => {
              return <div className='ui lable' value={item.value}>
                <span>{item.text}</span>
                <Icon className="delete icon" onClick={this.onChange.bind(this, item.value, '')} />
              </div>
            })}
          </div>}
        </div>

      </div>
    );
  }

  /**
   * 点击事件
   *
   * @memberof HyInputSelectCom
   */
  onClick = (event, data) => {
      //点击时把选中value 返回 组件名称
    const _props = this.packageReProps();
    let comValue = {
      ..._props,
      value: this.state.value
    };
    this.props.onClick && this.props.onClick(event, comValue);
    if (_.size(this.state.value) !== 0) {
      this.setState({
        isOpen: !this.state.isOpen
      })
    }
    event.preventDefault();
    event.stopPropagation();

  }
  /**
   * 单个删除事件
   *
   * @memberof HyInputSelectCom
   */

  onChange = (value, handel = '', event) => {
    //点击时把选中value 返回 组件名称
    let _value = this.state.value;

    if (this.props.multiple && handel !== 'all') {
      //多选删除
      _value = _.filter(_value, item => item != value)
    } else if (handel === 'all') {
      //全部删除
      _value = [];
    } else {
      //单选置空
      _value = '';
    }
    this.setState({
      value: _value,
      isOpen: _.size(_value) == 0 ? false : this.state.isOpen
    }, () => {
      const _props = this.packageReProps();
      let comValue = {
        ..._props,
        value: this.state.value
      };
      this.props.onChange && this.props.onChange(this, comValue);
    })
    event.preventDefault();
    event.stopPropagation();
  }

  /**
   * 打开关闭已选项
   *
   * @memberof HyInputSelectCom
   */
  onOpenClick = (event, data) => {
    if (_.size(this.state.value) !== 0) {
      this.setState({
        isOpen: !this.state.isOpen
      })
    }
  }

  onClose = (event) => {
    console.log('sss',event.target)
    if(this.state.isOpen){
      this.setState({
        isOpen: false
      })
    }
    event.preventDefault();
    event.stopPropagation();
  }

  //校验
  checkDataSource = (props) => {
    if (!_.has(props, "value") || _.isNull(props.value) || props.value === "" || _.size(`${props.value}`) === 0) {
      return { error: false, message: "" };
    }
    if (_.isArray(props.value)) {
      let _checkValue = { error: false, message: "" };
      // 如果传入的value是数组，则循环value查询数据源
      const _filterData = _.filter(props.value, item => {
        const _value = `${item}`;
        return _.findIndex(props.options, opt => opt.value == _value) !== -1;
      });

      if (_.size(_filterData) != _.size(props.value)) {
        _checkValue = { error: true, message: languageConfig.errmessage };
      }
      return _checkValue;
    } else {
      // 根据传入的值在数据源中查找
      const findIndex = _.findIndex(props.options, (item, index) => {
        return `${props.value}` == `${item.value}`;
      });

      if (findIndex === -1) {
        return { error: true, message: languageConfig.errmessage };
      } else {
        return { error: false, message: "" };
      }
    }
  }
}

export default HyInputSelectCom;
