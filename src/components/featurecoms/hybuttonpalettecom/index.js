import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';
import classNames from "classnames";
import HyButtonCom from '../hybuttoncom';
import HyPaletteCom from 'Hycomponents/featurecoms/hypalettecom';
@BaseComponent
class HyButtonPaletteCom extends Component {
  static propTypes = {
    //類名
    className: PropTypes.string,
    //标题
    label:PropTypes.string,
    //參考popup 打開位置
    position:PropTypes.string,
    //value 顏色值  #98i7jw  #+6位
    value:PropTypes.string,
    //
    onChange:PropTypes.func,
  }


   // eslint-disable-next-line no-useless-constructor
   constructor(props) {
    super(props);
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }


  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    const { color, position, value, onChange, label, className, ...componentProps } = this.props;
    const style = {
      backgroundColor: value
    }
    const _className = classNames(className, "palettebutton")
    return (
      <div className={_className}>
        <div className='ui label'>{label}</div>
        <HyPaletteCom
          color={color}
          position={position}
          value={value}
          onChange={this._onChange}
          trigger={
            <HyButtonCom
              className='palettebutton-trigger'
              style={style}
              {...componentProps}>
            </HyButtonCom>
          } />
      </div>
    )
  }

  _onChange = (event, data) => {
    const _props = this.packageReProps();
    let comValue = {
      ..._props,
      value: data.value
    }
    if (this.props.onChange) {
      this.props.onChange(event, comValue)
    }
  }
}



HyButtonPaletteCom.propTypes = {

};

export default HyButtonPaletteCom;