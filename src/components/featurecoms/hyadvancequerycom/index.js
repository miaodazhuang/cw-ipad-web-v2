import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";
import {Icon,Popup,Transition} from 'semantic-ui-react';
import HyFormCom from 'Hycomponents/featurecoms/hyformcom';
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hyadvancequerycom';
import ja_jp from 'ja_jp/components/featurecoms/hyadvancequerycom';
import zh_cn from 'zh_cn/components/featurecoms/hyadvancequerycom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default class HyAdvanceQueryCom extends React.Component{
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
      super(props);
    }
  static defaultProps = {
    btnText: languageConfig.advanceQuery,
    defaultFormData: {}
  }
  static propTypes = {
    //触发显示popup按钮的文本
    btnText: PropTypes.string,
    //默认表单数据，一键重置时会用该值作为formData，触发form的onChange
    defaultFormData: PropTypes.object
    //其余props参考HyFormCom
  }
  static childContextTypes = {
    setCloseOnDocumentClick: PropTypes.func
  }
  getChildContext(){
    return {
      setCloseOnDocumentClick: this._setCloseOnDocumentClick
    }
  }
  //是否打开
  state = {
    isopen: false,
    closeOnDocumentClick: true //点击其他区域是否关闭该窗口
  }
  componentDidMount(){
    //先通过触发triggerEle的click事件让弹出层找到合适的位置，再关闭他
    this.refs.triggerEle.click();
    this._handleClose();
  }
  render(){
    const {btnText,defaultFormData, ...formProps} = this.props;
    //formData的值有更改时，改变高级查询和一键重置的颜色
    const isEqual = _.isEqual(defaultFormData, formProps.formData);
    const changedClass = isEqual ? "" : "dataChange-btn-color";
    const btnClass = classNames("advanceQ-btn", changedClass);
    const resetClass = classNames("advanceQ-form-reset", changedClass);
    const formClass = classNames("advanceQ-form", formProps.className);
    const triggerEle = (<div onClick={this._handleOpen} className={btnClass} ref="triggerEle">
      <span className="advanceQ-btn-span">{btnText}</span>
      <HyIconCom keyboard="GF02" name={this.state.isopen ? "chevron up" : "chevron down"} onClick={this._handleOpen}></HyIconCom>
    </div>);
    return <Popup className="advance-main" trigger={triggerEle} position="bottom center" closeOnDocumentClick={this.state.closeOnDocumentClick} on="click" basic open={this.state.isopen} onClose={this._handleClose} onOpen={this._handleOpen}>
      <Transition visible={this.state.isopen} animation="fade down" duration={500}>
        <HyFormCom {...formProps} className={formClass}>
          <div className={resetClass} onClick={this._resetData}>
            <Icon className={classNames("icon14", "advanceQ-reset-icon", {img11_1: isEqual, img11_2: !isEqual})}></Icon>
            {languageConfig.reset}
          </div>
        </HyFormCom>
      </Transition>
    </Popup>
  }
  /**
   * 打开popup
   */
  _handleOpen = () => {
    this.setState({
      isopen: true
    });
  }
  /**
   * 关闭popup
   */
  _handleClose = () => {
    this.setState({
      isopen: false
    });
  }


  handleClose=(event)=>{
    this.setState({
      isopen: false
    });
  }


  /**
   * 设置popup在点击其他区域时是否关闭
   */
  _setCloseOnDocumentClick = (isclose) => {
    //if(this.state.closeOnDocumentClick !== isclose){
      this.setState({
        closeOnDocumentClick: isclose
      });
    //}
  }

  /**
   * 一键重置按钮点击事件，通过该事件触发表单的change事件，使用props的defaultFormData去更新form里面的值
   */
  _resetData = (event) => {
    this.props.onChange && this.props.onChange(event, {formData: this.props.defaultFormData,eventData:{fieldname:'reset'}});
  }
}