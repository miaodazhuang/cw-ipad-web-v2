import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Icon, Checkbox } from 'semantic-ui-react';
import { BaseComponent } from 'Hyutilities/component_helper';
import classNames from "classnames";
import HyButtonCom from '../hybuttoncom';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';

@BaseComponent
class HyExtendCheckBoxCom extends Component {

  static defaultProps = {
    isshowbutton: true,
  }

  static propTypes = {
    //数据源 例如：[{buttonlist:{key1:{name:name1,permsCode:[]}},value:value,className:,checkbox的属性}]
    checkboxlist: PropTypes.array,
    //是否显示结尾的按钮
    isshowbutton: PropTypes.bool,
    //[true,false,true] boolarray 于checkboxlist元素数相同,
    checked: PropTypes.array,
    //
    disabled: PropTypes.bool,
    //全选属性 例如 value，lable, radio
    selectAll: PropTypes.object
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);

  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  shouldComponentUpdate(nextProps, nextState) {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    let _com = this._getcom();
    let className = classNames(this.props.className, "extendselectbox");
    const checkedIndex = _.findIndex(this.props.checked, item => !item);

    return (
      <div className={className}>
        {this.props.selectAll ? <Checkbox
          {...this.props.selectAll}
          className = {classNames(this.props.selectAll && this.props.selectAll.className, "extendselectbox-selectAll")}
          label = '全选'
          checked={ checkedIndex!='-1' ? false : true}
          disabled={this.props.selectAll && this.props.selectAll.disabled ? this.props.selectAll.disabled : this.props.disabled || HyAuthCom.CheckPermsCode(this.props.permsCode)}
          onChange={(event, data) => { this.toggle(event, data, '-1') }}></Checkbox> : ""}
        {_com}
      </div>
    );
  }

  _getcom = () => {
    let _checkboxlist = this.props.checkboxlist;
    if (!_checkboxlist) {
      return null
    }
    let checkBoxComs = [];
    _.forEach(_checkboxlist, (item, index) => {
      const { className, disabled, buttonList, Value, id, label, radio } = item;
      checkBoxComs.push(<div className={this.props.checked[index] ? `${className} checked-bkground` : className}>
        <Checkbox
          //id = {id}
          radio={radio}
          value={Value}
          label={label}
          key={index}
          checked={this.props.checked[index]}
          disabled={disabled ? disabled : this.props.disabled || HyAuthCom.CheckPermsCode(this.props.permsCode)}
          onChange={(event, data) => { this.toggle(event, data, index, id) }}></Checkbox>
        {this._getbuttonList(item)}
      </div>)
    })
    return checkBoxComs;
  }

  // 取checkbox后面的操作按钮
  _getbuttonList = (datasoruce) => {
    let buttonList = [];
    if (!datasoruce.buttonList || !this.props.isshowbutton) {
      return;
    }
    for (let key in datasoruce.buttonList) {
      let _className = classNames("icon14", datasoruce.buttonList[key].name);
      buttonList.push(
        <HyButtonCom className='icon-button' data={datasoruce} permsCode={datasoruce.buttonList[key].permsCode} onClick={(event, data) => { this._onClickBtn(event, data, key) }}><Icon className={_className} /></HyButtonCom>
      )
    }
    return buttonList
  }

  _onClickBtn = (event, data, key) => {
    let comValue = null;
    comValue = {
      ...data,
      name: key
    };
    this.props.onClickBtn && this.props.onClickBtn(event, comValue);
  }

  toggle = (event, data, index, id) => {
    let comValue = null;
    comValue = {
      ...data,
      id: id || "",
      index: (index || index === 0) ? index : ""
    };
    this.props.onChange && this.props.onChange(event, comValue);
  }
}


export default HyExtendCheckBoxCom;