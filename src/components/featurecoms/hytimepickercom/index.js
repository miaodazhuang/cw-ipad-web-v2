import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Input, Segment, Popup, List, Label } from 'semantic-ui-react';
import moment from 'moment';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hytimepickercom';
import ja_jp from 'ja_jp/hytimepickercom';
import zh_cn from 'zh_cn/hytimepickercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

const hoursOptions = [
    {
        "text": "00",
        "value": "00"
    }, {
        text: '01',
        value: '01'
    }, {
        text: '02',
        value: '02'
    }, {
        text: '03',
        value: '03'
    }, {
        text: '04',
        value: '04'
    }, {
        text: '05',
        value: '05'
    }, {
        text: '06',
        value: '06'
    }, {
        text: '07',
        value: '07'
    }, {
        text: '08',
        value: '08'
    }, {
        text: '09',
        value: '09'
    }, {
        text: '10',
        value: '10'
    }, {
        text: '11',
        value: '11'
    }, {
        text: '12',
        value: '12'
    }, {
        text: '13',
        value: '13'
    }, {
        text: '14',
        value: '14'
    }, {
        text: '15',
        value: '15'
    }, {
        text: '16',
        value: '16'
    }, {
        text: '17',
        value: '17'
    }, {
        text: '18',
        value: '18'
    }, {
        text: '19',
        value: '19'
    }, {
        text: '20',
        value: '20'
    }, {
        text: '21',
        value: '21'
    }, {
        text: '22',
        value: '22'
    }, {
        text: '23',
        value: '23'
    }
];

const minutesOptions = [
    {
        text: '00',
        value: '00'
    }, {
        text: '01',
        value: '01'
    }, {
        text: '02',
        value: '02'
    }, {
        text: '03',
        value: '03'
    }, {
        text: '04',
        value: '04'
    }, {
        text: '05',
        value: '05'
    }, {
        text: '06',
        value: '06'
    }, {
        text: '07',
        value: '07'
    }, {
        text: '08',
        value: '08'
    }, {
        text: '09',
        value: '09'
    }, {
        text: '10',
        value: '10'
    }, {
        text: '11',
        value: '11'
    }, {
        text: '12',
        value: '12'
    }, {
        text: '13',
        value: '13'
    }, {
        text: '14',
        value: '14'
    }, {
        text: '15',
        value: '15'
    }, {
        text: '16',
        value: '16'
    }, {
        text: '17',
        value: '17'
    }, {
        text: '18',
        value: '18'
    }, {
        text: '19',
        value: '19'
    }, {
        text: '20',
        value: '20'
    }, {
        text: '21',
        value: '21'
    }, {
        text: '22',
        value: '22'
    }, {
        text: '23',
        value: '23'
    }, {
        text: '24',
        value: '24'
    }, {
        text: '25',
        value: '25'
    }, {
        text: '26',
        value: '26'
    }, {
        text: '27',
        value: '27'
    }, {
        text: '28',
        value: '28'
    }, {
        text: '29',
        value: '29'
    }, {
        text: '30',
        value: '30'
    }, {
        text: '31',
        value: '31'
    }, {
        text: '32',
        value: '32'
    }, {
        text: '33',
        value: '33'
    }, {
        text: '34',
        value: '34'
    }, {
        text: '35',
        value: '35'
    }, {
        text: '36',
        value: '36'
    }, {
        text: '37',
        value: '37'
    }, {
        text: '38',
        value: '38'
    }, {
        text: '39',
        value: '39'
    }, {
        text: '40',
        value: '40'
    }, {
        text: '41',
        value: '41'
    }, {
        text: '42',
        value: '42'
    }, {
        text: '43',
        value: '43'
    }, {
        text: '44',
        value: '44'
    }, {
        text: '45',
        value: '45'
    }, {
        text: '46',
        value: '46'
    }, {
        text: '47',
        value: '47'
    }, {
        text: '48',
        value: '48'
    }, {
        text: '49',
        value: '49'
    }, {
        text: '50',
        value: '50'
    }, {
        text: '51',
        value: '51'
    }, {
        text: '52',
        value: '52'
    }, {
        text: '53',
        value: '53'
    }, {
        text: '54',
        value: '54'
    }, {
        text: '55',
        value: '55'
    }, {
        text: '56',
        value: '56'
    }, {
        text: '57',
        value: '57'
    }, {
        text: '58',
        value: '58'
    }, {
        text: '59',
        value: '59'
    }
];

@BaseComponent
export default class HyTimePickerCom extends Component {
    static propTypes = {
        value: PropTypes
            .PropTypes
            .oneOfType([PropTypes.string, PropTypes.number]), // 默认显示时间,支持字符串和数字两种类型
        text: PropTypes.string, // timepicker前的label字段
        timeStamp: PropTypes.bool, //是否返回时间戳
        disabled: PropTypes.bool //是否无效状态
    }

    static contextTypes = {
        setCloseOnDocumentClick: PropTypes.func
    };

    static defaultProps = {
        value: '12:00',
        timeStamp: false //默认返回字符串
    }

    isBlur = true;

    constructor(props) {
        super(props);
        this.state = {
            hour: '',
            min: '',
            val_h: '',
            val_min: '',
            isOpen: false
        }
        const value = props.value;
        this.initState(value, true);
    };

    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(nextProps.value, this.props.value)) {
            let value = nextProps.value;
            this.initState(value)
        }
    }

    componentDidMount() {
        document.getElementById("root").addEventListener('scroll', this.hideOnScroll);
        //先通过触发triggerEle的click事件让弹出层找到合适的位置，再关闭他
        this.refs.timepicker.inputRef.click();
        this.setState({
            isOpen: false
        },()=>{
            if (this.context.setCloseOnDocumentClick) {
                this.context.setCloseOnDocumentClick(!this.state.isOpen)
            }
        })
    }

    render() {
        const {
            children,
            showmessage,
            text,
            isMust,
            ...childrenProps
        } = this.props;
        const { hour, min, val_h, val_min } = this.state;

        const eventProps = {
            onChange: this.onChange
        }

        let inputCom = null;

        if (_.has(this.props, "text")) {
            if (isMust) {
                inputCom = <Input
                    ref="timepicker"
                    {...eventProps}
                    onBlur={this._onBlur}
                    onFocus={this._onFocus}
                    className={ClassNames("time_selector", this.props.className)}
                    title={this.props.title}
                    disabled={this.props.disabled}
                    onChange={this._change}
                    value={hour + ':' + min}>
                    <div className='form-must'>
                        <span>*</span>
                        <Label>{text}</Label>
                    </div>
                    <input />
                </Input>
            } else {
                inputCom =
                    <Input
                        ref="timepicker"
                        {...eventProps}
                        onBlur={this._onBlur}
                        onFocus={this._onFocus}
                        className={ClassNames("time_selector", this.props.className)}
                        title={this.props.title}
                        disabled={this.props.disabled}
                        onChange={this._change}
                        value={hour + ':' + min}>
                        <Label>{text}</Label>
                        <input />
                    </Input>

            }
        } else {
            inputCom =
                <Input
                    ref="timepicker"
                    {...eventProps}
                    onBlur={this._onBlur}
                    onFocus={this._onFocus}
                    className={ClassNames("time_selector", this.props.className)}
                    title={this.props.title}
                    disabled={this.props.disabled}
                    onChange={this._change}
                    value={hour + ':' + min} />

        }

        return (
            <Popup
                hideOnScroll
                trigger={inputCom}
                on='click'
                basic
                position='bottom left'
                className='selector_menu'
                open={this.state.isOpen}
                onOpen={(e,data)=>this.handleIsOpen(e,data,true)}
                onClose={this.handleCancel}>
                <div onClick={this._onClickTime}>
                    <Segment.Group horizontal className='popup_segments'>
                        <Segment>
                            <List>
                                {hoursOptions.map(optionItem => <List.Item
                                    key={optionItem.value}
                                    {...optionItem}
                                    onClick={this.hourChange}
                                    active={this.state.hour === optionItem.value
                                        ? true
                                        : false}>
                                    {optionItem.text}
                                </List.Item>)
                                }
                            </List>
                        </Segment>
                        <Segment>
                            <List>
                                {minutesOptions.map(optionItem => <List.Item
                                    key={optionItem.value}
                                    {...optionItem}
                                    onClick={this.minChange}
                                    active={this.state.min === optionItem.value
                                        ? true
                                        : false}>
                                    {optionItem.text}
                                </List.Item>)
                                }
                            </List>
                        </Segment>
                    </Segment.Group>
                    <div className='popup_button'>
                        <span className='button sure' onClick={this.handleSure}>{languageConfig.ok}</span>
                        <span className='button cancel' onClick={this._onCancel}>{languageConfig.cancel}</span>
                    </div>
                </div>
            </Popup>
        )
    }

    hideOnScroll = () => {
        this.setState({
            isOpen:false
        })
    }

    _onClickTime = (event, data) => {
        this.isBlur = false;
        event.preventDefault();
        event.stopPropagation();
    }

    _onFocus = (event, data) => {
        //if(!this.state.isOpen){
            // this.refs.timepicker.inputRef.click();
            // event.preventDefault();
            // event.nativeEvent.stopImmediatePropagation();
            // event.stopPropagation();
        //}
    }
    _change = (event,data) => {
        let hour = this.state.hour;
        let min = this.state.min
        if(data.value == ''){
            hour = '';
            min = '';
        }else if(/^\d{0,2}:\d{0,2}$/.test(data.value)){
            const time = format(data.value);
            hour = time.hour;
            min = time.min
        }
        this.setState({
            isOpen: false,
            hour,
            min
        },()=>{
            if (this.context.setCloseOnDocumentClick) {
                this.context.setCloseOnDocumentClick(!this.state.isOpen)
            }
        });
    }

    _onBlur = (event, data) => {
        if(!this.state.isOpen){
            if(this.state.hour == '' && this.state.min == ''){
                this.setState({
                    hour:'',
                    min:''
                }, ()=>{
                    this.onChange()
                })
            }else if(this.state.hour.length!==2 || this.state.min.length!==2 || parseInt(this.state.hour)>=24 || parseInt(this.state.min)>=60){
               this.setState({
                   hour:this.state.val_h,
                   min:this.state.val_min
               },()=>{
                this.onChange()
            })
           }else{
            this.setState({
                val_h:this.state.val_h,
                val_min:this.state.val_min,
            },()=>{
             this.onChange()
         })
           }

        }

    }

    handleIsOpen = (e) => {
        this.setState({
            isOpen: !this.state.isOpen
        },()=>{
            if (this.context.setCloseOnDocumentClick) {
                this.context.setCloseOnDocumentClick(!this.state.isOpen)
            }
        });
    }

    hourChange = (e) => {
        let item = e.target;
        let itemText = item.textContent;
        if (itemText) {
            this.setState({ hour: itemText })
        }
    }

    minChange = (e) => {
        let item = e.target;
        let itemText = item.textContent;
        if (itemText) {
            this.setState({ min: itemText })
        }
    }

    handleCancel = (event, data) => {
        this.isBlur = true;
        if(this.state.hour == '' && this.state.min == ''){
            this.setState({
                hour:'',
                min:''
            })
        }else{
            this.setState({ hour: this.state.val_h, min: this.state.val_min })
        }
        // 点击 timePicker 的 input 时，popup 弹起，同时这个点击事件可能触发 popup 的关闭事件
        if (this.state.isOpen) {
            this.handleIsOpen(event, data,true);
        }
    }

    _onCancel = (event, data) => {
        this.isBlur = true;
        this.setState({ hour: this.state.val_h, min: this.state.val_min })
        if (this.state.isOpen) {
            this.handleIsOpen(event, data, true);
        }
        event.preventDefault();
        event.nativeEvent.stopImmediatePropagation();
        event.stopPropagation();
    }

    handleSure = (event, data) => {
        this.isBlur = true;
        if(this.state.hour == '' || this.state.min == '') return;
        this.setState({ val_h: this.state.hour, val_min: this.state.min })
        this.onChange();
        if (this.state.isOpen) {
            this.handleIsOpen(event, data, true);
        }
        event.preventDefault();
        event.nativeEvent.stopImmediatePropagation();
        event.stopPropagation();
    }

    onChange = (event, data) => {
        let comValue = {};
        comValue = {
            ...this.props,
            value: this.props.timeStamp
                ? moment(this.state.hour + ':' + this.state.min, "HH:mm").valueOf()
                : (this.state.hour == ''&& this.state.min==''? '': this.state.hour + ':' + this.state.min)
        }
        this.props.onChange && this
            .props
            .onChange(event, comValue);
    }

    initState = (value, init) => {
        if(value === null || value === undefined || value === ''){
            this.state.val_h = '';
            this.state.val_min = '';
            this.state.hour = '';
            this.state.min = '';
            return;
        }
        if (init) {
            if (_.isNumber(value)) {
                let default_hour = moment(value).hour() + '',
                    default_min = moment(value).minute() + '';
                if (default_hour <= 9) {
                    default_hour = '0' + default_hour;
                }
                if (default_min <= 9) {
                    default_min = '0' + default_min;
                }
                this.state.val_h = default_hour;
                this.state.val_min = default_min;
                this.state.hour = default_hour;
                this.state.min = default_min;
            } else {
                let timeVal = value.split(':');
                let tampTime = timeVal[0].split(' ');
                let default_hour = tampTime[tampTime.length - 1];
                let default_min = timeVal[1];
                this.state.val_h = default_hour;
                this.state.val_min = default_min;
                this.state.hour = default_hour;
                this.state.min = default_min;
            }
        } else {
            if (_.isNumber(value)) {
                let default_hour = moment(value).hour() + '',
                    default_min = moment(value).minute() + '';
                if (default_hour <= 9) {
                    default_hour = '0' + default_hour;
                }
                if (default_min <= 9) {
                    default_min = '0' + default_min;
                }
                this.setState({ val_h: default_hour, val_min: default_min, hour: default_hour, min: default_min })
            } else {
                let timeVal = value.split(':');
                let tampTime = timeVal[0].split(' ');
                let default_hour = tampTime[tampTime.length - 1];
                let default_min = timeVal[1];
                this.setState({ val_h: default_hour, val_min: default_min, hour: default_hour, min: default_min })
            }
        }
    }
}

function format (str){
   let ary = str.split(':');
    return {
        hour:ary[0],
        min:ary[1]
    }
}