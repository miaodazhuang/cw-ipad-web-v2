import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import _ from 'lodash';
import HyAuthCom from '../hyauthcom';
import { Popup } from "semantic-ui-react";
import { GetLanguage } from 'Hyutilities/language_helper';
import ClassNames from 'classnames';
import en_us from 'en_us/hycolordatecom';
import ja_jp from 'ja_jp/hycolordatecom';
import zh_cn from 'zh_cn/hycolordatecom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default class HyColorDateCom extends Component {
  static propTypes = {
    //日期时间戳
    date: PropTypes.number,
    //数据源 //itemTempFun
    dataSource: PropTypes.array.isRequired,
    //类名
    className: PropTypes.string,
    //是否是房价日历 渠道日历不用传
    isRoomRank: PropTypes.bool,
    //点击事件
    onClick: PropTypes.func,
    //权限
    permsCode: PropTypes.array,
    //如果传了就按照传递的内容如果没传就走原来的
    isContent: PropTypes.bool,
  }
  // Primary content.
  static defaultProps = {
    // date: window.__GETBUSINESSDT__ ? window.__GETBUSINESSDT__() : moment()
  }

  componentDidMount() {

  }

  componentDidUpdate() {
  }

  componentWillReceiveProps(nextProps) {

  }

  render() {
    let showCalendarData = this.showCalendarData();
    return (
      <div id='calendar' className='colorCalendar' style={this.props.style}>
        <div className='calendar-title-box'>
          <span className='calendar-title' id='calendarTitle'>{showCalendarData.titleStr}</span>
        </div>
        <div className='calendar-body-box'>
          <div className='calendar-week'>{languageConfig.Sunday}</div>
          <div className='calendar-week'>{languageConfig.Monday}</div>
          <div className='calendar-week'>{languageConfig.Tuesday}</div>
          <div className='calendar-week'>{languageConfig.Wednesday}</div>
          <div className='calendar-week'>{languageConfig.Thursday}</div>
          <div className='calendar-week'>{languageConfig.Friday}</div>
          <div className='calendar-week'>{languageConfig.Saturday}</div>
          {this.renderHtml()}
        </div>
      </div>
    )
  }

  renderHtml = () => {
    let _bodyHtml = [];
    let _showCalendarData = this.showCalendarData();
    let datasource = _showCalendarData.datasource;

    _.forEach(datasource, (item, index) => {
      if (_.includes(item.className, 'otherMonth')) {
          _bodyHtml.push(
            <div key={index} data={item.thisDayStr} className={item.className}></div>
          )
      }
      else {
        if(this.props.isContent){
          let _content = this.props.itemTempFun(item,index);
          _bodyHtml.push(_content);
        }else{
          if (_.size(item.subDataSource) != 0 && item.subDataSource.rankCode != '-1' && item.subDataSource.date >= moment(moment().format('YYYY-MM-DD')).valueOf()) {
            let trigger;
            this.props.isRoomRank ? trigger =
              //_bodyHtml.push(
              <div key={index} data={item.thisDayStr} className={ClassNames(item.className, 'colorCalendar-roomRank')}
                style={{ "backgroundColor": item.subDataSource.color || "" }}>
                <div>{item.thisDay}</div>
                <div className='calendar-body-div-text'>{item.subDataSource.rankDrpt}</div>
              </div> : trigger = <div key={index} data={item.thisDayStr} className={ClassNames(item.className, 'colorCalendar-channel')}
                style={{ "backgroundColor": item.subDataSource.color || "" }}>
                <div className='calendar-body-box-textdate'>
                  <div>{item.thisDay}</div>
                  {item.subDataSource.salesClosed === '1' && <div>{languageConfig.close}</div>}
                </div>
                <div className='calendar-body-div-drpt'>{item.subDataSource.rankDrpt}</div>
                <div className='calendar-body-div-text'>{item.subDataSource.type === '1' ? languageConfig.sum : languageConfig.percentage}：{item.subDataSource.amount || ""}</div>
              </div>
            //)
            _bodyHtml.push(<Popup on="click" trigger={trigger} position="bottom center">
              {this._getPopupContent(item.subDataSource)}
            </Popup>)
          } else {
            this.props.isRoomRank && _bodyHtml.push(
              <div key={index} data={item.thisDayStr}  className={ClassNames(item.className, 'colorCalendar-roomRank')}
                style={{ "backgroundColor": item.subDataSource.color || "", cursor: "auto" }}>
                <div>{item.thisDay}</div>
                <div className='calendar-body-div-text'>{item.subDataSource.rankDrpt}</div>
              </div>
            )
            !this.props.isRoomRank && _bodyHtml.push(
              <div key={index} data={item.thisDayStr} className={ClassNames(item.className, 'colorCalendar-channel')}
                style={{ "backgroundColor": item.subDataSource.color || "", cursor: "auto" }}>
                <div className='calendar-body-box-textdate'>
                  <div>{item.thisDay}</div>
                  {item.subDataSource.salesClosed === '1' && <div>{languageConfig.close}</div>}
                </div>
                <div className='calendar-body-div-drpt'>{item.subDataSource.rankDrpt}</div>
                <div className='calendar-body-div-text'>{item.subDataSource.type === '1' ? languageConfig.sum : languageConfig.percentage}：{item.subDataSource.amount || ""}</div>
              </div>
            )
          }
        }
      }

    })
    return _bodyHtml
  }

  _getPopupContent = (clickObj) => {
    return <section className="crc-table-popup">
      {[1, 2, 3, "..."].map((item, index) => {
        let isNotMore = typeof item === "number";
        return <div className="crct-popup-con" key={index}>
          <div className="crct-popup-btn" onClick={this._popupClick.bind(this, clickObj, isNotMore ? item - 1 : 4)}>{isNotMore ? item : <span className="crct-popup-span">{item}</span>}</div>
          <span className="crct-popup-text">{isNotMore ? languageConfig.day : languageConfig.more}</span>
        </div>
      })}
    </section>
  }

  _popupClick = (clickObj, days, event ) => {
    if(_.has(this.props, "permsCode") && HyAuthCom.CheckPermsCode(this.props.permsCode)){
      return
    }
    let obj = {
      ...clickObj,
      days: days
    }
    if (this.props.onClick) {
      return this.props.onClick(event, obj);
    }
  }

  /**
   * 表格中显示数据，并设置类名
   */
  showCalendarData = () => {
    const { dataSource, date } = this.props;
    //const date = new Date(this.props.date)
    const curdate = date ? new Date(date) : (dataSource[0] ? new Date(dataSource[0].date) : new Date(moment())) ;
    //const date = new Date(1556640000000)
    let _year = curdate.getFullYear();//年
    let _month = curdate.getMonth() + 1;//月
    let _day = curdate.getDay();//周几
    let _dateStr = this.getDateStr(curdate);
    // 设置顶部标题栏中的 年、月信息
    let titleStr = _dateStr.substr(0, 4) + languageConfig.year + _dateStr.substr(4, 2) + languageConfig.month;
    // 设置表格中的日期数据
    let _firstDay = new Date(_year, _month - 1, 1);  // 当前月第一天
    let data = [];
    let className = '';
    for (let i = 0; i < 42; i++) {
      let subDataSource = {}
      let _thisDay = new Date(_year, _month - 1, i + 1 - _firstDay.getDay());
      let _thisDayStr = this.getDateStr(_thisDay);
      if (_thisDayStr === this.getDateStr(curdate)) {    // 当前天
        className = 'currentMonth';
        subDataSource = dataSource[i - _day] || {};
      } else if (_thisDayStr.substr(0, 6) === this.getDateStr(_firstDay).substr(0, 6)) {
        subDataSource = dataSource[i - _day] || {};
        className = 'currentMonth';  // 当前月
      } else {    // 其他月
        className = 'otherMonth';
      }

      data.push({
        subDataSource: subDataSource,
        thisDayStr: _thisDayStr,
        thisDay: _thisDay.getDate(),
        className: className
      })

    }
    return { titleStr: titleStr, datasource: data }
  }

  /**
   * 日期转化为字符串， 4位年+2位月+2位日
   */
  getDateStr = (date) => {
    let _year = date.getFullYear();
    let _month = date.getMonth() + 1;    // 月从0开始计数
    let _d = date.getDate();

    _month = (_month > 9) ? ("" + _month) : ("0" + _month);
    _d = (_d > 9) ? ("" + _d) : ("0" + _d);
    return _year + _month + _d;
  }
}
