import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import { Icon } from 'semantic-ui-react'
import { TransBaseDate } from 'Hyutilities/moment_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hydatepickercom';
import ja_jp from 'ja_jp/hydatepickercom';
import zh_cn from 'zh_cn/hydatepickercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export class HyCalendarCom extends Component {
    constructor(props) {
        super(props);
        this.format = window.__GETCUSTOMDATEFORMAT__ || moment.localeData()._longDateFormat.L;
        let value = this.props.value !== '' ? moment(this.props.value, this.format, true).isValid() ? new Date(this.props.value) : new Date(TransBaseDate(this.props.value)) : new Date();
        this.state = {
            _date: value,//当前时间
            minDate: this.props.minDate,//最小时间
            maxData: this.props.maxData,//最大时间
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {
    }

    componentWillReceiveProps(nextProps) {

    }

    render() {
        let showCalendarData = this.showCalendarData();
        return (
            <div id='calendar' onClick = {this._onClick} className='calendar' style={this.props.style}>
                <div className='calendar-title-box'>
                    <Icon className='icon14 img96 handleIcon' onClick = {this.toPrevYear}/>
                    <Icon className='icon14 img99_1 handleIcon' onClick = {this.toPrevMonth}/>
                    {/* <span className='prev-month' id='prevMonth' onClick={this.toPrevMonth}></span> */}
                    <span className='calendar-title' id='calendarTitle'>{showCalendarData.titleStr}</span>
                    {/* <span id='nextMonth' className='next-month' onClick={this.toNextMonth}></span> */}
                    <Icon className='icon14 img98_1 handleIcon' onClick = {this.toNextMonth}/>
                    <Icon className='icon14 img97 handleIcon' onClick = {this.toNextYear}/>
                </div>
                <div className='calendar-body-box'>
                    <div className='calendar-week'>{languageConfig.Sunday}</div>
                    <div className='calendar-week'>{languageConfig.Monday}</div>
                    <div className='calendar-week'>{languageConfig.Tuesday}</div>
                    <div className='calendar-week'>{languageConfig.Wednesday}</div>
                    <div className='calendar-week'>{languageConfig.Thursday}</div>
                    <div className='calendar-week'>{languageConfig.Friday}</div>
                    <div className='calendar-week'>{languageConfig.Saturday}</div>
                    {this.renderHtml()}
                    <div className = 'calendar-today' onClick = {this._onTodayClick}>{languageConfig.today}</div>
                </div>
            </div>
        )
    }

    //回到今天
    _onTodayClick = (event,data) => {
        let obj =this.props.today ? this.getDateStr(new Date()) : this.getDateStr(new Date((global.__BUSINESSDT__).valueOf()));
        if (this.props.clickDate) {
            return this.props.clickDate(event, obj);
        }
    }

    _onClick = (event,data) => {
      if(this.props.onClick){
        this.props.onClick(event,data)
      }
    }

    getCurDate = () => {
        return this.state._date;
    }

    setDate = (data) => {
        this.setState({ _date: data });
    }

    renderHtml = () => {
        let _bodyHtml = [];
        let _showCalendarData = this.showCalendarData();
        let datasource = _showCalendarData.datasource;
        _.forEach(datasource, (item, index) => {
            if (_.includes(item.className, 'disable-calendar')) {
                _bodyHtml.push(
                    <div key={index} data={item.thisDayStr} className={item.className}>{item.thisDay}</div>
                )
            }
            else {
                _bodyHtml.push(
                    <div key={index} data={item.thisDayStr} className={item.className} onClick={(e) => { this.clickDate(e, item) }}>{item.thisDay}</div>
                )
            }

        })
        return _bodyHtml
    }

    clickDate = (e, item) => {
        let obj = item.thisDayStr;
        if (this.props.clickDate) {
            return this.props.clickDate(e, obj);
        }
    }

    /**
     * 表格中显示数据，并设置类名
     */
    showCalendarData = () => {
        let _year = this.getCurDate().getFullYear();
        let _month = this.getCurDate().getMonth() + 1;
        let _dateStr = this.getDateStr(this.getCurDate());
        // 设置顶部标题栏中的 年、月信息
        let titleStr = _dateStr.substr(0, 4) + languageConfig.year + _dateStr.substr(4, 2) + languageConfig.month;
        //let titleStr  = moment(this.getCurDate()).valueOf();
       // titleStr = moment(titleStr).format('L');
        // 设置表格中的日期数据
        let _firstDay = new Date(_year, _month - 1, 1);  // 当前月第一天
        let data = [];
        let className = '';
        for (let i = 0; i < 42; i++) {
            let _thisDay = new Date(_year, _month - 1, i + 1 - _firstDay.getDay());
            let _thisDayStr = this.getDateStr(_thisDay);
            if (_thisDayStr === this.getDateStr(this.state._date)) {    // 当前天
                className = 'currentDay';
            } else if (_thisDayStr.substr(0, 6) === this.getDateStr(_firstDay).substr(0, 6)) {
                className = 'currentMonth';  // 当前月
            } else {    // 其他月
                className = 'otherMonth';
            }
            let result = moment(_thisDayStr, this.format);
            if (this.props.minDate) {
                let minDate;
                minDate = this.props.minDate;

                if (moment(result).valueOf()<minDate) {
                    className = `${className} disable-calendar`
                }
            }
            if (this.props.maxDate) {
                let maxDate;
                maxDate = this.props.maxDate;
                if (moment(maxDate).valueOf()<result) {
                    className = `${className} disable-calendar`
                }
            }

            data.push({
                thisDayStr: _thisDayStr,
                thisDay: _thisDay.getDate(),
                className: className
            })

        }
        return { titleStr: titleStr, datasource: data }
    }

    /**
     * 点击上个月图标触发
     */
    toPrevMonth = () => {
        let date = this.getCurDate();
        this.setDate(new Date(date.getFullYear(), date.getMonth() - 1, 1));
        this.showCalendarData();
    }

    /**
     * 点击下个月图标触发
     */
    toNextMonth = () => {
        let date = this.getCurDate();
        this.setDate(new Date(date.getFullYear(), date.getMonth() + 1, 1));
        this.showCalendarData();
    }

    /**
     * 点击下一年图标触发
     */
    toNextYear= () => {
        let date = this.getCurDate();
        this.setDate(new Date(date.getFullYear() + 1, date.getMonth(), 1));
        this.showCalendarData();
    }

    /**
     * 点击上一年图标触发
     */
    toPrevYear= () => {
        let date = this.getCurDate();
        this.setDate(new Date(date.getFullYear() - 1, date.getMonth(), 1));
        this.showCalendarData();
    }

    /**
     * 日期转化为字符串， 4位年+2位月+2位日
     */
    getDateStr = (date) => {
        let _year = date.getFullYear();
        let _month = date.getMonth() + 1;    // 月从0开始计数
        let _d = date.getDate();

        _month = (_month > 9) ? ("" + _month) : ("0" + _month);
        _d = (_d > 9) ? ("" + _d) : ("0" + _d);
        return _year + _month + _d;
    }
}
