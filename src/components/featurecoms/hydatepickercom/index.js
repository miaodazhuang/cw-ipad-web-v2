
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Input, Checkbox } from 'semantic-ui-react'
import moment from 'moment';
import _ from 'lodash';
import HyPopupCom from '../hypopupcom/index.js';
import { BaseComponent } from 'Hyutilities/component_helper';
import { FormatMoment } from 'Hyutilities/moment_helper';
import { TransBaseDate } from 'Hyutilities/moment_helper';
import { HyCalendarCom } from './calendar.js';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hydatepickercom';
import ja_jp from 'ja_jp/hydatepickercom';
import zh_cn from 'zh_cn/hydatepickercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
export default class HyDatePickerCom extends Component {

    static propTypes = {
        className: PropTypes.string,
        //自定义样式 string
        customStyle: PropTypes.string,
        //自定义行内样式 string
        minDate: PropTypes.number,
        //可支持的最小时间 number
        isAllowEmpty: PropTypes.bool,
        //bool IsExactMatch为false时，是否允许为空
        maxDate: PropTypes.number,
        //可支持的最大时间 number
        defaultValue: PropTypes.number,
        //IsExactMatch为false，也就是与业务日期有关的日期选择时的默认值, 如果没有DefaultValue以业务日期为准
        option: PropTypes.object,
        //Input的属性
        text: PropTypes.string,
        //输入框标题 string
        labelCustomClassName: PropTypes.string,
        //label的自定义样式
        popCustomClassName: PropTypes.string,
        // //hypopcom的自定义样式
        // isHiddenTitle: PropTypes.bool,
        //是否隐藏标题 bool
        isExactMatch: PropTypes.bool,
        //是否精确匹配，和业务日期无关的需要精确匹配
        // value: PropTypes.oneOfType([
        //     PropTypes.number
        // ]),
        // 是否开启checkbox
        enableCheckBox: PropTypes.bool,
        // 组件是否被选中
        selected: PropTypes.bool,
        //初值
        OnChange: PropTypes.func,
        //数据改变后会调用的方法 function
        isMust: PropTypes.bool,
        //是否必输
        equalMaxMinValue: PropTypes.bool,
        //如果输入的值大于最大值则等于最大值  如果输入的值小于最小值则等于最小值
        today: PropTypes.bool,
       //默认false业务日期  如果是当前时间需要传true
    };

    static defaultProps = {
        enableCheckBox: false,
        selected: false
    }

    static contextTypes = {
        setCloseOnDocumentClick: PropTypes.func
    };

    isBlur = true;

    constructor(props) {
        super(props);
        //this.format = window.__GETCUSTOMDATEFORMAT__ || moment.localeData()._longDateFormat.L;
        this.format =FormatMoment(window.__GETCUSTOMDATEFORMAT__ || moment.localeData()._longDateFormat.L);
        this.bussinessDt = window.__GETBUSINESSDT__ ? window.__GETBUSINESSDT__() : moment();
        //let value = props.value ? _.trim(props.value) === "" ? '' : moment(props.value).format('L') : !this.props.isExactMatch && !this.props.isAllowEmpty ? this._getDefaultValue(props) : '';
        let value = props.value ? _.trim(props.value) === "" ? '' : moment(props.value).format(FormatMoment('L')) : !this.props.isExactMatch && !this.props.isAllowEmpty ? this._getDefaultValue(props) : '';
        this.state = {
            error: false,
            open: false,
            Value: value,
            errmessage: false,
            position: this.props.position||'bottom left',
            ...this.props.DefaultState
                ? this.props.DefaultState.thisState
                : {}
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    componentWillReceiveProps(nextProps) {
        if (_.trim(this.props.value) !== _.trim(nextProps.value)) {
            this.setState({
               // Value: nextProps.value ? _.trim(nextProps.value) === "" ? '' : moment(nextProps.value).format('L') : !nextProps.isExactMatch && !nextProps.isAllowEmpty ? this._getDefaultValue(nextProps) : ''
               Value: nextProps.value ? _.trim(nextProps.value) === "" ? '' : moment(nextProps.value).format(FormatMoment('L')) : !nextProps.isExactMatch && !nextProps.isAllowEmpty ? this._getDefaultValue(nextProps) : ''
            });
        }
    }

    render() {
        const style = { position: 'absolute' }
        const { enableCheckBox, selected } = this.props;
        const checked = enableCheckBox && selected ? true : false;
        let customClassName = `${this.props.className || ''} ui left labeled input hydatepickercom`;
        // let stateValue =  this._replaceAll(this.state.Value, ["-", "/"], ["", ""]);
        //let formatValue = _.trim(this.state.Value) ;
        // if(stateValue.length ===8){
        //     if(!moment(formatValue, this.format, true).isValid()){
        //         formatValue = TransBaseDate(formatValue);
        //     }
        //     formatValue = new Date(formatValue);
        //     formatValue = formatValue ? moment(formatValue).format(FormatMoment('L')) : "";
        // }
        let temp = moment(this.state.Value, this.format, true);
        let calendarValue = moment(this.bussinessDt);
        if (temp.isValid()) {  //日期格式如果正确
            calendarValue = _.trim(this.state.Value);
        }
        return (
            <div className={customClassName} style={this.props.customStyle} ref="datepicker">
                {this.props.text ? (this.props.isMust ? <div className='form-must'><span>*</span><div key="label" className="ui label">{this.props.text}</div></div> : <div key="label" className="ui label">{this.props.text}</div>) : ''}
                {/* <span className={`ui label datepickerlabel ${this.props.labelCustomClassName || ''}`}>{this.props.isHiddenTitle ? '' : this.props.text}</span> */}
                {
                    enableCheckBox ? <Checkbox className="input-checkbox-div" checked={checked} onClick={this.onCheckBoxClick.bind(this)} /> : null
                }
                <HyPopupCom
                    ref="datepickerpop"
                    className={this.state.errmessage?"datepickererrmes_popup":`datepickerpop ${this.props.popCustomClassName || ''}`}
                    isopen={this.state.open}
                    //defaultOpen={false}
                    onOpen={this._onOpenHandle}
                    onClose={this._onClose}
                    position={this.state.position}
                    coords={this.coords}
                    style={style}
                    hideOnScroll={true}
                    trigger={
                        <Input {...this.props.option}
                            disabled={this.props.disabled}
                            ref='inputcalendar'
                            icon='calendar'
                            onClick={this._onClick}
                            value={_.trim(this.state.Value)}
                            //value={formatValue}
                            onChange={this._onChange}
                            onBlur={this._onBlur}
                            onFocus={this._onFocus}
                            error={this.props.error ? this.props.error : this.state.error} />
                    }>
                    {this.state.errmessage ? <div className='datepickererrmes'>{this.errmessage}</div> :
                        <HyCalendarCom
                            onClick={this._onCalendar}
                            clickDate={this.clickDate}
                            minDate={this.props.minDate}
                            maxDate={this.props.maxDate}
                            //value={_.trim(this.state.Value)}
                            value={calendarValue}
                            today={this.props.today}
                            />}
                </HyPopupCom>
            </div>
        )
    }

    _onCalendar = (event, data) => {
      this.isBlur = false;
    }

    /**
   * 点击checkbox触发
   *
   * @memberof HyInputCom
   */
  onCheckBoxClick = (event, data) => {
    if (this.props.disabled) {
      return;
    }
    this.props.onClick && this.props.onClick(event, { ...data, type: "checkbox" });
  }

    /**
       * 获取defaultValue
       * 返回字符串类型的format
       *
       * @memberof HyDatePickerCom
    **/
    _getDefaultValue = (props) => {
        let pickerValue;
        if (props.minDate) {
            // let minDate = !isNaN(props.minDate) ? moment(props.minDate, this.format) : _.cloneDeep(props.minDate);
            pickerValue = moment(props.minDate).add(1, "days");
        }
        else if (props.maxDate) {
            //let maxDate = !isNaN(props.maxDate) ? moment(props.maxDate, this.format) : _.cloneDeep(props.maxDate);
            pickerValue = moment(props.maxDate).add(-1, "days");
        }
        else if (props.defaultValue != null) {
            //pickerValue = (!isNaN(props.defaultValue)) && props.defaultValue !== "" ? moment(props.defaultValue, this.format) : props.defaultValue;
            pickerValue = moment(props.defaultValue);
        }
        else {
            pickerValue = moment(this.bussinessDt);
        }
        return pickerValue !== "" ? moment(pickerValue).format(FormatMoment('L')) : "";
    }

    _onOpenHandle = (e, obj) => {
        this.coords = e.currentTarget.getBoundingClientRect();
        var tempValue = this.state.Value;
        this._matchDate(tempValue, false);
        if (this.context.setCloseOnDocumentClick) {
            this.context.setCloseOnDocumentClick(false);
        }
    }

    clickDate = (event, obj) => {
        this.isBlur = false;
        const _props = this.packageReProps();
        let comValue = {
            ..._props,
            value: moment(obj).valueOf()
        };
        this.setState({ Value: moment(obj).format(this.format), open: false }, () => {
               if(this.props.error){
                this._matchDate(moment(obj).format(this.format), false, null, true);
               }
                if (this.props.onChange) {
                    this.props.onChange(event, comValue);
                }

            this.closeWithTimeout();
        });
        this._closeWindow();

    }

    _closeWindow = () => {
        if (this.context.setCloseOnDocumentClick) {
            setTimeout(() => {
                this.context.setCloseOnDocumentClick(true);
            }, 50)
        }
    }

    _onClose = () => {
        //setTimeout(() => {
            if (!this.state.error) {
                this.setState({ open: false })
            }

            if (this.context.setCloseOnDocumentClick) {
                this.context.setCloseOnDocumentClick(true);
            }
        //},60)
    }

    closeWithTimeout = () => {
        if (this.context.setCloseOnDocumentClick) {
            setTimeout(() => {
                this.context.setCloseOnDocumentClick(true);
            }, 50);
        }
    }

    _onClick =  (event, data) => {
        const _props = this.packageReProps();
        let comValue = {
            ..._props
        };
        this.setState({ open: true }, () => {
            if(this.props.onClick && (this.props.value != moment(this.state.Value).valueOf() || this.state.error || this.props.error)){
                this.props.onChange(event, comValue);
            }
        })
    }

    /**
       * 日期输入框改变
       * @param  {[object]} sender [输入框对象]
       * @param  {[object]} obj [输入框数据对象]
       * @return {[type]}         null
       */
    _onChange = (event, data) => {
        if (this.state.open) {
            this.setState({ error: false, open: false, errmessage: false, position: 'bottom left' });
        }
        const _props = this.packageReProps();

        this.setState({ Value: data.value })
        // if (this.props.onChange && data.value==="") {
        //     let comValue = {
        //         ..._props,
        //         value: ""
        //     }
        //     this.props.onChange(event, comValue);
        // }
    }
    /**
     * 日期输入框失去焦点事件
     *
     * @memberof HyDatePickerCom
    * */
    _onBlur = (event, data) => {
        if(this.state.open){
            return;
        }
        //setTimeout(() => {
            if(!this.isBlur){
                this.isBlur = true;
            }
            let tempValue = this.state.Value;
            if (tempValue === '' && this.props.isAllowEmpty) {
                if (this.state.open) {
                    this._onClose();
                    return
                }
                this.setState({
                    Value: tempValue,
                    //open: true
                }, () => {
                    if (this.props.onBlur) {
                        const _props = this.packageReProps();
                        let comValue = null;
                        comValue = {
                            ..._props,
                            value: ""
                        }
                        this.props.onBlur(event, comValue)
                    }
                    if (this.props.onChange) {
                        const _props = this.packageReProps();
                        let comValue = null;
                        comValue = {
                            ..._props,
                            value: ""
                        }
                        this.props.onChange(event, comValue);
                    }
                    if (this.context.setCloseOnDocumentClick) {
                        this.context.setCloseOnDocumentClick(true);
                    }
                });
            } else {
                if(moment(TransBaseDate(tempValue)).valueOf()===this.props.value && !this.props.error){

                    this._onClose();
                    return;
                }
                this._matchDate(tempValue, true, event);
            }

        //}, 500)

    }
    /**
         * 日期输入框获取焦点事件
         *
         * @memberof HyDatePickerCom
        * */
    _onFocus = (event, data) => {
        if (this.state.Value === '' && this.props.isAllowEmpty) {
            this.coords = event.currentTarget.getBoundingClientRect();
            this.setState({ open: true });
            if (this.context.setCloseOnDocumentClick) {
                this.context.setCloseOnDocumentClick(false);
            }
            if (this.props.showmessage) {
                this.props.showmessage(false, '');
            }
        }
        else{
           this._onOpenHandle(event,data);
        }
        if (this.props.onFocus) {
            this.props.onFocus(event, data)
        }
    }

    /**
     * 匹配日期
     *
     * @memberof HyDatePickerCom
     */
    _matchDate = (matchvalue, flg, event = null, errorFlg = false) => {
        let value = _.trim(matchvalue);
        if (!value) {
            if (!this.props.isExactMatch && !this.props.isAllowEmpty) {
                value = this._getDefaultValue(this.props);
                this.setState({
                    Value: value
                }, () => {
                    if (this.props.showmessage) {
                        this.props.showmessage(false, '');
                    }
                    if (flg) {
                        if (this.props.onBlur) {
                            const _props = this.packageReProps();
                            let comValue = null;
                            comValue = {
                                ..._props,
                                value: comValue
                            }
                            this.props.onBlur(event, comValue)
                        }
                        if (this.props.onChange && (this.props.value != moment(value).valueOf() || !this.state.error || this.props.error)){
                            const _props = this.packageReProps();
                            let comValue = null;
                            comValue = {
                                ..._props,
                                value: moment(TransBaseDate(_.trim(value))).valueOf()
                            }
                            this.props.onChange(event, comValue);
                        }
                    }
                });
            } else if (this.props.isAllowEmpty && value === '') {
                this.setState({ Value: '', open: !flg?true:false });
                if (this.props.onBlur) {
                    let _props = this.packageReProps();
                    let comValue = null;
                    comValue = {
                        ..._props,
                        value: ''
                    }
                    this.props.onBlur(event, comValue)
                }
                if (this.props.onChange) {
                    let _props = this.packageReProps();
                    let comValue = null;
                    comValue = {
                        ..._props,
                        value: ''
                    }
                    this.props.onChange(event, comValue);
                }
                return;
            }
        }
        let result, resultFormat;
        let temp = moment(value, this.format, true);
        if (temp.isValid()) {  //日期格式如果正确，触发Change事件，传递正确的值
            result = temp;
        }
        else {
            let formatValue = this._replaceAll(value, ["-", "/"], ["", ""]);
            if (!isNaN(formatValue)) {
                if (this.props.isExactMatch) { //如果是精确匹配
                    if (formatValue.length === 6 || formatValue.length === 8) {
                        result = this._checkDateInExactMode(formatValue, window.__GETCUSTOMDATEFORMAT__);
                        if (!result) {
                            resultFormat = this._getDefaultValue(this.props);
                        }
                    }
                    else {
                        resultFormat = this._getDefaultValue(this.props);
                    }
                }
                else {
                    if (formatValue.length !== 5 && formatValue.length !== 7 && formatValue.length <= 8) {
                        result = this._checkDateInExactMode(formatValue, window.__GETCUSTOMDATEFORMAT__, this.bussinessDt);

                        if (!result) {
                            if (this.props.showmessage) {
                                this.refs.inputcalendar.focus();
                                let message = languageConfig.showmessage + ':' + moment().format(this.format);
                                this.props.showmessage(true, message);
                                return;
                            } else {
                                this.errmessage = `${languageConfig.showmessage}:${moment().format(this.format)}`;
                                this.setState({ error: true, errmessage: true, position: 'top center', open: true })
                                return;
                            }
                        }
                    }
                    else {
                        if (this.props.showmessage) {
                            this.refs.inputcalendar.focus();
                            let message = languageConfig.showmessage + ':' + moment().format(this.format);
                            this.props.showmessage(true, message);
                            return;
                        } else {
                            this.errmessage = `${languageConfig.showmessage}:${moment().format(this.format)}`;
                            this.setState({ error: true, errmessage: true, position: 'top center', open: true })
                            return;

                        }
                    }
                }
            }
            else {
                if (this.props.isExactMatch) {
                    resultFormat = this._getDefaultValue(this.props);
                }
                else {
                    if (this.props.showmessage) {
                        this.refs.inputcalendar.focus();
                        let message = languageConfig.showmessage + ':' + moment().format(this.format);
                        this.props.showmessage(true, message);

                        return;
                    } else {
                        this.errmessage = `${languageConfig.showmessage}:${moment().format(this.format)}`;
                        this.setState({ error: true, errmessage: true, position: 'top center', open: true })
                        return;
                    }
                }
            }
        }
        if (this.props.showmessage) {
            this.props.showmessage(false, '');
        }
        if (!resultFormat) resultFormat = result.format(this.format);
        if (!result) result = moment(resultFormat, this.format);
        //验证最大最小日期
        if(!this.props.poequalMaxMinValue){
            let isValidDate = this._validateMaxMinValue(result, resultFormat, false, true);
            if (!isValidDate) return;
        }else{
            //超出最大值等于最大值  小于最小值等于最小值
            if (moment(result).valueOf() < this.props.minDate) {
                resultFormat = moment(this.props.minDate).format('L')
            }
            if(this.props.maxDate < moment(result).valueOf()){
                resultFormat = moment(this.props.maxDate).format('L')
            }

        }
        value = resultFormat;
        if (flg) {
            // if(moment(value).valueOf()===this.props.value){
            //     return
            // }
            if (this.props.onBlur) {
                const _props = this.packageReProps();
                let comValue = null;
                comValue = {
                    ..._props,
                    value: moment(TransBaseDate(_.trim(value))).valueOf()
                }
                this.props.onBlur(event, comValue)
            }
            if (this.props.onChange && (this.props.value === moment(value).valueOf() || !this.state.error || this.props.error)) {
                const _props = this.packageReProps();
                let comValue = null;
                comValue = {
                    ..._props,
                    value: moment(TransBaseDate(_.trim(value))).valueOf()
                }
                this.props.onChange(event, comValue);
            }
           if(this.props.isExactMatch && this.props.isMust){
            this.setState({
                Value: value
            });
           }
           return ;
        }
        this.setState({
            Value: value,
            open: errorFlg?false:true
        });
    }



    /**
     * 字符串全局替换
     * @param {[type]} str         [进行替换的字符串]
     * @param {[type]} needRepStr str或array 如果是array会替换数组中的每一个字符串  [需要替换的字符串]
     * @param {[type]} replacedStr str或array 如果是array顺序和needRepStr保持相同 [替换后的字符串]
     * @param {[type]} isIgnoreCase bool 是否忽略大小写
     */
    _replaceAll = (str, needRepStr, replacedStr, isIgnoreCase) => {
        //let regStr = isIgnoreCase ? "gmi" : "gm";
        if (_.isArray(needRepStr) && _.isArray(replacedStr)) {
            _.forEach(needRepStr, (item, index) => {
                str = str.replace(new RegExp(item, 'gm'), replacedStr[index]);
            });
            return str;
        }
        return str.replace(new RegExp(needRepStr, 'gm'), replacedStr);
    }

    /**
     * 验证日期在不需要校验业务日期时,返回null代表验证失败，否则返回验证后的日期
     * @param {*} formatValue
     */
    _checkDateInExactMode = (formatValue, customFormat) => {
        let format = customFormat || moment.localeData()._longDateFormat.L;
        let upperFormat = this._replaceAll(format.toUpperCase(), ["-", "/"], ["", ""], true);
        let yearIndex = upperFormat.indexOf("Y");
        let monthIndex = upperFormat.indexOf("M");
        let dayIndex = upperFormat.indexOf("D");
        let year = formatValue.substr(yearIndex, 4);
        let month = formatValue.substr(monthIndex, 2);
        let day = formatValue.substr(dayIndex, 2);
        if (formatValue.length === 6) {
            //年的位置只可能在最前面或最后面
            let inputYear = yearIndex === 0 ? formatValue.substr(0, 2) : formatValue.substr(4, 2);
            //如果当前时间的年减去 19和输入的年 大于150，取20为前缀，否则取19
            let fillNumber = moment().diff(moment(`19${inputYear}`), "years") > 150 ? "20" : "19";
            year = `${fillNumber}${inputYear}`;
            if (yearIndex === 0) { //年只输入两位并且年在前面的时候，取月和日的索引减2
                month = formatValue.substr(monthIndex - 2, 2);
                day = formatValue.substr(dayIndex - 2, 2);
            }
        }
        if ((+month) > 12) return;
        if ((+day) > 31) return;
        if (day === "00") day = "01";
        if (month === "00") month = "01";
        let maxDay = moment(`${year}-${month}`, "YYYY-MM").daysInMonth();
        if (day > maxDay) return;
        let current = moment(`${year}-${month}-${day}`, "YYYY-MM-DD");
        return current;
    }

    /**
   * 验证最大最小日期,返回是否验证通过 bool
   * result: 需要验证的值 moment对象
   * resultFormat： 需要验证的值的字符串格式
   * isValidateDisable： 是否是验证是否禁用日期
   * isShowToolTip： 是否显示提示信息
   *
   * @memberof HyDatePickerCom
  * */
    _validateMaxMinValue = (result, resultFormat, isValidateDisable, isShowToolTip) => {
        if (this.props.minDate) {
            let minDate;
            let minDateFormat;
            minDate = this.props.minDate;
            if (moment(result).valueOf() < minDate) {
                if (isShowToolTip) {
                    if (this.props.showmessage) {
                        this.refs.inputcalendar.focus();
                        let message = languageConfig.minErrmessage;
                        this.props.showmessage(true, message);
                        return;
                    } else {
                        this.errmessage = languageConfig.minErrmessage;
                        this.setState({ error: true, errmessage: true, position: 'top center', open: true })
                        return;
                    }
                }
                return false;
            }
        }
        if (this.props.maxDate) {
            let maxDate;
            maxDate = this.props.maxDate;

            if (maxDate < moment(result).valueOf()) {
                if (isShowToolTip) {
                    if (this.props.showmessage) {
                        this.refs.inputcalendar.focus();
                        let message = languageConfig.maxErrmessage;
                        this.props.showmessage(true, message);
                        return;
                    } else {
                        this.errmessage = languageConfig.maxErrmessage;
                        this.setState({ error: true, errmessage: true, position: 'top center', open: true })
                        return;
                    }
                }
                return false;
            }
        }
        return true;
    }
}


