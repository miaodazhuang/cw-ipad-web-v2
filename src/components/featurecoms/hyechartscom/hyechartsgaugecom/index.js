import React, {Component} from 'react';
import PropTypes from 'prop-types';
// 引入 ECharts 主模块
import echarts from 'echarts/lib/echarts';
//引入仪表盘
import 'echarts/lib/chart/gauge';
// 引入提示框和标题组件
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

class HyEchartsGaugeCom extends Component {

  static propTypes = {
    //定义配置项
    options: PropTypes.shape({
      //图表标题
      title: PropTypes.shape({
        //标题文本
        text: PropTypes.string,
        //标题是否显示
        show: PropTypes.bool,
        //标题文本样式设置,参见文档吧，太多了
        textStyle: PropTypes.object
      }),


    }),
    //定义容器大小
    containerStyle:  PropTypes.shape({
      width: PropTypes.number,
      height: PropTypes.number
    })
  }
  static defaultProps = {
    containerStyle:{
      width: 500,
      height: 300
    },

  }

  componentDidMount() {
    this.init();
  }
//   componentWillReceiveProps(nextProps){
//       debugger
//     if(!_.isEqual(this.props.options, nextProps.options)){
//     if(!_.isEmpty(nextProps.options)&&nextProps.options)
//       this.myChart.setOption(nextProps.options)
//     }
//   }
    componentDidUpdate() {
        this.init()
    }

  render() {
    return (
      <div ref={node => this.node = node}  style={{width:"100%", height:"100%"}}></div>
    );
  }
  //初始化图标
  init = () =>{
    const { options = {}} = this.props //外部传入的data数据
    // 基于准备好的dom，初始化echarts实例
    this.myChart = echarts.init(this.node);
    // 绘制图表
    this.myChart.setOption(options);
    //绑定点击事件
    this.myChart.on('click', this.onClick);
  }
  /**
   *  params说明
   *  // 当前点击的图形元素所属的组件名称，
      // 其值如 'series'、'markLine'、'markPoint'、'timeLine' 等。
      componentType: string,
      // 系列类型。值可能为：'line'、'bar'、'pie' 等。当 componentType 为 'series' 时有意义。
      seriesType: string,
      // 系列在传入的 option.series 中的 index。当 componentType 为 'series' 时有意义。
      seriesIndex: number,
      // 系列名称。当 componentType 为 'series' 时有意义。
      seriesName: string,
      // 数据名，类目名
      name: string,
      // 数据在传入的 data 数组中的 index
      dataIndex: number,
      // 传入的原始数据项
      data: Object,
      // sankey、graph 等图表同时含有 nodeData 和 edgeData 两种 data，
      // dataType 的值会是 'node' 或者 'edge'，表示当前点击在 node 还是 edge 上。
      // 其他大部分图表中只有一种 data，dataType 无意义。
      dataType: string,
      // 传入的数据值
      value: number|Array
      // 数据图形的颜色。当 componentType 为 'series' 时有意义。
      color: string
   *
   * @memberof HyEchartsBarCom
   */
  onClick = (params) =>{
    this.props.onClick && this.props.onClick(this.myChart, params)
  }
}

export default HyEchartsGaugeCom;