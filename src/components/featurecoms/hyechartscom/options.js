export const gaugeOption =  {
        title:[{
            text: `${'50'}% 当前`,
            top: '45%',
            left: '70%',
            textStyle:{
                color: '#4E85E0'
            }},
            {text: `${'80'}% 预计`,
            top: '55%',
            left: '70%',
            textStyle:{
                color: '#FD9E69'
            }
        },],
        tooltip : {
            formatter: "{b} : {c}%"
        },
        toolbox: {
            feature: {
                restore: {},
                saveAsImage: {}
            }
        },
        series: [
            {
                title: {
                    text: '出租率',
                    offsetCenter:[ '-150%', 0]
                },
                radius:'90%',//可以是相对于容器高宽中较小的一项的一半的百分比，也可以是绝对的数值
                splitNumber: 10,
                axisLine: {
                    lineStyle:{
                        color: [[0.5, '#4E85E0'], [0.8, '#FD9E69'], [1, '#F1F3F3']],
                        width: 10
                    }
                },//轴线配置
                splitLine:{
                    length: 20,
                    lineStyle: {
                        color: 'black',
                        width: 1

                    }
                },//分隔线配置
                axisTick:{
                    show: false
                },//刻度线配置
                axisLabel:{
                    show: true,
                    color: 'black'
                },//刻度标签设置
                pointer:{
                    length: '60%',
                },//指针设置
                name: '出租率',
                type: 'gauge',
                detail: {
                    formatter:'{value}%',
                    fontSize: 14
                },
                markPoint:{
                    symbol: 'roundRect',
                    label:'50% 当前'
                },
                data: [{value: 50, name: '出租率'}],
                itemStyle: {
                    color: 'black'
                }
            }
        ]
    }

    export const lineBarOption = {
        colors :['#f66a7f', '#396FF5', '#30DAF0', '#32AE56','#FEA408'],
        categoryData : ['23','24','25','26','27','28','29','30','31','01','02','03','04','05','06','07','08','09','10'],
        options: {
            backgroundColor: '#fff',
            color:  colors,
        
            tooltip: {
                trigger: 'axis',
                axisPointer: {type: 'cross'},
                formatter: (params) => {
                    let res = '';
                    for (let i = 0, length = params.length; i < length; i++) {
                        if(params[i].seriesName === '出租率'){
                            res += params[i].seriesName + '：' + params[i].value + '%' + '<br/>'
                        }else{
                            res += params[i].seriesName + '：' + params[i].value + '<br/>'
                        }
                    }
                    return res
                }
            },
            axisPointer:{
                link: {xAxisIndex: 'all'},
                label: {
                    backgroundColor: '#777'
                }
            },
            grid: [
                {
                    left: '5%',
                    right: '5%',
                    height: '45%'
                },  {
                    left: '5%',
                    right: '5%',
                    bottom: '5%',
                    height: '30%'
                }
            ],
            toolbox: {
                feature: {
                    dataView: {show: true, readOnly: false},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            legend: {
                left: 50,
                data:[{
                    name: '出租率',
                    // 强制设置图形为圆。
                    icon: 'circle',
                },{
                    name: '房费',
                    icon: 'circle',
                },{
                    name: '餐费',
                    icon: 'circle',
                },{                
                    name: '其它',
                    icon: 'circle',
                },{                
                    name: '房数',
                    icon: 'circle',
                    x:{
                        left: 0,
                        bottom: 0
                    }
                }]
            },
            xAxis: [
                {
                    type: 'category',
                    axisTick: {
                        show: false,
                        alignWithLabel: true
                    },
                    boundaryGap : false,
                    axisPointer: {
                        z: 100
                    },
                    axisLine:{
                        show: true,
                        width: 3,
                        onZero: false,
                        lineStyle:{
                            color: '#DADFEA'
                        }
                    },
                    // scale: true,
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#333',
                            fontSize: 12,
                        },
                    },
                    min: 'dataMin',
                    max: 'dataMax',
                    data: categoryData
                },
                {
                    type: 'category',
                    gridIndex: 1,
                    data: categoryData,
                    // scale: true,
                    boundaryGap : false,
                    axisLine:{
                        show: true,
                        width: 3,
                        onZero: false,
                        lineStyle:{
                            color: '#DADFEA'
                        }
                    },
                    axisTick: {show: false},
                    splitLine: {show: false},
                    axisLabel: {show: false},
                    // splitNumber: 20,
                    min: 'dataMin',
                    max: 'dataMax',
                    // axisPointer: {
                    //     z: 100,
                    //     label: {
                    //         formatter: function (params) {
                    //             var seriesValue = (params.seriesData[0] || {}).value;
                    //             return params.value
                    //             + (seriesValue != null
                    //                 ? '\n' + echarts.format.addCommas(seriesValue)
                    //                 : ''
                    //             );
                    //         }
                    //     }
                    // }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    // name: '出租率',
                    min: 0,
                    max: 100,
                    position: 'left',
                    axisLine: {
                        //设置轴线不显示
                        // lineStyle: {
                        //     color: colors[0]
                        // }
                        show: false
                    },
                    axisTick:{
                        //设置刻度不显示
                        show: false
                    },
                    axisLabel: {
                        //格式化轴线上的文字,支持模板和回调
                        formatter: '{value} %'
                    }
                },
                {
                    type: 'value',
                    // name: '收入',
                    min: 0,
                    axisTick:{
                        show: false
                    },
                    position: 'right',
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        formatter: function (value, index) {
                            return `${value/1000} K`
                        }
                    }
                },
                {   
                    type: 'value',
                    min: 'dataMin',
                    max: 'dataMax',
                    position: 'right',
                    // scale: true,
                    gridIndex: 1,
                    // splitNumber: 2,
                    axisLabel: {show: true},
                    axisLine: {show: false},
                    axisTick: {show: false},
                    splitLine: {show: true}
                }
            ],
            series: [
                {
                    name:'出租率',
                    type:'line',
                    symbol: 'rect',
                    symbolSize: 6,
                    data:[20, 30, 40, 50, 60, 72, 83, 94, 100, 70, 30, 20, 20, 30, 40, 50, 60, 72, 83]
                },
                {
                    name:'房费',
                    type:'line',
                    smooth: true,
                    showSymbol: false,
                    areaStyle: {
                        opacity: 0.2
                    },
                    yAxisIndex: 1,
                    data:[ 6300, 7300, 8300, 8300, 4000, 63000, 83000, 20000, 3000, 2000, 4000, 5000, 6300, 70300, 83000, 83000, 40000, 5200, 6300 ]
                },
                {
                    name:'餐费',
                    type:'line',
                    smooth: true,
                    showSymbol: false,
                    areaStyle: {
                        opacity: 0.2
                    },
                    yAxisIndex: 1,
                    data:[4000, 5000, 6300, 7300, 8300, 8300, 4000, 63000, 8300, 2000, 3000, 2000, 4000, 50000, 6300, 7300, 8300, 8300, 4000 ]
                },

                {
                    name:'其它',
                    type:'line',
                    smooth: true,
                    showSymbol: false,
                    areaStyle: {
                        opacity: 0.2
                    },
                    yAxisIndex: 1,
                    data:[ 6300, 8300, 2000, 3000, 2000, 4000, 5000, 6300, 7300, 83000, 8300, 4000, 5200, 6300, 6300, 73000, 83000, 8300, 4000]
                },
                {
                    name:'房数',
                    type:'bar',
                    xAxisIndex: 1,
                    barWidth: 10,
                    yAxisIndex: 2,
                    data:[ 100, 200, 255, 120, 300, 140, 160, 182, 193, 255, 230, 260, 310, 170, 55, 66, 99, 222, 130]
                }
            ]
        }
    }
    