import HyFeatureButtonGroupCom from '../../hyfeaturebuttongroupcom';
import React from 'react';
import PropTypes from 'prop-types';

const HyNormalColumnCom = (props) => {
  return <HyFeatureButtonGroupCom down={true} show={props.show} basic={true} className='table-featurecolumn' data={props.data} openway='click'
                                  position='bottom right' onClick={(event, data) => {
                                    data.datasource = props.datasource;
                                    data.rowindex = props.rowindex;
                                    props.onClick && props.onClick(event, data);
                                  }} />;
}

HyNormalColumnCom.propTypes = {
  /** table 普通功能列按钮， 直接根据data生成按钮 */
  /**功能列显示的按钮配置 */
  data: PropTypes.object
}

export default HyNormalColumnCom;