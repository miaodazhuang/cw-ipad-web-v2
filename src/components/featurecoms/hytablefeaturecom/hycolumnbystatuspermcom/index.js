import HyFeatureButtonGroupCom from '../../hyfeaturebuttongroupcom';
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

const HyColumnByStatusPermCom = (props) => {
  const validFeatureData = _.map(props.validFeatureData, (item, key)=>{
    return{
      key: {
        text: item.text,
        permsCode: props.permsDataObj[props.permsFlg][key]
      }
    }
  });
  const invalidFeatureData = _.map(props.invalidFeatureData, (item, key)=>{
    return{
      key: {
        text: item.text,
        permsCode: props.permsDataObj[props.permsFlg][key]
      }
    }
  });

  const featureData = props.datasource[props.name] === props.value ? validFeatureData :invalidFeatureData;
  return <HyFeatureButtonGroupCom down={true} show={props.show} basic={true} className='table-featurecolumn' data={featureData} openway='click'
                                  position='bottom right' onClick={(event, data) => {
                                    data.datasource = props.datasource;
                                    data.rowindex = props.rowindex;
                                    props.onClick && props.onClick(event, data);
                                  }} />;
}

HyColumnByStatusPermCom.propTypes = {
  /** table 根据datasource中的status_flg生成功能列按钮
   */
  /**status_flg为1时， 功能列显示的按钮配置 */
  validFeatureData: PropTypes.object,
   /**status_flg为0时， 功能列显示的按钮配置 */
  invalidFeatureData: PropTypes.object,

  /**实现对比有效性的字段 */
  name: PropTypes.string,
  /**对比的值 */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
   /**决定权限号的字段 */
  permsFlg: PropTypes.string,
  /**权限配置对象,以permsFlg可能取值为key,值为操作对应的key和权限号为value组成的对象
  如：permsDataObj={{1:{edit:'33200026',cancel:'33200026',recover:'33200028'},2:{edit:'33200027',cancel:'33200027',recover:'33200028'}}}*/
  permsDataObj: PropTypes.object,
}

export default HyColumnByStatusPermCom;