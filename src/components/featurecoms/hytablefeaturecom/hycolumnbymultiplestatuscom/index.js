import HyFeatureButtonGroupCom from '../../hyfeaturebuttongroupcom';
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

const HyColumnByMultipleStatusCom = (props) => {
  let featureData = {};
  if(props.name === 'permanentValidity'){
    featureData = props.validFeatureData;
  }else if(_.isArray(props.value)){
    featureData = _.find(props.value, item => item === props.datasource[props.name]) ? props.validFeatureData : props.invalidFeatureData;
  }else{
    featureData = props.datasource[props.name] === props.value ? props.validFeatureData : props.invalidFeatureData;
  }
  return <HyFeatureButtonGroupCom down={true} show={props.show} basic={true} className='table-featurecolumn' data={featureData} openway='click'
                                  position='bottom right' onClick={(event, data) => {
                                    data.datasource = props.datasource;
                                    data.rowindex = props.rowindex;
                                    props.onClick && props.onClick(event, data);
                                  }} />;
}

HyColumnByMultipleStatusCom.propTypes = {
  /** table 根据datasource中的status_flg生成功能列按钮
   */
  /**status_flg为1时， 功能列显示的按钮配置 */
  validFeatureData: PropTypes.object,
   /**status_flg为0时， 功能列显示的按钮配置 */
  invalidFeatureData: PropTypes.object,

  /**实现对比有效性的字段 */
  name: PropTypes.string,
  /**对比的值 */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array
  ])
}

export default HyColumnByMultipleStatusCom;