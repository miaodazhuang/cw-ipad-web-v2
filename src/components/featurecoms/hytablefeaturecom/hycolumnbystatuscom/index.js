import HyFeatureButtonGroupCom from '../../hyfeaturebuttongroupcom';
import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

const HyColumnByStatusCom = (props) => {
  let featureData;
  if (props.isAvailable) {
    if (props.datasource["status_flg_base"] === '0' || props.datasource["use_flg"] === 'IMP' || (props.datasource["use_flg_base"] === 'USE' && _.toString(props.datasource["use_flg_base"]) !== '')) {
      featureData = {}
    }else{
      featureData = props.datasource[props.name] === props.value ? props.validFeatureData : props.invalidFeatureData;
    }
  }else if(props.isMainBill){
    //主单功能列
    if(props.datasource[props.name] === 'CXL'){
      featureData = props.validFeatureData;
    }else if(props.datasource[props.name] === 'RSV'){
      featureData = props.validFeatureDataRsv;
    }else{
      featureData = props.invalidFeatureData;
    }
  }
  else{
    featureData = props.datasource[props.name] === props.value ? props.validFeatureData : props.invalidFeatureData;
  }
  //const featureData = props.datasource[props.name] === props.value ? props.validFeatureData : props.invalidFeatureData;
  return <HyFeatureButtonGroupCom down={true} show={props.show} basic={true} className='table-featurecolumn' data={featureData} openway='click'
    position='bottom right' onClick={(event, data) => {
      data.datasource = props.datasource;
      data.rowindex = props.rowindex;
      props.onClick && props.onClick(event, data);
    }} />;
}

HyColumnByStatusCom.propTypes = {
  /** table 根据datasource中的status_flg生成功能列按钮
   */
  /**status_flg为1时， 功能列显示的按钮配置 */
  validFeatureData: PropTypes.object,
  /**status_flg为0时， 功能列显示的按钮配置 */
  invalidFeatureData: PropTypes.object,

  /**实现对比有效性的字段 */
  name: PropTypes.string,
  /**对比的值 */
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
}

export default HyColumnByStatusCom;