import React from 'react';
import PropTypes from 'prop-types';

const HyTableEditCom = (props) => {
  var onChangeFunc = function(event, data){
    data.rowindex = props.rowindex;
    props.onChange && props.onChange(event, data);
  };
  return React.cloneElement(props.children, {
    value: props.text,
    onBlur: onChangeFunc,
    onChange: onChangeFunc,
  });
}

HyTableEditCom.propTypes = {
  /** table行内编辑组件，根据children生成对应组件 自动注入value和改变事件， children里面只需写基础配置即可*/
  children: PropTypes.object
}

export default HyTableEditCom;