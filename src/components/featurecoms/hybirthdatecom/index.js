import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import _ from 'lodash';
import moment from 'moment';

import { BaseComponent } from 'Hyutilities/component_helper';
import { Input, Segment, Popup, List, Label } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hytimepickercom';
import ja_jp from 'ja_jp/hytimepickercom';
import zh_cn from 'zh_cn/hytimepickercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

let monthOptions = [
     {
        text: '01',
        value: '01'
    }, {
        text: '02',
        value: '02'
    }, {
        text: '03',
        value: '03'
    }, {
        text: '04',
        value: '04'
    }, {
        text: '05',
        value: '05'
    }, {
        text: '06',
        value: '06'
    }, {
        text: '07',
        value: '07'
    }, {
        text: '08',
        value: '08'
    }, {
        text: '09',
        value: '09'
    }, {
        text: '10',
        value: '10'
    }, {
        text: '11',
        value: '11'
    }, {
        text: '12',
        value: '12'
    }
];

let dateOptions = [
    {
        text: '01',
        value: '01'
    }, {
        text: '02',
        value: '02'
    }, {
        text: '03',
        value: '03'
    }, {
        text: '04',
        value: '04'
    }, {
        text: '05',
        value: '05'
    }, {
        text: '06',
        value: '06'
    }, {
        text: '07',
        value: '07'
    }, {
        text: '08',
        value: '08'
    }, {
        text: '09',
        value: '09'
    }, {
        text: '10',
        value: '10'
    }, {
        text: '11',
        value: '11'
    }, {
        text: '12',
        value: '12'
    }, {
        text: '13',
        value: '13'
    }, {
        text: '14',
        value: '14'
    }, {
        text: '15',
        value: '15'
    }, {
        text: '16',
        value: '16'
    }, {
        text: '17',
        value: '17'
    }, {
        text: '18',
        value: '18'
    }, {
        text: '19',
        value: '19'
    }, {
        text: '20',
        value: '20'
    }, {
        text: '21',
        value: '21'
    }, {
        text: '22',
        value: '22'
    }, {
        text: '23',
        value: '23'
    }, {
        text: '24',
        value: '24'
    }, {
        text: '25',
        value: '25'
    }, {
        text: '26',
        value: '26'
    }, {
        text: '27',
        value: '27'
    }, {
        text: '28',
        value: '28'
    }, {
        text: '29',
        value: '29'
    }, {
        text: '30',
        value: '30'
    }, {
        text: '31',
        value: '31'
    }
];

@BaseComponent
export default class HyBirthDateCom extends Component {
    static propTypes = {
        value: PropTypes
            .PropTypes
            .oneOfType([PropTypes.string, PropTypes.number]), // 默认显示时间,支持字符串和数字两种类型
        text: PropTypes.string, // birthdatepicker前的label字段
        timeStamp: PropTypes.bool, //是否返回时间戳
        disabled: PropTypes.bool //是否无效状态
    }

    static contextTypes = {
        setCloseOnDocumentClick: PropTypes.func
    };

    static defaultProps = {
        // value: '01:01',
        timeStamp: false //默认返回字符串
    }

    isBlur = true;

    constructor(props) {
        super(props);
        this.state = {
            month: '',
            day: '',
            val_m: '',
            val_day: '',
            dateOptions:_.cloneDeep(dateOptions),
            isOpen: false
        }
        const value = props.value || '';
        this.initState(value, true);
    };

    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(nextProps.value, this.props.value)) {
            let value = nextProps.value;
            this.initState(value)
        }
    }

    componentDidMount() {
        //先通过触发triggerEle的click事件让弹出层找到合适的位置，再关闭他
            this.refs.birthdatepicker.inputRef.click();
            this.setState({
                isOpen: false
            },()=>{
                if (this.context.setCloseOnDocumentClick) {
                    this.context.setCloseOnDocumentClick(!this.state.isOpen)
                }
            })

    }

    render() {
        const {
            children,
            showmessage,
            text,
            isMust,
            ...childrenProps
        } = this.props;
        const { month, day } = this.state;

        const eventProps = {
            onChange: this.onChange
        }

        let inputCom = null;

        if (_.has(this.props, "text")) {
            if (isMust) {
                inputCom = <Input
                    {...childrenProps}
                    ref="birthdatepicker"
                    {...eventProps}
                    onBlur={this._onBlur}
                    onFocus={this._onFocus}
                    className={ClassNames("birthdate_selector ", this.props.className)}
                    title={this.props.title}
                    disabled={this.props.disabled}
                    value={month + '-' + day}>
                    <div className='form-must'>
                        <span>*</span>
                        <Label>{text}</Label>
                    </div>
                    <input />
                </Input>
            } else {
                inputCom =
                    <Input
                        {...childrenProps}
                        ref="birthdatepicker"
                        {...eventProps}
                        onBlur={this._onBlur}
                        onFocus={this._onFocus}
                        className={ClassNames("birthdate_selector", this.props.className)}
                        title={this.props.title}
                        disabled={this.props.disabled}
                        value={month + '-' + day}>
                        <Label>{text}</Label>
                        <input />
                    </Input>

            }
        } else {
            inputCom =
                <Input
                {...childrenProps}
                    ref="birthdatepicker"
                    {...eventProps}
                    onBlur={this._onBlur}
                    onFocus={this._onFocus}
                    className={ClassNames("birthdate_selector", this.props.className)}
                    title={this.props.title}
                    disabled={this.props.disabled}
                    value={month + '-' + day} />

        }

        return (
            <Popup
                trigger={inputCom}
                on='click'
                basic
                position='bottom left'
                className='selector_menu'
                open={this.state.isOpen}
                onOpen={(e,data)=>this.handleIsOpen(e,data,true)}
                onClose={this.handleCancel}>
                <div onClick={this._onClickTime}>
                    <Segment.Group horizontal className='popup_segments'>
                        <Segment>
                            <List>
                                {monthOptions.map(optionItem => <List.Item
                                    key={optionItem.value}
                                    {...optionItem}
                                    onClick={this.monthChange}
                                    active={`${this.state.month}` === `${optionItem.value}`
                                        ? true
                                        : false}>
                                    {optionItem.text}
                                </List.Item>)
                                }
                            </List>
                        </Segment>
                        <Segment>
                            <List>
                                {this.state.dateOptions.map(optionItem => <List.Item
                                    key={optionItem.value}
                                    {...optionItem}
                                    onClick={this.dayChange}
                                    active={`${this.state.day}` === `${optionItem.value}`
                                        ? true
                                        : false}>
                                    {optionItem.text}
                                </List.Item>)
                                }
                            </List>
                        </Segment>
                    </Segment.Group>
                    <div className='popup_button'>
                        <span className='button sure' onClick={this.handleSure}>{languageConfig.ok}</span>
                        <span className='button cancel' onClick={this._onCancel}>{languageConfig.cancel}</span>
                    </div>
                </div>
            </Popup>
        )
    }

    _onClickTime = (event, data) => {
        this.isBlur = false;
        event.preventDefault();
        event.stopPropagation();
    }

    // _onFocus = (event, data) => {
    //     this.handleIsOpen(event, data, true)
    // }

    _onBlur = (event, data) => {
        // setTimeout(() => {
        //     if (!this.isBlur) {
        //         this.isBlur = true;
        //         return;
        //     }
        //     this.setState({
        //         isOpen: false
        //     });
        // }, 300)

    }

    handleIsOpen = (e, obj, flg) => {
        //if (flg) {
            this.setState({
                isOpen: !this.state.isOpen
            },()=>{
                if (this.context.setCloseOnDocumentClick) {
                    this.context.setCloseOnDocumentClick(!this.state.isOpen)
                }
            });

        //}
    }

    monthChange = ( e ) => {
    	let item = e.target;
    	let itemText = item.textContent;
    	if ( itemText ) {
    		this.setState( { month: itemText }, ( ) => {
    			if ( itemText === '02' ) {
    				if ( this.state.day && ( this.state.day === '30' || this.state.day === '31' ) ) {
    					this.setState( {
    						dateOptions: dateOptions.slice( 0, 29 ),
    						day: '01',
    						val_day: '01',
    					} )
    				} else {
    					this.setState( { dateOptions: dateOptions.slice( 0, 29 ) } )
    				}
    			} else if ( itemText === '04' || itemText === '06' || itemText === '09' || itemText === '11' ) {
    				if ( this.state.day === '31' ) {
    					this.setState( {
    						dateOptions: dateOptions.slice( 0, 30 ),
    						day: '01',
    						val_day: '01',
    					} )
    				} else {
    					this.setState( {
    						dateOptions: dateOptions.slice( 0, 30 )
    					} )
    				}
    			} else {
    				this.setState( {
    					dateOptions: dateOptions
    				} )
    			}
    		} )
    	}
    }

    dayChange = (e) => {
        let item = e.target;
        let itemText = item.textContent;
        if (itemText) {
            this.setState({ day: itemText })
        }
    }

    handleCancel = (event, data) => {
        this.isBlur = true;
        this.setState({ month: this.state.val_m, day: this.state.val_day })
        if (this.state.isOpen) {
            this.handleIsOpen(event, data,true);
        }
    }

    _onCancel = (event, data) => {
        this.isBlur = true;
        this.setState({ month: this.state.val_m, day: this.state.val_day, isOpen: false })
        event.preventDefault();
        event.nativeEvent.stopImmediatePropagation();
        event.stopPropagation();
    }

    handleSure = (event, data) => {
        if(this.state.month == '' || this.state.day == '')return;
        this.isBlur = true;
        this.setState({ val_m: this.state.month, val_day: this.state.day })
        this.onChange();
        if (this.state.isOpen) {
            this.handleIsOpen(event, data, true);
        }
        event.preventDefault();
        event.nativeEvent.stopImmediatePropagation();
        event.stopPropagation();
    }

    onChange = (event, data) => {
        const { timeStamp } = this.props;
        let comValue = null;
        comValue = {
            ...this.props,
            value: timeStamp?  moment(this.state.month + '-' + this.state.day).valueOf(): this.state.month + '-' + this.state.day,
        }
        this.props.onChange && this
            .props
            .onChange(event, comValue);
    }

    initState = ( value, init ) => {
    	if ( !value ) {
            this.state.val_m = '';
            this.state.val_day = '';
            this.state.month = '';
            this.state.day = '';
    		return;
        }
        let default_month = '',default_day='';
    	if ( init ) {
            if (_.isNumber(value)) {
                let tempMonth = moment(value).get('month') + 1;
                let tempDay = moment(value).get('date');
                default_month = tempMonth >= 10? `${tempMonth}`: `0${tempMonth}`;
                default_day = tempDay >= 10? `${tempDay}`: `0${tempDay}`;
            }else{
                let timeVal = value.split( '-' );
                default_month = timeVal[ 0 ];
                default_day = timeVal[ 1 ];
            }
            this.state.val_m = default_month;
            this.state.val_day = default_day;
            this.state.month = default_month;
            this.state.day = default_day;
    	} else {
            if(_.isNumber(value)){
                let tempMonth = moment(value).get('month') + 1;
                let tempDay = moment(value).get('date');
                default_month = tempMonth >= 10? `${tempMonth}`: `0${tempMonth}`;
                default_day = tempDay >= 10? `${tempDay}`: `0${tempDay}`;
            }else{
                let timeVal = value.split( '-' );
                default_month = timeVal[ 0 ]
                default_day = timeVal[ 1 ];
            }
            this.setState( { val_m: default_month, val_day: default_day, month: default_month, day: default_day } )
    	}
    }
}
