import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { Accordion, Menu } from 'semantic-ui-react'

class HyAccordionMenuCom extends Component {

  static propTypes = {
    // 菜单数据
    menuData: PropTypes.arrayOf(PropTypes.shape({
      // 数据唯一标识
      id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
      ]).isRequired,
      // 菜单描述
      text: PropTypes.string.isRequired,
      // 对应的url
      url: PropTypes.string.isRequired,
      // 子菜单
      children: PropTypes.array,
      // 尾部组件
      endCom: PropTypes.element,
    })),
    // 激活的项目ID
    activeMenuId: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ]),
    //组件自定义样式
    componentStyle:PropTypes.object,
    // 项目点击事件
    onMenuClick: PropTypes.func,
    // 分组标题点击事件
    onGroupClick: PropTypes.func,
    // 尾部组件点击事件
    onEndComClick: PropTypes.func,
  }

  state = {}

  render() {
    const { menuData, activeMenuId, componentStyle = {} } = this.props;
    const parentActiveIndex = _.has(this.state, "parentActiveIndex") ? this.state.parentActiveIndex : this.getParentActionIndex();
    let className = null;
    return (
      <Menu vertical className="hyaccordionmenucom" style={componentStyle}>
        {
          _.map(menuData, (item, index) => {
            const { id, url, text, children, endCom } = item;
            const content = endCom ? (
              <div className='endCom'>
                <span data-text title={text}>{text}</span>
                <span data-edit>
                  {React.cloneElement(endCom, {
                    onClick: this.onEndComClick.bind(this, item)
                  })}
                </span>
              </div>
            ) : text

            if (_.size(children) > 0) {
              const childrenMenu = this.getChildrenMenu(children, activeMenuId);
              const parentActiveState = `${parentActiveIndex}` === `${id}`;
              className = ClassNames({ "parentMenuActive": parentActiveState });
              const titleClassName = ClassNames({ "childrenActive": childrenMenu.isChildrenActive, "endComItem": endCom });
              return (
                <Accordion as={Menu.Item} fitted styled key={index} className={className}>
                  <Accordion.Title
                    className={titleClassName}
                    active={parentActiveState}
                    content={content}
                    index={0}
                    onClick={this.handleClick.bind(this, id)}
                  />
                  <Accordion.Content active={parentActiveState} content={childrenMenu.childrenMenuData} />
                </Accordion>
              )
            } else {
              const activeState = `${id}` === `${activeMenuId}`;
              className = ClassNames("menu-item", { "menuActive": activeState, "endComItem": endCom });
              return <Menu.Item className={className} fitted key={index} content={content} onClick={this.onClick.bind(this, { id, url, text })}></Menu.Item>
            }
          })
        }
      </Menu>
    );
  }

  onEndComClick = (item, event) => {
    this.props.onEndComClick && this.props.onEndComClick(event, item)
  }

  getChildrenMenu = (children, activeMenuId) => {
    let isChildrenActive = false;
    const childrenMenuData = _.map(children, (item, index) => {
      const { id, url, text, endCom } = item;
      const activeState = `${id}` === `${activeMenuId}`;
      if (activeState) {
        isChildrenActive = activeState;
      }
      const content = endCom ? (
        <div className='endCom'>
          <span data-text title={text}>{text}</span>
          <span data-edit>
            {React.cloneElement(endCom, {
              onClick: this.onEndComClick.bind(this, item)
            })}
          </span>
        </div>
      ) : text
      const className = ClassNames("menu-item", { "menuActive": activeState, "endComItem": endCom });
      return <Menu.Item fitted key={`children${index}`} className={className} content={content} onClick={this.onClick.bind(this, { id, url, text })} />
    })

    return { isChildrenActive, childrenMenuData };
  }

  handleClick = (parentId, event, data) => {
    if (parentId === this.state.parentActiveIndex) {
      this.props.groupOnClick && this.props.groupOnClick(parentId, event, data);
      return;
    }
    this.setState({
      parentActiveIndex: parentId
    }, () => {
      this.props.groupOnClick && this.props.groupOnClick(parentId, event, data);
    })
  }

  onClick = (menuData, event, data) => {
    const params = {
      ...data,
      ...menuData
    }
    this.props.onClick && this.props.onClick(event, params);
  }

  getParentActionIndex = () => {
    const { menuData, activeMenuId } = this.props;
    let parentActiveIndex = null;
    _.forEach(menuData, (item, index) => {
      const { id, children } = item;
      const findIndex = _.findIndex(children, (childrenItem) => {
        return `${childrenItem.id}` === `${activeMenuId}`;
      })
      if (findIndex > -1) {
        parentActiveIndex = id;
        return false;
      }
    })
    return parentActiveIndex;
  }
}

HyAccordionMenuCom.propTypes = {

};

export default HyAccordionMenuCom;