import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { Accordion, Menu, Checkbox} from 'semantic-ui-react'

class HyMultipleMenuCom extends Component {

  static propTypes = {
    // 菜单数据
    menuData: PropTypes.arrayOf(PropTypes.shape({
      // 数据唯一标识
      id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
      ]).isRequired,
      // 菜单描述
      text: PropTypes.string.isRequired,
      // 对应的url
      url: PropTypes.string.isRequired,
      // 子菜单
      children: PropTypes.array
    })),
    // 激活的项目ID
    activeMenuId: PropTypes.array,
    // 项目点击事件
    onMenuClick: PropTypes.func,
    // 分组标题点击事件
    onGroupClick: PropTypes.func
  }

  state = {}

  render() {
    const { menuData, activeMenuId } = this.props;
    const parentActiveIndex = _.has(this.state, "parentActiveIndex") ? this.state.parentActiveIndex : this.getParentActionIndex();
    let className = null;
    return (
      <Menu vertical className="hyaccordionmenucom">
        {
          _.map(menuData, (item, index) => {
            const { id, url, text, children } = item;

            if (_.size(children) > 0) {
              const childrenMenu = this.getChildrenMenu(children, activeMenuId);
              const parentActiveState = `${parentActiveIndex}` === `${id}`;
              className = ClassNames({ "parentMenuActive": parentActiveState });
              const titleClassName = ClassNames({ "childrenActive": childrenMenu.isChildrenActive });
              return (
                <Accordion as={Menu.Item} fitted styled key={index} className={className}>
                  <Accordion.Title
                    className={titleClassName}
                    active={parentActiveState}
                    content={text}
                    index={0}
                    onClick={this.handleClick.bind(this, id)}
                  />
                  <Accordion.Content active={parentActiveState} content={childrenMenu.childrenMenuData} />
                </Accordion>
              )
            } else {
              const activeState = _.find(activeMenuId, item => item.id + '' === id + '')?true: false;

              className = ClassNames("menu-item", { "menuActive": activeState });
              return <Menu.Item className={className} fitted key={index} name={text} >
                <Checkbox onChange={this.onChange.bind(this, { id, text, ...item})}/>
                <span>{text}</span>
              </Menu.Item>
            }
          })
        }
      </Menu>
    );
  }
  getChildrenMenu = (children, activeMenuId) => {
    let isChildrenActive = false;
    const childrenMenuData = _.map(children, (item, index) => {
      const { id, url, text } = item;
      const activeState = `${id}` === `${activeMenuId}`;
      if (activeState) {
        isChildrenActive = activeState;
      }
      const className = ClassNames("menu-item", { "menuActive": activeState });
      return <Menu.Item fitted key={`children${index}`} className={className} name={text} onClick={this.onClick.bind(this, { id, url, text })} />
    })

    return { isChildrenActive, childrenMenuData };
  }
  handleClick = (parentId, event, data) => {
    if (parentId === this.state.parentActiveIndex) {
      this.props.groupOnClick && this.props.groupOnClick(parentId, event, data);
      return;
    }
    this.setState({
      parentActiveIndex: parentId
    }, () => {
      this.props.groupOnClick && this.props.groupOnClick(parentId, event, data);
    })
  }
  onClick = (menuData, event, data) => {
    const params = {
      ...data,
      ...menuData
    }
    this.props.onClick && this.props.onClick(event, params);
  }
  onChange = (menuData, event, data) =>{
    let activeMenuId = _.assign([],this.props.activeMenuId);
    if(data.checked){
      activeMenuId.push(menuData.id)
    }else{
      let findIndex = _.findIndex(activeMenuId, item => item + '' ===  menuData.id + '');
      activeMenuId.splice(findIndex, 1)
    }
    const params = {
      ...data,
      ...menuData,
      menuData:menuData,
      activeMenuId
    }
    this.props.onChange && this.props.onChange(event, params);
  }
  getParentActionIndex = () => {
    const { menuData, activeMenuId } = this.props;
    let parentActiveIndex = null;
    _.forEach(menuData, (item, index) => {
      const { id, children } = item;
      const findIndex = _.findIndex(children, (childrenItem) => {
        return `${childrenItem.id}` === `${activeMenuId}`;
      })
      if (findIndex > -1) {
        parentActiveIndex = id;
        return false;
      }
    })
    return parentActiveIndex;
  }
}

HyMultipleMenuCom.propTypes = {

};

export default HyMultipleMenuCom;