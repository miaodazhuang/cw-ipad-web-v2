import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Icon } from 'semantic-ui-react';
import HyButtonCom from '../hybuttoncom';


@BaseComponent
class HyChannelButtonCom extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }
  static propTypes = {
    // 传入的数据data:[{icon:img289,text:'接口推送'，name:""]
    data: PropTypes.object,
    onClick: PropTypes.func
  }

  static defaultProps = {
    data: {},
  }


  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  componentDidUpdate() {

  }

  render() {
    let _content = this.getContent();
    let dataSource = this.props.data;

    const className = ClassNames(
      { "hychannelbutton-more": _.size(dataSource) > 4 },
      { "hychannelbutton-less": _.size(dataSource) <= 4 },
      "hychannelbutton"
    )

    return (
      <div className={className}>
        {_content}
      </div>
    );
  }

  getContent = () => {
    let dataSource = this.props.data;
    const buttonClassName = _.size(dataSource) > 4 ? "channelbutton-more" : "channelbutton-less";
    let con = [];
    _.forEach(dataSource, (item, index) => {
      const { icon, text, ...otherProps } = item;
      con.push(<HyButtonCom
        className={buttonClassName}
        key={index}
        icon={true}
        onClick={this.onClick.bind(this, index)}
        {...otherProps}>
        <Icon className={ClassNames("icon20", icon)} />
        <span>{text}</span>
      </HyButtonCom>)
    })
    return con;
  }

  onClick = (index, event, data) => {
    let comValue = null;
    comValue = {
      ...data,
      key: index
    };
    this.props.onClick && this.props.onClick(event, comValue);
  }
}



export default HyChannelButtonCom;