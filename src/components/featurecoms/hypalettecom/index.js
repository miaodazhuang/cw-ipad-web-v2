import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {  Input, Popup } from 'semantic-ui-react';
import { BaseComponent } from 'Hyutilities/component_helper';
import classNames from "classnames";
@BaseComponent
class HyPaletteCom extends Component {
  static propTypes = {
    //類名
    className:PropTypes.string,
    //常用顏色 數組 例如 ["#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#00BCD4", "#009688","#4CAF50"];
    color: PropTypes.array,
    //參考popup 打開位置
    position:PropTypes.string,
    //value 顏色值  #98i7jw  #+6位
    value:PropTypes.string,
    //
    onChange:PropTypes.func,
    //trigger 要赢用的元素

  }

  static defaultProps = {
    color: ["#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3",
     "#00BCD4", "#009688","#4CAF50", "#8BC34A", "#CDDC39", "#FFEB3B", "#FFC107",
    "#FF9800", "#FF5722", "#795548", "#9E9E9E", "#607D85", "#FFFFFF", "#000000"],
    value:"#FFFFFF",
    position:"bottom center"
  }

  //是否打开
  state = {
    isopen: false,
    value:this.props.value
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if(this.props.value!==nextProps.value){
      this.setState({
        value:nextProps.value
      })
    }
  }


  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {

    let colorDiv = this.getColorDiv();
    // let style = {
    //   backgroundColor:/(^#[0-9A-F]{6}$)/i.test(this.state.value)?this.state.value:this.props.value
    // };
    // let trigger = (<div style ={style}>{this.props.trigger}</div>);
    return(<Popup className="palette-main"
            trigger={this.props.trigger}
            position={this.props.position}
            on="click" basic
            open={this.state.isopen}
            onClose={this._handleClose}
            onOpen={this._handleOpen}>
              <div className = {classNames("palette",this.props.className)}>
                <div className = "palette-model-bottom">
                    {colorDiv}
                </div>
                <Input className = {(/(^#[0-9A-F]{6}$)/i.test(this.state.value) || this.state.value === 'transparent')?"palette-input":"palette-input-not"} type="text" value={this.state.value} onChange={this._onChange}/>
              </div>
          </Popup>)
  }

  // 常用颜色
  getColorDiv = () =>{
    let _colors = this.props.color;
    let com = [];
    _.forEach(_colors,(item,index) => {
      let style = {
        backgroundColor:item
      }
      com.push(<div className = "palette-model-bottom-item" style={style} onClick = {(e) => {this._onClick(e,item)}}></div>)
    })
    return com;
  }

  /**
   * 常用颜色选择
   */
  _onClick = (event, data) => {
    this.setState({
      value:data
    })
    const _props = this.packageReProps();
    let comValue = {
      ..._props,
      value: data
    }
    if(this.props.onChange){
      this.props.onChange(event, comValue)
    }
  }

  /**
   * 颜色输入值改变
   */
  _onChange = (event, data) => {
    let value = data.value;
    this.setState({
      value:value
    })
    const _props = this.packageReProps();
    let comValue = {
      ..._props,
      value: value
    }
    if(/(^#[0-9A-F]{6}$)/i.test(value)||value === 'transparent')
    if(this.props.onChange){
      this.props.onChange(event, comValue)
    }
  }



  /**
   * 打开popup
   */
  _handleOpen = () => {
    this.setState({
      isopen: true
    });
  }

  /**
   * 关闭popup
   */
  _handleClose = () => {
    this.setState({
      isopen: false
    });
  }
}

HyPaletteCom.propTypes = {

};

export default HyPaletteCom;