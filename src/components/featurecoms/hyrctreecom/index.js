
import 'rc-tree/assets/index.css';
import React from 'react';
import PropTypes from 'prop-types';
import Tree, {TreeNode} from 'rc-tree';

export default class HyRcTreeCom extends React.Component {

  static propTypes = {
    //数据源
    treeData: PropTypes.array,
    //类名
    treeClassName:PropTypes.string,
    //onClick
    onClick:PropTypes.func,
    //是否可勾选
    checkable: PropTypes.bool,
    //是否可单选
    selectable: PropTypes.bool,
    //是否显示连接线
    showLine: PropTypes.bool,

  }

  static defaultProps = {
    checkable: true,
    selectable: false,
    showLine: true
  }

  constructor(props) {
    super()
    this.props = props
  }

  /**
   * 选中值设置
   * @param  {[type]} checkedKeys [description]
   * @return {[type]}             [description]
   */
  _onCheck = (checkedKeys,e) => {
    //调用父组件函数
    this.props.onClick && this.props.onClick(checkedKeys,e);
  }
  _onSelect = (checkedKeys,e) => {
    this.props.onSelect && this.props.onSelect(checkedKeys,e)
  }

  render() {
    const {
      treeClassName,
      treeData,
      selectable,
      checkable,
      showLine,
      ...childrenProps
    } = this.props;
    const _className = this.props.treeClassName || '';
    const _loop = (data) => {
      return data?(data.map((item) => {
        if (item.children) {
          return <TreeNode  title={item.name} key={item.key} isLeaf={item.isLeaf} disableCheckbox={item.disableCheckbox}>{_loop(item.children)}</TreeNode>;
        }else {
          return (<TreeNode title={item.name} key={item.key} isLeaf={item.isLeaf} disableCheckbox={item.disableCheckbox}/>);
        }

      })):'';
    };
    const treeNodes = _loop(this.props.treeData);

    if(treeNodes){
      return (
        <div className={_className}>
          <Tree checkable={checkable} {...childrenProps} onCheck={this._onCheck} onSelect={this._onSelect} showLine={showLine}
            selectable={selectable}>
            {treeNodes}
          </Tree>
        </div>
      );
    }else{
      return (<div className={_className}> </div>);
    }

  }
}
