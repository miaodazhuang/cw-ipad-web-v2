import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Checkbox } from 'semantic-ui-react';
import { BaseComponent } from 'Hyutilities/component_helper';
import HyButtonCom from '../hybuttoncom';
import classNames from "classnames";
@BaseComponent
class HyQueryCom extends Component {
  static propTypes = {
    //類名
    className:PropTypes.string,
    //未绑定,已绑定,[true,false]
    checked:PropTypes.array,
    //
    buttonLanguage:PropTypes.array,
    //点击事件
    onClick:PropTypes.func,
    //onChange  checkboxonChange事件
    onChange:PropTypes.func,
  }

  static defaultProps = {
    state_flg:"1"
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
  }


  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    const { buttonLanguage, className, checked } = this.props;
    return(
      <div className = {classNames('hyquerycom',className)}>
        <Checkbox
            label = {buttonLanguage[0]}
            key = '1'
            checked = {checked?checked[0]:""}
            onChange={(event, data) => { this.toggle(event, data, "0")}}></Checkbox>
        <Checkbox
            label = {buttonLanguage[1]}
            key = '2'
            checked = {checked?checked[1]:""}
            onChange={(event, data) => { this.toggle(event, data, "1")}}></Checkbox>
          {buttonLanguage[2]? <Checkbox
            label = {buttonLanguage[2]}
            key = '3'
            checked = {checked?checked[2]:""}
            onChange={(event, data) => { this.toggle(event, data, "2")}}></Checkbox>:""}
        <HyButtonCom className='' onClick = {(event,data)=>this._onClick(event,data,"3")}>{buttonLanguage[3]}</HyButtonCom>
      </div>
    )
  }

  /**
   *同步按钮点击时间
   */
  _onClick = (event, data) => {
    const _props = this.packageReProps();
    let comValue = {
      ..._props
    }
    if(this.props.onClick){
      this.props.onClick(event, comValue)
    }
  }

  toggle = (event, data ,index) => {
    let comValue = null;
    comValue = {
      ...data,
      index: index
    };
    this.props.onChange && this.props.onChange(event, comValue);
  }

}

export default HyQueryCom;