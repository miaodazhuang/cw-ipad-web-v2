import React from 'react';
import { Table, Checkbox } from 'semantic-ui-react';
import classNames from "classnames";
import HyAuthCom from "../hyauthcom";
import _ from 'lodash';
import PropTypes from 'prop-types';
import { BaseComponent } from 'Hyutilities/component_helper';

@BaseComponent
export default class HyTableRowCom extends React.Component{
  static propTypes = {
    /** checkbox或行选是否选中 */
    isChecked: PropTypes.bool,
    /** 当前hover的是否是该行*/
    isHover: PropTypes.bool
  }

  static contextTypes = {
    mergeCell: PropTypes.object
  }

  render(){
    const {trItem, trIndex} = this.props;
    let trAttribute = this._getTrAttribute(trItem, trIndex);
    return <Table.Row {...trAttribute}>
      {this._generateTableCell(trItem, trIndex)}
    </Table.Row>
  }

  /**
   * 获取tr的attribute
   */
  _getTrAttribute = (trItem, trIndex) => {
    let trAttribute = {};
    let trClass, rowClickedClass;
    if(this.props.trClassName){ //自定义tr className
      trClass = this.props.trClassName({rowindex: trIndex, datasource: trItem});
    }
    if(this.props.rowClickedClassName){ //行选className
      rowClickedClass = this.props.isChecked ? this.props.rowClickedClassName : "";
    }
    trAttribute.className = classNames(trClass, rowClickedClass, {
      "tb-tr-hover": this.props.isHover || this.props.isActive, //如果当前hover或者active的是当前行，增加className,
      "tb-tr-checked": this.props.isChecked, //如果当前行选中，增加className
      "tb-tr-checked-hover": this.props.isChecked && (this.props.isHover || this.props.isActive) //如果当前行选中并且鼠标经过或active，增加className
    });
    if(this.props.className){
      trAttribute.className = classNames(trAttribute.className, this.props.className);
    }
    if(this.props.onRowClickEvent){
      trAttribute.onClick = this._onRowClickEvent.bind(this, trItem, trIndex);
    }
    if(this.props.onMouseEnterEvent){
      trAttribute.onMouseEnter = this._onMouseEnter.bind(this, trItem, trIndex);
    }
    if(this.props.onMouseLeaveEvent){
      trAttribute.onMouseLeave = this._onMouseLeave.bind(this, trItem, trIndex);
    }
    return trAttribute;
  }

  /**
   * 生成一行的table cell
   */
  _generateTableCell = (trItem, trIndex, ischecked) => {
    //如果是自定单元行
    if(!this.props.isEmptyRow && _.isFunction(this.props.dataSource[trIndex].customDatasTd)){
      return this.props.dataSource[trIndex].customDatasTd({rowIndex: this.props.trIndex, dataSource: this.props.datasSource, name: this.props.name});
    }else{
      return this.props.columnData.map((tdItem, tdIndex) => {
        if(this.props.isEmptyRow){
          return <Table.Cell key={tdIndex}></Table.Cell>
        }
        //提出不需要传递给Cell的props
        let {width,checkbox,checkboxIsDisabled,featureData,template,className = "",toolTipText,contentAlign = "center",authCode, ...cellItem} = tdItem;
        let isAuth = HyAuthCom.CheckAuthCode(authCode);
        if(isAuth){
          return null;
        }
        //对齐方式 , 默认居中对齐
        cellItem.textAlign = contentAlign;
        if(checkbox){ //如果是checkbox列
          return <Table.Cell key={tdIndex} {...cellItem}>{this._getCheckbox(trItem, trIndex, checkboxIsDisabled)}</Table.Cell>
        }
        else if(featureData){ //如果是功能列
          return <Table.Cell key={tdIndex}>{this._getFeatureData(featureData, trItem, trIndex)}</Table.Cell>
        }
        //找该单元格需不需要被合并
        let isNeedRender = this._isNeedRenderByMergeCell(trIndex, tdIndex);
        if(!isNeedRender){
          return null;
        }
        let {title, text, cellClassName, rowColSpanObj} = this._getContent(trItem, trIndex, tdItem, tdIndex, template, toolTipText, className);
        return <Table.Cell key={tdIndex} {...rowColSpanObj} {...cellItem} title={title} content={text} className={cellClassName}></Table.Cell>
      });
    }
  }

  /**
   * checkbox框
   */
  _getCheckbox = (trItem, trIndex, checkboxIsDisabled = false) => {
    if(!this.props.dataIdentityPropName){
      console.error("table带有checkbox时，必须传props为dataIdentityPropName标识数据行的唯一标识属性名");
      return null;
    }
    if(_.isFunction(this.props.checkboxWillShowEvent)){
      let isShow = this.props.checkboxWillShowEvent({datasource: trItem, rowindex: trIndex});
      if(isShow !== true){
        return null;
      }
    }
    return <Checkbox checked={this.props.isChecked} dataidentity={trItem[this.props.dataIdentityPropName]} className="table-checkbox" onChange={this.props.checkboxChange} disabled={checkboxIsDisabled}/>
  }

  /**
   * 功能列
   */
  _getFeatureData = (featureData, trItem, trIndex) => {
    let content = featureData.content;
    if(_.isFunction(featureData.render)){
      content = featureData.render({rowindex: trIndex, datasource: trItem, scope: this.props.scope});
    }else if(React.isValidElement(content)){
      content = React.cloneElement(content,{
        rowindex: trIndex,
        datasource: trItem,
        scope: this.props.scope,
        onClick: this.props.onFeatureEvent && this.props.onFeatureEvent,
        show: this.props.isActive
      });
    }
    return content;
  }

  /**
   * 内容区域
   */
  _getContent = (trItem, trIndex, tdItem, tdIndex, template, toolTipText, className) => {
    let text = trItem[tdItem.field];
    let title = toolTipText || text;
    let cellClassName = className;
    let rowColSpanObj = null;
    if(_.isFunction(template)){
      const returnData = template({text, rowindex: trIndex, datasource: trItem, columndata: tdItem});
      if(returnData && returnData.hasOwnProperty("rowColSpanObj")){
        rowColSpanObj = returnData.rowColSpanObj;
        this._calcNotRenderCell(rowColSpanObj, trIndex, tdIndex);
        if(returnData.hasOwnProperty("content")){
          text = returnData.content;
        }
      }else if(returnData && typeof returnData === "object" && returnData.hasOwnProperty("toolTipText")){
        title = returnData.toolTipText;
        text = returnData.content;
      }else{
        text = returnData;
      }
    }else if(React.isValidElement(template)){
      text = React.cloneElement(template,{
        text,
        columndata: tdItem,
        rowindex: trIndex,
        datasource: trItem,
        onChange: this.props.onEditTdEvent && this.props.onEditTdEvent
      });
    }
    //else{
      //普通列，增加文字溢出...处理
      cellClassName = classNames(className, "text-ellipsis");
   // }
    return {text, title, cellClassName, rowColSpanObj};
  }

  /**
   * 根据合并单元格数据判断该单元格需不需要渲染
   */
  _isNeedRenderByMergeCell = (trIndex, tdIndex) => {
    if(!this.context.mergeCell || this.context.mergeCell[this._getMergeCellKey(trIndex, tdIndex)] !== true){
      return true;
    }
    return false;
  }

  /**
   * 计算不需要渲染的cell
   */
  _calcNotRenderCell = (rowColSpanObj, trIndex, tdIndex) => {
    let mergeCell = this.context.mergeCell;
    if(rowColSpanObj.colspan && rowColSpanObj.rowspan){
      for(let i = 0; i < +rowColSpanObj.rowspan; i++){
        for(let j = 0; j < +rowColSpanObj.colspan; j++){
          if(i === 0 && j === 0){
            continue;
          }
          mergeCell[this._getMergeCellKey(trIndex + i, tdIndex + j)] = true;
        }
      }
    }
    else if(rowColSpanObj.colspan){
      for(let i = 1; i < +rowColSpanObj.colspan; i++){
        mergeCell[this._getMergeCellKey(trIndex, tdIndex + i)] = true;
      }
    }
    else if(rowColSpanObj.rowspan){
      for(let i = 1; i < +rowColSpanObj.rowspan; i++){
        mergeCell[this._getMergeCellKey(trIndex + i, tdIndex)] = true;
      }
    }
  }

  /**
   * 获取mergeCellKey
   */
  _getMergeCellKey = (trIndex, tdIndex) => {
    return `${trIndex}-${tdIndex}`;
  }

    /**
   * 鼠标移入td事件
   */
  _onMouseEnter = (trItem, trIndex, event) => {
    this.props.onMouseEnterEvent(event, {trItem, trIndex});
  }

  /**
   * 鼠标移出td事件
   */
  _onMouseLeave = (trItem, trIndex, event) => {
    this.props.onMouseLeaveEvent(event, {trItem, trIndex});
  }

  /**
   * 行点击事件
   */
  _onRowClickEvent = (datasource, trIndex, event) => {
    this.props.onRowClickEvent(event, {datasource, trIndex});
  }
}