import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Segment } from 'semantic-ui-react';
import HyBaseTableCom from './hybasetablecom';
import HyScrollCom from '../hyscrollcom';
import classNames from "classnames";
import HyModelBoxCom from '../hymodelboxcom';
import moment from "moment";
import Observe from 'Hyutilities/observe_helper';
import { debounce } from 'Hyutilities/optimize_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hytablecom';
import ja_jp from 'ja_jp/components/featurecoms/hytablecom';
import zh_cn from 'zh_cn/components/featurecoms/hytablecom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
export default class HyTableCom extends React.Component {
  static defaultProps = {
    columnData: [],
    dataSource: [],
    checkedAllRowKey: [],
    isShowModalBox: false
  }
  static propTypes = {
    /**自定义className */
    className: PropTypes.string,
    /** 标题行数据 对象数组
      * field: 列数据key
      * width: 宽度 px(number) 或 百分比
      * fixed：enum: left right, 如果不配置，代表不需要锁定
      * checkbox: 代表该列是否是checkbox，bool, 下面属性不需要传递
      * checkboxIsDisabled: 如果是checkbox，checkbox是否禁用
      * featureData: 功能列数据， 如果传递该属性，代表该列为功能列 object {title: 标题，content：内容， render: function 渲染时回调的方法，回传data: {rowindex, datasource}}， 下面属性不需要传递
      * title: 标题行内容
      * headerAlign: string， 标题行对齐方式 left right center, 默认center
      * contentAlign: string, 内容行对齐方式 left right center, 默认center
      * toolTipText: 鼠标移入显示文字
      * template：  模版 可以是一个字符串或者jsx,也可以是一个function 这个function会在td渲染时调用，回传{text,rowindex,datasource}，
                   返回值: 如果只返回td区域  直接返回， 如果返回值中有鼠标移上去显示的内容和td区域内容，以对象形式返回｛toolTipText: 鼠标移上去显示的内容， content： td中的内容｝
                   如果需要合并单元格，返回rowColSpanObj｛rowspan， colspan｝，如果需要自定义单元格里面的内容返回content
      * authCode: SKU号
      * titleTdTemplate：自定义title的内容
    */
    columnData: PropTypes.array,
    //如果左侧有checkbox，checkbox将要显示事件，回传{rowindex,datasource}，返回值为bool类型
    checkboxWillShowEvent: PropTypes.func,
    //这个function会在tr渲染时调用，回传{rowindex,datasource}，根据返回值决定tr的className
    trClassName: PropTypes.func,
    /** 标题行中的td点击事件  参数：event 事件  data: object columnData中关于该列配置的属性*/
    onHeaderClickEvent: PropTypes.func,
    /** * 数据源  array  如果数据里面有customTr为true会调用customTrCallback*/
    dataSource: PropTypes.array,
    /** 数据行的标识属性名 */
    dataIdentityPropName: PropTypes.string,
    /** 左侧功能列显示checkbox,或者行选的选中的数据key  array */
    checkedAllRowKey: PropTypes.array,
    /** checkbox的change事件  参数：event 事件  data: object {checked: 是否选中， dataIdentity：数据行标识值，isAllCheckChange: 是否点击了标题上的全选按钮} */
    onCheckboxChangeEvent: PropTypes.func,
    /** 是否显示右上角更改columnData按钮 */
    isShowModalBox: PropTypes.bool,
    /** table右上角修改显示的columnData变更事件 */
    onColumnDataChangeEvent: PropTypes.func,
    /** 功能列按钮点击事件  参数： event， data object {datasource: 行数据， name： 数据的key}*/
    onFeatureEvent: PropTypes.func,
    /** table行内编辑数据改变事件  参数: event data object{rowindex: 行索引， 其余的是编辑组件的change或blur事件数据} */
    onEditTdEvent: PropTypes.func,
    /** 关于分页数据信息，没有分页的不用传 object {currentPage: 当前页码, pageSize: 每页显示数量, recordsTotal: 记录总数} */
    pageData: PropTypes.object,
    /** 分页信息更改事件，没有分页的不用传，获取更多数据，{currentPage: 需要加载的页索引， pageSize: 每页显示数量} */
    onCurrentPageChangeEvent: PropTypes.func,
    /** 行鼠标移入事件 参数： event， data: object {datasource, trIndex} */
    onMouseEnterEvent: PropTypes.func,
    /** 行鼠标移出事件  参数： event， data: object {datasource, trIndex} */
    onMouseLeaveEvent: PropTypes.func,
    /** 行选中后的tr自定义类 */
    rowClickedClassName: PropTypes.string,
    /** 行选事件 */
    onRowClickEvent: PropTypes.func,
    /** 自定义传输的数据，会在功能列回调或渲染组件时传出来 */
    scope: PropTypes.any,
    //当前活动行索引
    activeIndex: PropTypes.number,
    // 自定义tr
    customTrCallback: PropTypes.func,
    //是否禁用disabled
    checkboxIsDisabled: PropTypes.bool,
    //是否记录上次滚动条垂直滚动的位置 默认true记住
    isRecordPreviousVeriticalPos: PropTypes.bool,
    //是否记录上次滚动条水平滚动的位置 默认true记住
    isRecordPreviousHorizontalPos: PropTypes.bool
  }

  static childContextTypes = {
    mergeCell: PropTypes.object
  }

  EmptyArray = [];
  ActiveName = moment().valueOf().toString();
  //需要合并单元格的数据  格式｛0-2:true 代表第1行第3列需要合并｝
  mergeCell = {};
  state = {
    //鼠标移入的行index
    hoverRowIndex: -1,
    activeIndex: this.props.activeIndex || 0
  }

  getChildContext() {
    return {
      mergeCell: this.mergeCell
    }
  }

  componentWillMount(){
    if(!global.TABLEACTIVEOBJ){
      global.TABLEACTIVEOBJ = new Observe();
    }
    //初始化table选中项
    this._initTableCheckedObj();

  }
  componentDidMount(){
    //设置键盘监听
    var func = debounce(this._keyCodeEvent, 50);
    global.__KEYMASTER__("up", this.ActiveName, e => {
      this._eventPrevent(e) && func(true, true, e);
    });
    global.__KEYMASTER__("down", this.ActiveName, e => {
      this._eventPrevent(e) && func(true, false, e);
    });//"space"
    let key = global.__SHORTKEYMAP__["GF999"];
    if(key){
      global.__KEYMASTER__(key, this.ActiveName, e => {
        this._eventPrevent(e) && func(false, false, e);
      });
    }
    global.__KEYMASTER__.setScope(this.ActiveName);
    //监听活动的table改变
    this.observeUnListen = global.TABLEACTIVEOBJ.listen(this._activeObjListen);
    this._setActiveTable();

    //计算底部tip文本距离左边的距离
    let tipRef = this.refs.tableTip;
    let contentMiddleLeft = (this.refs.container.clientWidth - tipRef.clientWidth) / 2;
    tipRef.style.setProperty("left", `${contentMiddleLeft}px`);
  }
  componentWillReceiveProps(nextProps){
    //当点击查询按钮或重新打开界面时，让滚动条滚到顶部， （currentPage为1并且改变后的currentPage小于之前的currentPage）
    if(this.props.pageData && nextProps.pageData && nextProps.pageData.currentPage === 1
      && nextProps.pageData.currentPage < this.props.pageData.currentPage){
      this._scrollToTop();
    }

    if(this.props.activeIndex !== nextProps.activeIndex){
      this._setActiveIndex(nextProps.activeIndex);
    }

    if(!this.props.dataSource || !nextProps.dataSource || this.props.dataSource.length !== nextProps.dataSource.length){
      this.refs.tableTip.style.setProperty("visibility", "hidden");
    }

    if(this.props.dataSource !== nextProps.dataSource){
      _.mapKeys(this.mergeCell, (value, key) => { //如果dataSource变更，删除所有的key
        delete this.mergeCell[key];
      });
    }
  }
  componentDidUpdate(prevProps){
    if(prevProps.dataSource && prevProps.dataSource.length === 0 && this.props.dataSource && this.props.dataSource.length > 0){
      let activeIndex = global.TABLECHECKEDOBJ[this.TabIndex][this.PathName][this.Hashcode];
      if( activeIndex > 0){
        this._setActiveIndex(activeIndex);
        // let scrollTop = (activeIndex - 1) * 30; //默认将选择行滚动到第一行
        // if(scrollTop === 0) return;
        // let isLastRow = activeIndex === this.props.dataSource.length;
        // this._onSrollEvent(null, {
        //   name: "tableContent",
        //   activeHandler: "vertial",
        //   scroll: scrollTop,
        //   isScrollToBottom: isLastRow
        // }, true);
      }
    }
  }
  render() {
    //拆分columnData
    let leftColumnData = [], contentColumnData = [], rightColumnData = [];
    this.props.columnData.forEach(item => {
      if (item.fixed === "left") {
        leftColumnData.push(item);
      } else if (item.fixed === "right") {
        rightColumnData.push(item);
      } else {
        contentColumnData.push(item);
      }
    });
    //设置滚动时，其余需要滚动的table的refs
    this._setScrollMapping(leftColumnData, contentColumnData, rightColumnData);

    let className = classNames(this.props.className, "table-main");
    return <div className={className} ref="container" data-actname={this.ActiveName}>
      {this.props.isShowModalBox ? <HyModelBoxCom className="table-modalbox" boxData={this.props.columnData} onSaveClick={this._modalBoxSave}></HyModelBoxCom> : null}
      <Segment.Group horizontal className="segment-main">
        {this._getTableLeftFixed(leftColumnData)}
        {this._getTableContent(leftColumnData, contentColumnData, rightColumnData)}
        {this._getTableRightFixed(rightColumnData)}
      </Segment.Group>
      <div className="tb-tip-text" ref="tableTip">{this._getTipText()}</div>
    </div>
  }

  componentWillUnmount(){
    this.observeUnListen && this.observeUnListen();
    //当前是活动table时，指定为重新选择新的活动table
    if(this.ActiveName === global.TABLEACTIVEOBJ.observeValue){
      global.TABLEACTIVEOBJ.observeValue = "RESELECT";
    }
    //解绑事件
    let keys = ["up", "down"];
    let key = global.__SHORTKEYMAP__["GF999"];
    if(key){
      keys.push(key);
    }
    keys.forEach(item => {
      global.__KEYMASTER__.unbind(item, this.ActiveName);
    });
    global.__KEYMASTER__.deleteScope(this.ActiveName);
  }

  /**
   * 初始化table选中项
   *
   * @memberof HyTableCom
   */
  _initTableCheckedObj = () => {
    if(!global.TABLECHECKEDOBJ){
      global.TABLECHECKEDOBJ = [];
    }
    const hashcode = this._getHashCode(this.props.columnData);
    let str =  _.split(window.location.pathname, "/");
    this.TabIndex = str[2];
    this.PathName = _.drop(str, 3).join("/");
    this.Hashcode = hashcode;
    if(!global.TABLECHECKEDOBJ[this.TabIndex]){
      global.TABLECHECKEDOBJ[this.TabIndex] = {
        [this.PathName]: {
          [hashcode]: this.state.activeIndex
        }
      };
    }else if(global.TABLECHECKEDOBJ[this.TabIndex][this.PathName] == undefined){
      global.TABLECHECKEDOBJ[this.TabIndex][this.PathName] = {
        [hashcode]: this.state.activeIndex
      }
    }else if(global.TABLECHECKEDOBJ[this.TabIndex][this.PathName][hashcode] == undefined){
      global.TABLECHECKEDOBJ[this.TabIndex][this.PathName][hashcode] = this.state.activeIndex;
    }
  }

  /**
   * 设置当前活动的table
   */
  _setActiveTable = () => {
    let modals = document.querySelector(".modals");
    if(modals){
      let modalTables = document.querySelectorAll(".modals .table-main");
      if(modalTables.length > 0){
        let name = modalTables[0].getAttribute("data-actname");
        global.TABLEACTIVEOBJ.observeValue = name;
      }
      return;
    }
    let pageTables = document.querySelectorAll(".frame-view .table-main");
    if(pageTables.length > 0){
      let name = pageTables[0].getAttribute("data-actname");
      global.TABLEACTIVEOBJ.observeValue = name;
    }
  }

  _activeObjListen = (name) => {
    this.setState({
      activeIndex: this.ActiveName === name ? (this.props.activeIndex || 0) : -1
    })
    if(name === "RESELECT"){
      this._setActiveTable();
    }
  }

  /**
   * 设置table上下键活动的index
   */
  _setActiveIndex = (activeIndex) => {
    this.setState({
      activeIndex: activeIndex
    });
    global.TABLECHECKEDOBJ[this.TabIndex][this.PathName][this.Hashcode] = activeIndex;
  }

  setActiveIndex = (activeIndex) => {
    this._setActiveIndex(activeIndex);
  }

  /**
   * 处理事件
   */
  _eventPrevent = (e) => {
    if(e.target.tagName === "INPUT"){
      return false;
    }
    e.stopPropagation();
    e.preventDefault();
    return true;
  }

  /**
   * 键盘事件
   */
  _keyCodeEvent = (isUpDown, isUp, e) => {
    if(isUpDown){
      this._upDownEvent(isUp);
    }else{
      this._spaceEvent(e);
    }
  }

  /**
   * 上下箭头键盘点击事件
   */
  _upDownEvent = (isUp) => {
    if(global.TABLEACTIVEOBJ.observeValue === this.ActiveName){
      let activeIndex = this.state.activeIndex;
      let isLastData = false;
      if(isUp){
        if(activeIndex === 0){
          return;
        }
        activeIndex--;
      }else{
        //为了使滚动时能滚动到最底下
        isLastData = (this._isNoData() && (activeIndex === this.props.dataSource.length - 1) && !isUp);
        activeIndex =  isLastData ? activeIndex : (activeIndex + 1);
      }
      this._setActiveIndex(activeIndex);
      //检查滚动条滚动
      this._checkScrollInKeyCodeEvent(isUp, isLastData ? activeIndex + 1 : activeIndex);
    }
  }

  /**
   * 空格事件
   */
  _spaceEvent = (e) => {
    if(this.props.onCheckboxChangeEvent){
      let datasource = this.props.dataSource[this.state.activeIndex];
      let dataidentity = datasource[this.props.dataIdentityPropName];
      this.props.onCheckboxChangeEvent(e, {
        isAllCheckChange: false,
        dataidentity,datasource,
        checked: _.findIndex(this.props.checkedAllRowKey, item => item === dataidentity) === -1
      });
      return;
    }
    if(this.props.onRowClickEvent){
      this.props.onRowClickEvent(e, {
        rowindex: this.state.activeIndex,
        datasource: this.props.dataSource[this.state.activeIndex]
      });
    }
  }

  /**
   * 检查滚动条滚动
   */
  _checkScrollInKeyCodeEvent = (isUp, activeIndex) => {
    const tableContainer = this.refs.tableContent.el;
    const activeTr = tableContainer.firstChild.childNodes[1].childNodes[activeIndex];
    //activeTr距离tableContainer的顶部距离
    let top = activeTr.offsetTop - tableContainer.scrollTop;
    let scrollTop = tableContainer.scrollTop;
    if(isUp){ //向上
      if(top >= 0) return;
      scrollTop = scrollTop - 30;
    }else{
      //如果是向下需加上自身高度
      top = top + 30;
      if(top <= tableContainer.clientHeight) return;
      scrollTop = tableContainer.scrollTop + 30;
    }
    let isLastRow = !isUp && activeIndex === this.props.dataSource.length;
    this._onSrollEvent(null, {
      name: "tableContent",
      activeHandler: "vertial",
      scroll: scrollTop,
      isScrollToBottom: isLastRow
    }, true);
  }

  /**
   * modalbox保存事件
   */
  _modalBoxSave = (event, data) => {
    this.props.onColumnDataChangeEvent && this.props.onColumnDataChangeEvent(event, data);
  }

  /**
   * 滚动到顶部
   */
  _scrollToTop = () => {
    ["tableLeft","tableContent","tableRight"].forEach(item => {
      if(this.refs[item]){
        this.refs[item].SetScrollPos({
          activeHandler: "vertial",
          scroll: 0
        });
      }
    });
  }

  /**
   * 获取需要传递给HyBaseTableCom的props
   */
  _getNeedTransferProps = () => {
    let {className, isShowModalBox, onColumnDataChangeEvent, pageData, onCurrentPageChangeEvent, ...needTransferProps } = this.props;
    if(!_.isArray(needTransferProps.dataSource)){
      needTransferProps.dataSource = [];
    }
    return needTransferProps;
  }

  /**
   * 获取左侧锁定的table
   */
  _getTableLeftFixed = (leftColumnData) => {
    if (leftColumnData.length == 0) {
      return null;
    }
    //将需要传输给子组件的props解构出来，传递下去
    let needTransferProps = this._getNeedTransferProps();
    return (<Segment.Group className="segment-group-table group-table-left">
      <Segment className="segment-table segment-table-header">
        <HyBaseTableCom {...needTransferProps} isNotShowTableBody={true} columnData={leftColumnData}></HyBaseTableCom>
      </Segment>
      <Segment className="segment-table segment-table-body">
        <HyScrollCom name="tableLeft" ref="tableLeft" onScroll={this._onSrollEvent} showVeriticalTrack={false} showHorizontalTrack={false}>
          <HyBaseTableCom {...needTransferProps} name="left" isNotShowColumnData={true} columnData={leftColumnData}
          hoverRowIndex = {this.state.hoverRowIndex} activeIndex={this.state.activeIndex} onMouseEnterEvent={this._onMouseEnter} onMouseLeaveEvent={this._onMouseLeave}></HyBaseTableCom>
        </HyScrollCom>
      </Segment>
    </Segment.Group>);
  }

  /**
   * 获取右侧锁定的table
   */
  _getTableRightFixed = (rightColumnData) => {
    if (rightColumnData.length == 0) {
      return null;
    }
    //将需要传输给子组件的props解构出来，传递下去
    let {dataIdentityPropName, checkedAllRowKey, onCheckboxChangeEvent, ...needTransferProps } = this._getNeedTransferProps();
    return (<Segment.Group className="segment-group-table group-table-right">
      <Segment className="segment-table segment-table-header">
        <HyBaseTableCom {...needTransferProps} columnData={rightColumnData} dataSource={this.EmptyArray}></HyBaseTableCom>
      </Segment>
      <Segment className="segment-table segment-table-body">
        <HyScrollCom name="tableRight" ref="tableRight" onScroll={this._onSrollEvent} showHorizontalTrack={false}>
          <HyBaseTableCom {...needTransferProps} name="right" isNotShowColumnData={true} columnData={rightColumnData}
          hoverRowIndex = {this.state.hoverRowIndex} activeIndex={this.state.activeIndex} onMouseEnterEvent={this._onMouseEnter} onMouseLeaveEvent={this._onMouseLeave}></HyBaseTableCom>
        </HyScrollCom>
      </Segment>
    </Segment.Group>);
  }

  /**
   * 获取table中心区的内容
   */
  _getTableContent = (leftColumnData, contentColumnData, rightColumnData) => {
    if (contentColumnData.length == 0) {
      return null;
    }
    let isRecordPreviousVeriticalPos = this.props.isRecordPreviousVeriticalPos === false ? this.props.isRecordPreviousVeriticalPos : true;
    let isRecordPreviousHorizontalPos = this.props.isRecordPreviousHorizontalPos === false ? this.props.isRecordPreviousHorizontalPos : true;
    //将需要传输给子组件的props解构出来，传递下去
    let needTransferProps = this._getNeedTransferProps();
    return (<Segment.Group className="segment-group-table group-table-content">
      <Segment className="segment-table segment-table-header">
        <HyScrollCom name="tableHearderContent" ref="tableHearderContent" showVeriticalTrack={false} showHorizontalTrack={false}>
          <HyBaseTableCom {...needTransferProps} isNotShowTableBody={true} columnData={contentColumnData}></HyBaseTableCom>
        </HyScrollCom>
      </Segment>
      <Segment className="segment-table segment-table-body">
        <HyScrollCom name="tableContent" ref="tableContent" isRecordPreviousVeriticalPos={isRecordPreviousVeriticalPos} isRecordPreviousHorizontalPos = {isRecordPreviousHorizontalPos} onScroll={this._onSrollEvent} showHorizontalTrack={true} showVeriticalTrack={rightColumnData.length === 0}>
          <HyBaseTableCom {...needTransferProps} ref="table" name="middle" isNotShowColumnData={true} columnData={contentColumnData} onRowClickEvent={this._onRowClick}
          hoverRowIndex = {this.state.hoverRowIndex} activeIndex={this.state.activeIndex} onMouseEnterEvent={this._onMouseEnter} onMouseLeaveEvent={this._onMouseLeave}></HyBaseTableCom>
        </HyScrollCom>
      </Segment>
    </Segment.Group>);
  }

  /**
   * 行点击事件
   */
  _onRowClick = (event, data) => {
    global.__KEYMASTER__.setScope(this.ActiveName);
    global.TABLEACTIVEOBJ.observeValue = this.ActiveName;
    this._setActiveIndex(data.trIndex);
    this.props.onRowClickEvent && this.props.onRowClickEvent(event, data);
  }

  /*
   * table底部显示的提示信息
   */
  _getTipText = () => {
    return this._isNoData() ? languageConfig.noData : languageConfig.loadingData;
  }

  /**
   * 是否已经没有数据
   */
  _isNoData = () => {
    return !this.props.pageData || !this.props.dataSource || this.props.dataSource.length >= this.props.pageData.recordsTotal;
  }

  /**
   * 鼠标移入行事件
   */
  _onMouseEnter = (event, data) => {
    this.setState({
      hoverRowIndex: data.trIndex
    });

    this.props.onMouseEnterEvent && this.props.onMouseEnterEvent(event, data);
  }

  /**
  * 鼠标移出行事件
  */
  _onMouseLeave = (event, data) => {
    this.setState({
      hoverRowIndex: -1
    });
    this.props.onMouseLeaveEvent && this.props.onMouseLeaveEvent(event, data);
  }

  /**
   * table 滚动条滑动事件, 通过滚动的其中一个table设置其他table的滚动位置
   */
  _onSrollEvent = (event, obj, isManualTrigger) => {
    //处理tabel滚动关闭popup
    this.props.tableScroll && this.props.tableScroll(true);
    //处理table底部tip内容显示不显示
    let isShowTip = obj.isScrollToBottom ? "visible" : "hidden";
    this.refs.tableTip.style.setProperty("visibility",isShowTip);

    let needScroll = this.ScrollRefsMapping[obj.name];
    const activeHandler = obj.activeHandler ? obj.activeHandler : (obj.isHorizontal ? "horizontal" : "vertial");
    if(obj.name === "tableContent") {
      needScroll = this.ScrollRefsMapping[obj.name][activeHandler];
    }
    if(isManualTrigger && needScroll.length === 0){ //单个table时，设置tableContent的Position
      needScroll = ["tableContent"];
    }
    //只在tableContent时，设置scrollLeft
    obj.isSetScrollLeft = obj.name === "tableContent";
    needScroll.forEach(item => {
      if(this.refs[item]){
        this.refs[item].SetScrollPos(obj);
      }
    });

    //没有数据不处理滚动分页
    if(this._isNoData()){
      return;
    }
    //处理滚动条滚动时加载更多数据事件
    if(activeHandler === "horizontal"){ //水平滚动不处理
      return;
    }
    //没有滚动到位不处理
    if(!obj.isScrollToTop && !obj.isScrollToBottom){
      return;
    }
    if(this.scrollHandle){
      clearTimeout(this.scrollHandle);
      this.scrollHandle = null;
    }
    this.scrollHandle = setTimeout(() => {
      this._loadMoreData(obj.isScrollToTop);
    }, 80);
  }

  /**
   * 设置滚动时，其余需要滚动的table的refs
   */
  _setScrollMapping = (leftColumnData, contentColumnData, rightColumnData) => {
    let scrollRefsMapping = {
      tableLeft: [],
      tableContent: {
        horizontal: [],
        vertial: []
      },
      tableRight: []
    };
    if(leftColumnData.length > 0) {
      scrollRefsMapping.tableContent.vertial.push("tableLeft");
      scrollRefsMapping.tableRight.push("tableLeft");
    }
    if(contentColumnData.length > 0){
      scrollRefsMapping.tableLeft.push("tableContent");
      scrollRefsMapping.tableContent.horizontal.push("tableHearderContent");
      scrollRefsMapping.tableRight.push("tableContent");
    }
    if(rightColumnData.length > 0) {
      scrollRefsMapping.tableLeft.push("tableRight");
      scrollRefsMapping.tableContent.vertial.push("tableRight");
    }
    this.ScrollRefsMapping = scrollRefsMapping;
  }

  /**
   * 分页，加载更多数据
   */
  _loadMoreData = (isScrollToTop) => {
    let currentPage = this.props.pageData.currentPage;
    const loadedCount = currentPage * this.props.pageData.pageSize;
    if(isScrollToTop){
      //已经在第一页，或者数据已经加载过，不处理
      if(currentPage === 1 || loadedCount >= this.props.dataSource.length){
        return;
      }
      currentPage--;
    }else{
      currentPage++;
    }
    this.props.onCurrentPageChangeEvent && this.props.onCurrentPageChangeEvent(null, {currentPage, pageSize: this.props.pageData.pageSize});
  }

  _getHashCode(params){
    if(params == null){
      return "";
    }
    var str = params;
    if(typeof params === "object"){
      str = JSON.stringify(params);
    }
    var hash = 0, i, chr, len;
    if (str.length === 0) return hash;
    for (i = 0, len = str.length; i < len; i++) {
      chr   = str.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }
}
