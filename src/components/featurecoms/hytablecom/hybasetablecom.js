
import React from 'react';
import { Table, Checkbox } from 'semantic-ui-react';
import HyTableRowCom from './hytablerowcom';
import classNames from "classnames";
import HyAuthCom from "../hyauthcom";
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';

//table全选checkbox的标识名
const CBALLDATAIDENTITY = "checkAllData";

@BaseComponent
export default class HyBaseTableCom extends React.Component {
  //是否有header
  isHasTableHeader = false;
  render() {
    if (!Array.isArray(this.props.columnData) && !Array.isArray(this.props.dataSource)) {
      return null;
    }
    let className = classNames("table-custom", this.props.className);
    return <Table className={className}>
      {this._getTableHeader()}
      {this._getTableContent()}
    </Table>
  }

  /**
   * 获取colgroup
   */
  _getColGroup = () => {
    return (<colgroup>
      {
        this.props.columnData.map((item, index) => {
          let isAuth = HyAuthCom.CheckAuthCode(item.authCode);
          if(isAuth){
            return null;
          }
          let width = typeof item.width === "number" ? `${item.width}px` : item.width;
          return <col key={index} style={{ width: width }} ></col>
        })
      }
    </colgroup>);
  }

  /**
   * 获取table头
   */
  _getTableHeader = () => {
    if (this.props.isNotShowColumnData !== true && Array.isArray(this.props.columnData)) {
      let content = [];
      this.isHasTableHeader = true;
      content.push(this._getColGroup());
      content.push(<Table.Header><Table.Row>
        {
          this.props.columnData.map((item, index) => {
            //提出不需要传递给headerCell的props
            let { width, checkbox, checkboxIsDisabled, featureData, template, toolTipText, headerAlign = "center", authCode, className, ...cellItem } = item;
            let isAuth = HyAuthCom.CheckAuthCode(authCode);
            if(isAuth){
              return null;
            }
            //默认居中显示
            cellItem.textAlign = headerAlign;
            cellItem.className = classNames(className, "text-ellipsis");
            if (_.isFunction(this.props.titleTdTemplate)) {
              return <Table.HeaderCell key={index} {...cellItem} onClick={(e) => { this._onHeaderClickEvent(e, item) }}>{this.props.titleTdTemplate({ columnIndex: index, text: item.title, value: item })}</Table.HeaderCell>
            } else {
              if (checkbox) { //checkbox列
                return <Table.HeaderCell key={index} {...cellItem}><Checkbox checked={this.props.checkedAllRowKey.length !== 0 && this._isAllChecked()} dataidentity={CBALLDATAIDENTITY} className="table-checkbox" onChange={this._checkboxChange} disabled={checkboxIsDisabled || false}/></Table.HeaderCell>
              } else if (featureData) { //右侧功能列
                return <Table.HeaderCell key={index} {...cellItem}>{featureData.title}</Table.HeaderCell>
              }
              return <Table.HeaderCell key={index} {...cellItem} onClick={(e) => { this._onHeaderClickEvent(e, item) }}>{item.title}</Table.HeaderCell>
            }
          })}
      </Table.Row></Table.Header>
      );
      return content;
    }
    return null;
  }

  /**
   * 标题行中的列点击事件
   */
  _onHeaderClickEvent = (event, data) => {
    this.props.onHeaderClickEvent && this.props.onHeaderClickEvent(event, data);
  }

  /**
   * checkbox是否全选
   */
  _isAllChecked = () => {
    if (!_.isFunction(this.props.checkboxWillShowEvent) && this.props.dataSource.length !== this.props.checkedAllRowKey.length) {
      return false;
    }
    let isAllCheck = true;
    const len = this.props.dataSource.length;
    for(let i = 0; i < len; i++){
      let item = this.props.dataSource[i];
      if(_.isFunction(this.props.checkboxWillShowEvent)){
        const isShow = this.props.checkboxWillShowEvent({rowindex: i, datasource: item}); //如果checkbox不显示时，忽略该条数据
        if(!isShow) continue;
      }
      if(_.findIndex(this.props.checkedAllRowKey, eve => item[this.props.dataIdentityPropName] === eve) === -1){
        isAllCheck = false;
        break;
      }
    }
    return isAllCheck;
  }

  /**
   * checkbox或者行选是否选中
   */
  _isChecked = (trItem) => {
    return _.findIndex(this.props.checkedAllRowKey, eve => eve === trItem[this.props.dataIdentityPropName]) !== -1;
  }

  /**
   * 获取table内容
   */
  _getTableContent = () => {
    if (this.props.isNotShowTableBody !== true && Array.isArray(this.props.dataSource)) {
      let content = [];
      if (!this.isHasTableHeader) {
        content.push(this._getColGroup());
      }
      content.push(<Table.Body>
        {
          this.props.dataSource.map((trItem, trIndex) => {
            if(trItem.customTr && _.isFunction(this.props.customTrCallback)){
              if(this.props.name === "left" || this.props.name === "right"){
                return <tr><td></td></tr>
              }
              return this.props.customTrCallback({ trIndex: trIndex, datasource: trItem });
            }
            const { checkedAllRowKey, hoverRowIndex, activeIndex, ...tranProps } = this.props;
            let otherAttr = _.isFunction(this.props.customTrAttribute) && this.props.customTrAttribute({ trIndex: trIndex, datasource: trItem });
            return <HyTableRowCom {...otherAttr} {...tranProps} key={trIndex} trItem={trItem} trIndex={trIndex} isChecked={this._isChecked(trItem)}
              isHover={hoverRowIndex === trIndex} isActive={activeIndex === trIndex} checkboxChange={this._checkboxChange}></HyTableRowCom>
          })}
          <HyTableRowCom key="emptyRow" isEmptyRow={true} columnData={this.props.columnData}></HyTableRowCom>
      </Table.Body>);
      return content;
    }
    return null;
  }

  /**
   * checkbox的change事件
   */
  _checkboxChange = (event, data) => {
    //全选按钮
    if (data.dataidentity === CBALLDATAIDENTITY) {
      data.isAllCheckChange = true;
    }
    this.props.onCheckboxChangeEvent && this.props.onCheckboxChangeEvent(event, data);
  }
}