/** props参考index.js文件中定义的props */
import React from 'react';
import _ from 'lodash';
import HyTableCom from './index';
import { BaseComponent } from 'Hyutilities/component_helper';

@BaseComponent
export default class HyTableWithCheckboxCom extends React.Component{
  constructor(props){
    super(props);
    this.columnData = this._getCbColumnData(props);
  }
  componentWillReceiveProps(nextProps){
    if(this.props.columnData !== nextProps.columnData){
      this.columnData = this._getCbColumnData(nextProps);
    }
  }
  _getCbColumnData = (props) => {
    let checkboxObj = {
      textAlign: 'center',
      checkbox: true,
      width: props.leftFeatureWidth || 50,
      checkboxIsDisabled: props.checkboxIsDisabled
    }
    if(props.checkboxIsFixed){
      checkboxObj.fixed = "left";
    }
    let columnData = Object.assign([], props.columnData);
    columnData.unshift(checkboxObj);
    return columnData;
  }
  render(){
    return <HyTableCom {...this.props} ref="table" columnData={this.columnData} onCheckboxChangeEvent={this._checkboxChange}>
    </HyTableCom>
  }
  _checkboxChange = (event, data) => {
    if(data.isAllCheckChange){ //点击了全选按钮
      let checkedData = [];
      if(data.checked){
        this.refs.table.setActiveIndex(this.props.dataSource.length - 1);
        this.props.dataSource.forEach((item, index) => {
          if(_.isFunction(this.props.checkboxWillShowEvent)){
            let isHasCheckbox = this.props.checkboxWillShowEvent({datasource: item, rowindex: index});
            if(!isHasCheckbox){
              return;
            }
          }
          checkedData.push(item[this.props.dataIdentityPropName]);
        });
      }
      data.checkedAllRowKey = checkedData;
    }else{
      let checkedData = Object.assign([], this.props.checkedAllRowKey);
      if(data.checked){
        checkedData.push(data.dataidentity);
        let index = _.findIndex(this.props.dataSource, item => item[this.props.dataIdentityPropName] === data.dataidentity);
        this.refs.table.setActiveIndex(index);
      }else{
        let index = _.findIndex(checkedData, item => item === data.dataidentity);
        checkedData.splice(index, 1);
      }
      data.checkedAllRowKey = checkedData;
    }
    this.props.onCheckboxChangeEvent && this.props.onCheckboxChangeEvent(event, data);
  }
}