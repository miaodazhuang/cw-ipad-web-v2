import React, { Component } from 'react';

import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hyhelpnopagecom';
import ja_jp from 'ja_jp/hyhelpnopagecom';
import zh_cn from 'zh_cn/hyhelpnopagecom';

let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyHelpNoPageCom extends Component {

  render() {
    return <div className='no-page-wrap' style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%', flexDirection: 'column'}}>
      <div className='no-page-image'></div>
      <h3>{languageConfig.construction}</h3>
    </div>
  
  }

}

export default HyHelpNoPageCom;