import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from "classnames";
import {Icon,Popup,Transition,Segment} from 'semantic-ui-react';
import HyFormCom from 'Hycomponents/featurecoms/hyformcom';
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hyadvancequerycom';
import ja_jp from 'ja_jp/components/featurecoms/hyadvancequerycom';
import zh_cn from 'zh_cn/components/featurecoms/hyadvancequerycom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default class HyMoreQueryCom extends React.Component{
  static defaultProps = {
    btnText: languageConfig.advanceQuery,
    defaultFormData: {}
  }
  static propTypes = {
    //触发显示popup按钮的文本
    btnText: PropTypes.string,
    //默认表单数据，一键重置时会用该值作为formData，触发form的onChange
    defaultFormData: PropTypes.object
    //其余props参考HyFormCom
  }
  static childContextTypes = {
    setCloseOnDocumentClick: PropTypes.func
  }
  getChildContext(){
    return {
      setCloseOnDocumentClick: this._setCloseOnDocumentClick
    }
  }
  
  state = {
    isopen: false,//是否打开
    closeOnDocumentClick: false, //点击其他区域是否关闭该窗口
    isBottomOpen: false //下方查询条件是否展开
  }
  componentDidMount(){
    //先通过触发triggerEle的click事件让弹出层找到合适的位置，再关闭他
    this.refs.triggerEle.click();
    this._handleClose();
  }

  componentWillReceiveProps(nextProps){
    //控制浮层的关闭情况
    if(nextProps.modalwindowstate && nextProps.modalwindowstate.key === '01322010A032LP00101'){
      if(this.state.closeOnDocumentClick){
        this.setState({
          closeOnDocumentClick: false
        })
      }
    }
  }

  

  render(){
    const {btnText,defaultFormData, ...formProps} = this.props;
    //formData的值有更改时，改变高级查询和一键重置的颜色
    const isEqual = _.isEqual(defaultFormData, formProps.formData);
    const changedClass = isEqual ? "" : "dataChange-btn-color";
    const btnClass = classNames("advanceQ-btn", changedClass);
    const resetClass = classNames("advanceQ-form-reset", "reset",changedClass);
    const formClass = classNames("", formProps.className);
    const triggerEle = (<div onClick={this._handleOpen} className={btnClass} ref="triggerEle">
      <span className="advanceQ-btn-span">{btnText}</span>
      <HyIconCom keyboard="GF02" name={this.state.isopen ? "chevron up" : "chevron down"} onClick={this._handleOpen}></HyIconCom>
    </div>);
    return <Popup className="advance-main" position='bottom center' trigger={triggerEle} keepInViewPort={true} closeOnDocumentClick={this.state.closeOnDocumentClick} on="click" basic open={this.state.isopen}  onOpen={this._handleOpen} onClose={this._handleClose}>
      <Transition visible={this.state.isopen} animation="fade down" duration={500}>
        <Segment basic className="padding-0 advanceQ-form outer">
          <Segment basic className="padding-0">
            <div className='outer outer_hidden'>
              <Segment basic className="padding-0 ele1">
                <HyFormCom {...formProps} elements={formProps.element1} onClick={this._onFormClick} className={formClass}/>
              </Segment>
              <Segment basic className="padding-0 ele2">
                <HyFormCom {...formProps} elements={formProps.element2} className={formClass}></HyFormCom>
              </Segment>
              {
                this.state.isBottomOpen? <Segment basic className="padding-0 ele3">
                  <HyFormCom {...formProps} elements={formProps.element3} className={`${formClass}`}/>
                </Segment>: null
              }
              {
                this.state.isBottomOpen? <Segment basic className="padding-0 ele4">
                  <HyFormCom {...formProps} elements={formProps.element4} className={formClass}></HyFormCom>
                </Segment>: null
              }
            </div>
            <div className='bottom-handle-area'>
              <div className={resetClass} onClick={this._resetData}>
                <Icon className={classNames("icon14", "advanceQ-reset-icon", {img11_1: isEqual, img11_2: !isEqual})}></Icon>
                {languageConfig.reset}
              </div>
              <div className='expand-or-shrink' onClick={this._switchQuery}>
                <HyIconCom name={this.state.isBottomOpen ? "chevron up" : "chevron down"} ></HyIconCom>
                {this.state.isBottomOpen ? languageConfig.shrink: languageConfig.expand}
              </div>
            </div>
          </Segment>
        </Segment>
      </Transition>
    </Popup>
  }

  
  /**
   * 处理form中icon的点击事件
   *
   * @memberof HyMoreQueryCom
   */
  _onFormClick = (e, data) =>{
    this.props.onFormClick && this.props.onFormClick(e, data, this._setCloseOnDocumentClick)
  }

  /**
   * 切换子查询条件的展开关闭
   *
   * @memberof HyMoreQueryCom
   */
  _switchQuery = () =>{
    this.setState({
      isBottomOpen: !this.state.isBottomOpen
    });
  }

  /**
   * 打开popup
   */
  _handleOpen = () => {
    this.setState({
      isopen: true
    });
  }
  /**
   * 关闭popup
   */
  _handleClose = () => {
    this.setState({
      isopen: false
    });
  }

  /**
   * 设置popup在点击其他区域时是否关闭
   */
  _setCloseOnDocumentClick = (isclose) => {
    this.setState({
      closeOnDocumentClick: isclose
    });
  }

  /**
   * 一键重置按钮点击事件，通过该事件触发表单的change事件，使用props的defaultFormData去更新form里面的值
   */
  _resetData = (event) => {
    this.props.onChange && this.props.onChange(event, {formData: this.props.defaultFormData,eventData:{fieldname:'reset'}});
  }
}