import React from "react";
import PropTypes from 'prop-types';
import {Popup} from 'semantic-ui-react';
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';

export default class HyUserPortraitCom extends React.Component{
  static propTypes = {
    //手机号
    memberPhone: PropTypes.string,
    //icon的props
    iconProps: PropTypes.object
  }
  constructor(props){
    super(props);
    this.domain = process.env && process.env.NODE_ENV === 'production' ? "" : "http://testpro.jwcloudpms.com";
    this.path = `/ht90000/${this.props.memberPhone}/`;
    this.state = {
      memberPhone: this.props.memberPhone,
      isCheckData: true,
      isHaveData: false,
      portraitUrl: `${this.domain}${this.path}`
    }

    //添加postMessage事件监听，监听iframe页面发过来的关于有没有数据的消息
    window.addEventListener('message', this._onMessageEvent, false);
  }

  componentWillReceiveProps(nextProps){
    if(this.props.memberPhone !== nextProps.memberPhone){
      this.path = `/ht90000/${nextProps.memberPhone}/`;
      this.setState({
        memberPhone: nextProps.memberPhone,
        portraitUrl: `${this.domain}${this.path}`
      });
    }
  }

  componentWillUnmount(){
    window.removeEventListener("message", this._onMessageEvent, false);
  }

  /**
   * 监听到消息事件
   */
  _onMessageEvent = (messageEvent) => {
    const msgData = messageEvent.data;
    if(msgData.source === "user_portrait_service" && msgData.path === this.path){
      this.setState({
        isCheckData: false,
        isHaveData: msgData.isHaveData === "false" ? false : true
      });
    }
  }

  /**
   * 点击icon事件, 请求数据
   *
   * @memberof HyUserPortraitCom
   */
  _onClick = () => {
    this.setState({
      portraitUrl: `${this.domain}${this.path}?token=${global.__USERINFO__.JW_DATA}`
    });
  }

  _getIframe = () => {
    return <iframe className="user-portrait-iframe" src={this.state.portraitUrl}></iframe>
  }

  render(){
    if(!this.state.memberPhone){
      return null
    }
    if(this.state.isCheckData){
      return <div className="user-portrait">{this._getIframe()}</div>
    }
    if(!this.state.isHaveData){
      return null;
    }
    const triggerEle = <HyIconCom link={true} className="icon16 img339 menu-item" onClick={this._onClick} {...this.props.iconProps}></HyIconCom>;
    return <Popup className="user-portrait-main" trigger={triggerEle} position="bottom center" on="click">
      {this._getIframe()}
    </Popup>
  }
}