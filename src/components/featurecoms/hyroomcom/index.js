import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import classNames from 'classnames';
import {Icon} from 'semantic-ui-react';
import { BaseComponent } from 'Hyutilities/component_helper';
import { FormatMoment } from 'Hyutilities/moment_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/01325010A010LT00101/roomscom/roomcom';
import ja_jp from 'ja_jp/01325010A010LT00101/roomscom/roomcom';
import zh_cn from 'zh_cn/01325010A010LT00101/roomscom/roomcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
export default class HyRoomCom extends React.Component {
  static propTypes = {
    //自定义的className
    className: PropTypes.string,
    //是否是故障房
    isFaultRoom: PropTypes.bool,
    //是否是大图状态
    isBigPic: PropTypes.bool,
    //当前是否选中
    isSelected: PropTypes.bool,
    //是否单选选中
    isSingleSelected: PropTypes.bool,
    //房间数据
    roomData: PropTypes.object,
    //组件的click事件
    onClickEvent: PropTypes.func,
    //根据每一个元素的className后缀  例：{item, selected, arrival} 设置style
    styleObj: PropTypes.object,
    //自定义项的配置数据
    customItemData: PropTypes.array
  }
  static defaultProps = {
    styleObj: {}
  }
  constructor(props){
    super(props);
     //房态图上显示房型时，是显示房型代码还是房型简称, 为1时显示代码，为2时显示简称
     let roomTyp = global.__OPTIONMAP__['3250-010-009'];
     this.IsShowRoomTypCd = roomTyp && roomTyp.option_str1 == "1" ? true : false;
  }
  render() {
    //是否是故障房
    const {roomStus, roomTypCd, roomTypNm} = this.props.roomData;
    const isFaultRoom = this.props.isFaultRoom;
    //房间信息
    const roomTypeInfo = this.props.isBigPic ? (this.IsShowRoomTypCd ? roomTypCd : roomTypNm) : roomTypCd;
    const isArrival = this._isArrival(this.props.roomData);
    const className = classNames(this.props.className, {
      "roomstatus-item": true,
      "roomstatus-selected": this.props.isSelected,
      "roomstatus-single-selected": this.props.isSingleSelected,
      "roomstatus-fault": isFaultRoom, //故障房时，className为roomstatus-fault，
      [`roomstatus-${roomStus}`]: !isFaultRoom, //非故障房时，className为roomstatus-房间状态
      "roomstatus-arrival": isArrival //如果是预抵添加粉色背景的样式，必须满足当前房间为空房
    });
    let styleObj = this.props.styleObj;
    let style = {
      ...styleObj["roomstatus-item"],
      ...this.props.isSelected ? styleObj["roomstatus-selected"] : null,
      ...this.props.isSingleSelected ? styleObj["roomstatus-single-selected"] : null,
      ...isFaultRoom ? styleObj["roomstatus-fault"] : null,
      ...isFaultRoom ? null : styleObj[`roomstatus-${roomStus}`],
      ...isArrival ? styleObj["roomstatus-arrival"] : null,
    }
    return (<section className={className} style={style} ref={node => this._setImportantStyle(node, style)} onClick={this._onClick}>
        {isFaultRoom ? this._getFaultRoomStatus(roomTypeInfo) : this._getNormalRoomStatus(roomTypeInfo)}
      </section>);
  }

  /**
   * 设置带!important的style
   */
  _setImportantStyle = (node, style) => {
    if(!node || !style) return;
    let element = ReactDOM.findDOMNode(node);
    Object.keys(style).forEach(key => {
      var value = style[key];
      if(!value) return;
      if (value.indexOf('!important') === -1) return;
      element.style.setProperty(
        key,
        value.replace(/\s*!important/g, ''),
        "important"
      );
    });
  }

  /**
   * 获取正常房房间状态
   */
  _getNormalRoomStatus = (roomTypeInfo) => {
    const {roomNum, useUserName, cleanStus} = this.props.roomData;
    let content = [];
    let styleObj = this.props.styleObj;
    //清扫状态样式
    let csClassName = `roomstatus-${cleanStus}`;
    content.push(<div className={csClassName} style={styleObj[csClassName]} ref={node => this._setImportantStyle(node, styleObj[csClassName])}>
      </div>);
    content.push(<div className="text-ellipsis roomstatus-txt-roomnum" style={styleObj["roomstatus-txt-roomnum"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-roomnum"])} title={roomNum}>{roomNum}</div>)
    content.push(<div className="text-ellipsis roomstatus-txt-type" style={styleObj["roomstatus-txt-type"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-type"])} title={roomTypeInfo}>{roomTypeInfo}</div>);
    content.push(<div className="text-ellipsis roomstatus-txt-user" style={styleObj["roomstatus-txt-user"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-user"])} title={useUserName || ""}>{useUserName || ""}</div>);
    content.push(this._getStatusIcon());
    return content;
  }

  /**
   * 获取故障房房间状态
   */
  _getFaultRoomStatus = (roomTypeInfo) => {
    const roomData = this.props.roomData;
    const startDt = `${this.props.isBigPic ? `${languageConfig.start}: ` : ""}${moment(roomData.startDt).format(FormatMoment('L'))}`;
    const endDt = `${this.props.isBigPic ? `${languageConfig.end}: ` : ""}${moment(roomData.endDt).format(FormatMoment('L'))}`;
    let content = [];
    let styleObj = this.props.styleObj;
    content.push(<div className="text-ellipsis roomstatus-txt-roomnum" style={styleObj["roomstatus-txt-roomnum"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-roomnum"])} title={roomData.roomNum}>{roomData.roomNum}</div>)
    content.push(<div className="text-ellipsis roomstatus-txt-type" style={styleObj["roomstatus-txt-type"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-type"])} title={roomTypeInfo}>{roomTypeInfo}</div>);
    content.push(<div className="text-ellipsis roomstatus-txt-startdt" style={styleObj["roomstatus-txt-startdt"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-startdt"])} title={startDt}>{startDt}</div>);
    content.push(<div className="text-ellipsis roomstatus-txt-enddt" style={styleObj["roomstatus-txt-enddt"]} ref={node => this._setImportantStyle(node, styleObj["roomstatus-txt-enddt"])} title={endDt}>{endDt}</div>)
    content.push(this._getStatusIcon());
    return content;
  }

  /**
   * 获取底部图标
   */
  _getStatusIcon = () => {
    return this._getBottomIcon().map((item, index) => {
      return (<Icon key={index} className={item.className} style={this.props.styleObj[item.styleKey]} ref={node => this._setImportantStyle(node, this.props.styleObj[item.styleKey])}/>);
    });
  }

  _onClick = (e) => {
    if(this.props.onClickEvent){
      let data = this._packageReProps();
      this.props.onClickEvent(e, data);
    }
  }

  /**
   * 是否是预抵
   */
  _isArrival = (roomData) => {
    return roomData.roomStus === "V" && roomData.etaFlg == "1";
  }

  /**
   * 底部图标
   */
  _getBottomIcon = () => {
    let statusFlg = [];
    let {roomData, imageExampleData} = this.props;
    if(roomData.roomStus === "OOO"){  //停用房
      statusFlg.push({className: "font font-size-14 icon-23 roomstatus-icon-OOO", styleKey: "roomstatus-icon-OOO"});
    }
    else if(roomData.roomStus === "OOS"){ //维修
      statusFlg.push({className: "font font-size-14 icon-24 roomstatus-icon-OOS", styleKey: "roomstatus-icon-OOS"});
    }
    else if(roomData.roomStus === "NBA"){ //临时锁房
      statusFlg.push({className: "font font-size-14 icon-23 roomstatus-icon-NBA", styleKey: "roomstatus-icon-NBA"});
    }

    if(roomData.etaFlg == "1" && _.includes(imageExampleData,"etaFlg")){ //预抵
      statusFlg.push({className: "font font-size-14 icon-21 roomstatus-icon-etaFlg", styleKey: "roomstatus-icon-etaFlg"});
    }
    if(roomData.etdFlg == "1" && _.includes(imageExampleData,"etdFlg")){ //预离
      statusFlg.push({className: "font font-size-14 icon-22 roomstatus-icon-etdFlg", styleKey: "roomstatus-icon-etdFlg"});
    }
    if(roomData.useFlg == "1" && _.includes(imageExampleData,"useFlg")){ //自用
      statusFlg.push({className: "font font-size-14 icon-25 roomstatus-icon-useFlg", styleKey: "roomstatus-icon-useFlg"});
    }
    if(roomData.freeFlg == "1" && _.includes(imageExampleData,"freeFlg")){ //免费
      statusFlg.push({className: "font font-size-14 icon-26 roomstatus-icon-freeFlg", styleKey: "roomstatus-icon-freeFlg"});
    }
    if(roomData.oweFlg == "1" && _.includes(imageExampleData,"oweFlg")){ //押金不足
      statusFlg.push({className: "font font-size-14 icon-27 roomstatus-icon-oweFlg", styleKey: "roomstatus-icon-oweFlg"});
    }
    if(roomData.hourFlg == "1" && _.includes(imageExampleData,"hourFlg")){ //钟点
      statusFlg.push({className: "font font-size-14 icon-33 roomstatus-icon-hourFlg", styleKey: "roomstatus-icon-hourFlg"});
    }
    if(roomData.withRoomFlg != null && roomData.withRoomFlg != "" && _.includes(imageExampleData,"withRoomFlg")){ //同来人
      statusFlg.push({className: "font font-size-14 icon-128 roomstatus-icon-withRoomFlg", styleKey: "roomstatus-icon-withRoomFlg"});
    }
    if(roomData.smokingFlg === "N" && _.includes(imageExampleData,"smokingFlg")){ //无烟房
      statusFlg.push({className: "font font-size-14 icon-127 roomstatus-icon-smokingFlg", styleKey: "roomstatus-icon-smokingFlg"});
    }
    if(roomData.reserveInFuture === "1" && _.includes(imageExampleData,"reserveInFuture")){ //锁房
      statusFlg.push({className: "font font-size-14 roomstatus-icon-reserveInFuture", styleKey: "roomstatus-icon-reserveInFuture"});
    }
    if(this.props.customItemData){
      _.forEach(this.props.customItemData, item => {
        statusFlg.push({className: `roomstatus-custom-item ${item.className}`, styleKey: item.styleKey});
      });
    }
    return statusFlg;
  }
}