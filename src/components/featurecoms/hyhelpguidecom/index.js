/**
 * @file 帮助引导组件
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon, Popup } from 'semantic-ui-react';
import { connect } from 'dva';
import SliderCom from './slidercom';

class HyHelpGuideCom extends Component {

  static propTypes = {
    // 模式
    model: PropTypes.oneOf(['normal', 'edit', 'guide'])
  }

  static defaultProps = {
    model: 'normal'
  }

  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props)
  }

  state = {
    footerWidth: '100vw',
  }

  // 收起点击事件
  onHideClick = () => {
    this.setState({footerWidth: '52px'})
  }

  // 展开点击事件
  onShowClick = () => {
    this.setState({footerWidth: '100vw'})
  }

  render() {

    const style = {
      padding: '8px 8px 0 8px',
      backgroundColor: '#FBFBEB',
      border: 'solid 1px #F9833A',
      opacity: '0.9',
    }
    return (
      <div className='HyHelpGuideCom'>
        {this.props.model == 'edit' && (
          <div className='disableClick' />
        )}
        {this.props.model == 'guide' ? (
          <Popup
            trigger={<div className='popContainer'>{this.props.children}</div>}
            on='click'
            style={style}
          >
            <div className='guidePop'>
              <div className='popContainer'>
                <span data-step>
                  1
                </span>
                <span data-name>
                  {'测试测试'}
                </span>
              </div>
              <div className='popFooter'>
                <span data-before>
                  {'< 上一步'}
                </span>
                <span data-next>
                  {'下一步 >'}
                </span>
              </div>
            </div>
          </Popup>
        ) : (
          this.props.children
        )}
        <div
          className='footer'
          style={{width: this.state.footerWidth}}
        >
          <div className='footerShow'>
            <Icon className="icon14 img9" onClick={this.onShowClick} />
          </div>
          <div className='footerContainer'>
            <div className='stepName'>
              测试测试测
            </div>
            <SliderCom
              dataSource={
                [{value: '1'}, {value: '2'}, {value: '3'}, {value: '4'}, {value: '5'}, {value: '6'}, {value: '7'},{value: '8'}, {value: '9'},
                {value: '10'}, {value: '11'}, {value: '12'}, {value: '13'}, {value: '14'}]
              }
              activeIndex={8}
            />
            <div className='fixCenter' />
          </div>
          <div className='menuFunc'>
            <span onClick={this.onHideClick}>×</span>
          </div>
        </div>
      </div>
    )

  }

}

function mapStateToProps(state) {
  return {};
}
export default connect(mapStateToProps)(HyHelpGuideCom);
