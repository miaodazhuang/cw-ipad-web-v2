import React from 'react';
import uuid from 'node-uuid';

const getLeftEllipsis = (dataSource, activeIndex) => {
  const container = []
  // 如果是大于10
  if (Number(activeIndex) > 10) {
    const temp = []
    for (let one = Number(activeIndex) - 7; one < Number(activeIndex) - 1; one++) {
      temp.push((<li data-item>{dataSource[one - 1].value}</li>))
    }
    container.push((<li data-item>{dataSource[0].value}</li>), (<li data-omit>•••</li>), ...temp)
  } else {
    const temp = []
    for (let one = 1; one < Number(activeIndex) - 1; one++) {
      temp.push((<li data-item>{dataSource[one - 1].value}</li>))
    }
    container.push(...temp)
  }
  return container
}

const getRightEllipsis = (dataSource, activeIndex) => {
  const container = []
  // 如果是大于10
  if (Number(activeIndex) < (Number(dataSource.length) - 9)) {
    const temp = []
    for (let one = Number(activeIndex) + 2; one < Number(activeIndex) + 8; one++) {
      temp.push((<li data-item>{dataSource[one - 1].value}</li>))
    }
    container.push(...temp, (<li data-omit>•••</li>), (<li data-item>{dataSource[dataSource.length - 1].value}</li>))
  } else {
    const temp = []
    for (let one = Number(activeIndex) + 2; one < dataSource.length; one++) {
      temp.push((<li data-item>{dataSource[one - 1].value}</li>))
    }
    container.push(...temp)
  }
  return container
}

const SliderCom = (props) => {
  const { dataSource, activeIndex } = props
  return (
    <ul className='sliderCom' key={uuid.v4()}>
      <div className='leftEllipsis'>
        {getLeftEllipsis(dataSource, activeIndex)}
      </div>
      <div className='midEllipsis'>
        {Number(activeIndex) != 1 ? (
          <li data-left-second>
            {Number(activeIndex) - 1}
          </li>
        ) : (
          <span style={{width: '25px', display: 'inline-block'}} />
        )}
        <li data-currt>
          {activeIndex}
        </li>
        <li data-right-second>
          {Number(activeIndex) + 1}
        </li>
      </div>
      <div className='rightEllipsis'>
        {getRightEllipsis(dataSource, activeIndex)}
      </div>
    </ul>
  )
}

export default SliderCom;