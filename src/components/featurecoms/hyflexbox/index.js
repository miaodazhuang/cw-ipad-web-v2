/**
 * 盒子组件
 *
 *Direction按钮方向,默认true 为上下，false为左右
 *Openstate指定初始状态，true 是打开，false是关闭
 *BtnCustomClassName按钮样式
 *CustomClassName组件样式（水平方向时，需要传一个组件高度，和children高度一样 ）
 *OnBthClickEvent  按钮点击事件，如若不传执行默认的。
 *OnClickFinishedEvent 窗口改变后的回调事件
 *IsHideType Children的隐藏类型 默认是false，即隐藏子元素时是删除子元素;改为true时，隐藏子元素时是display:none。
 */
import React from 'react';
import { Icon } from 'semantic-ui-react';
import HyButtonCom from '../../featurecoms/hybuttoncom';
import _ from 'lodash';
export default class HyFlexBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openchild: this.props.Openstate,
      openchildDisplay: this.props.Openstate,
      leftstyle: this.props.Openstate,
      topstyle: this.props.Openstate,
      linebtn: this.props.Openstate,
      heightbtn: this.props.Openstate,
      hline: !this.props.Openstate,
      wline: !this.props.Openstate,
    }

  }
  componentWillReceiveProps(nextProps) {
    // console.log(nextProps,'nextprops')
    if (this.props.Openstate !== nextProps.Openstate) {
      this.setState({
        openchild: nextProps.Openstate,
        openchildDisplay: nextProps.Openstate,
        leftstyle: nextProps.Openstate,
        topstyle: nextProps.Openstate,
        linebtn: nextProps.Openstate,
        heightbtn: nextProps.Openstate,
        hline: !nextProps.Openstate,
        wline: !nextProps.Openstate,
      })
    }
  }
  //按钮向下向上，向左向左
  getContent = () => {
    let icon = [];
    let leftstyle = this.state.leftstyle ? 'flexboxbtn-l btnleftstyle' : 'flexboxbtn-l';
    let topstyle = this.state.topstyle ? 'flexboxbtn-t btntopstyle' : 'flexboxbtn-t btntopstyle';
    //fa fa-caret-down  向下
    //fa fa-caret-up  向上
    //fa fa-caret-left  向左
    //fa fa-caret-right  向右
    let lineicon = this.state.linebtn ? 'caret left' : 'caret right'
    let heighticon = this.state.heightbtn ? 'caret up' : 'caret down'
    let style = this.state.hline ? 'hclick' : ''
    let wstyle = this.state.wline ? 'wclick' : ''
    if (this.props.Direction) {
      icon.push(
        <div className={`${style} heightbox`}>
          <HyButtonCom icon onClick={this._onclickEvent} className={topstyle}>
            <Icon name={heighticon} />
          </HyButtonCom>
        </div>)

    } else {
      icon.push(
        <div className={`${wstyle} widthbox`}>
          <HyButtonCom onClick={this._onclickEvent} className={leftstyle}>
            <Icon name={lineicon} ></Icon>
          </HyButtonCom>
        </div>)

    }
    return icon;
  }
  //关闭或打开
  _onclickEvent = () => {
    if (this.props.OnBthClickEvent) {
      this.props.OnBthClickEvent(this, { openchild: !this.state.openchild })
      return;
    }
    let open = _.clone(this.state.openchild);
    let openDisplay = _.clone(this.state.openchildDisplay)
    let leftstyle = _.clone(this.state.leftstyle)
    let topstyle = _.clone(this.state.topstyle)
    let linebtn = _.clone(this.state.linebtn)
    let heightbtn = _.clone(this.state.heightbtn)
    let hline = _.clone(this.state.hline)
    let wline = _.clone(this.state.wline)
    this.setState({
      openchild: !open,
      openchildDisplay: !openDisplay,
      leftstyle: !leftstyle,
      topstyle: !topstyle,
      linebtn: !linebtn,
      heightbtn: !heightbtn,
      hline: !hline,
      wline: !wline,
    }, () => {
      if (this.props.OnClickFinishedEvent) {
        this.props.OnClickFinishedEvent(this, { openchild: this.state.openchild })
        return;
      }

    })
  }
  //控制children的显示或隐藏方式
  _handleChildren = () => {
    let IsHideType = this.props.IsHideType || false;
    let openchild = this.state.openchild;
    let openDisplay = this.state.openchildDisplay ? 'Cblock' : 'Cnone';
    let ChildDom;
    if (IsHideType) {
      ChildDom = (<div className={`children ${openDisplay}`}>
        {this.props.children}
      </div>)
    } else {
      ChildDom = (<div className='children'>
        {openchild ? this.props.children : ''}
      </div>)
    }
    return ChildDom;
  }
  render() {
    //  console.log(this.props,'HyFlexBox')
    const _className = `HyFlexBox ${this.props.CustomClassName || ''} `;
    return (
      <div className={_className}>
        {this.getContent()}
        {this._handleChildren()}
      </div>
    )
  }
}
