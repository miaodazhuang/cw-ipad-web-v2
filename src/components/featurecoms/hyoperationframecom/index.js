import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Icon, Segment } from 'semantic-ui-react';
import classNames from "classnames";
import HyButtonCom from '../hybuttoncom';
import HyExtendCheckBoxCom from "../hyextendcheckboxcom"
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/01322010A020LT00101';
import ja_jp from 'ja_jp/01322010A020LT00101';
import zh_cn from 'zh_cn/01322010A020LT00101';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
class HyOperationFrameCom extends Component {
  static propTypes = {
    //例如：[{buttonlist:{key1:{name:name1,permsCode:[]}},value:value,checkbox的属性}]
    checkboxlist: PropTypes.array,
    //标题尾部按钮{key1:{name:name1,permsCode:[]}}
    titlebtnlist: PropTypes.array,
    //标题[title1,title2]
    title: PropTypes.obj,
    //hyextendcheckboxcom属性
    options: PropTypes.obj,
    //标题后的select选择框
    pageHySelectDataSource: PropTypes.obj,
    productId: PropTypes.oneOf([
      PropTypes.string,
      PropTypes.number
    ]),
    //+按钮是否点亮
    isSelected: PropTypes.bool,
    //为特殊付款转入增加
    details: PropTypes.array,
  }
  constructor(props) {
    super(props);
  }

  componentWillMount() {

  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }


  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {

  }

  render() {
    let buttonlist = this._getButtonList();
    let title = this._getTitle()
    let _className = classNames(this.props.className, "operframe","ui","segments");
    let page = this.props.pageHySelectDataSource;
    let _details = this.getDetails();
    let _pageClassName = page && page.className? classNames(page.className, 'operframe-title-select'):'operframe-title-select';
    return (
      <div className={_className} ref="operationframe">
        <Segment className='operframe-title'>
          <div className='operframe-title-left text-ellipsis'>
           {this.props.notTitleBtn?"":<HyButtonCom className='icon-button' permsCode={this.props.permsCode} onClick={(event, data) => { this._onClickBtn(event, data, 'add') }}>
              <Icon className={!this.props.isSelected ? 'specialpay_plus' : 'specialpay_plused'} />
            </HyButtonCom>}
            {title}
          </div>
          {page ? <div className={_pageClassName}>
              <div>{page.text}:</div>
              {this.getPageData(page.DataSource,page.currentPage)}
              {/* <Select
                className='chopage'
                options={page.DataSource}
                value={page.currentPage}
                onChange={this._onSelectEvent}
              /> */}
            </div> : ''}
          {buttonlist}
        </Segment>
        <Segment className='operframe-content'>
          <HyExtendCheckBoxCom
            {...this.props.options}
            permsCode={this.props.permsCode}
            disabled={this.props.disabled}
            checked={this.props.checkValue}
            isshowbutton={this.props.isshowbutton}
            checkboxlist={this.props.checkboxlist}
            onChange={this._onClickCheckBox}
            onClickBtn={this._onClickBoxBtn} />
            <div className='operframe-content-info'>
              {_details}
            </div>
        </Segment>
      </div>
    );
  }

  getPageData = (data,currentPage) => {
    let con = [];
    if(data&&currentPage){
      _.forEach(data,(item,index) => {
        if(item === currentPage){
          con.push(<div className = 'operframe-currentPage' onClick = {(event) => this._onChangePage(event,item)}>{item}</div>)
        }else{
          con.push(<div onClick = {(event) => this._onChangePage(event,item)}>{item}</div>)
        }
      })
    }
    return con;
  }

  getDetails = () => {
    let details = this.props.details;
    let _com = [];
    let _className;
    if (details) {
      _.forEach(details, (item, index) => {
        let itemacctStusDrpt = "";
        if (item.acctStus == "STY") {
          itemacctStusDrpt = languageConfig.STY;
        }
        else if (item.acctStus == "RSV") {
          itemacctStusDrpt = languageConfig.RSV;
        }
        else if (item.acctStus == "OUT") {
          itemacctStusDrpt = languageConfig.OUT;
          _className = "operframe-details-item-forbid";
        }
        else if (item.acctStus == "CXL") {
          itemacctStusDrpt = languageConfig.CXL;
          _className = "operframe-details-item-forbid";
        }
        else if (item.acctStus == "NSW") {
          itemacctStusDrpt = languageConfig.NSW;
          _className = "operframe-details-item-forbid";
        }
        else if (item.acctStus == "WAT") {
          itemacctStusDrpt = languageConfig.WAT;
          _className = "operframe-details-item-forbid";
        }
        else if (item.acctStus == "WAC") {
          itemacctStusDrpt = languageConfig.WAC;
          _className = "operframe-details-item-forbid";
        }
        else if (item.acctStus == "GRP") {
          itemacctStusDrpt = languageConfig.GRP;
          _className = "operframe-details-item-forbid";
        }
        _com.push(<div classNames = {("operframe-details-item",_className)}>
          <div>
            <div>{item.roomNum}</div>
            <div>{item.acctNo}</div>
          </div>
          <div>
            <div>{itemacctStusDrpt}</div>
            <div>{item.altNm}</div>
          </div>
        </div>)
      })
      return _com;
    } else {
      return null;
    }
  }

  _getButtonList = () => {
    let btnGroup = [];
    let _btnlist = this.props.titlebtnlist;
    if (!_btnlist) {
      return;
    }
    for (let key in _btnlist) {
      let _className = classNames("icon14", _btnlist[key].name);
      btnGroup.push(
        <HyButtonCom
          className='operframe-title-button icon-button'
          permsCode={_btnlist[key].permsCode}
          onClick={(event, data) => { this._onClickBtn(event, data, key) }}>
          <Icon className={_className} />
        </HyButtonCom>
      )
    }
    return btnGroup;
  }

  _getTitle = () => {
    let data = this.props.title;
    if (!data) {
      return;
    }
    let ele = [];
    _.forEach(data, (item, index) => {
      ele.push(
        <span title={item} className='operframe-titleText'>{item||"--"}</span>
      )
    })
    return ele;
  }

  _onClickBtn = (event, data, name) => {
    let comValue = null;
    comValue = {
      ...data,
      ...this.props,
      name: name,
      productId: this.props.productId
    };
    this.props.onClickBtn && this.props.onClickBtn(event, comValue);
  }

  _onClickCheckBox = (event, data) => {
    let comValue = null;
    comValue = {
      ...data,
      ...this.props,
      productId: this.props.productId
    };
    this.props.onClickCheckBox && this.props.onClickCheckBox(event, comValue);
  }

  _onClickBoxBtn = (event, data) => {
    let comValue = null;
    comValue = {
      ...data,
      ...this.props,
      productId: this.props.productId
    };
    this.props.onClickBoxBtn && this.props.onClickBoxBtn(event, comValue);
  }

  _onChangePage = (event, data) => {
    let componentId = this.props.ComponentId;
    let _data = {
      accountIndex: data,
      productId: this.props.productId
    }
    if (this.props.onChangeEvent) {
      this.props.onChangeEvent(event, _data);
    }
  }
}

HyOperationFrameCom.propTypes = {

};

export default HyOperationFrameCom;