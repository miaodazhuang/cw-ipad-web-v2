import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Button, Popup, Checkbox, Icon } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import _ from 'lodash';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/featurecoms/hymodelboxcom';
import ja_jp from 'ja_jp/components/featurecoms/hymodelboxcom';
import zh_cn from 'zh_cn/components/featurecoms/hymodelboxcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyModelBoxCom extends Component {
        static propTypes = {
            // 示例：boxData为必传，弹框数据
            // [
            //{
            // fixed: "right", left:列表左部  right:列表右部
            // title: "操作",//列名
            //checkbox:该列是否是checkbox列
            //}
            // ]
            boxData: PropTypes.array,
            // 自定义className
            className: PropTypes.string,
            // 组件的保存事件
            onSaveClick: PropTypes.func,
        }
        static defaultProps = {}
        constructor(props) {
            super(props);
            this.state = this._dealBoxData( this.props.boxData );
        };
        componentWillReceiveProps( nextProps ) {
            //如果boxData之前没有配置了boxData或只配置了一个checkbox列， 代表columnData是异步获取，重新处理boxData
            if ( ( this.props.boxData.length === 0 || ( this.props.boxData.length === 1 && this.props.boxData[0].checkbox ) )
                && this.props.boxData !== nextProps.boxData ) {
                let data = this._dealBoxData( nextProps.boxData );
                this.setState({
                    ...data
                });
            }
        }

        render() {
            // console.log(this.props,'===========')
            let custoClassName = `HyModelBox ${this.props.className || ''}`
            return (
                <div className={custoClassName}>
                    <Popup
                        className='HyModelBox-content'
                        position='bottom right '
                        trigger={< Icon name = 'ordered list' />}
                        content={<div>
                                    <div className='HyModelBox-Lstyle'>{this._Lcontent()}</div>
                                    <div className = 'HyModelBox-Rstyle'> {this._Rcontent()}</div>
                                    <Button.Group className='HyModelBox-Btn'>
                                        <Button className='HyModelBox-Btnstyle' color='green' compact onClick={this._saveClick}>{languageConfig.save}</Button >
                                        <Button className='HyModelBox-Btnstyle' compact onClick={this._cancelClick}>{languageConfig.cancel}</Button>
                                    </Button.Group>
                                </div>}
                        on='click'
                        open={ this.state.isOpen }
                        onClose={ this.handleClose }
                        onOpen={ this.handleOpen }/>
                </div>
            );
        }
        /**
         * 根据fixed属性处理boxData
         */
        _dealBoxData = ( boxData ) => {
            let upperhalf = [ ],
            bottomhalf = [ ];
            _.forEach( boxData, ( node, index ) => {
                if ( node.fixed === "right" || node.checkbox )  return
                node = Object.assign( {}, node )
                node.fixed === 'left' ?upperhalf.push( node ):bottomhalf.push( node );
                if ( !node.isShow ) { node.isShow = true; }
            } )
            return {
                Upperhalf: upperhalf,
                Bottomhalf:bottomhalf,
                isOpen: false
            };
        }
        /**
         * 复选框数据处理
         */
        checkChange = (flag,e, data) => {
            e.nativeEvent.stopImmediatePropagation( );
            let halfData = [];
            let half;
            flag===0?half = _.clone( this.state.Upperhalf ):half = _.clone( this.state.Bottomhalf );
            _.forEach( half, ( item, index ) => {
                if ( item.title === data.label ) {
                    item.isShow = !item.isShow;
                }
                halfData.push(item);
            } )
            flag===0?this.setState({ Upperhalf: halfData}):this.setState({Bottomhalf: halfData})
        }

        renderContent =(data,flag)=>{
            let content = [];
            let up = 'up';
            let down = 'down';
            _.forEach( data, ( item, i ) => {
                content.push(
                    <div className='HyModelBox-linecontent' key={i}>
                        <Checkbox className='HyModelBox-checkbox' defaultChecked = {item.isShow} label={item.title} onChange={this.checkChange.bind(this,flag)}/>
                        {item === _.head(data)
                            ? <Icon title={languageConfig.invalid} disabled name='long arrow up'/>
                            : <Icon title={languageConfig.moveUpward} name='long arrow up' onClick={this._clickArrow.bind(this, data, i,flag,up)}/>
                        }
                        {item === _.last(data)
                            ? <Icon title={languageConfig.invalid} disabled name='long arrow down'/>
                            : <Icon title={languageConfig.moveDown} name='long arrow down' onClick={this._clickArrow.bind(this, data, i,flag,down)}/>}
                        {flag?
                        <Icon name='upload' title={languageConfig.moveToTheTop} onClick={(e) => this._LtoR(e, data, i,flag)}/>
                        :<Icon name='download' title={languageConfig.moveToTheBottom} onClick={(e) => this._LtoR(e, data, i,flag)}/>}
                    </div>
                )
            })
            return content;
        }
        //弹框上部content
        _Lcontent = ( ) => {
            let Upperhalf = _.clone( this.state.Upperhalf );
            let flag = 0;
            return this.renderContent(Upperhalf,flag);
        };

        //弹框下部content
        _Rcontent = ( ) => {
            let Bottomhalf = _.clone( this.state.Bottomhalf );
            let flag = 1;
            return this.renderContent(Bottomhalf,flag);
        };


        //上下移动
        _LtoR = ( e, data, index ,flag) => {
            // console.log(data,index,'移动到下面')
            e.nativeEvent.stopImmediatePropagation( );
            let Lall = data;
            let Ldel = _.remove( data, ( node, i ) => {
                return i === index;
            } );
            flag===0?
            this.setState( {
                Upperhalf: Lall,
                Bottomhalf: _.concat( this.state.Bottomhalf, Ldel ),
                isOpen: true
            } ):
            this.setState( {
                Upperhalf: _.concat( this.state.Upperhalf, Ldel ),
                Bottomhalf: Lall,
                isOpen: true
            } )
        }

        //点击上下箭头
        _clickArrow = ( data, index,flag,dir ) => {//先删除当前项，然后再添加
            //  console.log(data,index,'上箭头')
            let current = data;
            let handel = _.remove( data, ( node, i ) => {
                return i === index;
            } );
            dir==='up'?current.splice(index-1, 0,handel[0]):current.splice(index+1, 0,handel[0])
            flag===0?
            this.setState({
                Upperhalf: current,
                isOpen: true
            }):
            this.setState({
                Bottomhalf: current,
                isOpen: true
            })
        }

        //点击按钮关闭
        handleClose = ( event, data ) => {
            this.setState( {
                isOpen: false
            } )
        }
        //点击按钮打开
        handleOpen = ( event, data ) => {
            this.setState( { isOpen: true } )
        }
        // 取消按钮
        _cancelClick = ( ) => {
            this.handleClose( );
        }
        //保存按钮
        _saveClick = ( e ) => {
            e.nativeEvent.stopImmediatePropagation( );
            if ( this.props.onSaveClick ) {
                let updateUpperhalf = _.cloneDeep( this.state.Upperhalf );
                let updateBottomhalf = _.cloneDeep( this.state.Bottomhalf );
                let updatedData = _.concat( updateUpperhalf, updateBottomhalf );
                let updatecolumnData = [ ];
                _.forEach( updateUpperhalf, ( node, index ) => {
                    node.fixed = 'left';
                } )
                _.forEach( updateBottomhalf, ( node, index ) => {
                    node.fixed = null;
                } )
                _.forEach( updatedData, ( node, index ) => {
                    if ( node.isShow === true ) {
                        delete node.isShow;
                        updatecolumnData.push( node )
                    }
                } )
                this.props.onSaveClick( e, updatecolumnData );
            }
            this.setState( { isOpen: false } )
        }

}

export default HyModelBoxCom;