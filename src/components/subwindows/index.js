import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'dva';
import HyModalCom from 'Hycomponents/featurecoms/hymodalcom';


//#region 业务模块

//销售点选择列表
import Subsubsalespointselection from './subsalespointselection';
//使用房含-有可用房含
import Subsubusingpackage from './subusingpackage'
//无可用房含
import Subsubaddpackage from './subaddpackage';
//使用房含
import Subusing_package from './using_package';
//开台
import Subusing_opentable from './using_opentable';
//#endregion

class HySubWindowCom extends Component {

  static contextTypes = {
    app: PropTypes.object
  }

  render() {
    const { modalkey, modalParams, customClassName, dispatch, size, closeIcon, ...modalProps } = this.props;
    if (modalkey === "") {
      return null;
    }
    // 整理size属性
    if (_.trim(size) !== "") {
      modalProps["size"] = size;
    }
    let _closeIcon = true;
    if (closeIcon !== undefined && closeIcon !== true) {
      _closeIcon = false;
    }


    const SubComponent = new this.getComponent(modalkey);
    return (
      <HyModalCom closeOnDimmerClick={false} closeOnEscape={false} closeOnRootNodeClick={false} isOpen={true} closeIcon={_closeIcon}  {...modalProps} onClose={this.onClose}>
        <SubComponent modalParams={this.props.modalParams} onClose={this.onClose} />
      </HyModalCom>
    );
  }

  getComponent = (key, modalParams, customContentStyle, customClassName) => {
    let component = null;
    switch (key) {
      case 'subsalespointselection':
        component = Subsubsalespointselection(this.context.app);
        break;
      case 'subusingpackage':
        component = Subsubusingpackage(this.context.app);
        break;
      case 'subsubaddpackage':
        component = Subsubaddpackage(this.context.app);
        break;
      case 'using_package':
        component = Subusing_package(this.context.app);
        break;
      case 'using_opentable':
        component = Subusing_opentable(this.context.app);
        break;
      default:
        component = null;
        break;
    }
    return component;
  }

  onClose = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: "",
      allkey: "",
      title: "",
      customClassName: "",
      customContentStyle: null,
      params: null
    })
    //实现popup上弹窗不关闭pop浮层功能
    //const func = this.props.modalParams && this.props.modalParams.setCloseOnDocumentClick;
    if (this.props.modalParams && _.has(this.props.modalParams, 'setCloseOnDocumentClick') && _.isFunction(this.props.modalParams.setCloseOnDocumentClick)) {
      const func = this.props.modalParams.setCloseOnDocumentClick;
      if (func) {
        setTimeout(() => {
          func(true);
        }, 300)
      }
    }
  }
}



function mapStateToProps({ SystemModel }) {
  return {
    // 需要弹出的子窗口
    modalkey: SystemModel.modalwindowstate.key,
    modalallkey: SystemModel.modalwindowstate.allkey,
    title: SystemModel.modalwindowstate.title,
    customContentStyle: SystemModel.modalwindowstate.customContentStyle,
    customClassName: SystemModel.modalwindowstate.customClassName,
    modalParams: SystemModel.modalwindowstate.params,
    size: SystemModel.modalwindowstate.size,
    closeIcon: SystemModel.modalwindowstate.closeIcon,
    className: SystemModel.modalwindowstate.className
  };
}

export default connect(mapStateToProps)(HySubWindowCom);;
