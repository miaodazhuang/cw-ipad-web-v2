import dynamic from 'Hyutilities/dynamicload_help';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subusingpackage';
import ja_jp from 'ja_jp/subusingpackage';
import zh_cn from 'zh_cn/subusingpackage';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default(app) => {
  return dynamic({
    app: app,
    title: languageConfig.title,
    isModal: true,
    models: () => [import ("Hymodels/subusingpackage")],
    component: () => import ('./component')
  });
}