/**
 * systemName: 使用房含
 * author:
 * remark:
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import _ from 'lodash';
// import { FormatMoney } from 'Hyutilities/money_helper'
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyItemContentCom from 'Hycomponents/featurecoms/hyitemcontentcom';
import HyBtnInputCom from 'Hycomponents/featurecoms/hybtninputcom';
import { Modal, Card } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subusingpackage';
import ja_jp from 'ja_jp/subusingpackage';
import zh_cn from 'zh_cn/subusingpackage';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class Subsubusingpackage extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {}

  /**
   * props属性定义
   */
  static propTypes = {}

  componentDidMount() {
    // debugger
    this.props.dispatch({
      type: "Msubusingpackage/init",
      ...this.props.modalParams,
    })
  }
  render() {
    const { acctNo, guestNm, balAmt, totalData, acctStus } = this.props;
    let contentitem = this._handelContent(this.props.contentitem);
    let summation = 0;
    let totalInfo = this._getTotalInfo();
    if (totalData) {
      _.forEach(totalData, (node, index) => {
        summation = summation + (parseInt(node.packageQuant) * node.packagePrice)
      })
    }
    return (
      <div className="sub-view UsingPackage">
        <div className="msg">
          <p>{`${languageConfig.roomNo}：${ this.props.modalParams.roomNumber}`}</p>
          <p>{`${languageConfig.name}：${guestNm||null}`}</p>
          <p>{`${languageConfig.balance}：${balAmt}`}</p>
        </div>
        <div className="title">
          <div className="startview">
          <div className="line"/>
          <p className='P-class'>{languageConfig.availableRoomFlag}</p>
          </div>
          {
            acctStus === 'OUT' ? null: 
            (<div className="postview" onClick={this.initPost}>
              <span >
                {languageConfig.givePay}
              </span>
            </div>)
          }

        </div>
        <HyItemContentCom
          customClassName="container"
          itemContent={_.values(contentitem)}
          summation={summation}
          hiddenAddButton={true}
          isClickAble={true}
          isHandleIcon={true}
          isNewAddBefore={false} />
        <div className='sum'>
          {/*<p className='all'>{`${languageConfig.summation}:${FormatMoney(summation)}`}</p>*/}
          <p className='totalInfo'><span>{languageConfig.package}</span> {totalInfo.package}</p>
          <p className='totalInfo'><span>{languageConfig.extraPackage}</span> {totalInfo.extra}</p>
        </div>
        <Modal.Actions key="btn" className="modal_actions">
          <HyButtonCom keyboard="GF11" key='submitBtn' name='submitBtn' onClick={this.onSubmit}>{languageConfig.sure}</HyButtonCom>
          <HyButtonCom key='cancelBtn' name='cancelBtn' onClick={this.onCancel}>{languageConfig.cancel}</HyButtonCom>
        </Modal.Actions>
      </div>
    );
  }

  initPost = () =>{
    // this.props.dispatch({
    //   type: "SystemModel/updateModalWindowState"
    // },()=>{
      this.props.dispatch({
        type: "SystemModel/updateModalWindowState",
        key: 'subsubaddpackage',
        title: languageConfig.useRoomContains,
        size: 'large',
        params: {
          roomNumber: this.props.modalParams.roomNumber,
          resName: this.props.modalParams.resName,
          surplus: this.props.modalParams.surplus,
          acctNo: this.props.modalParams.acctNo,
          cbDispatch: "Mpostableview/queryList",
          cbParams: {
            successmessage: languageConfig.successmessage
          }
        },
      })
    // },1000)
  }
  /**
   * 监听表单的焦点离开事件
   *
   * @memberof GroupLogin
   */
  onBlur = (event, data) => {
    this.props.dispatch({
      type: "M01324010A030LP01001/updateFormData",
      formData: data.formData
    })
  }

  // 合计
  _getTotalInfo = () => {
    let _submmitdata = this.props.totalData;
    let info = {};
    let obj = [];
    let extra = [];
    // {_.multiply(item.packageQuant, item.packagePrice)}
    _.forEach(_submmitdata, (item, index) => {
      if(item.pkgFlg == '1'){
        extra. push(<span>{item.packageNm} ×{item.packageQuant} </span>)
      }else{
        obj.push(<span>{item.packageNm} ×{item.packageQuant} </span>)
      }
    })
    info.extra = extra;
    info.package = obj;
    return info
  }

  _handelContent = (data) => {
    return (_.forEach(data, (node, index) => {
      node.textCBfunc = this._textCBfunc
    }))
  }
  //自定义itemcontent显示的内容
  _textCBfunc = (index, item) => {
    //  debugger
    let totalData = this.props.totalData;
    let current = _.find(totalData, (node, index) => { return (node.packageId == _.last(item.textData).packageId && node.pkgFlg == _.last(item.textData).pkg_flg)});
    let textContent = [];
    {item.textData[item.textData.length-1].pkg_flg === '1' ? textContent.push(<Card.Description>{languageConfig.extraPackage}</Card.Description>) : textContent.push(<Card.Description>{languageConfig.package}</Card.Description>)}
    _.map(item.textData, (node, i) => {
      if (node.hasOwnProperty('value')) {
        textContent.push(<Card.Description key={i}>{node.value}</Card.Description>)
      }
    })
    textContent.push(
      <HyBtnInputCom className="submitCount"
                     max={_.last(item.textData).packageCnt}
                     min={0}
                     readOnly={true}
                     isConstraint={true}
                     onChange = {this._inputChangeEvent.bind(this,_.last(item.textData))}
                     value={current?current.packageQuant:0} />
    )
    return textContent
  }
  _inputChangeEvent = (currentData, event, data) => {
    // debugger
    let totalSubmitData = this.props.totalSubmitData;
    let totalData = this.props.totalData;
    let selectobj = _.find(totalSubmitData, (node, index) => { return node.packageId == currentData.packageId && node.pkgFlg == currentData.pkg_flg });
    let select = _.find(totalData, (node, index) => { return node.packageId == currentData.packageId && node.pkgFlg == currentData.pkg_flg });
    if (selectobj !== undefined && select !== undefined) {
      let selectObjIndex = totalSubmitData.indexOf(selectobj);

      let selectIndex = totalData.indexOf(select);
      selectobj.quantity = parseInt(data.value);
      select.packageQuant = parseInt(data.value);
      totalData.splice(selectIndex,1, select);
      totalSubmitData.splice(selectObjIndex,1,selectobj);
    } else {
      totalData.push({ pkgFlg: currentData.pkg_flg, packageNm: currentData.packageNm, packageId: currentData.packageId, packageQuant: parseInt(data.value), packagePrice: currentData.price, packageCnt: currentData.packageCnt });
      totalSubmitData.push({ pkgFlg: currentData.pkg_flg, packageId: currentData.packageId, quantity: parseInt(data.value) })
    }
    totalData = _.filter(totalData,(item,index)=>{
      return `${item.packageQuant}`!== '0';
    })
    totalSubmitData = _.filter(totalSubmitData,(item,index)=>{
      return `${item.quantity}`!== '0';
    })
    this.props.dispatch({
      type: 'Msubusingpackage/submitData',
      param: {
        totalSubmitData: totalSubmitData,
        totalData: totalData
      }
    });
  }
  /**
   * 提交数据
   *
   * @memberof Sub01328025A020LP00301
   */
  onSubmit = (event, data) => {
    // debugger
    let have = true;
    _.forEach(this.props.totalData, (node, index) => {
      if (node.packageQuant == '0') {
        have = false;
      }
    })
    if (_.size(this.props.totalData) > 0 && have) {
      let _dispatch = {
        type: "Msubusingpackage/submitAllData",
        ...this.props.modalParams,
        successmessage: languageConfig.package_success_message
      };
      this.props.dispatch(_dispatch);
    }
    else {
      this.props.dispatch({
        type: "SystemModel/updateMessageState",
        statecode: 4,
        message: languageConfig.package_select_message
      })
    }

  }
  // 取消
  onCancel = (event, data) =>{
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState"
    })
  }
}

function mapStateToProps({ Msubusingpackage}) {
  return {
    acctNo: Msubusingpackage.acctNo,  //  客人编号
    guestNm: Msubusingpackage.guestNm, // 客人名
    balAmt: Msubusingpackage.balAmt, // 余额
    acctStus: Msubusingpackage.acctStus, // 订单状态
    contentitem: Msubusingpackage.contentitem,
    totalData: Msubusingpackage.totalData,
    totalSubmitData: Msubusingpackage.totalSubmitData,
  };
}

export default connect(mapStateToProps)(Subsubusingpackage);
