/**
 * systemName: 无可用房含
 * author: zhanghg
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { GetLanguage } from 'Hyutilities/language_helper';
import HyTableItemCom from 'Hycomponents/featurecoms/hytableitemcom';
import { Segment, Modal, Card } from 'semantic-ui-react';
import HyItemContentCom from 'Hycomponents/featurecoms/hyitemcontentcom';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyBtnInputCom from 'Hycomponents/featurecoms/hybtninputcom';
import HyScrollCom from 'Hycomponents/featurecoms/hyscrollcom';
import _ from 'lodash';
import { FormatMoney } from 'Hyutilities/money_helper';
import en_us from 'en_us/subaddpackage';
import ja_jp from 'ja_jp/subaddpackage';
import zh_cn from 'zh_cn/subaddpackage';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class Subsubaddpackage extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {}

  /**
   * props属性定义
   */
  static propTypes = {};

  state = {
    showScroll: false,
    showEndScroll: false,
    showRoomScroll: false,
    showTotalScroll: false,
  }

  componentDidMount() {
    this.props.dispatch({
      type: "Msubaddpackage/init",
      ...this.props.modalParams
    })
  }


  render() {
    const { contentItem, submmitData, roomNumber, resName, surplus } = this.props;
    let contentitem = this._handleContent(contentItem);
    let totalInfo = this.getTotalInfo();
    let summation = 0;
    if (submmitData) {
      _.forEach(submmitData, (node, index) => {
        summation = summation + (parseInt(node.goodsQuant) * node.price)
      })
    }
    return (
      <div className="sub-view addpackage">
        <div className="pack_msg">
          <div className='lineH' />
          {this.props.pseudo_flg === "PF" || this.props.pseudo_flg === "POS" ?
            <div className='msg_info'>
              <p>{`${languageConfig.roomNumberPF}：${roomNumber ? roomNumber : ''}`}</p>
            </div>
            : <div className='msg_info'>
              <p>{`${languageConfig.roomNumber}：${roomNumber ? roomNumber : ''}`}</p>
              <p>{`${languageConfig.resName}：${resName ? resName : ''}`}</p>
              <p>{`${languageConfig.surplus}：${surplus ? surplus : ''}`}</p>
            </div>
          }
        </div>
        <Segment.Group horizontal className="Comprehensive">
          <Segment className="Comprehensive_left">

            <Segment className="segment-header" vertical>
              <p className='P-class'>{languageConfig.household}</p>
            </Segment>
            <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} isRecordPreviousVeriticalPos={false} onScroll={this.onRoomScroll} showVeriticalTrack={true}>
              <HyItemContentCom
                customClassName="container"
                itemContent={_.values(contentitem)}
                btnDisabled={_.size(this.props.blockIds) === 1 ? true : false}
                summation={summation}
                hiddenAddButton={true}
                isClickAble={true}
                isHandleIcon={true}
                isNewAddBefore={false} />
            </HyScrollCom>
          </Segment>
          <Segment className="Comprehensive_right">
            <Segment className="segment-header" vertical>
              <p className='P-class'>{languageConfig.paid}</p>
            </Segment>
            <div className="end-right">

              <div className='subchangetable-content-bottom'>
                {this.getRightCom()}
              </div>
              <div className='end-view'>
                {this.getBankCom()}
              </div>
            </div>
          </Segment>
        </Segment.Group>

        <div className='sum'>
          <p className='account'>{`${languageConfig.usetotal}：${FormatMoney(summation)}`}</p>
          <span style={{ display: `${_.size(this.props.blockIds) === 1 ? '' : 'none'}` }} className='account'>{`${languageConfig.payMoney}：${FormatMoney(this.props.allAmt)}`}</span>
          <div className='totals'>
            <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} isRecordPreviousVeriticalPos={false} onScroll={this.onTotalScroll} >
              <p className='totalInfo'>{totalInfo}</p>
            </HyScrollCom>

          </div>
        </div>

        <Modal.Actions key="btn" className="modal_actions">
          <HyButtonCom keyboard="GF11" key='submitBtn' name='submitBtn' onClick={this.onSubmit}>{languageConfig.sure}</HyButtonCom>
          <HyButtonCom key='cancelBtn' name='cancelBtn' onClick={this.onCancel}>{languageConfig.cancel}</HyButtonCom>
        </Modal.Actions>
      </div>
    );
  }

  /**
   * 滚动事件
   * @param e
   * @param data
   */
  onScroll = (e, data) => {
    if (!this.state.showScroll) {
      this.setState({
        showScroll: true
      }, () => {
        setTimeout(() => {
          this.setState({
            showScroll: false
          })
        }, 1000)
      })
    }
  }

  onEndScroll = (e, data) => {
    if (!this.state.showEndScroll) {
      this.setState({
        showEndScroll: true
      }, () => {
        setTimeout(() => {
          this.setState({
            showEndScroll: false
          })
        }, 1000)
      })
    }
  }

  onRoomScroll = (e, data) => {
    if (!this.state.showRoomScroll) {
      this.setState({
        showRoomScroll: true
      }, () => {
        setTimeout(() => {
          this.setState({
            showRoomScroll: false
          })
        }, 1000)
      })
    }
  }

  onTotalScroll = () => {
    if (!this.state.showTotalScroll) {
      this.setState({
        showTotalScroll: true
      }, () => {
        setTimeout(() => {
          this.setState({
            showTotalScroll: false
          })
        }, 1000)
      })
    }
  }
  /**
   * 右侧显示支付方式
   */
  getRightCom = () => {
    return (

      <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} isRecordPreviousVeriticalPos={false} onScroll={this.onEndScroll} showVeriticalTrack={this.state.showEndScroll}>
        <div className='tableDiv-wrap'>
          <div className='tableDiv'>
            {this._getPackage()}
          </div>
        </div>
      </HyScrollCom>
    )
  }
  /**
   * 获取银行卡类型
   * @returns {*}
   */
  getBankCom = () => {
    return (

      <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} isRecordPreviousVeriticalPos={false} onScroll={this.onScroll} showVeriticalTrack={this.state.showScroll}>
        <div className='tableDiv-wrap'>
          <div className='tableDiv'>
            {this._getBankItem()}
          </div>

        </div>
      </HyScrollCom>
    )
  }

  _getBankItem = () => {
    let coms = [];
    _.forEach(this.props.typeData, (i, index) => {
      let titleArray = [];
      let _text = `${i.text}`;
      titleArray.push(_text);
      let checked = _.find(this.props.typeIds, item => item === i.value) ? true : false;
      coms.push(<HyTableItemCom
        key={`block-${index}`}
        showtopSum={false}
        data={i}
        titleArray={titleArray}
        blackStatus={`0`}
        // className=''
        checked={checked}
        onClick={this._onClickBank}
      ></HyTableItemCom>);
    })
    return coms;
  };

  _getPackage = () => {
    let coms = [];
    _.forEach(this.props.packageData, (i, index) => {
      let titleArray = [];
      let _text = `${i.text}`;
      titleArray.push(_text);
      let checked = _.find(this.props.blockIds, item => item === i.value) ? true : false;
      coms.push(<HyTableItemCom
        key={`block-${index}`}
        showtopSum={false}
        data={i}
        titleArray={titleArray}
        blackStatus={`0`}
        // className=''
        checked={checked}
        onClick={this._onClickBlock}
      ></HyTableItemCom>);
    });
    return coms;
  };

  _onClickBank = (event, data) => {
    let newdata = [];
    let checkIndex = _.findIndex(this.props.typeIds, type => type === data.value);
    if (checkIndex === -1) {
      newdata.push(data.value);
    } else {
      newdata.splice(checkIndex, 1);
    }

    this.props.dispatch({
      type: 'Msubaddpackage/updateBank',
      typeIds: newdata
    })
  }

  /**
   * 右侧item条目的点击事件
   * @param event
   * @param data
   * @private
   */
  _onClickBlock = (event, data) => {
    this.props.dispatch({
      type: 'Msubaddpackage/calcGoods',
      data: data,
    })

  };

  getTotalInfo = () => {
    let _submitdata = this.props.submmitData;
    let obj = [];
    _.forEach(_submitdata, item => {
      obj.push(<span>{item.packageNm} ×{item.goodsQuant} {_.multiply(item.goodsQuant, item.price)}</span>)
    })
    return obj
  };

  _handleContent = (data) => {
    return (_.forEach(data, (node, index) => {
      node.textCBfunc = this._textCBfunc
    }))
  }

  _textCBfunc = (index, item, btnDisabled) => {
    //  debugger
    let submitData = this.props.submmitData;
    let current = _.find(submitData, (node, index) => { return node.packageId == _.last(item.textData).packageId });
    let textContent = [];
    _.map(item.textData, (node, i) => {
      if (node.hasOwnProperty('value')) {
        textContent.push(<Card.Description key={i}>{node.value}</Card.Description>)
      }
    })
    textContent.push(
      <HyBtnInputCom className="submitCount"
        max={9999}
        min={0}
        readOnly={true}
        btnDisabled={btnDisabled}
        isConstraint={true}
        onChange={this._inputChangeEvent.bind(this, _.last(item.textData))}
        value={current ? current.goodsQuant : 0} />
    )
    return textContent
  }
  /***
   * 点击加减的事件
   * @param event
   * @param data
   * @private
   */
  _inputChangeEvent = (currentData, event, data) => {

    let submitData = this.props.submmitData;
    let select = _.find(submitData, (node, index) => { return node.packageId == currentData.packageId });
    if (select !== undefined) {
      let selectIndex = submitData.indexOf(select);
      select.goodsQuant = parseInt(data.value);
      submitData.splice(selectIndex, 1, select);
    } else {
      submitData.push({ packageNm: currentData.packageNm, packageId: currentData.packageId, goodsQuant: parseInt(data.value), price: currentData.price, audittrncdId: currentData.audittrncdId })
    }
    submitData = _.filter(submitData, (item, index) => {
      return `${item.goodsQuant}` !== '0';
    })
    this.props.dispatch({
      type: 'Msubaddpackage/changeSubmit',
      param: {
        submmitData: submitData
      }
    })
  }
  /**
   * 提交数据
   * @param event
   * @param data
   */
  onSubmit = (event, data) => {
    if (_.size(this.props.submmitData) === 0) {
      this.props.dispatch({
        type: "SystemModel/updateMessageState",
        statecode: 4,
        message: languageConfig.choiceroom
      });
      return
    }


    if (this.props.pseudo_flg === "POS" && _.size(this.props.blockIds) === 0) {
      this.props.dispatch({
        type: "SystemModel/updateMessageState",
        statecode: 4,
        message: languageConfig.pseudo_flg_waring
      });
      return
    }
    let da = ''
    _.forEach(this.props.packageData, item => {
      if (item.key === this.props.blockIds[0]) {
        if (item.trn_typ === '890' && _.size(this.props.typeIds) === 0) {
          return da = true
        }
      }
    });
    if (da === true) {
      this.props.dispatch({
        type: "SystemModel/updateMessageState",
        statecode: 4,
        message: languageConfig.creditypt
      });
      return
    }





    this.props.dispatch({
      type: 'Msubaddpackage/submitData',
      acctNo: this.props.modalParams.acctNo,
      cbDispatch: this.props.modalParams.cbDispatch,
      cbParams: this.props.modalParams.cbParams,
    })
  }

  /**
   * 取消关闭弹窗
   */
  onCancel = () => {
    if (this.props.modalParams.cbDispatch) {
      this.props.dispatch({
        type: this.props.modalParams.cbDispatch,
        cbParams: this.props.modalParams.cbParams
      })
    }
    this.props.dispatch({
      type: 'SystemModel/updateModalWindowState'
    })
  }
}

function mapStateToProps({ Msubaddpackage, SystemPosModel }) {
  return {
    contentItem: Msubaddpackage.contentItem,
    submmitData: Msubaddpackage.submmitData,
    roomNumber: Msubaddpackage.roomNumber,
    resName: Msubaddpackage.resName,
    surplus: Msubaddpackage.surplus,
    typeIds: Msubaddpackage.typeIds,
    blockIds: Msubaddpackage.blockIds,
    packageData: Msubaddpackage.packageData,
    typeData: Msubaddpackage.typeData,
    tracksize: SystemPosModel.tracksize,
    allAmt: Msubaddpackage.allAmt,
    pseudo_flg: Msubaddpackage.pseudo_flg,
  };
}
export default connect(mapStateToProps)(Subsubaddpackage);
