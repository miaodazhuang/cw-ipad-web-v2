/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'dva';
import { Modal, Icon } from 'semantic-ui-react';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subsalespointselection';
import ja_jp from 'ja_jp/subsalespointselection';
import zh_cn from 'zh_cn/subsalespointselection';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class Subsubsalespointselection extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {}

  /**
   * props属性定义
   */
  static propTypes = {}
  componentDidMount() {
    this.props.dispatch({
      type: 'Msubsalespointselection/init',
      ...this.props.modalParams
    });
  }
  render() {

    let com = [];
    //没有销售点时显示注销按钮
    if (_.size(this.props.data) === 0 ) {
      com.push(<HyButtonCom key='out' name='out' onClick={this._onClick}>{languageConfig.out}</HyButtonCom>)
    } else if ((_.size(this.props.data) !== 0 && global.__SALEPOINT__ !== null) || this.props.routeParams.pageType === "productFlg") {
      //有销售点并且已经选择了消费点的情况下显示关闭按钮
      com.push(<HyButtonCom key='cancelBtn' name='cancelBtn' onClick={this._onClick}>{languageConfig.cancel}</HyButtonCom>);
    }

    return (
      <div className="sub-view Msubsalespointselection">
        {this._getCom()}
        <Modal.Actions key="btn" className="modal_actions">
          <HyButtonCom keyboard="GF11" key='submitBtn' name='submitBtn' onClick={this._onClick}>{languageConfig.save}</HyButtonCom>
          {com}
        </Modal.Actions>
      </div>
    );
  }


  /**
   * 生成每一个元素
   *
   * @memberof Subsubsalespointselection
   */
  _getCom = () => {
    let coms = [];
    _.forEach(this.props.data, (i) => {
      let text = `${i["data-datasource"].name || i.text}`;
      let _classNmae = this.props.checked.value === i.value ? 'selected' : '';
      let _ico = (_classNmae === "selected" ? <Icon className={`icon14 img405 selectedioc`} /> : null);
      coms.push(
        <div className={`item-div ${_classNmae}`}
          onClick={(event) => { this._onClickPoint(event, i) }}
        >{text}{_ico}</div>
      )
    });
    return (
      <div className='point-div'>{coms}</div>
    )
  }



  /**
   * 选中销售点
   *
   * @memberof Subsubsalespointselection
   */
  _onClickPoint = (event, data) => {
    this.props.dispatch({
      type: "Msubsalespointselection/updateRootStatePropComplete",
      updateObj: {
        checked: {
          ...data
        }
      }
    })
  }





  /**
  * 按钮点击事件
  *
  * @memberof Sub013280SW
  */
  _onClick = async (event, data) => {
    if (data.name === 'submitBtn') {
      if (this.props.checked.sales_id === -1) {
        this.props.dispatch({
          type: "SystemModel/updateMessageState",
          statecode: 4,
          message: this.props.routeParams.pageType === "" ? languageConfig.wring : languageConfig.wring_1,//请选择销售点
        });
        return false;
      }
      this.props.dispatch({
        type: "Msubsalespointselection/saveInitSales",
      })
    } else if (data.name === 'out') {//当前登录的酒店没有销售点是添加注销按钮
      await global.__LOGOUT__();
    } else {
      this.props.dispatch({
        type: "SystemModel/updateModalWindowState"
      })
    }
  }





}

function mapStateToProps({ Msubsalespointselection }) {
  return {
    "data": Msubsalespointselection.data,
    "checked": Msubsalespointselection.checked,
    "routeParams": Msubsalespointselection.routeParams,
  };
}

export default connect(mapStateToProps)(Subsubsalespointselection);