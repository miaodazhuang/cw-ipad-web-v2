/**
 * systemName:
 * author:
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyTableItemCom from 'Hycomponents/featurecoms/hytableitemcom';
import { Modal } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/using_package';
import ja_jp from 'ja_jp/using_package';
import zh_cn from 'zh_cn/using_package';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class Subusing_package extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {}

  /**
   * props属性定义
   */
  static propTypes = {}


  componentDidMount() {
    // debugger
    this.props.dispatch({
      type: "Musing_package/init",
      ...this.props.modalParams,
    })
  }

  render() {
    return (
      <div className="sub-view UsingPackage">

        {this.props.pseudo_flg === "PF" || this.props.pseudo_flg === "POS" ?
          <div className='msg_info'>
            <p>{`${languageConfig.roomNumberPF}：${this.props.room_num ? this.props.room_num : ''}`}</p>
          </div>
          :
          <div className="msg">
            <p>{`${languageConfig.room_num}：${this.props.room_num || ""}`}</p>
            <p>{`${languageConfig.alt_nm}：${this.props.alt_nm || ""}`}</p>
            <p>{`${languageConfig.bal_amt}：${this.props.bal_amt || "0"}`}</p>
          </div>}
        <div className="title">
          <div className="startview">
            <div className="line" />
            <p className='P-class'>{languageConfig.title_keyong}</p>
          </div>
          <div className="postview" onClick={this.initPost}>
            <span >
              {languageConfig.givePay}
            </span>
          </div>
        </div>
        <div className='getRoomCom'>
          {this.getRoomCom()}
        </div>
        <div className={'memo-font'}>
          {this.props.memo ? `${languageConfig.memo}:${this.props.memo}` : null}
        </div>
        <Modal.Actions key="btn" className="modal_actions">
          <HyButtonCom keyboard="GF11" key='submitBtn' name='submitBtn' onClick={this.onSubmit}>{languageConfig.save}</HyButtonCom>
          <HyButtonCom key='cancelBtn' name='cancelBtn' onClick={this.onCancel}>{languageConfig.cancel}</HyButtonCom>
        </Modal.Actions>
      </div>
    );
  }



  /**
   * 生成桌台
   *
   * @memberof Vpostableview
   */
  getRoomCom = () => {
    let coms = [];
    _.forEach(this.props.pkg_list, (i, index) => {
      //显示内容
      let titleArray = [languageConfig.pkg_list];//房含
      titleArray.push(`${i.pkg_nm || ""}`)//名称
      titleArray.push(`${i.pkg_quant || ""}`) //数量
      coms.push(<HyTableItemCom
        key={`black-${index}`}
        className="Using_package"
        showtopSum={false}
        data={i}
        titleArray={titleArray}
        blackStatus={`2`}
      ></HyTableItemCom>);
    })
    return coms;
  }


  //挂账
  initPost = () => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: 'subsubaddpackage',
      title: languageConfig.title,
      size: 'large',
      params: {
        roomNumber: this.props.room_num,
        resName: this.props.alt_nm,
        surplus: this.props.bal_amt,
        acctNo: this.props.acct_no,
        blockItemId: this.props.blockItemId,
        blockKey: this.props.blockKey,
        blockId: this.props.blockId,
        pseudo_flg: this.props.pseudo_flg,
        cbDispatch: "Musingtableview/queryList",
        cbParams: {
          successmessage: languageConfig.successmessage
        }
      },
    })
  }

  /**
 * 提交数据
 *
 * @memberof Sub01328025A020LP00301
 */
  onSubmit = (event, data) => {
    //if (_.size(this.props.pkg_list) > 0) {
    this.props.dispatch({
      type: "Musing_package/submitAllData",
    });
    // }
  }



  // 取消
  onCancel = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState"
    })
  }


}

function mapStateToProps({ Musing_package }) {
  return {
    alt_nm: Musing_package.alt_nm,
    acct_no: Musing_package.acct_no,
    bal_amt: Musing_package.bal_amt,
    room_num: Musing_package.room_num,
    pkg_list: Musing_package.pkg_list,
    blockKey: Musing_package.blockKey,
    blockItemId: Musing_package.blockItemId,
    blockId: Musing_package.blockId,
    pseudo_flg: Musing_package.pseudo_flg,
    memo: Musing_package.memo,
  };
}

export default connect(mapStateToProps)(Subusing_package);