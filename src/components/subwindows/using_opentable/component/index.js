/**
 * systemName:
 * author:
 * remark:
 */
/**
 * systemName:
 * author:
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyFormCom from 'Hycomponents/featurecoms/hyformcom';
import { Modal, Icon } from 'semantic-ui-react';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/using_opentable';
import ja_jp from 'ja_jp/using_opentable';
import zh_cn from 'zh_cn/using_opentable';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });


class Subusing_opentable extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {}

  /**
   * props属性定义
   */
  static propTypes = {}


  componentDidMount() {
    this.props.dispatch({
      type: 'Musing_opentable/init',
      ...this.props.modalParams
    });
  }

  render() {
    return (
      <div className="sub-view Musing_opentable">
        <div className="titleHead">
          {`${this.props.routeParams.data.block_drpt} X ${this.props.routeParams.data.block_quant}`}
        </div>

        <HyFormCom
          ref={c => this.formCom = c}
          elements={this.props.element}
          formData={this.props.formData}
          onChange={this._onBlur}
          onBlur={this._onBlur}>
          <HyButtonCom style={{ height: "28px", padding: '6px 28px' }} className={'query'} onClick={this._query} keyboard="GF03">{languageConfig.queryBtn}</HyButtonCom>
        </HyFormCom>
        <div className='getCom'>
          {this._getCom()}
        </div>
        <Modal.Actions key="btn" className="modal_actions">
          <HyButtonCom keyboard="GF11" key='submitBtn' name='submitBtn' onClick={this.onSubmit}>{languageConfig.save}</HyButtonCom>
          <HyButtonCom key='cancelBtn' name='cancelBtn' onClick={this.onCancel}>{languageConfig.cancel}</HyButtonCom>
        </Modal.Actions>
      </div>
    );
  }




  /**
   * 生成每一个元素
   *
   * @memberof Subsubsalespointselection
   */
  _getCom = () => {
    let coms = [];
    _.forEach(this.props.data, (i) => {
      let room_num = `${i.room_num || ""}`;
      let alt_nm = `${i.alt_nm || ""}`;
      let _classNmae = this.props.checked.acct_no === i.acct_no ? 'selected' : '';
      let _ico = (_classNmae === "selected" ? <Icon className={`icon14 img405 selectedioc`} /> : null);
      coms.push(
        <div className={`item-div ${_classNmae}`}
          onClick={(event) => { this._onClickPoint(event, i) }}
        >
          <div>{room_num}</div>
          <div>{alt_nm}</div>
          {_ico}</div>
      )
    });

    if (_.size(coms) === 0) {
      return (
        <div className='point-notitle'>{languageConfig.notitle}</div>
      )
    } else {


      return (
        <div className='point-div'>{coms}</div>
      )
    }
  }



  /**
 * 表单值更改事件, 第三个参数代表是哪个form需要更新数据，reducer中的方法以update前缀+form的name为方法名
 */
  _onBlur = (event, data) => {
    let formData = data.formData;
    formData = {
      ...data.formData,
    };
    this.props.dispatch({
      type: `Musing_opentable/updateBaseFormData`,
      formData: formData
    })
  }


  _query = () => {
    this.props.dispatch({
      type: `Musing_opentable/queryAcctForBlock`,
    })
  }


  _onClickPoint = (event, data) => {
    this.props.dispatch({
      type: "Musing_opentable/updateRootStatePropComplete",
      updateObj: {
        checked: data
      }
    })
  }



  /**
 * 按钮点击事件
 *
 * @memberof Sub013280SW
 */
  onSubmit = async (event, data) => {
    if (data.name === 'submitBtn') {

      const submitData = this.formCom.GetValue();
      // 如果表单验证失败，则直接返回
      if (submitData.error) {
        return;
      }


      if (!this.props.checked) {
        this.props.dispatch({
          type: "SystemModel/updateMessageState",
          statecode: 1,
          message: languageConfig.waring,//请选择销售点
        });
        return false;
      }
      this.props.dispatch({
        type: "Musing_opentable/openBlock",
      })
    }
  }



  // 取消
  onCancel = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState"
    })
  }





}

function mapStateToProps({ Musing_opentable }) {
  return {
    element: Musing_opentable.element,
    formData: Musing_opentable.formData,
    data: Musing_opentable.data,
    checked: Musing_opentable.checked,
    routeParams: Musing_opentable.routeParams,
  }
}

export default connect(mapStateToProps)(Subusing_opentable);