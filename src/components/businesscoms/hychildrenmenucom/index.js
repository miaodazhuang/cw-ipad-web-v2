import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'dva';
import { BaseComponent } from 'Hyutilities/component_helper';
import HyAccordionMenuCom from 'Hycomponents/featurecoms/hyaccordionmenucom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hychildrenmenucom';
import ja_jp from 'ja_jp/components/businesscoms/hychildrenmenucom';
import zh_cn from 'zh_cn/components/businesscoms/hychildrenmenucom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
// 三四级菜单
@BaseComponent
// 获取dispatch函数
@connect()

class HyChildrenMenuCom extends Component {

  static contextTypes = {
    history: PropTypes.object,
    match: PropTypes.object
  }

  static propTypes = {
    parentId: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ]),
    menuId: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ])
  }

  render() {
    const { parentId, menuId } = this.props;
    if (!global.__USERMENU__ || _.size(global.__USERMENU__) <= 0) {
      return null;
    }

    const packageMenu = this.getChildrenMenuData(global.__USERMENU__, parentId, true);

    if (_.size(packageMenu) > 0) {
      return (
        <HyAccordionMenuCom
          menuData={_.size(packageMenu) > 0 ? packageMenu[0].children : []}
          activeMenuId={menuId}
          onClick={this.onClick}
        ></HyAccordionMenuCom>
      );
    } else {
      return null;
    }
  }


  getChildrenMenuData = (menuData, parent_id, isChildren) => {
    if (_.size(menuData) === 0) {
      return null;
    }
    let dataSource = [];
    _.forEach(menuData, (item, index) => {
      if (`${item.menu_id}` === `${parent_id}`) {
        dataSource.push({
          id: `${item.menu_id}`,
          text: `${item.menu_nm}`,
          url: `${item.menu_url}`,
          children: this.getChildrenMenuData(item.children, item.menu_id, true)
        })
      } else if (`${item.parent_menu_id}` === `${parent_id}`) {
        dataSource.push({
          id: `${item.menu_id}`,
          text: `${item.menu_nm}`,
          url: `${item.menu_url}`,
          children: this.getChildrenMenuData(item.children, item.menu_id, false)
        })
      } else {
        const data = this.getChildrenMenuData(item.children, parent_id, true);
        if (_.size(data) > 0) {
          dataSource = data;
        }
      }
    })
    return dataSource;
  }

  onClick = (event, data) => {
    if (!_.has(data, "url")) {
      return;
    }
    if (data.url === "") {
      alert(languageConfig.menuException);
      return;
    } else if (data.url === "#") {
      alert(languageConfig.nauthorizedAccess);
      return;
    } else if (data.url === "*") {
      alert(languageConfig.invalidAddress);
      return;
    }

    const tabIndex = this.context.match.params.tabIndex;
    const urlParams = {
      "parentId": this.props.parentId,
      "menuId": data.id
    }
    const _encryptStr = global.__ENCRYPT__.Encrypt(escape(JSON.stringify(urlParams)));
    this.props.dispatch({type: "SystemRouterModel/push", url: `/${data.url}/${_encryptStr.orgData}/${_encryptStr.encryptData}`})
  }
}


export default HyChildrenMenuCom;