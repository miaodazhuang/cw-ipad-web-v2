import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'dva';
import { Segment, TransitionablePortal, Icon, Button, Modal, Header } from 'semantic-ui-react';
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';
import { GetLanguage } from 'Hyutilities/language_helper';
import key from 'keymaster';
import en_us from 'en_us/components/businesscoms/hymessagecom';
import ja_jp from 'ja_jp/components/businesscoms/hymessagecom';
import zh_cn from 'zh_cn/components/businesscoms/hymessagecom';
const _key = global.__KEYMASTER__ || key;
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyMessageCom extends Component {

  state = {
    // 消息显示状态
    open: false,
    animation: "fade down",
    duration: 1000
  }

  // 定时器
  timehandle = -1;

  stateForColor = {
    "0": {
      color: "",
      iconname: ""
    },
    "1": {
      color: "#20bb44",
      iconname: "check circle"
    },
    "2": {
      color: "orange",
      iconname: "warning"
    },
    "3": {
      color: "red",
      iconname: "warning"
    },
    "4": {
      color: "red",
      iconname: "warning"
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.statecode > 0 && _.trim(nextProps.message) !== "") {
      clearTimeout(this.timehandle);
      if (this.state.open) {
        this.onOpen(null, null);
      } else {
        this.setState({
          open: true
        })
      }
    } else if (this.props.statecode !== nextProps.statecode && nextProps.statecode === 0) {
      this.setState({
        open: false
      })
    }
  }

  componentWillUnmount(){
    const modalsEle = document.getElementsByClassName("ui page modals")[0];
    modalsEle && modalsEle.firstChild.removeEventListener('focus')
  }

  render() {
    const { animation, duration, open } = this.state;
    const stateInfo = this.stateForColor[this.props.statecode];



    if (this.props.statecode === 1 || this.props.statecode===4) {
      return (
        <TransitionablePortal
          transition={{ animation, duration }}
          onClose={this.onClose}
          onOpen={this.onOpen}
          open={open}>
          <Segment className="successmessage padding-0 margin-0" sizes="mini" vertical basic inverted style={{ left: '40%', position: 'fixed', top: '50px', zIndex: 100000 }}>
            <Segment.Group className="padding-0 margin-0 transparent" horizontal>
              <Segment className="margin-0 icon-segment" vertical basic>
                <Icon name={stateInfo.iconname} />
              </Segment>
              <Segment className="margin-0 text-segment" vertical basic>{this.props.message}</Segment>
              <Segment className="margin-0 button-segment" vertical basic>
                <Button className="dark-bgcolor" icon basic size='mini' onClick={this.closePortal}>
                  <Icon name="remove" />
                </Button>
              </Segment>
            </Segment.Group>
          </Segment>
        </TransitionablePortal>
      )
    } else if (this.props.statecode === 2 || this.props.statecode === 3) {
      let _message = "";
      if (this.props.messageCode) {
        _message = `${this.props.messageCode || ''} : ${this.props.message || languageConfig.content}`;
      } else {
        _message = `${this.props.message || languageConfig.content}`;
      }
      const content = (
        <div className="confirm-content">
          <div className="confirm-content-ico">
            <HyIconCom className="icon20 img410" />
          </div>
          <div className="confirm-content-text">{_message}</div>
        </div>
      )
      return (
        <Modal
          open={open}
          onClose={this.onClose}
          size='small'
        >
          <Header icon='comment' content={languageConfig.header} />
          {content}
          <Modal.Actions>
            <Button onClick={this.onClose}>{languageConfig.button}</Button>
          </Modal.Actions>
        </Modal>
      )
    } else if (this.props.statecode === 100) {
      return (
        <Modal
          open={open}
          onClose={this.onClose}
          size='small'
        >
          <Header icon='protect' content={languageConfig.noPermsHeader} />
          <Modal.Content>
            <Modal.Description>
              {this.props.message}
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={this.onClose}>{languageConfig.button}</Button>
          </Modal.Actions>
        </Modal>
      )
    } else {
      return null;
    }

  }

  /**
   * 消息关闭事件
   */
  onClose = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateMessageState",
      statecode: 0,
      message: "",
      messageCode: "",
    })
  }

  /**
   * 消息打开事件
   */
  onOpen = (event, data) => {
    this.timehandle = setTimeout(() => {
      this.closePortal(null, null);
    }, 3000);
  }

  /**
   * 处理消息关闭逻辑
   */
  closePortal = (event, data) => {
    if (!this.state.open) {
      return;
    }
    this.timehandle > -1 && clearTimeout(this.timehandle);
    this.setState({
      open: false
    })
  }
}




function mapStateToProps({ SystemModel }) {
  return {
    // 异步加载状态
    // 状态代码 0 无消息 1 成功消息 2 警告消息 3 错误消息
    statecode: SystemModel.messagestate.statecode,
    message: SystemModel.messagestate.message,
    messageCode: SystemModel.messagestate.messageCode,//消息编码
  };
}

export default connect(mapStateToProps)(HyMessageCom);