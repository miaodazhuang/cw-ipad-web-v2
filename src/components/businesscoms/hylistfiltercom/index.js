import HyInputCom from "Hycomponents/featurecoms/hyinputcom";
import React from "react";
import PropTypes from 'prop-types';
import {connect} from "dva";
import classNames from "classnames";
import _ from "lodash";
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hylistfiltercom';
import ja_jp from 'ja_jp/components/businesscoms/hylistfiltercom';
import zh_cn from 'zh_cn/components/businesscoms/hylistfiltercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
class HyListFilterCom extends React.Component {
  static propTypes = {
    //model 名称
    namespace: "",
    //数据集合  {key, text}
    dataSource: PropTypes.array,
    //提示信息
    placeholder: PropTypes.string,
    //筛选条件
    filterText: PropTypes.string,
    // 选中项
    checked: "",
    //点击筛选项
    onClick: PropTypes.func
  }
  static defaultProps = {
    dataSource: global.__UNITLIST__ || [],
    placeholder:languageConfig.placeholder,
  }
  state = {
    filterText: "",
    checked: this.props.checked ? this.props.checked : (_.isArray(this.props.dataSource) && this.props.dataSource.length > 0 ? this.props.dataSource[0].value : "")
  }
  componentWillReceiveProps(nextProps){
    if(this.props.filterText !== nextProps.filterText){
      this.setState({
        filterText: nextProps.filterText
      });
    }
    if(this.props.checked !== nextProps.checked){
      this.setState({
        checked: nextProps.checked
      });
    }
  }
  render(){
    return <div className={classNames("list-filter", this.props.className)}>
      <HyInputCom placeholder={this.props.placeholder} value={this.state.filterText} onChange={this._onChange}
        icon={{ link: true, className: "list-filter-icon icon20 img135"}}></HyInputCom>
      {this._getList()}
    </div>
  }

  _getList = () => {
    return (
      <ul className="list-filter-ul">
        {_.isArray(this.props.dataSource) && this.props.dataSource.map((item, index) => {
          if(this.state.filterText && item.text.indexOf(this.state.filterText) === -1) return;
          return <li key={index} className={classNames("list-filter-li", {"lfl-checked": item.value === this.state.checked})} onClick={this._click.bind(this, item)}>{item.text}</li>
        })}
      </ul>
    )
  }

  _onChange = (event, data) => {
    this.setState({
      filterText: data.value
    });
  }

  _click = (item, event) => {
    this.setState({
      checked: item.value
    });
    this.props.onClick && this.props.onClick(event, item);
  }
}

function mapStateToProps(state, defaultProps) {
  const { namespace = "" } = defaultProps;
  return {
    dataSource: state[namespace].listData
  };
}

export default connect(mapStateToProps)(HyListFilterCom);