/**
 * systemName:顶部信息组件
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';
import { Label, Icon } from 'semantic-ui-react';
import { FormatMoment } from 'Hyutilities/moment_helper';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyInputCom from 'Hycomponents/featurecoms/hyinputcom';
import moment from 'moment';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hytopcom';
import ja_jp from 'ja_jp/components/businesscoms/hytopcom';
import zh_cn from 'zh_cn/components/businesscoms/hytopcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyTopCom extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  render() {


    let systemBackgroundColor = "systemTopComColor_1";
    if (this.props.SystemPosModel.salesPointTotal && this.props.SystemPosModel.salesPointTotal["data-datasource"].sales_typ === "2") {
      systemBackgroundColor = 'systemTopComColor_2'
    }

    return (
      <div className={`HyTopCom ${systemBackgroundColor}`} >
        <div className='LeftCom'>{this._getLeftComs()}</div>
        <div className='RightCom'> {this._getRightComs()}</div></div>
    );
  }

  /**
   * 获取左侧信息组件
   *
   * @memberof HyTopCom
   */
  _getLeftComs = () => {
    let coms = [];

    coms.push(<div className={`ja_jp`}></div>)
    //当前销售点
    if (this.props.salesPointTotal) {
      coms.push(<Label className='salesPointTotal' onClick={this._onOpenSales} ><Icon className={'icon25 img7'} />{`${this.props.salesPointTotal["data-datasource"]["name"]}`}</Label>);
    }
    //当前餐点
    if (this.props.period) {
      coms.push(<Label className='salesPointTotal' onClick={this._onOpenProduct} ><Icon className={'icon25 img8'} />{`${this.props.period["data-datasource"]["name"]}`}</Label>);
    }
    //业务日期
    coms.push(<Label className='BUSINESSDT' ><Icon className={'icon25 img6'} />{`${moment(global.__BUSINESSDT__).format(FormatMoment('yyyy-MM-dd'))}`}</Label>);
    //当前操作员
    if (global.__USERINFO__) {
      coms.push(<Label className='USERINFO' ><Icon className={'icon25 img5'} /> {`${global.__USERINFO__.UserNm}`}</Label>);
    }

    //房号查询
    coms.push(<HyInputCom className='fastOpen' value={this.props.SystemPosModel.fastQuery} onClick={this._fastOpen} onChange={this._onChange} name='fastOpen' placeholder={languageConfig.fastOpen} ></HyInputCom>);

    //查询按钮
    coms.push(<Label className='search' onClick={this._searchRoomContains} >{`${languageConfig.search}`}</Label>);

    //
    coms.push(<Label className='search' onClick={this._Reset} >{`Reset`}</Label>);


    return coms;
  }

  /**
   * 获取右侧信息组件
   *
   * @memberof HyTopCom
   */
  _getRightComs = () => {
    let coms = [];
    //注销按钮
    coms.push(
      <Label className='_out' onClick={this._out} >{`ログアウト`}</Label>
    );
    return coms;
  }




  //更新快捷查询条件
  _onChange = (event, data) => {
    let _value = data.value;
    this.props.dispatch({
      type: 'SystemPosModel/changeStatusComplete',
      data: {
        fastQuery: _value
      }
    })
  }


  /**
   * 点击查询按钮事件
   *
   * @memberof HyTopCom
   */
  _onOpenSales = () => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: "subsalespointselection",
      title: languageConfig.subsalespointselection,
      size: "large",
      params: {
        pageType: "salesPointTotal",//选择销售点
      }
    })
  }

  /**
   * 选择餐别
   *
   * @memberof HyTopCom
   */
  _onOpenProduct = () => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: "subsalespointselection",
      title: languageConfig.select,
      size: "large",
      params: {
        pageType: "productFlg",//选择餐别或者时间
      }
    })
  }



  /**
   * 查询房含
   *
   * @memberof HyTopCom
   */
  _searchRoomContains = () => {
    this.props.dispatch({
      type: `${this.props.salesPointTotal['data-datasource']["sales_typ"] === "1" ? 'Mpostableview' : 'Musingtableview'}/queryList`,
    })
  }


  _Reset = () => {
    this.props.dispatch({
      type: 'SystemPosModel/changeStatusComplete',
      data: {
        fastQuery: ""
      }
    })
  }


  /**
   * 注销操作
   *
   * @memberof HyTopCom
   */
  _out = async () => {
    await global.__LOGOUT__();
  }

}

function mapStateToProps({ SystemPosModel, Mpostableview }) {
  return {
    // 异步加载状态
    salesPointTotal: SystemPosModel.salesPointTotal,
    period: SystemPosModel.period,
    Mpostableview: Mpostableview || {},
    SystemPosModel: SystemPosModel || {},
  };
}

export default connect(mapStateToProps)(HyTopCom);