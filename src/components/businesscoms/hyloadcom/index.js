import React, { Component } from 'react';
import { connect } from 'dva';
import { Dimmer } from 'semantic-ui-react';

class HyLoadCom extends Component {

  // 出发blur事件计时器
  timer = null;

  state = {
    open: false
  }

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.statecode === 1) {
  //     this.timer = setTimeout(() => {
  //       this.setState({ open: true });
  //     }, 500);
  //   } else {
  //     clearTimeout(this.timer);
  //     this.setState({ open: false });
  //   }
  // }

  render() {
    if (this.props.statecode === 1) {
      return <Dimmer active inverted page className="custom-load" >
        <div className="rond">
          <div className="ball"></div>
        </div>
        <div className="load">Loading ...</div>
      </Dimmer>
    } else {
      return null;
    }
  }
}

function mapStateToProps({ SystemModel }) {
  return {
    // 异步加载状态
    statecode: SystemModel.fatchstate.statecode,
    message: SystemModel.fatchstate.message
  };
}

export default connect(mapStateToProps)(HyLoadCom);