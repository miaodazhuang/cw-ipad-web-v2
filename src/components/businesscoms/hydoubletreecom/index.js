/**
 * 双树
 */
import React, { Component } from 'react';
import {  Segment } from 'semantic-ui-react'
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyRcTreeCom from 'Hycomponents/featurecoms/hyrctreecom';
import { BaseComponent } from 'Hyutilities/component_helper';
import _ from "lodash";
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hydoubletreecom';
import ja_jp from 'ja_jp/components/businesscoms/hydoubletreecom';
import zh_cn from 'zh_cn/components/businesscoms/hydoubletreecom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
// 组件基础功能注解
@BaseComponent
class HyDoubleTreeCom extends Component {
  static propTypes = {};
  constructor(props) {
    super(props);
    this.state = {
      treeDataLeft: [],
      treeDataRight: [],
      checkedKeysLeft: [],
      checkedKeysRight: [],
      isChange: false
    }
  }

  // 初始化渲染完成后调用
  componentDidMount() {}

  // 当Props或者state发生变更时触发
  componentWillReceiveProps(nextProps) {
    if (this.props.LeftSubTreeData !== nextProps.LeftSubTreeData) {
      let LeftSubTreeData = this._dataHandle(nextProps.LeftSubTreeData)
      this.setState({treeDataLeft: LeftSubTreeData.data})
    }
    if (this.props.RightSubTreeData !== nextProps.RightSubTreeData) {
      let RightSubTreeData = this._dataHandle(nextProps.RightSubTreeData)
      this.setState({treeDataRight: RightSubTreeData.data})
    }
  }

  _OnClickLeft = (checkedKeys, e) => {
    this.setState({
      checkedKeysLeft: checkedKeys
    });
  }
  _OnClickRight = (checkedKeys, e) => {
    this.setState({
      checkedKeysRight: checkedKeys
    });
  }
  /**
   * 整理数据
   * @return {[type]} [description]
   */

  _dataHandle = (data) => {
    let i = data.length - 1;
    for (i; i >= 0; i--) {
      let item = data[i];
      if (item.children && _.size(item.children) === 0) {
        data.splice(i, 1);
      } else {
        if (item.children) {
          let reData = this._dataHandle(item.children);
          if (reData.isDelSelf) {
            data.splice(i, 1);
          } else {
            item.children = reData.data;
          }
        }
      }
    }
    data = _.sortBy(data, (item) => {
      let numdata = +item.key;
      if (isNaN(numdata)) {
        return item.key;
      } else {
        return numdata;
      }
    });
    return {
      data,
      isDelSelf: data.length === 0
    };
  }
  /**
   * 获取数据
   * @return {[type]} [description]
   */
  _getData = (dataDs, checkedKeys) => {
    if (checkedKeys) {
      for (let index = dataDs.length - 1; index >= 0; index--) {
        let dataD = dataDs[index];

        if (dataD.children) {
          this._getData(dataD.children, checkedKeys);
          if (dataD.children.length === 0) {
            dataDs.splice(index, 1)
          }
        } else {
          if (checkedKeys.length !== 0) {
            let checkIndex = _.findIndex(checkedKeys, item => item == dataD.key);
            if (checkIndex === -1) {
              dataDs.splice(index, 1);
            }
          } else {
            dataDs.splice(0)
          }
        }
      }
    }
  }

  /**
   * 删除选中的数据
   * @return {[type]} [description]
   */
  _deleteData = (dataDs, checkedKeys) => {
    if (checkedKeys) {
      dataDs.forEach((dataD) => {
        if (dataD.children) {
          this._deleteData(dataD.children, checkedKeys);
        } else {
          checkedKeys.forEach((dataKey) => {
            _
              .remove(dataDs, function (dataChil) {
                if (dataKey == dataChil.key) {
                  return true;
                }
              });
          });
        }
      });
    }
  }
  /**
   * 增加选中的数据
   * @return {[type]} [description]
   */
  _addData = (target, resource) => {
    if (target.length === 0) {
      for (var n = 0; n < resource.length; n++) {
        target.push(resource[n])
      }
    } else {
      for (var i = 0; i < resource.length; i++) {
        for (var j = 0; j < target.length; j++) {
          if (target[j].key === resource[i].key) {
            if (target[j].children && resource[i].children) {
              this._addData(target[j].children, resource[i].children)
            } else if (!target[j].children) {
              target.splice(i, 1);
              if (target[0].children) {
                target.push(resource[i])
              } else {
                for (var m = 0; m < resource.length; m++) {
                  target.push(resource[m])
                }
              }
            }
            break;
          }
        }
        if (j === target.length) {
          if (target[0]['children']) {
            target.push(resource[i])
          } else {
            for (var m = 0; m < resource.length; m++) {
              target.push(resource[m])
            }
            break;
          }
        }
      }
    }
  }

  /**
   * 移除选中
   * @return {[type]} [description]
   */
  _removeNode = () => {
    let _treeDataLeft = _.cloneDeep(this.state.treeDataLeft);
    let _treeDataRight = _.cloneDeep(this.state.treeDataRight);
    let _treeDataRight1 = _.cloneDeep(this.state.treeDataRight);
    //判断是否选中值
    if (this.state.checkedKeysRight.length !== 0) {
      this._getData(_treeDataRight1, this.state.checkedKeysRight);
      this._dataHandle(_treeDataRight1);
      //删除右侧选中的数据
      this._deleteData(_treeDataRight, this.state.checkedKeysRight);
      // 处理数据
      _treeDataRight = this._dataHandle(_treeDataRight);
      //左侧增加右侧勾选的数据
      this._addData(_treeDataLeft, _treeDataRight1)
      // 处理数据
      _treeDataLeft = this._dataHandle(_treeDataLeft);
      this.props.GetAddedData && this.props.GetAddedData(this, _treeDataRight.data)
      this.setState({treeDataLeft: _treeDataLeft.data, treeDataRight: _treeDataRight.data, isChange: true});
    }
  }

  _getAddedValue = () => {
    const treeDataRight = this
      ._dataHandle(this.state.treeDataRight)
      .data;
    if (!treeDataRight.length) {
      return {Ids: [], isChange: this.state.isChange};
    }
    let Ids = [];
    _.forEach(treeDataRight, data => {
      if (data.children) {
        _.forEach(data.children, subdata => {
          Ids.push(+ subdata.key);
        })
      }
    })

    return {Ids, isChange: this.state.isChange};
  }

  /**
   * 增加选中
   */
  _addNode = () => {
    let _treeDataLeft = _.cloneDeep(this.state.treeDataLeft);
    let _treeDataLeft1 = _.cloneDeep(this.state.treeDataLeft);
    let _treeDataRight = _.cloneDeep(this.state.treeDataRight);
    //判断是否选中值
    if (this.state.checkedKeysLeft.length !== 0) {
      this._getData(_treeDataLeft1, this.state.checkedKeysLeft)
      this._dataHandle(_treeDataLeft1);
      //删除左侧选中的数据
      this._deleteData(_treeDataLeft, this.state.checkedKeysLeft)

      // 处理数据
      _treeDataLeft = this._dataHandle(_treeDataLeft);
      //右侧增加右侧勾选的数据
      this._addData(_treeDataRight, _treeDataLeft1)
      // 处理数据
      _treeDataRight = this._dataHandle(_treeDataRight);

      this.setState({treeDataLeft: _treeDataLeft.data, treeDataRight: _treeDataRight.data, isChange: true});
    }
  }

  // 渲染函数
  render() {
    return (
      <Segment.Group horizontal className='HyDoubleTreeCom'>
        <Segment className='tree-left'>
          <Segment className='tree_title hypaneltitlecom' vertical>{this.props.leftTreeTitle}</Segment>
        {/* 显示左侧列表 */}
          <HyRcTreeCom
            DefaultExpandAll={true}
            treeData={this.state.treeDataLeft}
            ShowIcon={true}
            onClick={this._OnClickLeft}
          />
        </Segment>
        <Segment className='operate-btn'>
          <HyButtonCom onClick={this._addNode}>{languageConfig.right}</HyButtonCom>
          <HyButtonCom onClick={this._removeNode}>{languageConfig.left}</HyButtonCom>
        </Segment>
        <Segment className='tree-right'>
          <Segment className='tree_title hypaneltitlecom' vertical>{this.props.rightTreeTitle}</Segment>
          {/* 显示右侧列表 */}
          <HyRcTreeCom
            DefaultExpandAll={true}
            treeData={this.state.treeDataRight}
            ShowIcon={true}
            onClick={this._OnClickRight}
          />
        </Segment>
      </Segment.Group>
    );
  }
}

export default HyDoubleTreeCom;
