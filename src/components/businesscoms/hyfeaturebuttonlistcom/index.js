import React from "react";
import PropTypes from 'prop-types';
import {connect} from "dva";
import classNames from "classnames";
import {Icon} from 'semantic-ui-react'
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import _ from "lodash";
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hyfeaturebuttonlistcom';
import ja_jp from 'ja_jp/components/businesscoms/hyfeaturebuttonlistcom';
import zh_cn from 'zh_cn/components/businesscoms/hyfeaturebuttonlistcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyFeatureButtonListCom extends React.Component {
  static propTypes = {
    //点击按钮
    onClick: PropTypes.func,
    //自定义icon处理事件
    onClickIcon: PropTypes.func,
    //快捷查询按钮变更
    cbDispatchObj: PropTypes.object
    // quickQueryButton定义 quickQueryButton: PropTypes.shape({   viewUrl: {     text: '',//按钮描述文本     data: [''],//按钮显示的额外文本
    //   value: '',//按钮的查询值     permsCode:'',     authCode: '',     className: '',//按钮的icon默认close     labelPosition:
    // 'right'//按钮icon的位置，默认在右边   }, }),
  }
  static defaultProps = {}

  state = {}

  quickQueryButton = {}

  // componentWillReceiveProps(nextProps) {
  //   let viewUrl = this
  //     .props
  //     .currentTabData
  //     .match
  //     .url
  //     .substring(8, 35);
  //   let currentQuery = {};
  //   if (!this.quickQueryButton[viewUrl]) {
  //     this.quickQueryButton[viewUrl] = {};
  //   }
  //   _.forEach(this.quickQueryButton[viewUrl], (option, key) => {
  //     currentQuery[key] = {
  //       ...option
  //     };
  //   })
  //   let nextQuery = {};
  //   let search = true;
  //   _.forEach(nextProps.quickQueryButton[viewUrl], (option, key) => {
  //     nextQuery[key] = {
  //       ...option
  //     }
  //     if(option.search === false){
  //       search = false;
  //     }
  //   })
  //   debugger
  //   if(!search|| (_.isEmpty(currentQuery) && _.isEmpty(nextQuery))){
  //     return;
  //   }
  //   if ((!_.isEqual(currentQuery, nextQuery) || search ) && nextProps.currentTabData !== 'closeTab') {
  //     const {quickQueryButton} = nextProps;

  //     this.props.onQuickQueryChange && this
  //       .props
  //       .onQuickQueryChange(quickQueryButton[viewUrl])
  //     this.quickQueryButton[viewUrl] = nextProps.quickQueryButton[viewUrl]
  //   }
  // }

  render() {
    const {quickQueryButton} = this.props;
    if (this.props.currentTabData === 'closeTab') {
      return null;
    }
    let viewUrl = this.props.currentTabData.match && this.props.currentTabData.match.url.substring(8, 35);
    return <div className={classNames("feature-button-list-com", this.props.className)}>
      {this._getList(quickQueryButton[viewUrl])}
      {_.size(quickQueryButton[viewUrl]) > 0
        ? <span className='clearSearchCon' onClick={this.initQuery}>{languageConfig.scavengingScreening}</span>
        : null}
    </div>
  }

  _getList = (dataSource) => {
    if (_.size(dataSource) === 0) {
      return null;
    }

    let buttonList = [];
    _.forEach(dataSource, (dataOption, key) => {
      let {
        permsCode,
        authCode,
        className,
        text,
        data,
        value,
        labelPosition
      } = dataOption;
      buttonList.push(
        <HyAuthCom key={value} authCode={authCode}>
          <HyButtonCom
            icon
            {...{ permsCode, authCode }}
            labelPosition={labelPosition || 'right'}
            onClick={this
            ._onClick
            .bind(this, dataOption)}>
            <span className='button-text'>{text}</span>
            <span className='button-data'>{data.join('')}</span>
            <Icon className={className || 'close'} onClick={this
              ._onClickIcon
              .bind(this, key)}/>
          </HyButtonCom>
        </HyAuthCom>
      )
    })

    return (
      <div className='button-wrap'>
        {buttonList}
      </div>
    );
  }

  /**
   *点击ICON处理函数
   *
   * @memberof HyFeatureButtonListCom
   */
  _onClickIcon = (key, e) => {
    const {quickQueryButton} = this.props;
    let viewUrl = this
      .props
      .currentTabData
      .match
      .url
      .substring(8, 35);
    if (this.props.onClickIcon) {
      this
        .props
        .onClickIcon(quickQueryButton[viewUrl])
    } else {
      this
        .props
        .dispatch({type: 'SystemQueryModel/updateQuickQueryButtonEffect', closeKey: true, closeKeyString: key, cbDispatchTyp: this.props.cbDispatchObj.cbDispatchTyp, cbParams: this.props.cbDispatchObj.cbParams})
    }
  }

  /**
   *点击按钮处理函数
   *
   * @memberof HyFeatureButtonListCom
   */
  _onClick = (data, e) => {
    if (this.props.onClick) {
      this
        .props
        .onClick(data, e)
    }
  }

  /**
   *
   * 清空查询条件
   * @memberof HyFeatureButtonListCom
   */
  initQuery = () => {
    let viewUrl = this
      .props
      .currentTabData
      .match
      .url
      .substring(8, 35);
    this
      .props
      .dispatch({type: 'SystemQueryModel/clearUpViewQuickQueryButton', viewUrl, cbDispatchObj: this.props.cbDispatchObj})
  }
}

function mapStateToProps({SystemQueryModel, SystemRouterModel}) {
  return {quickQueryButton: SystemQueryModel.quickQueryButton, currentTabData: SystemRouterModel.currentTabData};
}

export default connect(mapStateToProps)(HyFeatureButtonListCom);
