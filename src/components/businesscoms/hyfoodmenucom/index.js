/**
 * systemName:菜单组件
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyInputCom from 'Hycomponents/featurecoms/hyinputcom';
import HyMenu from './hymenu';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hyfoodmenucom';
import ja_jp from 'ja_jp/components/businesscoms/hyfoodmenucom';
import zh_cn from 'zh_cn/components/businesscoms/hyfoodmenucom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyFoodMenuCom extends Component {
  /**
   * 默认props属性
   */
  static defaultProps = {
    menuData: [],
    showtopSum: false,
    cookBookId: "",
    cookBookNm: "",
    cookBookTyp: [],
    cookBookTypSub: [],
    cookBookMenu: [],
  }

  /**
   * props属性定义
   */
  static propTypes = {
    //是否为点菜使用 true 是(右上显示已点菜的数量 如果当前菜品已沽清则显示沽清标识并不可再点次菜品)
    isMenu: PropTypes.bool,
    //已点菜数据 isMen=true时读取
    // [{
    //   item_quant:5,//数量
    //   item_price_id: 1027,
    //   item_spec_id: 3433,
    // },]
    menuData: PropTypes.array,
    //菜单id
    cookBookId: PropTypes.string,
    //菜单名称
    cookBookNm: PropTypes.string,
    //菜单大分类
    cookBookTyp: PropTypes.array,
    //菜单小分类
    cookBookTypSub: PropTypes.array,
    //菜单数据
    cookBookMenu: PropTypes.array,
  }

  constructor(props) {
    super(props);
    this.state = {
      showType: false,//切换查询状态 bool true :一级二级分类显示  false:全部显示模糊查找
      queryValue: '',//查询值
      cookBookTyp_id: '',//一级菜单
      cookBookTyp_index: 5,
      cookBookTypSub_id: '',//二级菜单
      cookBookTypSub_index: 5,
      cookBookTyp: [],
      cookBookTypSub: [],
    };
  }
  componentWillMount() {
  }

  componentDidMount() {
    let tempArray = [], tempArraySub = [];
    if (_.size(this.props.cookBookTyp) > 5) {
      tempArray = _.slice(this.props.cookBookTyp, 0, this.state.cookBookTyp_index);
    }
    let item = _.head(this.props.cookBookTyp);
    let _cookBookTypId, _cookBookTypSub_id;
    if (item) {
      _cookBookTypId = item.cookbook_typ_id;
      //循环出一级菜单下的二级菜单
      let _cookBookTypSub = _.filter(this.props.cookBookTypSub, (i) => { return i.parent_typ_id === _cookBookTypId });

      if (_.size(_cookBookTypSub) > 5) {
        tempArraySub = _.slice(_cookBookTypSub, 0, this.state.cookBookTypSub_index);
      } else {
        tempArraySub = _cookBookTypSub;
      }

      if (_.size(_cookBookTypSub) !== 0) {
        _cookBookTypSub_id = _.head(_cookBookTypSub).cookbook_typ_id;
      }
      this.setState({
        cookBookTyp_id: _cookBookTypId,//一级菜单
        cookBookTypSub_id: _cookBookTypSub_id,//二级菜单
        cookBookTyp: tempArray,
        cookBookTypSub: tempArraySub,
      })
    }
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {
  }

  render() {
    let _queryValue = _.cloneDeep(this.state.queryValue);
    let menuContentHight = this.state.showType ? '400px' : '300px';
    return (
      <div className='HyFoodMenuCom' >
        <div className='queryDiv'>
          <HyInputCom value={_queryValue} onChange={this._onChange} text={languageConfig.fastquery} placeholder={languageConfig.fastqueryPlaceholder} className='queryInput' name='query' onClick={this._onClickInput} ></HyInputCom>
          {this.state.showType ? <HyButtonCom key='clare' className='queryBtn' name='clare' onClick={this._onClickQueryBtn}><Icon className={'icon25 img11'} /></HyButtonCom> : null}
        </div>
        {this.getMenuOperationCom()}
        <div className='MenuContent' style={
          { height: menuContentHight }
        }>
          {this.getMenuItemCom()}
        </div>
      </div>
    );
  };


  /**
   * 生成菜单类别组件
   *
   * @memberof HyFoodMenuCom
   */
  getMenuOperationCom = () => {
    if (this.state.showType) {
      return null;
    }
    let coms = [];

    let l_btn, r_btn;
    if (_.size(this.props.cookBookTyp) <= 5) {
      l_btn = true;
      r_btn = true;
    } else if (this.state.cookBookTyp_index === 5) {
      l_btn = true;
      r_btn = false;
    } else if (this.state.cookBookTyp_index > 5 && this.state.cookBookTyp_index < _.size(this.props.cookBookTyp)) {
      l_btn = true;
      r_btn = true;
    } else if (this.state.cookBookTyp_index >= _.size(this.props.cookBookTyp)) {
      l_btn = false;
      r_btn = true;
    }
    coms.push(<HyMenu key={'10001'} name='cookBookTyp' onClickMenu={this._onClickMenu} onClick={this.onClick} LeftBtn={l_btn} RightBtn={r_btn} dataSource={this.state.cookBookTyp} id={this.state.cookBookTyp_id}></HyMenu>)
    if (_.size(this.state.cookBookTypSub) <= 5) {
      l_btn = true;
      r_btn = true;
    } else if (this.state.cookBookTypSub_index === 5) {
      l_btn = true;
      r_btn = false;
    } else if (this.state.cookBookTypSub_index > 5 && this.state.cookBookTypSub_index < _.size(this.props.cookBookTypSub)) {
      l_btn = true;
      r_btn = true;
    } else if (this.state.cookBookTypSub_index >= _.size(this.props.cookBookTypSub)) {
      l_btn = false;
      r_btn = true;
    }
    coms.push(<HyMenu key={'10002'} name='cookBookTypSub' onClickMenu={this._onClickMenu} onClick={this.onClick} LeftBtn={l_btn} RightBtn={r_btn} dataSource={this.state.cookBookTypSub} id={this.state.cookBookTypSub_id}></HyMenu>)
    return coms;
  }

  /**
   * 获取单个菜单组件
   *
   * @memberof HyFoodMenuCom
   */
  getMenuItemCom = () => {
    let coms = [];
    if (this.state.cookBookTypSub_id === "") {
      return coms;
    }
    let tempData = [];
    if (this.state.showType) { //查询模式
      if (this.state.queryValue !== "") {
        tempData = _.filter(this.props.cookBookMenu, (i) => {
          return (_.includes(i.menu_nm, this.state.queryValue) || _.includes(i.menu_cd, this.state.queryValue))
        })
      } else {
        tempData = this.props.cookBookMenu;
      }
    } else {
      let allmenu = _.find(this.props.cookBookTypSub, (i) => { return i.cookbook_typ_id === this.state.cookBookTypSub_id });
      //解构出菜单列表
      tempData = allmenu["menu_ids"] || [];
    }
    _.forEach(tempData, (i) => {
      let menuobj = _.find(this.props.cookBookMenu, (m) => { return m.menu_id === i.menu_id });
      //循环单位
      _.forEach(menuobj.prices, (p) => {
        let itemData = {
          ...menuobj,
          //加上单位信息
          "price": p
        };
        //生成组件
        //let menuClear = '';
        let com = null;
        //判断是否添加沽清标志 和 已点菜数量
        if (this.props.isMenu) {
          //menuClear = itemData["price"].CLEAR ? 'Menu-Clear' : '';
          //查询已经点的菜品
          let quant = 0;
          _.forEach(this.props.menuData, k => {
            if (`${k.item_price_id}` === `${p.menu_price_id}`
              && `${k.item_spec_id}` === `${p.menu_spec_id}`
              && `${k.item_id}` === `${i.menu_id}`) {
              quant = _.add(quant, k.item_quant);
            }
          })
          if (quant > 0) {
            com = <span className='MenuSum'>{quant}</span>
          }
        }
        let _class = `MenuDIV`;
        coms.push(
          <div className={_class} onClick={(event) => { this._onClickMenuItem(event, { ...itemData }) }}>
            {itemData["price"].CLEAR ? <div className='Menu-Clear'></div> : null}
            {com}
            <div>{menuobj.menu_cd}</div>
            <div>{menuobj.menu_nm}</div>
            <div>{p.menu_spec_nm}</div>
          </div>
        )
      })
    })
    return coms;
  }

  /**
   * 查询框获得焦点事件
   *
   * @memberof HyFoodMenuCom
   */
  _onClickInput = (event) => {
    this.setState({
      showType: true,
    })
  }

  /**
   * 切换显示状态
   *
   * @memberof HyFoodMenuCom
   */
  _onClickQueryBtn = (event) => {
    this.setState({
      showType: false,
      queryValue: "",
    })
  }

  /**
   * 查询事件
   *
   * @memberof HyFoodMenuCom
   */
  _onChange = (event, data) => {

    this.setState({ queryValue: data.value })
  }

  /**
   * 点击移动菜单
   *
   * @memberof HyFoodMenuCom
   */
  onClick = (event, data) => {
    if (data.name === 'cookBookTyp') {
      let tempArray = [], tempArraySub = [];
      //获取新的位置
      let _cookBookTyp_index = this.state.cookBookTyp_index + data.index;
      let _start_index = _cookBookTyp_index - 5;
      if (_.size(this.props.cookBookTyp) > 5) {
        tempArray = _.slice(this.props.cookBookTyp, _start_index, _cookBookTyp_index);
      }
      let item = _.head(tempArray);
      let _cookBookTypId, _cookBookTypSub_id;
      if (item) {
        _cookBookTypId = item.cookbook_typ_id;
        //循环出一级菜单下的二级菜单
        let _cookBookTypSub = _.filter(this.props.cookBookTypSub, (i) => { return i.parent_typ_id === _cookBookTypId });
        if (_.size(_cookBookTypSub) > 5) {
          tempArraySub = _.slice(_cookBookTypSub, 0, 5);
        } else {
          tempArraySub = _cookBookTypSub;
        }
        if (_.size(_cookBookTypSub) !== 0) {
          _cookBookTypSub_id = _.head(_cookBookTypSub).cookbook_typ_id;
        }
        this.setState({
          cookBookTyp_id: _cookBookTypId,//一级菜单
          cookBookTyp_index: _cookBookTyp_index,
          cookBookTypSub_id: _cookBookTypSub_id,//二级菜单
          cookBookTypSub_index: 5,
          cookBookTyp: tempArray,
          cookBookTypSub: tempArraySub,
        })
      }
    } else if (data.name === 'cookBookTypSub') {
      let tempArraySub = [];
      //获取新的位置
      let _cookBookTypSub_index = this.state.cookBookTypSub_index + data.index;
      let _start_index = _cookBookTypSub_index - 5;
      if (_.size(this.props.cookBookTyp) > 5) {
        tempArraySub = _.slice(this.props.cookBookTypSub, _start_index, _cookBookTypSub_index);
      }
      this.setState({
        cookBookTypSub_index: _cookBookTypSub_index,
        cookBookTypSub: tempArraySub,
      })
    }
  }

  /**
   * 点击菜单
   *
   * @memberof HyFoodMenuCom
   */
  _onClickMenu = (event, data) => {
    if (data.name === 'cookBookTyp') {
      let tempArraySub = [];
      //获取选择的一级菜单id
      let _cookBookTypId = data.cookbook_typ_id;
      let _cookBookTypSub_id = "";
      //循环出一级菜单下的二级菜单
      let _cookBookTypSub = _.filter(this.props.cookBookTypSub, (i) => { return i.parent_typ_id === _cookBookTypId });
      if (_.size(_cookBookTypSub) > 5) {
        tempArraySub = _.slice(_cookBookTypSub, 0, 5);
      } else {
        tempArraySub = _cookBookTypSub;
      }
      if (_.size(_cookBookTypSub) !== 0) {
        _cookBookTypSub_id = _.head(_cookBookTypSub).cookbook_typ_id;
      }
      this.setState({
        cookBookTyp_id: _cookBookTypId,//一级菜单
        cookBookTypSub_id: _cookBookTypSub_id,//二级菜单
        cookBookTypSub_index: 5,
        cookBookTypSub: tempArraySub,
      })
    } else if (data.name === 'cookBookTypSub') {
      //获取选择的一级菜单id
      let _cookBookTypSub_id = data.cookbook_typ_id;
      this.setState({
        cookBookTypSub_id: _cookBookTypSub_id
      })
    }
  }



  /**
   * 点击菜单事件
   *
   * @memberof HyFoodMenuCom
   */
  _onClickMenuItem = (event, data) => {
    if (this.props.onClickMenuItem) {
      //已经沽清的菜品不可点击
      if (this.props.isMenu && data["price"].CLEAR) {
        return null;
      } else {
        console.log(data);
        this.props.onClickMenuItem(event, data);
      }
    }
  }

}

function mapStateToProps({ SystemPosModel }) {
  return {
    // 异步加载状态
    cookBookId: SystemPosModel.cookBook.cookBookId,
    cookBookNm: SystemPosModel.cookBook.cookBookNm,
    cookBookMenu: SystemPosModel.cookBook.cookBookMenu,
    cookBookTyp: SystemPosModel.cookBook.cookBookTyp,
    cookBookTypSub: SystemPosModel.cookBook.cookBookTypSub,
  };
}

export default connect(mapStateToProps)(HyFoodMenuCom);