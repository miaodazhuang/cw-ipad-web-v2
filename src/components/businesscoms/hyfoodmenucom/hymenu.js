/**
 * systemName:菜单显示组件
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import _ from 'lodash';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';

class HyMenu extends Component {
  /**
   * 默认props属性
   */
  static defaultProps = {
    dataSource: [],
    id: 0,
  }

  /**
   * props属性定义
   */
  static propTypes = {
    dataSource: PropTypes.array,//数据源
    id: PropTypes.int//选中id
  }

  constructor(props) {
    super(props);
    this.state = {}
  }
  componentWillMount() {

  }

  componentDidMount() {
  }

  componentWillReceiveProps(nextProps) {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  componentWillUnmount() {
  }

  render() {



    return (
      <div className='HyMenuOperation' >
        <HyButtonCom disabled={this.props.LeftBtn} onClick={(event)=>{this._onClick(event,{index:-5,name:this.props.name})}} className='movebtn'>
        <Icon className={'icon50 img10'} /></HyButtonCom>
        <div className='HyMenuOperation-content'>
          {this.getMenuOperationCom()}
        </div>
        <HyButtonCom disabled={this.props.RightBtn} onClick={(event)=>{this._onClick(event,{index:5,name:this.props.name})}} className='movebtn'>
        <Icon className={'icon50 img9'} />
        </HyButtonCom>
      </div>
    );
  };


  /**
   * 生成菜单类别组件
   *
   * @memberof HyMenu
   */
  getMenuOperationCom = () => {
    let coms = [];
    _.forEach(this.props.dataSource, (i) => {
      coms.push(
        <div kye={`MenuItem-${i.cookbook_typ_id}`} className={`MenuItem ${i.cookbook_typ_id === this.props.id ? 'activeItem' : ''}`}
          onClick={(event) => { this._onClickMenu(event, {...i,name:this.props.name}) }}
        >
          {i.cookbook_typ_nm}
        </div>
      )
    })
    return coms;
  }

  /**
   * 切换菜单
   *
   * @memberof HyMenu
   */
  _onClick = (event, data) => {
    if(this.props.onClick){
      this.props.onClick(event,data);
    }
  }

  /**
   * 点击菜单
   *
   * @memberof HyMenu
   */
  _onClickMenu=(event,data)=>{
    if(this.props.onClick){
      this.props.onClickMenu(event,data);
    }
  }


}

export default HyMenu;