import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import _ from 'lodash';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Segment, Breadcrumb, Icon } from 'semantic-ui-react';
import HyHandleCom from 'Hycomponents/businesscoms/hyhandlecom';
import HyHelpCenterCom from 'Hycomponents/businesscoms/hyhelpcentercom';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';

@BaseComponent
class HyBreadcrumbcom extends Component {

  static propTypes = {
    history: PropTypes.object.isRequired
  }

  render() {
    const { currentRouterData } = this.props;
    const routerSize = _.size(currentRouterData);
    const sections = _.map(currentRouterData, (item, index) => {
      return {
        "key": index,
        "data-url": item.url,
        "content": item.title,
        "onClick": index === routerSize - 1 ? null : this.onClick
      }
    })

    return (
      <Segment basic className="breadcrumb-segment">
        <Breadcrumb divider={<Icon name='right angle' />} sections={sections} />
        <HyAuthCom authCode={'0131-2010-0001-L-A-01'} >
          <div className="breadcrumb-segment-help">
            <HyHelpCenterCom />
          </div>
        </HyAuthCom>
        <HyHandleCom isBreadcrumb={true} />
      </Segment>
    );
  }
  onClick = (event, data) => {
    if (data["data-url"] === "") {
      return;
    }
    this.props.dispatch({type: "SystemRouterModel/push", url: `/${data["data-url"]}`})
  }
}

function mapStateToProps({ SystemRouterModel ,SystemModel}, props) {
  // 定义返回值
  let currentRouterData = null;
  // 查询当前tab数据中的tabindex参数
  const tabIndex = _.get(SystemRouterModel.currentTabData, ["match", "params", "tabIndex"]);
  // 如果找到则使用tabindex获取页签对应数据
  if (_.isUndefined(tabIndex)) {
    currentRouterData = [];
  } else {
    currentRouterData = _.assign([], _.get(SystemRouterModel.tabData, [tabIndex, "routerData"]));
  }


  return {
    currentRouterData: _.isUndefined(currentRouterData) ? [] : currentRouterData,
    tabIndex: _.isUndefined(tabIndex) ? 0 : tabIndex
  };
}

export default connect(mapStateToProps)(HyBreadcrumbcom);