import React, { Component } from 'react';
import { connect } from 'dva';
import { Confirm, Form, Checkbox } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import HyIconCom from 'Hycomponents/featurecoms/hyiconcom';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';
import HyInteractionCom from 'Hycomponents/featurecoms/hyinteractioncom';
import key from 'keymaster';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hyconfirmcom';
import ja_jp from 'ja_jp/components/businesscoms/hyconfirmcom';
import zh_cn from 'zh_cn/components/businesscoms/hyconfirmcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _key = global.__KEYMASTER__ || key;

/**
 * 确认窗口
 *
 * @class HyConfirmCom
 * @extends {Component}
 */
@BaseComponent
class HyConfirmCom extends Component {

  static propTypes = {
    // 是否打开确认框
    state: PropTypes.bool,
    // 标题 默认是：系统消息
    title: PropTypes.string,
    // 提示消息
    message: PropTypes.bool,
    // 需要会传的参数对象
    cbParams: PropTypes.object,
    //
    elements: PropTypes.object,
    //
    formData: PropTypes.object,
    //
    formClassName: PropTypes.string,
  }

  static defaultProps = {
    filterChangeFieldnames: ["timepicker", "select", "checkbox", "datepicker", "button", "buttonpalette", "birthdate"],
    className: "flex",
    size: "mini",
    saveFormData: true
  }

  componentDidMount() {
    if (_.has(global.__SHORTKEYMAP__, "GF11")) {
      _key(global.__SHORTKEYMAP__["GF11"], (e) => {
        this.handleConfirm(this, {});
      })
    }
  }

  componentWillUnmount() {
    _key.unbind(global.__SHORTKEYMAP__["GF11"], "all");
  }

  render() {

    const { state, title } = this.props.confirmstate;

    const content = this.getcontent();
    return (
      <Confirm
        className="hyconfirmcom"
        cancelButton={languageConfig.cancelButton}
        confirmButton={languageConfig.confirmButton}
        header={title || languageConfig.header}
        content={content}
        open={state}
        size="tiny"
        onCancel={this.handleCancel}
        onConfirm={this.handleConfirm}
      />
    );
  }


  getcontent = () => {
    const { elements, formData, message } = this.props.confirmstate;
    let content = [];
    content.push(<div className="confirm-content">
      <div className="confirm-content-ico">
        <HyIconCom className="icon20 img409" />
      </div>
      <div className="confirm-content-text">{message || languageConfig.content}</div>
    </div>);

    if (elements && _.size(elements) !== 0) {
      const {...defaultProps } = this.props;
      const formProps = this.packageReProps(defaultProps);
      const classname = ClassNames("ui form",formProps.className, formProps.size);
      let elementComponents = null;
      elementComponents = this.getComponents(elements, formData);
      content.push(
        <div className="confirm-content confirm-content-form">
          <div className={classname}>
            {elementComponents}
          </div>
        </div>
      );
    }
    return content;
  }


  getComponents = (elements, formData) => {
    let els = elements || {};
    const elementNode = _.map(els, (item, index) => {
      const { type, className, isMust, width, authOptions = {}, interactionOptions = {}, componentOptions = {} } = item;
      const authComProps = {
        key: index,
        className,
        width,
        ...authOptions
      };
      const _interactionOptions = _.cloneDeep(interactionOptions);
      let value = "";
      value = (_.has(formData, index)
        && !_.isNull(formData[index])
        && !_.isUndefined(formData[index])
      ) ? formData[index] : '';

      // 如果类型为datepicker，则默认为强管控字段
      if (type === "datepicker") {
        // 如果是日期选择控件，需要使用强制管控属性
        _interactionOptions["isConstraint"] = true;
        _interactionOptions["value"] = value;
      } else if (type === "checkbox") {
        // 如果是checkbox控件，则需要转换value为checked的bool类型
        _interactionOptions["checked"] = _.trim(value) === "" ? false : value;
      } else {
        _interactionOptions["value"] = value;
      }
      // 所有控件默认添加点击事件处理
      _interactionOptions["onClick"] = this.onClick;

      if (_.includes(this.props.filterChangeFieldnames, type)) {
        // 根据定义判断当前控件是否为需要出发change事件的控件，为其添加对应事件
        _interactionOptions["onChange"] = this.onChange;
      } else if (type === "input" || type === "textarea") {
        // 获取input控件的开启change事件标识
        let enableChange = _.get(_interactionOptions, "enableChange");

        // 如果开启了change标识，则为input控件添加change事件，否则只需要添加blur事件
        if (!_.isUndefined(enableChange) && enableChange) {
          _interactionOptions["onChange"] = this.onChange;
        } else {
          _interactionOptions["enableChange"] = false;
        }
        _interactionOptions["onBlur"] = this.onBlur;

      } else {
        // 其它控件默认添加change和blur事件
        _interactionOptions["onChange"] = this.onChange;
        _interactionOptions["onBlur"] = this.onBlur;
      }
      // 根据参数获取相应组件
      const _component = this[`get${type}Component`](componentOptions, index);

      const isHyComponent = this.getIsHyComponent(type);

      return (
        <Form.Field {...authComProps} type={type} control={HyAuthCom}>
          <HyInteractionCom
            ref={(c) => this[index] = c}
            fieldname={index}
            isMust={isMust}
            isHyComponent={isHyComponent}
            {..._interactionOptions} >
            {_component}
          </HyInteractionCom>
        </Form.Field>
      )
    })
    return elementNode;
  }


  /**
 * 获取checkbox组件
 */
  getcheckboxComponent = (option = {}, fieldname = "") => {
    const { isMust, showmessage, ...options } = option;
    return <Checkbox
      {...options}
    />
  }
  /**
   * 根据类型获取是否为自定义组件
   *
   * @memberof HyFormCom
   */
  getIsHyComponent = (type) => {
    if (type === "checkbox" || type === "button") {
      return false;
    } else {
      return true;
    }
  }
  /**
 * 焦点离开事件
 */
  onBlur = (event, data) => {
    // 获取state中的表单数据
    let _value = data.value || data.checked;
    if (_.isUndefined(_value)) {
      _value = "";
    }
    this.props.dispatch({
      type: "SystemModel/updateConfirmStateData",
      "formData": {
        ...this.props.confirmstate.formData,
        [data.fieldname]: _value
      }
    })
  }

  /**
   *
   * @param {*} event
   * @param {*} data
   */
  onChange = (event, data) => {
    let _value = data.value || data.checked;
    if (_.isUndefined(_value)) {
      _value = "";
    }
    this.props.dispatch({
      type: "SystemModel/updateConfirmStateData",
      "formData": {
        ...this.props.confirmstate.formData,
        [data.fieldname]: _value
      }
    })

  }




  handleCancel = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateConfirmState"
    })

    if (this.props.confirmstate.dispatchTyp !== "") {
      this.props.dispatch({
        type: this.props.confirmstate.dispatchTyp,
        confirm: false,
        cbParams: this.props.confirmstate.cbParams
      })
    }
  }

  handleConfirm = (event, data) => {

    let formData = _.cloneDeep(this.props.confirmstate.formData);
    this.props.dispatch({
      type: "SystemModel/updateConfirmState"
    })

    if (this.props.confirmstate.dispatchTyp !== "") {
      this.props.dispatch({
        type: this.props.confirmstate.dispatchTyp,
        confirm: true,
        cbParams: {...this.props.confirmstate.cbParams,comformData:{...formData}},
      })
    }
  }


  /**
 * 监听表单的焦点离开事件
 *
 * @memberof GroupLogin
 */
  onBlur = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateConfirmStateData",
      formData: data.formData,
    })
  }







}

function mapStateToProps({ SystemModel }) {
  return {
    confirmstate: SystemModel.confirmstate
  };
}

export default connect(mapStateToProps)(HyConfirmCom);