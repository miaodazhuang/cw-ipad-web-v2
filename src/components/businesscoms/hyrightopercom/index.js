import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'dva';
import ClassNames from 'classnames';
import { BaseComponent } from 'Hyutilities/component_helper';
import { Menu, Portal, Icon, Label, Segment } from 'semantic-ui-react'
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';
// plugin
// 基础插件-功能列表
import Plu01321010A010LS90101 from 'Hycomponents/plugin/01321010A010LS90101';


import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hyrightoper';
import ja_jp from 'ja_jp/hyrightoper';
import zh_cn from 'zh_cn/hyrightoper';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
@BaseComponent
class HyRightOperCom extends Component {

  static contextTypes = {
    app: PropTypes.object
  }

  static defaultProps = {
    // 关联的命名空间名称
    namespace: "",
    // 展开状态
    openStatus: false,
    // 插件编号
    pluginKey: "-1"

  }

  static propTypes = {
    // 命名空间名字为字符串
    namespace: PropTypes.string
  }

  state = {
    // 激活项目索引
    activeKey: "",
    // 显示状态 0:显示提示信息 1:显示子项
    showState: 0,
    // 显示位置
    parentOffset: {},
  }

  pluComponent = null;

  componentDidMount() {
    this.pluComponent = new this.getComponent("-1");
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pluginKey === "-999") {
      this.props.dispatch({
        type: "SystemModel/updateModalWindowState",
        key: "-999",
        title: languageConfig.title,
        size: 'small',
        params: {

        },
      })
      return
    }
    if (nextProps.openStatus === false) {
      this.setState({ "activeKey": "" });
    }

    if (nextProps.pluginKey !== this.props.pluginKey) {
      this.pluComponent = new this.getComponent(nextProps.pluginKey);
    }
  }

  componentWillUnmount() {
    this.props.dispatch({
      type: "SystemPluginModel/changeOpenStatus",
      pluginKey: "-1",
      openStatus: false
    })
  }

  render() {
    const {
      children,
      topList,
      list,
      buttomList,
      defaultSelectFirst,
      rightOperTop,
      rightOperCenter,
      rightOperBottom,
      openStatus,
      pluginKey,
      dispatch,
      onClick,
      ...componentProps } = this.props;
    const { activeKey, parentOffset, showState } = this.state;

    const PluComponent = this.pluComponent;

    // 获取主菜单数组对象
    const menuComs = this.getMenuList(rightOperCenter, activeKey, defaultSelectFirst);
    const menuTopComs = this.getTopMenuList(rightOperTop);
    // 根据激活项目索引，查找菜单子项
    const { open, portalChildren } = this.getChildrenMenuData(activeKey, showState);
    const className = ClassNames("right-segment", { "open": openStatus });
    return (
      <div className={className} onMouseEnter={this.onMouseEnter} onClick={this.onClose.bind(this, "aa")}>
        <div key="1" className="rightmenu" onClick={this.onPareClick}>
          <Menu key="menu" vertical className="rightoper-menu" {...componentProps}>
            {!openStatus && menuTopComs}
            <div className="top"></div>
            <HyButtonCom keyboard={"GF05"} className="top-logo" onClick={this.logoClick}></HyButtonCom>
            {menuComs}
            <Portal
              className="rightoper-portal"
              onClose={this.handlePortalClose}
              open={open} >
              <div
                ref={this.nodeRef}
                className={showState === 0 ? "prompt-portal" : "menu-portal"}
                onMouseOver={this.onPortalMouseover}
                style={{ "position": "fixed", "top": `${parentOffset.top}px`, "left": `${parentOffset.left}px` }}>
                {portalChildren}
              </div>
            </Portal>
          </Menu>
          {
            openStatus ?
              <Segment key="children" basic attached className="rightoper-children">
                {
                  children ?
                    children :
                    <PluComponent namespace={this.props.namespace} parentOnClick={this.onClick}></PluComponent>
                }
              </Segment>
              : null
          }
        </div>
        <div key="0" className="keyboard-none">{this.renderHiddenCom(rightOperTop, rightOperCenter, rightOperBottom)}</div>
      </div>
    );
  }

  getComponent = (key) => {
    let component = null;

    switch (key) {
      // 基础插件
      case "-1":
        component = Plu01321010A010LS90101(this.context.app);
        break;
      default:
          component=null;
        break;
    }
    return component;
  }

  // 鼠标移入时先时鼠标焦点移开当前激活元素
  onMouseEnter = (event, data) => {
    document.activeElement.blur();
  }

  // 获取顶部菜单
  getTopMenuList = (list) => {
    if (_.size(list) === 0) {
      return null;
    }

    const topList = _.map(list, (item, index) => {
      const { permsCode, authCode, className, controlFieldName, unitId, badgeMsg = "" } = item;
      return (
        <HyAuthCom key={index} authCode={authCode} controlFieldName={controlFieldName} unitId={unitId}>
          <Menu.Item>
            <HyButtonCom name={index} icon {...{ permsCode, authCode }} onClick={this.onClick}>
              {badgeMsg === "" ? null : <Label circular color='red' size="mini" floating>badgeMsg</Label>}
              <Icon className={className} />
            </HyButtonCom>
          </Menu.Item>
        </HyAuthCom>
      )
    })

    return (
      <Menu.Menu>
        {topList}
      </Menu.Menu>
    );
  }

  /**
   * 获取菜单列表
   *
   * @memberof MenuCom
   */
  getMenuList = (list, activeKey) => {
    const menuList = _.map(list, (item, index) => {
      const { list, permsCode, authCode, className, text, disabled, controlFieldName, unitId, ...componentProps } = item;
      const props = this.packageReProps(componentProps);
      const { showState } = this.state;
      const itemClassName = ClassNames(
        { "subset": _.size(list) > 0 },
        { "active": index === activeKey },
        { "active-tri": (index === activeKey && showState === 0) }
      )
      return (
        <HyAuthCom key={index} authCode={authCode} permsCode={permsCode} funCode={permsCode}  controlFieldName={controlFieldName} unitId={unitId}>
          <Menu.Item
            ref={`menuItem${index}`}
            className={itemClassName}
            {...props}
            onMouseOver={this.onMouseover.bind(this, index)}
            onMouseOut={this.onMouseout.bind(this, index)}
          >
            <div key="0" className="tri-icon"></div>
            <div key="1" className="subset-icon"></div>
            <HyButtonCom
              name={index}
              icon
              key={`ml-btn-${index}`}
              {...{ permsCode, authCode, disabled }}
              onClick={_.size(list) > 0 ? this.onShowSubClick : this.onClick}>
              <Icon className={className} />
              {
                global.__DOCUMENTWIDTH__ <= 1280 ? null : <span>{text}</span>
              }
            </HyButtonCom>
          </Menu.Item>
        </HyAuthCom>
      )
    })

    return (
      <Menu.Menu className="menu-center">
        {menuList}
      </Menu.Menu>
    )
  }

  /**
   * 获取子菜单项目
   *
   * @memberof MenuCom
   */
  getChildrenMenuData = (activeKey, showState) => {
    if (activeKey === "") {
      return { "open": false, "portalChildren": null };
    }

    if (showState === 0 && global.__DOCUMENTWIDTH__ <= 1280) {
      // 如果宽度大于1280则显示提示信息
      // 显示提示信息
      const activeMenu = this.props.rightOperCenter[activeKey];
      const childrenTitle = activeMenu.text;
      return { "open": true, "portalChildren": childrenTitle };

    } else if (showState === 0 && global.__DOCUMENTWIDTH__ > 1280) {
      // 如果小于1280则不显示提示信息
      return { "open": false, "portalChildren": null };
    } else if (showState === 1 && _.has(this.props.rightOperCenter, activeKey)) {
      // 显示菜单子项
      const { list } = this.props.rightOperCenter[activeKey];
      // 如果不包含子项，则直接返回
      if (_.size(list) <= 0) {
        return { "open": false, "portalChildren": null };
      }
      const childrenMenu = (
        <Menu vertical>
          {
            _.map(list, (item, index) => {
              const { permsCode, authCode, text, className } = item;
              return (
                <HyAuthCom authCode={authCode} funCode={permsCode} >
                  <Menu.Item>
                    <HyButtonCom
                      name={index}
                      parentname={activeKey}
                      key={`mcl-btn-${index}`}
                      icon
                      {...{ permsCode, authCode }}
                      onClick={this.onClick}>
                      <Icon className={className} />
                      <span>{text}</span>
                    </HyButtonCom>
                  </Menu.Item>
                </HyAuthCom>
              )
            })
          }
        </Menu>
      )
      return { "open": true, "portalChildren": childrenMenu };
    } else {
      return { "open": false, "portalChildren": null };
    }
  }

  /**
   * 弹出框关闭事件
   *
   * @memberof MenuCom
   */
  handlePortalClose = (event, data) => {
    // 清除鼠标移开事件计时器
    clearTimeout(this.mouseoutOutTimer);
    this.setState({
      activeKey: ""
    })
  }

  onPortalMouseover = (event, data) => {
    event.nativeEvent.stopImmediatePropagation();
    // 清除鼠标移开事件计时器
    if (this.state.showState === 1) {
      clearTimeout(this.mouseoutOutTimer);
    }
  }

  /**
   * 鼠标移入事件
   *
   * @memberof MenuCom
   */
  onMouseover = (index, event) => {
    _.has(event, "nativeEvent") && event.nativeEvent.stopImmediatePropagation();
    _.has(event, "stopPropagation") && event.stopPropagation();
    clearTimeout(this.mouseoutOutTimer);
    if (index !== this.state.activeKey) {
      const elem = getItemElement(event.target);
      const eventBox = elem.getBoundingClientRect();
      this.setState({
        // 设置激活索引
        activeKey: index,
        // 设置显示状态为提示信息
        showState: 0,
        // 设置显示位置信息
        parentOffset: {
          top: eventBox.top,
          left: eventBox.left - 90
        }
      })
    }
  }
  /**
   * 鼠标移出事件
   *
   * @memberof MenuCom
   */
  onMouseout = (index, event) => {
    // event.nativeEvent.stopImmediatePropagation();
    this.mouseoutOutTimer = setTimeout(() => {
      this.setState({
        activeKey: ""
      })
    }, 200);
  }

  // 显示子菜单点击事件
  onShowSubClick = (event, data) => {
    _.has(event, "nativeEvent") && event.nativeEvent.stopImmediatePropagation();
    _.has(event, "stopPropagation") && event.stopPropagation();
    clearTimeout(this.mouseoutOutTimer);
    const elem = getItemElement(event.target);
    const eventBox = elem.getBoundingClientRect();
    if(this.state.showState === 1){
      return false;
    }
    let lang=global.__LANGUAGE__||process.env.NODE_ENV_LANGUAGECODE;
    console.log('global.__LANGUAGE__',global.__LANGUAGE__);
    console.log('process.env.NODE_ENV_LANGUAGECODE',process.env.NODE_ENV_LANGUAGECODE);
    console.log('lang:',lang);
    this.setState({
      showState: 1,
      // 设置显示位置信息
      parentOffset: {
        top: eventBox.top,
        left: eventBox.left - (lang === "zh_cn" ? 116 : 188)
      }
    })
    return false;
  }

  /**
   * 鼠标点击事件
   *
   * @memberof MenuCom
   */
  onClick = (event, data) => {
    // _.has(event, "nativeEvent") && event.nativeEvent.stopImmediatePropagation();
    // _.has(event, "stopPropagation") && event.stopPropagation();
    this.setState({
      activeKey: ""
    }, () => {
      // 关闭右侧插件
      this.props.dispatch({
        type: "SystemPluginModel/changeOpenStatus",
        pluginKey: "-1",
        openStatus: false
      })
      // 响应点击事件
      this.props.onClick && this.props.onClick(event, data);
    })

    return false;
  }

  logoClick = (event, data) => {
    // _.has(event, "nativeEvent") && event.nativeEvent.stopImmediatePropagation();
    // _.has(event, "stopPropagation") && event.stopPropagation();
    this.props.dispatch({
      type: "SystemPluginModel/changeOpenStatus",
      pluginKey: "-1",
      openStatus: !this.props.openStatus
    })
    return false;
  }

  /**
   * 计算子菜单高度
   *
   * @memberof MenuCom
   */
  nodeRef = (prenodeRef) => {
    const { parentOffset } = this.state;
    if (!prenodeRef) {
      return;
    }
    const currentY = prenodeRef.offsetHeight + parentOffset.top;
    if (currentY > window.innerHeight) {
      this.setState({
        parentOffset: {
          ...this.state.parentOffset,
          top: window.innerHeight - prenodeRef.offsetHeight
        }
      })
    }
  }

  /**
   * 渲染隐藏组件
   *
   * @memberof HyRightOperCom
   */
  renderHiddenCom = (rightOperTop, rightOperCenter, rightOperBottom) => {
    const components = [];
    _.forEach(rightOperTop, (item, index) => {
      const { keyboard, permsCode, authCode } = item;
      components.push(<HyButtonCom key={`key-${index}`} name={index} {...{ keyboard, permsCode, authCode }} onClick={this.onClick}></HyButtonCom>);
    })
    _.forEach(rightOperBottom, (item, index) => {
      const { keyboard, permsCode, authCode } = item;
      components.push(<HyButtonCom key={`key-${index}`} name={index} {...{ keyboard, permsCode, authCode }} onClick={this.onClick}></HyButtonCom>);
    })
    _.forEach(rightOperCenter, (item, index) => {
      if (!_.has(item, "list") || _.size(item.list) === 0) {
        const { keyboard, permsCode, authCode } = item;
        components.push(<HyButtonCom key={`key-${index}`} name={index} {...{ keyboard, permsCode, authCode }} onClick={this.onClick}></HyButtonCom>);
      } else {
        // 遍历other内容
        _.forEach(item.list, (item2, index2) => {
          const { keyboard, permsCode, authCode } = item2;
          components.push(<HyButtonCom key={`key-${index}-${index2}`} name={index2} parentname={index} {...{ keyboard, permsCode, authCode }} onClick={this.onClick}></HyButtonCom>);
        })
      }
    })

    return components;
  }

  /**
   * 关闭plugin
   *
   * @memberof HyRightOperCom
   */
  onClose = (event, data) => {
    this.props.dispatch({
      type: "SystemPluginModel/changeOpenStatus",
      pluginKey: "-1",
      openStatus: false
    })
  }

  /**
   * 组织冒泡事件
   *
   * @memberof HyRightOperCom
   */
  onPareClick = (event, data) => {
    event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
    return false;
  }
}

// 递归获取类名为item的元素
function getItemElement(Element) {
  if (Element && (_.includes(Element.className, "item") || _.includes(Element.className, "item subset"))) {
    return Element;
  }
  return getItemElement(Element.parentElement);
}

function mapStateToProps(state, defaultProps) {
  const { namespace = "" } = defaultProps;
  return {
    // 右侧功能按钮
    rightOperTop: state[namespace]["rightOperTop"] || {},
    rightOperCenter: state[namespace]["rightOperCenter"] || {},
    rightOperBottom: state[namespace]["rightOperBottom"] || {},
    pluginKey: state["SystemPluginModel"].pluginKey || "-1",
    openStatus: state["SystemPluginModel"].openStatus || false,
  };
}

export default connect(mapStateToProps)(HyRightOperCom);