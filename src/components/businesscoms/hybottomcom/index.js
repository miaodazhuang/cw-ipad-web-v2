/**
 * systemName:底部信息组件
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import { connect } from 'dva';
import { Label } from 'semantic-ui-react';
import _ from 'lodash';
import { FormatMoney } from 'Hyutilities/money_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hybottomcom';
import ja_jp from 'ja_jp/components/businesscoms/hybottomcom';
import zh_cn from 'zh_cn/components/businesscoms/hybottomcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyBottomCom extends Component {
  constructor(props) {
    super(props);
    this._times = 300;
    this.POSREFRESH = 300;
    if (global.__OPTIONMAP__ && global.__OPTIONMAP__["POSREFRESH"]) {
      this._times = global.__OPTIONMAP__["POSREFRESH"].option_num1;
      this.POSREFRESH = global.__OPTIONMAP__["POSREFRESH"].option_num1;
    }
    this.fun = null;
    this.state = {
      _times: this._times
    }
  }

  componentDidMount() {
    this.refresh();
  }

  componentWillReceiveProps(nextProps) {
    //刷新重置标志改变则重置当前刷新时间
    if (this.props.refreshTimes !== nextProps.refreshTimes) {
      this._times = this.POSREFRESH;
      this.setState({
        _times: this.POSREFRESH,
      })
    }
    //计时器的暂停和重启
    if (this.props.stopTimer !== nextProps.stopTimer && nextProps.stopTimer) {
      if (_.isNumber(this.fun)) {
        clearInterval(this.fun);
        this.fun = null;
      }
    } else if (this.props.stopTimer !== nextProps.stopTimer && !nextProps.stopTimer) {
      if (!_.isNumber(this.fun)) {
        this.refresh();
      }

    }
  }

  componentWillUnmount() {
    window.clearTimeout(this.fun);
  }

  render() {
    return (
      <div className='HyBottomCom' >
        <div className='LeftCom'>{this._getLeftComs()}</div>
        <div className='RightCom'> {this._getRightComs()}</div></div>
    );
  }


  refresh = () => {
    if (!_.isNumber(this.fun)) {
      this.fun = setInterval(() => {
        let _disPatchObj = "";
        if (_.includes(this.props.location.pathname, 'postableview')) {
          _disPatchObj = {
            type: "Mpostableview/queryList"
          }
        } else if (_.includes(this.props.location.pathname, 'posbillview')) {
          _disPatchObj = {
            type: "Mposbillview/queryList"
          }
        } else if (_.includes(this.props.location.pathname, 'posreportview')) {
          _disPatchObj = null;
        }
        //执行当前计时器
        if (this._times <= 0) {
          if (_.size(this.props.salesPointTotal) !== 0) {
            this.props.dispatch({
              type: "SystemPosModel/refresh",
              disPatchObj: _disPatchObj,
            })
          }
          this._times = _.cloneDeep(this.POSREFRESH);
          this.setState({ _times: _.cloneDeep(this.POSREFRESH) });
        } else {
          //计时器减1
          this._times -= 1;
          this.setState({ _times: this._times });
        }
      }, 1000);
    }
  }


  /**
   * 获取左侧信息组件
   *
   * @memberof HyTopCom
   */
  _getLeftComs = () => {
    let coms = [];
    //账单详情页面返回null
    if (_.includes(this.props.location.pathname, 'posorderdetailview')) {
      window.clearTimeout(this.fun);
      return null;
    }
    //桌台
    coms.push(<Label className='Label-item' >{`${languageConfig.block_num} : ${this.props.salesPointTotal.block_num || 0}`}</Label>);
    //空台
    coms.push(<Label className='Label-item' >{`${languageConfig.empty_num} : ${this.props.salesPointTotal.empty_num || 0}`}</Label>);
    //占用
    coms.push(<Label className='Label-item' >{`${languageConfig.open_num} : ${this.props.salesPointTotal.open_num || 0}`}</Label>);
    //用餐金额
    coms.push(<Label className='Label-item' >{`${languageConfig.bill_amount} : ${FormatMoney(this.props.salesPointTotal.bill_amount)}`}</Label>);
    //用餐人数
    coms.push(<Label className='Label-item' >{`${languageConfig.psn_num} : ${this.props.salesPointTotal.psn_num || 0}`}</Label>);

    return coms;
  }

  /**
   * 获取右侧信息组件
   *
   * @memberof HyTopCom
   */
  _getRightComs = () => {
    let coms = [];
    //判断是否为账单详情页面

    let  service_rate = 0, discount_rate = 0, tax_rate = 0 ;
    if(this.props.Mposorderdetailview && this.props.Mposorderdetailview.billInFo){
      service_rate=this.props.Mposorderdetailview.billInFo.service_rate;
      discount_rate=this.props.Mposorderdetailview.billInFo.discount_rate;
      tax_rate=this.props.Mposorderdetailview.billInFo.tax_rate;
    }

    if (_.includes(this.props.location.pathname, 'posorderdetailview')) {
      coms.push(<Label className='Label-item' >{`${languageConfig.service_rate} : ${service_rate}%`}</Label>);
      coms.push(<Label className='Label-item' >{`${languageConfig.discount_rate} : ${discount_rate}%`}</Label>);
      coms.push(<Label className='Label-item' >{`${languageConfig.tax_rate} : ${tax_rate}%`}</Label>);
    } else {
      coms.push(<Label className='Label-item' >{`${languageConfig.nextrefreshtime} : ${this.state._times}`}</Label>);
    }
    return coms;
  }

}

function mapStateToProps({ SystemPosModel, Mposorderdetailview }) {
  return {
    // 异步加载状态
    period: SystemPosModel.period,
    refreshTimes: SystemPosModel.refreshTimes,
    stopTimer: SystemPosModel.stopTimer,
    salesPointTotal: SystemPosModel.salesPointTotal || {},
    Mposorderdetailview: Mposorderdetailview || {}
  };
}

export default connect(mapStateToProps)(HyBottomCom);
