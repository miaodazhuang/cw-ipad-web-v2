/**
 * 提醒
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Card, Dimmer, Icon } from 'semantic-ui-react';
import { connect } from 'dva';
import ClassNames from 'classnames';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/hyalertcom';
import ja_jp from 'ja_jp/hyalertcom';
import zh_cn from 'zh_cn/hyalertcom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });


/**
 * 确认窗口
 *
 * @class HyConfirmCom
 * @extends {Component}
 */
class HyAlertCom extends Component {

  static propTypes = {
    // 颜色
    color: PropTypes.string,
    // 类型
    alertTye: PropTypes.string,
    // 预定号
    preNo: PropTypes.string,
    // 客人姓名
    accName: PropTypes.string,
    // 会员号
    accountNo: PropTypes.string,
    // 入店日期
    timeStart: PropTypes.string,
    // 离店日期
    timeEnd: PropTypes.string,
    // 提醒内容
    alertNote: PropTypes.string,
    // 需要会传的参数对象
    cbDispatchTyp: PropTypes.object,
    //标题
    alertTypDept: PropTypes.alertTypDept
  }

  constructor(props) {
    super(props);
    this.state = {
      active: false
    }
  }



  static defaultProps = {

  }

  componentDidMount() {

  }

  render() {

    let { color, alertTye, alertNote, preNo, accName, active, timeStart, timeEnd, accountNo, alertTypDept } = this.props;

    timeStart = timeStart ? moment(timeStart).format("YYYY-MM-DD") : '';
    timeEnd = timeEnd ? moment(timeEnd).format("YYYY-MM-DD") : '';

    const iconMap = { KS: 'img354', KH: 'img355', YD: 'img356', HY: 'img357' }
    let backGroundColor = { backgroundColor: `#${color}` }
    return (
      <Dimmer active={active} inverted page className="dimmer_alert" onClickOutside={this.handleHide} >
        <Card>
          <Card.Content className="card_top">
            <div className='alert_content_top' style={backGroundColor}>
              {/* <Icon className="icon60 img354" size='large' /> */}
              <Icon className={ClassNames("icon60", iconMap[alertTye])} size='large' />
              <div className='alert_content_top_right'>
                <span className='font18 over_flow'>{alertTypDept}</span>
                <span className='over_flow'>
                  <span className='font14'>{preNo ? preNo : ''}&nbsp;</span><span className='font14'>{accName}</span>&nbsp;<span className='font14'>{accountNo ? accountNo : ''}</span>
                </span>
                {timeStart ? <span className='over_flow'>{`${languageConfig.inout}：${timeStart} ${languageConfig.to} ${timeEnd}`}</span> : ''}
              </div>
            </div>
            <Icon className="icon20 img358 " size='small' onClick={this.handleHide} />
          </Card.Content>
          <Card.Content className="card_bottom">
            <div className='alert_content'>
              {alertNote}
            </div>
          </Card.Content>
        </Card>
      </Dimmer>
    );
  }

  handleHide = (event, data) => {
    event.preventDefault();
    event.nativeEvent.stopImmediatePropagation();
    event.stopPropagation();
    this.props.dispatch({
      type: 'SystemAlertModel/closeWindow',
    })
  }
}

function mapStateToProps({ SystemAlertModel }) {
  return {
    ...SystemAlertModel
  };
}

export default connect(mapStateToProps)(HyAlertCom);
