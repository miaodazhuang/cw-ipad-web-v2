/**
 * @file 帮助中心 弹窗
 */

import React, { Component } from 'react';
import { connect } from 'dva';
import HyModalCom from 'Hycomponents/featurecoms/hymodalcom';
import HyHelpNoPageCom from 'Hycomponents/featurecoms/hyhelpnopagecom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hyhelpcentercom';
import ja_jp from 'ja_jp/components/businesscoms/hyhelpcentercom';
import zh_cn from 'zh_cn/components/businesscoms/hyhelpcentercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyPopHelpCom extends Component {

  static propTypes = {}

  // 关闭事件
  close = () => {
    this.props.dispatch({
      type: 'SystemHelpModel/updateHelpModalState',
      show: false,
    })
  }

  render() {

    const { show, size } = this.props.modalState
    const { iframeUrl } = this.props;
    return (
      <HyModalCom
        size={'large'}
        title={languageConfig.helpCenter}
        isOpen={show}
        helpOpen
        closeIcon
        customContentStyle={{
          height: '530px',
          width: '1020px',
        }}
        onClose={this.close}
      > {iframeUrl? <iframe
        frameBorder="0"
        style={{
          height: 'auto',
          width: '100%'
        }}
        src={iframeUrl}
      />: <HyHelpNoPageCom/>}

      </HyModalCom>
    )

  }

}

function mapStateToProps({ SystemHelpModel }) {
  return {
    modalState: SystemHelpModel.modalwindowstate,
    iframeUrl: SystemHelpModel.iframeUrl,
  };
}
export default connect(mapStateToProps)(HyPopHelpCom);