/**
 * @file 帮助中心
 */

import React, { Component } from 'react';
import {  Icon } from 'semantic-ui-react';
import { connect } from 'dva';
import _ from 'lodash';
import HyAuthCom from 'Hycomponents/featurecoms/hyauthcom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hyhelpcentercom';
import ja_jp from 'ja_jp/components/businesscoms/hyhelpcentercom';
import zh_cn from 'zh_cn/components/businesscoms/hyhelpcentercom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class HyHelpCenterCom extends Component {
  static propTypes = {}
//componentDidMount() {
//  document.onkeydown = (event) => {
//    if (event.keyCode == '112' && event.altKey && event.ctrlKey) {
//      let titleId = null
//      if (this.props.subModal) {
//        titleId = this.checkRouterId(this.props.modalwindowstate.allkey)
//      } else if (this.props.plugModal) {
//        titleId = this.checkRouterId(this.props.pluginKey)
//      } else {
//        titleId = this.checkRouterId(this.props.currentTabData && this.props.currentTabData.match && this.props.currentTabData.match.url.substring(8, 35))
//      }
//      alert(`${titleId}`)
//    }
//  }
//}

  // 点击事件
  onClickEvent = (event) => {
    // 判断是否是弹窗调用
    if (this.props.subModal) {
      if (event.ctrlKey) {
        return alert(`${this.checkRouterId(this.props.modalwindowstate.allkey)}`)
      }
      this.props.dispatch({
        type: 'SystemHelpModel/updateHelpModalState',
        size: this.props.size,
        params: {
          view_id: this.checkRouterId(this.props.modalwindowstate.allkey)
        },
        show: true,
      })
    } else if (this.props.plugModal) {
      if (event.ctrlKey) {
        return alert(`${this.checkRouterId(this.props.pluginKey)}`)
      }
      this.props.dispatch({
        type: 'SystemHelpModel/updateHelpModalState',
        size: this.props.size,
        params: {
          view_id: this.checkRouterId(this.props.pluginKey)
        },
        show: true,
      })
    } else {
      if (event.ctrlKey) {
        return alert(`${this.checkRouterId(this.props.currentTabData.match.url.substring(8, 35))}`)
      }
      const _encryptStr = global.__ENCRYPT__.Encrypt(escape(JSON.stringify({
        view_id: this.checkRouterId(this.props.currentTabData.match.url.substring(8, 35))
      })));
      this.props.dispatch({
        type: "SystemRouterModel/push",
        url: `/0131/10/10/A/090/L/T/001/01/${_encryptStr.orgData}/${_encryptStr.encryptData}`,
        tabIndex: 1,
        replaceUrl: true,
      })
    }
  }

  // 查询路由ID
  checkRouterId = (url) => {
    //如果是首页特殊处理
    if(url === 'home'){
      //判断是集团首页还是单店首页
      if (global.__CHAININFO__.ChainFlg === '1' && global.__UNITINFO__.UnitFlg === '0') {
          //集团登陆
          return 5;
      } else if (global.__CHAININFO__.ChainFlg === '1' && global.__UNITINFO__.UnitFlg === '1') {
          //单店登陆
          return 6;
      } else if (global.__CHAININFO__.ChainFlg === '0' && global.__UNITINFO__.UnitFlg === '1') {
          //单体酒店
          return 6;
      }
    }
    //如果是每日房价，特殊处理
    let transformUrl = url === '01322030A010LP00202'? HyAuthCom.CheckAuthCode(['0132-2010-0020-L-A-01'])? '01322030A010LP00201': '01322030A010LP00202' : url;
    //如果是换房画面，特殊处理
    transformUrl = transformUrl === '01322010A051LP00101' ? HyAuthCom.CheckAuthCode(['0132-2010-0020-L-A-01'])? '01322010A051LP00101': '01322010A051LP00102' : url;
    const currtRoute = global.__VIEWSMAP__.find((p) => [p.view_url.replace(/\//g, ''), p.view_url].includes(transformUrl))
    return currtRoute ? currtRoute.view_id : null
  }

  // 判断内容
  checkContent = () => {
    const { currentTabData } = this.props
    //判断是否有帮助中心sku
    const ISHELPCENTERSKU = HyAuthCom.CheckAuthCode('0131-2010-0001-L-A-01');
    let includeArr = ISHELPCENTERSKU? ['0'] : ['0', '1'];

    if (!(currentTabData && currentTabData.match && currentTabData.match.params)) return null;
    if (this.props.subModal) {
      return <Icon className='icon16 img80_1' title={`${languageConfig.helpTitle} : ${this.props.modalwindowstate.allkey}`} onClick={this.onClickEvent} />;
    } else if (!includeArr.includes(currentTabData.match.params.tabIndex)) {
      let viewCode = _.split(currentTabData.match.path.substring(16, 43), '/').join('')
      return <Icon className='icon16 img80_1' title={`${languageConfig.helpTitle} : ${viewCode}`} onClick={this.onClickEvent} />;
    } else {
      //区分是首页还是帮助中心，添加帮助
      if(currentTabData.match.params.tabIndex === '0'){
        return <Icon className='icon16 img80_1' title={`${languageConfig.helpTitle} : ${'home'}`} onClick={this.onClickEvent} />;
      }
      return null;
    }

  }

  render() {

    return (
      <div className='HyHelpCenterCom'>
        {this.checkContent()}
      </div>
    )

  }

}

function mapStateToProps({ SystemRouterModel, SystemModel, SystemPluginModel }) {
  return {
    panesValue: SystemRouterModel.tabData,
    currentTabData: SystemRouterModel.currentTabData,
    modalwindowstate: SystemModel.modalwindowstate,
    pluginKey: SystemPluginModel.pluginKey,
  };
}
export default connect(mapStateToProps)(HyHelpCenterCom);

