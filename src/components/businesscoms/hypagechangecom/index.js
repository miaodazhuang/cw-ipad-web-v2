/**
 * 页签切换组件
 * lxy
*/
import React from "react";
import PropTypes from 'prop-types';
import { connect } from "dva";
import classNames from "classnames";
import _ from "lodash";
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/components/businesscoms/hypagechangecom';
import ja_jp from 'ja_jp/components/businesscoms/hypagechangecom';
import zh_cn from 'zh_cn/components/businesscoms/hypagechangecom';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
class HyPageChangeCom extends React.Component {
  static propTypes = {
    //当前选中地址
    pageUrl:PropTypes.string,
    //样式
    className: PropTypes.string,
  }
  static defaultProps = {
    className: '',
  }

  componentWillReceiveProps(nextProps) {
  }

  render() {
    return (
      <div className={classNames("hypageChangeCom", this.props.className)}>
        <div className={classNames("pageItem", _.includes(this.props.pageUrl, 'postableview') ? 'selectedItem' : '')} onClick={(event) => this._onClick(event, 'postableview')}   >
          {languageConfig.postableview}
        </div>
        <div className={classNames("pageItem", _.includes(this.props.pageUrl, 'posbillview') ? 'selectedItem' : '')} onClick={(event) => this._onClick(event, 'posbillview')}>
          {languageConfig.posbillview}
        </div>
        <div className={classNames("pageItem", _.includes(this.props.pageUrl, 'posreportview') ? 'selectedItem' : '')} onClick={(event) => this._onClick(event, 'posreportview')}>
          {languageConfig.posreportview}
        </div>
      </div>)
  }

  /**
   * 跳转
   *
   * @memberof HyPageChangeCom
   */
  _onClick = (event, data) => {
    this.props.dispatch({
      type: "SystemRouterModel/push",
      url: `${data}/0/0`
    }
    );
  }
}

function mapStateToProps(SystemRouterModel) {
  return {
    currentTabData: SystemRouterModel.currentTabData
  };
}

export default connect(mapStateToProps)(HyPageChangeCom);