import moment from 'moment';
import _ from 'lodash';
import key from 'keymaster';
import { RemoveSessionItem, GetSessionItemByUrl, GetSessionItem } from 'Hyutilities/sessionstorage_helper';

key.filter = function (event) {
  // var tagName = (event.target || event.srcElement).tagName;
  // key.setScope(/^(INPUT|TEXTAREA|SELECT|IFRAME)$/.test(tagName) ? 'input' : 'other');
  return true;
}


global.__LANGUAGE__ = '';

global.__KEYMASTER__ = key;
// document宽度
global.__DOCUMENTWIDTH__ = 1400;
// document高度
global.__DOCUMENTHEIGHT__ = 900;
// 系统加密登录信息KEY
global.__USERDATAKEY__ = "jwcloudpms";
// 集团信息
global.__CHAININFO__ = null;
// 单位信息
global.__UNITINFO__ = null;
// 所属单位信息
global.__OUUNITINFO__ = null;
// 单位列表数据
global.__UNITLIST__ = null;
// 操作员信息
global.__USERINFO__ = null;
// 用户权限
global.__USERPARAMS__ = null;
// 开关表
global.__OPTIONMAP__ = null;
// 日期选择国际化
global.__CALENDARLOCAL__ = null;
// 币种
global.__CURRENCY__ = null;
// 操作员菜单
global.__USERMENU__ = null;
// 用户权级
global.__USERRIGHTCLS__ = null;
// 酒店业务日期
global.__BUSINESSDT__ = null;
// 管控字段数据
global.__CONTROLFIELD__ = null;
// 工作站号
global.__WSNO__ = null;
// 工作站名称
global.__WSNM__ = null;
// 工作站机器名称
global.__MACHINENM__ = null;
// 工作站IP
global.__IPADDRESS__ = null;
// 工作站MAC
global.__MACADDRESS__ = null;
// 加密类实力
global.__ENCRYPT__ = null;
// sku数据 {}
global.__SKUMAP__ = null;
// 当前酒店功能数据 {}
global.__FUNMAP__ = null;
// 快捷键数据
global.__SHORTKEYMAP__ = {};
// viewSKU
global.__VIEWSMAP__ = [];
// 获取默认楼区
global.__OPRAREA__ = [];
//基于页面地址和唯一标识记录滚动条滚动的位置
global.SCROLLPOSOBJ = [];
//记录当前活动的table
global.TABLEACTIVEOBJ = undefined;
//基于页面地址和唯一标识记录table的选中项
global.TABLECHECKEDOBJ = [];

//当前销售点属性对象
global.__SALEPOINT__=null;

//记录是否是登陆状态
global.__ISLOGIN__=false;

/**
 * 解析地址参数方法
 * @param {*} match
 */
global.__COMPARISON__ = async function (match) {
  let routeParams = {};
  const encryptParams = _.takeRight(match, 2);
  if (encryptParams[0] === "outSystem" || (encryptParams[0] === "0" && encryptParams[1] === "0")) {
    return {};
  }
  try {
    const urlParamsData = await GetSessionItem("UrlParamsData");
    const currentData = urlParamsData[Number(match[1])];
    const params = currentData[encryptParams[1]];
    const _encryptParams = _.startsWith(params, "%25") ? decodeURI(decodeURI(params)) : params;
    if (global.__ENCRYPT__.Comparison(_encryptParams, encryptParams[1])) {
      routeParams = JSON.parse(unescape(_encryptParams));
    } else {
      global.__LOGOUT__();
    }
  } catch (error) {
    global.__LOGOUT__();
  }
  return routeParams;
}

/**
 * 解析地址参数方法直接根据加密参数解密
 * @param {*} match
 */
global.__COMPARISONNOTMATCH__ = async function (encryptParams_0, encryptParams_1, tabIndex) {
  let routeParams = {};
  try {
    const urlParamsData = await GetSessionItem("UrlParamsData");
    const currentData = urlParamsData[Number(tabIndex)];
    const params = currentData[encryptParams_1];
    const _encryptParams = _.startsWith(params, "%25") ? decodeURI(decodeURI(params)) : params;
    if (global.__ENCRYPT__.Comparison(_encryptParams, encryptParams_1)) {
      routeParams = JSON.parse(unescape(decodeURI(_encryptParams)));
    } else {
      global.__LOGOUT__();
    }
  } catch (error) {
    global.__LOGOUT__();
  }
  return routeParams;
}

/**
 * 注销登录
 * @param {*} match
 */
global.__LOGOUT__ = async (msg) => {
  //注销操作 删除存储节点
  await RemoveSessionItem('tabData');
  await RemoveSessionItem('currentTabData');
  await RemoveSessionItem('cacheData');
  await RemoveSessionItem('UserData');
  await RemoveSessionItem('SystemPosModel');
  //清除销售点信息
  await RemoveSessionItem('salePoint');
  // 跳转到网站根目录
  if (_.trim(msg) !== "") {
    alert('Parametric parsing error!');
  }
  window.location.replace('/');
}

// 获取酒店业务日期
global.__GETBUSINESSDT__ = function () {
  let _businessDt = Number(global.__BUSINESSDT__);
  if (isNaN(_businessDt)) {
    return moment(moment().format('L'))
  } else {
    return moment(_businessDt);
  }
};

// 验证酒店日期
global.__CHECKBUSINESSDT__ = function (date) {
  let _businessDt = Number(global.__BUSINESSDT__);
  const _date = date ? moment(Number(date)) : moment(moment().format('L'));
  if (isNaN(_businessDt) || moment(_businessDt) < _date) {
    return 0;
  } else if (moment(_businessDt) == _date) {
    return 1;
  } else {
    return 2;
  }
};

/**
 * 统一下载方法
 *
 * @param {any} objectURL 文件地址
 * @param {any} filename 文件名称
 */
global.__CUSTOMDOWNLOAD__ = function (objectURL, fileName) {
  let downloadIframe = document.getElementById("downloadIframe");
  if (downloadIframe) {
    document.body.removeChild(downloadIframe);
  }
  downloadIframe = document.createElement('a');
  downloadIframe.setAttribute("style", "display:none");
  downloadIframe.setAttribute("download", fileName);
  downloadIframe.id = 'downloadIframe';
  downloadIframe.href = objectURL;
  document.body.appendChild(downloadIframe);
  document.getElementById("downloadIframe").click();
  document.body.removeChild(downloadIframe);
}

/**
 * 统一打印方法
 *
 * @param {any} url 文件地址
 */
global.__CUSTOMPRINT__ = function (url) {
  let iframe = document.getElementById("printIframe");
  if (iframe) {
    document.body.removeChild(iframe);
  }
  iframe = document.createElement('iframe');
  iframe.setAttribute("style", "display:none");
  iframe.id = 'printIframe';
  if (iframe.attachEvent) {
    iframe.attachEvent("onload", function () {
      this.contentWindow.print();
    })
  } else {
    iframe.onload = function () {
      this.contentWindow.print();
    }
  }

  iframe.src = url;

  document.body.appendChild(iframe);
}

/**
 * 获取路由缓存数据
 *
 * @param {string} namespace 命名空间
 * @param {object} apiData 接口数据 需要保持根model的数据格式相同
 * @param {array} diffCondition 查询条件
 */
global.__GETCACHEDATA__ = async function (namespace, apiData, diffCondition) {
  // 通过url获取路由缓存数据
  const cacheData = await GetSessionItemByUrl();
  // 根据命名空间获取model数据
  let cacheModelData = cacheData[namespace];

  if (!cacheModelData) {
    // 如果缓存数据为空，返回null
    return null;
  } else if (cacheModelData && !apiData) {
    // 如果缓存数据存在，api数据为空则直接返回缓存数据
    return cacheModelData;
  }

  // 循环查询条件对比缓存数据与api数据
  _.forEach(diffCondition, (item) => {
    const apiValue = _.get(apiData, item);
    const cacheValue = _.get(cacheModelData, item);
    if (`${apiValue}` !== `${cacheValue}`) {
      // 如果根据查询条件对比不相等，则放弃缓存数据
      cacheModelData = null;
      return false;
    }
  })

  return cacheModelData;

}