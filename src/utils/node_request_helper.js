//请求消息中心node的请求帮助类

import fetch from 'isomorphic-fetch';
import _ from "lodash";
import { SetSessionItem } from './sessionstorage_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/utils/node_request_helper';
import ja_jp from 'ja_jp/utils/node_request_helper';
import zh_cn from 'zh_cn/utils/node_request_helper';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
// 引入操作本地缓存库

/**
 * 发送请求的方法
 * @param {*} apiurl api地址
 * @param {*} method api method
 * @param {*} data  数据
 * @param {*} headers 请求头
 */
function FetchHandle(apiurl, method, body){
  return request(apiurl, {method, body}).then(checkResponseData)
}

function request(url, {
  body,
  mode = 'cors',
  cache = 'no-cache',
  method = 'POST',
  headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    // 设置请求头上的sessionKey
    'JW_DATA': global.__USERINFO__ ? global.__USERINFO__.JW_DATA : '',
    // //集团标识
    // 'JW_CHAIN': global.__CHAININFO__ ? global.__CHAININFO__.ChainUid : '',
    // //酒店标识
    // 'JW_UNIT': global.__UNITINFO__ ? global.__UNITINFO__.UnitUid : '',
  }
} = {}) {
  //如果是get 拼到url后面
  if(method === "get"){
    if(_.size(body) > 0){
      url = handleUrlInGet(url, body);
    }
    body = null;
  }
  // 设置body
  body = body ? JSON.stringify(body) : null;
  //发起异步调用
  return fetch(url, { body, mode, cache, method, headers })
    .then(checkStatus)
    .then(parseJSON)
    .catch(err => {
      if(err === "*#logout#*"){ //如果是跳转到登录，不处理
        throw err;
      }
      let error = new Error(-1);
      error.message = `M0000011 : ${languageConfig.error_message}`;
      throw error;
    });
}

//get方法时，处理请求地址，将参数拼到后面
function handleUrlInGet(url, body){
  url = _.trimEnd(url, "/");
  url = _.endsWith(url, "?") ? url : `${url}?`;
  _.forEach(body, (value, key) => {
    if(Object.prototype.toString.call(value) === "[object Array]" && value.length > 0){
      _.forEach(value, item => {
        url = `${url}${key}=${item}&`;
      });
    }else{
      if(value != undefined){
        url = `${url}${key}=${value}&`;
      }
    }
  });
  url = _.trimEnd(url, "&");
  return url;
}


/**
 * 验证网络返回数据状态
 * @param  {object} response 从API层接收到的返回值
 * @return {object} response 从API层接收到的返回值
 */
async function checkStatus(response) {
  // 判断状态
  if (response.ok && response.status >= 200 && response.status < 300) {
    return response;
  }

  if(response.status === 401){ //没有权限，跳转到登陆界面
    await SetSessionItem("DictionaryData", null);
    await SetSessionItem("EncryptStr", null);
    await SetSessionItem("UserData", null);
    await SetSessionItem("cacheData", null);
    await SetSessionItem("tabData", null);
    await SetSessionItem("currentTabData", null);
    throw '*#logout#*';
  }
  // 获取返回的错误信息
  const error = new Error(response.statusText);
  error.response = response;
  // 触发异常事件
  throw error;
}


/**
 * Json转换方法
 * @param  {object} response 从API层接收到的返回值
 * @return {jsonobject}   返回转换后的json对象
 */
function parseJSON(response) {
  // 转换json
  return response.json();
}

/**
 * 校验返回的数据处理结果
 */
function checkResponseData(responseData){
  if(responseData.resultCode === "1"){
    throw responseData.message;
  }
  return responseData.resultData;
}

export {
  FetchHandle
}