import { Component } from 'react';
import { connect } from 'dva';
import _ from 'lodash';

class RouterComponentHelp extends Component {

  constructor(...args) {
    super(...args);
  }

  componentWillMount() {

    const { app, namespaces, match, title } = this.props;
    let _title = "";

    if (match && _.size(match.params) > 0 && _.has(match.params, 'tabIndex')) {
      const params = match.url.split('/');
      let customMatch = null;
      if ((params[4] === "0" && params[5] === "0") || _.size(params) === 4) {
        customMatch = match;
      } else {
        params[4] = _.startsWith(params[4], "%25") ? params[4] : encodeURI(params[4]);
        customMatch = {
          ...match,
          url: params.join('/')
        }
      }

      if (_.isFunction(title)) {
        title(customMatch)
          .then(title => this.props.dispatch({ type: "SystemRouterModel/routerPackage", app, namespaces, match: customMatch, title }));
      } else {
        _title = title;
        this.props.dispatch({ type: "SystemRouterModel/routerPackage", app, namespaces, match: customMatch, title: _title })
      }


    }

  }

  render() {
    return this.props.children;
  }
}

export default connect()(RouterComponentHelp);