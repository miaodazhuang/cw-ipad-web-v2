export default class Observe{
  constructor(){
    this.listeners = [];
    this.observe = "";
  }
  set observeValue(value){
    this.observe = value;
    this.notify();
  }
  get observeValue(){
    return this.observe;
  }
  listen(listener){
    this.listeners.push(listener);
    return () => {
      this.unlisten(listener);
    }
  }
  unlisten(listener){
    let index = this.listeners.findIndex(item => item === listener);
    this.listeners.splice(index, 1);
  }
  notify(){
    this.listeners.forEach(item => {
      item(this.observe);
    });
  }
}