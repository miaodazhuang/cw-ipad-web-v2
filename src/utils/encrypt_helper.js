import _ from 'lodash';
import md5 from 'md5';

/**
 * 加密类，目前只提供简单加密功能(不提供揭秘功能，验证时需要调用对比函数验证密钥)
 *
 * @export
 * @class EncryptClass
 */
export default class EncryptClass {
  constructor(comCode) {
    this.comCode = _.trim(comCode);
  }

  /**
   * 加密方法
   *
   * @param {any} orgData 原始数据，需要经过escape编码
   * @returns
   * @memberof EncryptClass
   */
  Encrypt(orgData, notEncode) {
    let _orgData = null;
    let _compileString = '';
    if (orgData && orgData != '' && _.isString(orgData)) {
      _orgData = orgData;
    } else {
      throw 'Pass in an object or string';
    }

    _orgData = `${orgData}--${this.comCode}`;
    _compileString = md5(_orgData);

    return {
      "orgData": notEncode ? orgData : encodeURI(encodeURI(orgData)),
      "encryptData": _compileString
    };
  }

  /**
   * 对比方法
   *
   * @param {any} orgData 原始数据，需要经过escape编码
   * @param {any} complieString 加密字符串
   * @returns
   * @memberof EncryptClass
   */
  Comparison(orgData, complieString) {
    let _compileString = this.Encrypt(orgData);
    if (!_.isEqual(complieString, _compileString.encryptData)) {
      return false;
    } else {
      return true;
    }
  }
}
