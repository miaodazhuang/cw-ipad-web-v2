import _ from 'lodash';
import moment from 'moment';
//导入日期格式化帮助方法
import { FormatMoment } from 'Hyutilities/moment_helper';



/**
 * 通过模板方式替换字符串内容
 * 字符串模板中需要替换的位置采用：${“str”+索引}标记 如：字符串长度最小为${str0}，最大为${str1}.test
 * @param {string} strtemplate  字符串模板
 * @param {array} array 需要替换的字符串数组 ['20','30']
 */
export const ReplaceString = (strtemplate, array) => {
  // 初始化返回值
  let reval = '';
  // 转换待替换数组为根据索引为键值的对象
  let _obj = {};
  // 循环待替换字符串数组
  array.forEach((item, index) => {
    // 采用'str'+索引做为键值
    _obj[`str${index}`] = item;
  })

  // 根据模板字符串生成模板对象
  var _compiled = _.template(strtemplate);
  try {
    // 进行字符串替换
    reval = _compiled(_obj);
  } catch (e) {
    // 输出错误日志
    console.log('Error replacing string：', strtemplate, array);
  }
  return reval;
}

/**
 * 根据长度范围验证字符串
 * 如过范围条件为空，则不进行判断
 * @param {number} minsize  字符串模板
 * @param {number} maxsize  字符串模板
 */
export const CheckSize = (str, minsize, maxsize) => {
  let _check = true;
  // 判断最小长度
  if (!_.isNull(minsize) && _.isNumber(minsize)) {
    _check = (_.size(str) >= minsize);
  }
  // 判断最大长度
  if (!_.isNull(maxsize) && _.isNumber(maxsize)) {
    _check = (_.size(str) <= maxsize);
  }
  return _check;
}

/**
 * 字符串全局替换
 * @param {[type]} str         [进行替换的字符串]
 * @param {[type]} needRepStr str或array 如果是array会替换数组中的每一个字符串  [需要替换的字符串]
 * @param {[type]} replacedStr str或array 如果是array顺序和needRepStr保持相同 [替换后的字符串]
 * @param {[type]} isIgnoreCase bool 是否忽略大小写
 */
export const ReplaceAll = (str, needRepStr, replacedStr, isIgnoreCase) => {
  let regStr = isIgnoreCase ? "gmi" : "gm";
  if (_.isArray(needRepStr) && _.isArray(replacedStr)) {
    _.forEach(needRepStr, (item, index) => {
      str = str.replace(new RegExp(item, 'gm'), replacedStr[index]);
    });
    return str;
  }
  return str.replace(new RegExp(needRepStr, 'gm'), replacedStr);
}

/**
 * 验证日期在不需要校验业务日期时,返回null代表验证失败，否则返回验证后的日期
 * @param {*} formatValue
 */
export const CheckDateInExactMode = (formatValue, customFormat) => {
  let format = customFormat || moment.localeData()._longDateFormat.L;
  let upperFormat = ReplaceAll(format.toUpperCase(), ["-", "/"], ["", ""], true);
  let yearIndex = upperFormat.indexOf("Y");
  let monthIndex = upperFormat.indexOf("M");
  let dayIndex = upperFormat.indexOf("D");
  let year = formatValue.substr(yearIndex, 4);
  let month = formatValue.substr(monthIndex, 2);
  let day = formatValue.substr(dayIndex, 2);
  if (formatValue.length === 6) {
    //年的位置只可能在最前面或最后面
    let inputYear = yearIndex === 0 ? formatValue.substr(0, 2) : formatValue.substr(4, 2);
    //如果当前时间的年减去 19和输入的年 大于150，取20为前缀，否则取19
    let fillNumber = moment().diff(moment(`19${inputYear}`), "years") > 150 ? "20" : "19";
    year = `${fillNumber}${inputYear}`;
    if (yearIndex === 0) { //年只输入两位并且年在前面的时候，取月和日的索引减2
      month = formatValue.substr(monthIndex - 2, 2);
      day = formatValue.substr(dayIndex - 2, 2);
    }
  }
  if ((+month) > 12) return;
  if ((+day) > 31) return;
  if (day === "00") day = "01";
  if (month === "00") month = "01";
  let maxDay = moment(`${year}-${month}`, "YYYY-MM").daysInMonth();
  if (day > maxDay) return;
  let current = moment(`${year}-${month}-${day}`, "YYYY-MM-DD");
  return current;
}

/**
 * 验证日期在需要校验业务日期时，返回null代表验证失败，否则返回验证后的日期
 * @param {*} formatValue
 */
export const CheckDateInBusinessDate = (formatValue, customFormat, limitDate) => {
  let bussinessDt = limitDate;
  let formatLength = formatValue.length;
  let year = bussinessDt.format("YYYY");
  let month = bussinessDt.format("MM");
  let day = bussinessDt.format("DD");
  let busMaxDay = moment(`${year}-${month}`, "YYYY-MM").daysInMonth();
  let inputYear, inputMonth, inputDay;
  if (formatLength === 1 || formatLength === 2) {   //不需要根据forma索引来算的
    inputDay = formatLength === 1 ? `0${formatValue}` : formatValue;
    if (inputDay === "00") inputDay = "01";
    if ((+inputDay) > busMaxDay) return;
    inputMonth = month;
    inputYear = year;
  }
  else { //需要根据索引来算的
    let format = customFormat || moment.localeData()._longDateFormat.L;
    let upperFormat = ReplaceAll(format.toUpperCase(), ["-", "/"], ["", ""], true);
    let yearIndex = upperFormat.indexOf("Y");
    let monthIndex = upperFormat.indexOf("M");
    let dayIndex = upperFormat.indexOf("D");
    if (formatLength === 3 || formatLength === 4) {
      let inputValue = formatLength === 3 ? `0${formatValue}` : formatValue;
      if (Math.min(monthIndex, dayIndex) === dayIndex) { //找出格式是月日还是日月
        inputDay = inputValue.substr(0, 2);
        inputMonth = inputValue.substr(2, 2);
      }
      else {
        inputMonth = inputValue.substr(0, 2);
        inputDay = inputValue.substr(2, 2);
      }
    }
    else if (formatLength === 6) {
      inputYear = yearIndex === 0 ? formatValue.substr(0, 2) : formatValue.substr(4, 2);
      inputYear = `${20}${inputYear}`;
      if (yearIndex === 0) { //年只输入两位并且年在前面的时候，取月和日的索引减2
        inputMonth = formatValue.substr(monthIndex - 2, 2);
        inputDay = formatValue.substr(dayIndex - 2, 2);
      }
      else { //取月和日的索引
        inputMonth = formatValue.substr(monthIndex, 2);
        inputDay = formatValue.substr(dayIndex, 2);
      }
    }
    else {
      inputYear = formatValue.substr(yearIndex, 4);
      inputMonth = formatValue.substr(monthIndex, 2);
      inputDay = formatValue.substr(dayIndex, 2);
    }
    if ((+inputMonth) > 12) return;
    if (inputDay === "00") inputDay = "01";
    if (inputMonth === "00") inputMonth = "01";
    inputYear = inputYear || year;
    if ((+inputYear) < (+year)) inputYear = year;
    let inputMaxDay = moment(`${inputYear}-${inputMonth}`, "YYYY-MM").daysInMonth();
    if ((+inputDay) > inputMaxDay) return;
  }
  let inputDate = moment(`${inputYear}-${inputMonth}-${inputDay}`, "YYYY-MM-DD");
  let diff = inputDate.diff(bussinessDt, "days");
  if (diff >= 0) {
    month = inputMonth;
    day = inputDay;
    year = inputYear;
  }
  else {
    if (formatLength < 3) { //只输入日时
      day = inputDay;
      month = +month + 1;
      if (month > 12) {
        month = 1;
        year = +year + 1;
      }
    }
    else {  //输入年和月
      day = inputDay;
      month = inputMonth;
      year = +year + 1;
    }
  }
  let current = moment(`${year}-${month}-${day}`, "YYYY-MM-DD");
  return current;
}

/**
 * 整理路由地址，通过match返回router标识和业务地址
 * @param {*} match 路由参数
 */
export const PackageUrl = (match) => {
  const routeArray = match.url.split('/');
  const routeTag = _.slice(routeArray, 3, 12);
  const tabIndex = match.params.tabIndex;
  const identifiers = `/${tabIndex}/`;
  // 根据标识符在url中查找起始位置
  const strIndex = match.url.indexOf(identifiers);
  const url = match.url.substring(strIndex + identifiers.length);
  return {
    ...match,
    routeTag,
    url
  }
}