//+
export function Add(n, m) {
    n = typeof n == "string" ? n : numToString(n);
    m = typeof m == "string" ? m : numToString(m);
    var F = n.indexOf(".") != -1 ? handleNum(n) : [n, 0, 0],
        S = m.indexOf(".") != -1 ? handleNum(m) : [m, 0, 0],
        l1 = F[2],
        l2 = S[2],
        L = l1 > l2 ? l1 : l2,
        T = Math.pow(10, L);
        let temp = S[1] * T / Math.pow(10, l2);
        temp = m >= 0 ? temp : -temp;
    return (F[0] * T + F[1] * T / Math.pow(10, l1) + S[0] * T + temp) / T;
}
//-
export function Subtract(n, m) {
    n = typeof n == "string" ? n : numToString(n);
    m = typeof m == "string" ? m : numToString(m);
    var F = n.indexOf(".") != -1 ? handleNum(n) : [n, 0, 0],
        S = m.indexOf(".") != -1 ? handleNum(m) : [m, 0, 0],
        l1 = F[2],
        l2 = S[2],
        L = l1 > l2 ? l1 : l2,
        T = Math.pow(10, L);
        let temp = S[1] * T / Math.pow(10, l2);
        temp = m >= 0 ? temp : -temp;
    return (F[0] * T + F[1] * T / Math.pow(10, l1) - S[0] * T - temp) / T
}
// *
export function Multiply(n, m) {
    n = typeof n == "string" ? n : numToString(n);
    m = typeof m == "string" ? m : numToString(m);
    var F = n.indexOf(".") != -1 ? handleNum(n) : [n, 0, 0],
        S = m.indexOf(".") != -1 ? handleNum(m) : [m, 0, 0],
        l1 = F[2],
        l2 = S[2],
        L = l1 > l2 ? l1 : l2,
        T = Math.pow(10, L);
    return ((F[0] * T + F[1] * T / Math.pow(10, l1)) * (S[0] * T + S[1] * T / Math.pow(10, l2))) / T / T
}
// /
export function Divide(n, m) {
    n = typeof n == "string" ? n : numToString(n);
    m = typeof m == "string" ? m : numToString(m);
    var F = n.indexOf(".") != -1 ? handleNum(n) : [n, 0, 0],
        S = m.indexOf(".") != -1 ? handleNum(m) : [m, 0, 0],
        l1 = F[2],
        l2 = S[2],
        L = l1 > l2 ? l1 : l2,
        T = Math.pow(10, L);
    //console.log(((F[0] * T + F[1] * T / Math.pow(10, l1)) / (S[0] * T + S[1] * T / Math.pow(10, l2))),n / m + Math.pow(2,-52))
    return ((F[0] * T + F[1] * T / Math.pow(10, l1)) / (S[0] * T + S[1] * T / Math.pow(10, l2)))
    //return n / m + Math.pow(2,-52);
}
function numToString(tempArray) {
    if (Object.prototype.toString.call(tempArray) == "[object Array]") {
        var temp = tempArray.slice();
        for (var i, l = temp.length; i < l; i++) {
            temp[i] = typeof temp[i] == "number" ? temp[i].toString() : temp[i];
        }
        return temp;
    }
    if (typeof tempArray == "number") {
        return tempArray.toString();
    }
    return []
}
function handleNum(n) {
    n = typeof n !== "string" ? n + "" : n;
    var temp = n.split(".");
    temp.push(temp[1].length);
    return temp
}

//金额计算  val 值  r 计算方式 s 小数位数
export function Round(val,r,s){
    var isNegative = 1;
    var rtnVal;
    var check = Number(val);
    if(check !== check){
        return void 0;
    }
    if(check === 0){
        return 0;
    }
    var plus = Math.abs(check);
    if(check !== plus){
        isNegative = -1;
    }
    //取整
    if(r === 0){
        rtnVal = plus >>> 0;
    }else if(r === 1){//四舍五入
        rtnVal = round_half_up(plus, s);
    }else if(r === 2){//向上取整
        rtnVal = round_up(plus, s);
    }else if(r === 3){//向下取整
        rtnVal = round_down(plus, s);
    }else if(r === 4){//四舍六入 五成双
        rtnVal = plus.toFixed(s);
    }else{
        rtnVal = round_half_up(plus, s);
    }
    return rtnVal * isNegative;
};
//四舍五入
function round_half_up(val, s){
    var changenum = ((val * Math.pow(10, s) + 0.5) >>> 0) / Math.pow(10, s) ;
    return changenum ;
}
//向上取整
function round_up(val, s){
    return _up_down(val, s, 1)
}
//向下取整
function round_down(val, s){
    return _up_down(val, s, 0)
}
//向上取整 f=1 向下 f=0
function _up_down(val, s, f){
    var num = val * Math.pow(10, s);
    var num1 = num >>> 0;
    if(num1 !== num){
        num1 += f;
    }
    return num1 / Math.pow(10, s);
}
