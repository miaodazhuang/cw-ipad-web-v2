// 引入fetch前后端通用库
import fetch from 'isomorphic-fetch';
// 引入api地址映射模块
import { getApiHost } from '../api-host-config';
// 获取错误code对应的文字信息
import { GetLanguage as zh_cn_GetLanguage } from 'zh_cn/errormessage';
import { GetLanguage as ja_jp_GetLanguage } from 'ja_jp/errormessage';
import { GetLanguage as en_us_GetLanguage } from 'en_us/errormessage';

// 引入字符串帮助方法
import { ReplaceAll } from './string_helper';
// 引入cookie帮助方法
import { SetCookie, GetCookie, GetCookieJSON } from './cookie_helper';
// 引入操作本地缓存库
import { SetSessionItem } from './sessionstorage_helper';
// 引入基础类库
import _ from 'lodash';

let _responseData = null;

function getToken() {
  requestEx('/bs/BsToken/getToken', 'post', null, 'Component').then(data => {
    // console.log('data',data);
  })

}

/**
 * Json转换方法
 * @param  {object} response 从API层接收到的返回值
 * @return {jsonobject}   返回转换后的json对象
 */
function parseJSON(response) {
  // 转换json
  return response.json();
}

/**
 * 验证网络返回数据状态
 * @param  {object} response 从API层接收到的返回值
 * @return {object} response 从API层接收到的返回值
 */
function checkStatus(response) {
  // 判断状态
  if (response.ok && response.status >= 200 && response.status < 300) {
    return response;
  }
  // 获取返回的错误信息
  const error = new Error(response.statusText);
  error.response = response;
  // 触发异常事件
  throw error;
}

/**
 * 处理blob流数据类型
 *
 * @param {any} reData
 * @returns
 */
async function handleblob(reData) {
  let _fileName = reData.headers.get('Content-Disposition').split(';');
  _fileName = _.trim(_fileName[2]);
  _fileName = ReplaceAll(_fileName, '"', '');
  _fileName = unescape(ReplaceAll(_fileName, 'filename=', ''));
  return {
    fileName: _fileName,
    blobData: await reData.blob()
  };
}

function handleStream(reData) {
  const objectURL = URL.createObjectURL(reData.blobData);
  global.__CUSTOMDOWNLOAD__ && global.__CUSTOMDOWNLOAD__(objectURL, reData.fileName);
}

/**
 * 检查API层返回的公共对象
 * @param  {object} response 从API层接收到的返回值
 * @return {object} response 从API层接收到的返回值
 */
async function checkCommonDto(response) {
  _responseData = response;
  let error = new Error();
  //alert(response.responseCommonDto.resultCode);
  //查询状态未返回结果
  if (!response) {
    // error["message"] = 'Request interface not found';
    // error["responseCommonDto"] = {
    //   asyncId: null,
    //   errorLevel: "0",
    //   invokerEndTime: 10977996488150164,
    //   lans: null,
    //   message: "000000",
    //   replaceInfoMap: null,
    //   resultCode: "0",
    //   sessionKey: null,
    //   showMessage: "操作成功",
    //   token: null,
    //   tracerId: null,
    //   userUid: null
    // };
    //throw error;
  } else if (`${response.responseCommonDto.resultCode}` === '0') {
    return response;
  } else if (response.responseCommonDto.errorLevel === '9') {
    await SetSessionItem("DictionaryData", null);
    await SetSessionItem("EncryptStr", null);
    await SetSessionItem("UserData", null);
    await SetSessionItem("cacheData", null);
    await SetSessionItem("tabData", null);
    await SetSessionItem("currentTabData", null);
    // SetSessionItem("SystemMessage", `${response.error}(${response.responseCommonDto.message}):${response.errorDes}`);
    await SetSessionItem('SystemMessage', concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage));
    // alert(`${response.error}(${response.responseCommonDto.message}):${response.errorDes}`);
    // window.location.href = '/';
    throw '*#logout#*';
    //严重错误 跳转到登录界面 href="";
  } else if (response.responseCommonDto.errorLevel === '5') {
    error["message"] = concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage)
    error["responseCommonDto"] = response.responseCommonDto;
    //中间错误 进行错误提示
    if (response.responseCommonDto.resultCode === '1' || response.responseCommonDto.resultCode === '2') {
      error["message"] = concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage)
      //alert(error["message"])
      throw error;
    } else if (response.responseCommonDto.resultCode === '3') {
      // 重复提交 调用获取token的方法获取新方法
      getToken();
      // error.message = GetErrMessage(response.responseCommonDto.message);
      // error["message"] = `${response.error}(${response.responseCommonDto.message}):${response.errorDes}`;
      error["message"] = concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage)
      throw error;
    }
    //通过错误代码清单直接返回错误编码对应的错误信息
    // error["message"] = `${response.error}(${response.responseCommonDto.message}):${response.errorDes}`;
    error["message"] = concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage)
    throw error;
  } else {
    error["message"] = concatCodeAndMessage(response.responseCommonDto.message, response.responseCommonDto.showMessage)
    error["responseCommonDto"] = response.responseCommonDto;
    throw error;
  }
}

//处理错误代码
function concatCodeAndMessage(code, message) {
  //添加判断如果没有message消息则去errormessage.js文件中进行查找
  if (message === undefined || message === null) {

    let text = "";
    if (global.__LANGUAGE__ === 'en_us') {
      text = en_us_GetLanguage(code);
    } else if (global.__LANGUAGE__ === 'ja_jp') {
      text = ja_jp_GetLanguage(code);
    } else if (global.__LANGUAGE__ === 'zh_cn') {
      text = zh_cn_GetLanguage(code);
    }
    return `${code} : ${text}`;
  } else {
    return `${code} : ${message}`;
  }
}

function dispatchErrMessage(message){
  global.__DISPATCH__({
    type: "SystemModel/updateMessageState",
    statecode: 2,
    message: message,
  })
}

/**
 * 默认的异步请求方法
 * @param {string} apiurl Api地址
 * @param {string} method 请求类型 post|get
 * @param {obj} body  请求参数
 */
export function FetchHandle(apiurl, method, body, handleErr = true) {
  return requestEx(apiurl, method, body, 'Component', handleErr);
}

/**
 * 基于自定义token名称的异步请求
 * @param {string} apiurl Api地址
 * @param {string} method 请求类型 post|get
 * @param {obj} body  请求参数
 * @param {string} tokencode token代码
 */
export function FetchHandleForToken(apiurl, method, body, tokencode, handleErr = true) {
  return requestEx(apiurl, method, body, tokencode, handleErr);
}

/**
 * 发起主业务数据访问
 * @param {string} url    Api地址
 * @param {string} method 请求类型 get，post
 * @param {[type]} body   post数据
 */
async function requestEx(apiurl, method, body, tokencode, handleErr) {
  // 获取token节点
  let _jwToken = GetCookieJSON('JW_TOKEN') || {};
  let timeZone = new Date().getTimezoneOffset();
  // 拼写header
  const _headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    'JW_DATA': global.__USERINFO__ ? global.__USERINFO__.JW_DATA : '',
    'JW_TOKEN': _jwToken[tokencode],
    'JW_HOTEL_DT': global.__BUSINESSDT__ || '',
    'JW_UID': global.__USERINFO__ ? global.__USERINFO__.UserUid : '',
    'JW_CHAIN': global.__CHAININFO__ ? global.__CHAININFO__.ChainUid : '',
    'JW_UNIT': global.__UNITINFO__ ? global.__UNITINFO__.UnitUid : '',
    'JW_WSNO': global.__WSNO__ || '',
    'JW_TIMEZONE': timeZone,
    'JW_LANGUAGE': global.__LANGUAGE__ ? global.__LANGUAGE__.replace('_', '-') : 'zh-cn'
  }
  // 发起异步调用，并检查dto参数
  return request(apiurl, { body, headers: _headers, handleErr })
    .then(checkCommonDto)
    .then(response => {
      // 如过调用方式为post，则重置token
      if (method.toLowerCase() === 'post' && !_.startsWith(apiurl, 'http')) {
        // 通过commondto获取新的token
        _jwToken[tokencode] = (response.responseCommonDto && response.responseCommonDto.token)
          ? response.responseCommonDto.token
          : '';
        // 设置token
        SetCookie('JW_TOKEN', _jwToken);
      }
      // 返回response值
      return response;
    }).catch(err => {
      if (err instanceof Error){
        console.error('request-catch:',err.name + err.stack)
        err.stack = '';
        err.name = '';
      }
      throw err;
    });
}

function request(url, {
  body,
  mode = 'cors',
  cache = 'no-cache',
  method = 'POST',
  headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    // 设置请求头上的sessionKey
    'JW_DATA': global.__USERINFO__ ? global.__USERINFO__.JW_DATA : '',
    'JW_TOKEN': GetCookie('JW_TOKEN') || '',
    'JW_HOTEL_DT': global.__BUSINESSDT__ || '',
    'JW_UID': global.__USERINFO__ ? global.__USERINFO__.UserUid : '',
    'JW_CHAIN': global.__CHAININFO__ ? global.__CHAININFO__.ChainUid : '',
    'JW_UNIT': global.__UNITINFO__ ? global.__UNITINFO__.UnitUid : '',
    'JW_WSNO': global.__WSNO__ || '',
    'JW_TIMEZONE': new Date().getTimezoneOffset(),
    'JW_LANGUAGE': global.__LANGUAGE__ ? global.__LANGUAGE__.replace('_', '-') : 'zh-cn'
  },
  handleErr = true
} = {}) {
  // 获取环境变量
  if (!_.startsWith(url, 'http')) {
    url = getApiHost(url);
  } else if (_.startsWith(url, 'http')) {
    headers = {
      'content-type': 'application/json',
    }
  }
  // 设置body
  body = body
    ? JSON.stringify(body)
    : null;
  //发起异步调用
  return fetch(url, { body, mode, cache, method, headers })
    .then(checkStatus)
    .then(parseJSON)
    .catch(err => {
      if (handleErr) {
        let error = new Error(-1);
        error.message = `M0000001 : Internet connecting failed, please try again later.`;
        throw error;
        // dispatchErrMessage(error.message);
      }
    });
}

/**
 * 不检查返回值json格式，一般用于下载文件使用
 *
 * @export
 * @param {any} url
 * @param {any} [{
 *   body,
 *   mode = 'cors',
 *   cache = 'no-cache',
 *   method = 'POST',
 *   headers = {
 *     'Content-Type': 'application/json; charset=UTF-8',
 *     // 设置请求头上的sessionKey
 *     'JW_DATA': GetCookie('JW_DATA') || '',
 *     'JW_TOKEN': GetCookie('JW_TOKEN') || '',
 *     'JW_HOTEL_DT':GetCookie('businessDt') || '',
 *     'JW_UID': GetCookie('UserUid')
 *   }
 * }={}]
 * @returns
 */
export async function FetchHandleForDownload(url, {
  body,
  mode = 'cors',
  cache = 'no-cache',
  method = 'POST'
} = {}) {
  let _jwToken = GetCookieJSON('JW_TOKEN') || {};
  // 拼写header
  let _headers = {
    'Content-Type': 'application/json; charset=UTF-8',
    'JW_DATA': global.__USERINFO__ ? global.__USERINFO__.JW_DATA : '',
    'JW_TOKEN': _jwToken['Component'],
    'JW_HOTEL_DT': global.__BUSINESSDT__ || '',
    'JW_UID': global.__USERINFO__ ? global.__USERINFO__.UserUid : '',
    'JW_CHAIN': global.__CHAININFO__ ? global.__CHAININFO__.ChainUid : '',
    'JW_UNIT': global.__UNITINFO__ ? global.__UNITINFO__.UnitUid : '',
    'JW_WSNO': global.__WSNO__ || '',
    'JW_TIMEZONE': new Date().getTimezoneOffset(),
    'JW_LANGUAGE': global.__LANGUAGE__ ? global.__LANGUAGE__.replace('_', '-') : 'zh-cn'
  }
  // 获取环境变量
  if (!_.startsWith(url, 'http')) {
    url = getApiHost(url);
  } else if (_.startsWith(url, 'http')) {
    _headers = {
      'content-type': 'application/json',
    }
  }
  // 设置body
  body = body
    ? JSON.stringify(body)
    : null;
  //发起异步调用
  return fetch(url, { body, mode, cache, method, headers: _headers })
    .then(checkStatus)
    .then(handleblob)
    .then(handleStream)
    .catch(err => {
      let error = new Error(-1);
      error.message = `M0000012 : Internet connecting failed, please try again later.`;
      throw error;
      // dispatchErrMessage(error.message)
    });
}