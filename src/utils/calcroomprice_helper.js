import {Add,Subtract,Multiply,Divide,Round} from './mathcalc_helper.js';
/**
 * 计算方式
 * @type {Object}
 */
const RoundModeEnum = {
  /** 取整 */
  intValue: 0,

  /** 四舍五入 */
  roundHalfUp: 1,

  /** 向上取整 */
  roundUp: 2,

  /** 向下取整 */
  roundDown: 3,

  /** 四舍六入五成双
   * 1. 被修约的数字小于5时，该数字舍去；
   * 2. 被修约的数字大于5时，则进位；
   * 3. 被修约的数字等于5时，要看5前面的数字，若是奇数则进位，若是偶数则将5舍掉，即修约后末尾数字都成为偶数；若5的后面还有不为“0”的任何数，则此时无论5的前面是奇数还是偶数，均应进位。
   * */
  roundHalfEven: 4
}
/**
 * 拆解金额
 * @param {[type]} data [description]
 * return {
 * net: 净价
 * svc：服务费
 * totalTax：总税
 * gross: 总房价
 *  }
 */
export default function setBreak(data){
    let r = data.roundMode;
		let s = data.scale;
		let svcPcnt = toDouble(data.svcRates);
		let tax1Pcnt = toDouble(data.tax1Rates);
		let tax2Pcnt = toDouble(data.tax2Rates);
		let tax3Pcnt = toDouble(data.tax3Rates);
		let tax4Pcnt = toDouble(data.tax4Rates);
		let tax5Pcnt = toDouble(data.tax5Rates);
		let svcTax1Pcnt = toDouble(data.svctax1Rates);
		let svcTax2Pcnt = toDouble(data.svctax2Rates);
		let svcTax3Pcnt = toDouble(data.svctax3Rates);
		let svcTax4Pcnt = toDouble(data.svctax4Rates);
		let svcTax5Pcnt = toDouble(data.svctax5Rates);
  // 计算charge
		let charge = calcCharge(data);
    data.charge = charge;

		// 根据收费标记获取被乘数
		let arrXflg = getXflg(data);

		// 计算净额
		let net = calcNet(data, arrXflg);

		// 计算服务费(svc = round(net * svcPcnt / 100))
		let svc = Round(Divide(Multiply(net,svcPcnt),100),r,s);

		// 计算税1总额 sumTax1 = round(net * tax1Pcnt / 100 + net * svcPcnt / 100 * svctax1Pcnt / 100)
		let sumTax1 = calcTax(net, tax1Pcnt, svcPcnt, svcTax1Pcnt, r, s);
		// 计算服务费的税1
		let svcTax1 = calcSvcTax(net, svcPcnt, svcTax1Pcnt, r, s);
		// 倒减出净额的税1
		let tax1 = Subtract(sumTax1,svcTax1);

		// 计算税2总额 sumTax2 = round(net * tax2Pcnt / 100 + net * svcPcnt / 100 * svctax2Pcnt / 100)
		let sumTax2 = calcTax(net, tax2Pcnt, svcPcnt, svcTax2Pcnt, r, s);
		// 计算服务费的税2
		let svcTax2 = calcSvcTax(net, svcPcnt, svcTax2Pcnt, r, s);
		// 倒减出净额的税2
		let tax2 = Subtract(sumTax2,svcTax2);

		// 计算税3总额 sumTax3 = round(net * tax3Pcnt / 100 + net * svcPcnt / 100 * svctax3Pcnt / 100)
		let sumTax3 = calcTax(net, tax3Pcnt, svcPcnt, svcTax3Pcnt, r, s);
		// 计算服务费的税3
		let svcTax3 = calcSvcTax(net, svcPcnt, svcTax3Pcnt, r, s);
		// 倒减出净额的税3
		let tax3 = Subtract(sumTax3,svcTax3);

		// 计算税4总额 sumTax4 = round(net * tax4Pcnt / 100 + net * svcPcnt / 100 * svctax4Pcnt / 100)
		let sumTax4 = calcTax(net, tax4Pcnt, svcPcnt, svcTax4Pcnt, r, s);
		// 计算服务费的税4
		let svcTax4 = calcSvcTax(net, svcPcnt, svcTax4Pcnt, r, s);
		// 倒减出净额的税4
		let tax4 = Subtract(sumTax4,svcTax4);

		// 计算税5总额 sumTax5 = round(net * tax5Pcnt / 100 + net * svcPcnt / 100 * svctax5Pcnt / 100)
		let sumTax5 = calcTax(net, tax5Pcnt, svcPcnt, svcTax5Pcnt, r, s);
		// 计算服务费的税5
		let svcTax5 = calcSvcTax(net, svcPcnt, svcTax5Pcnt, r, s);
		// 倒减出净额的税5
		let tax5 = Subtract(sumTax5,svcTax5);

    charge = Round(charge,r,s);
    //svc 服务费
		// 最后重新倒减出净额
		net = reCalcNet(charge, svc, sumTax1, sumTax2, sumTax3, sumTax4, sumTax5, arrXflg); //净价
    //总税额： sumTax1, sumTax2, sumTax3, sumTax4, sumTax5相加
    let totalTax = Add(Add(Add(Add(sumTax1,sumTax2),sumTax3),sumTax4),sumTax5);

		// 计算出gross(将net、svc、tax1~5、svcTax1~5全部相加)
		let gross = Add(Add(Add(Add(Add(Add(net,svc),tax1),tax2),tax3),tax4),tax5);
		gross = Add(Add(Add(Add(Add(gross,svcTax1),svcTax2),svcTax3),svcTax4),svcTax5);  //总房价

    return {
      net,
      svc,
      totalTax,
      gross,
      charge
    }
}

/**
 * 计算价格，在房价编辑的时候
 * return {
 * net: 净价
 * svc：服务费
 * totalTax：总税
 * gross: 总房价
 * contains： 加收房含金额
 * }
 * @export
 * @param {any} data
 */
export function setBreakInEdit(data){
  let net = data.rateNet;
  let svc = data.rateSvc;
  let totalTax = Add(data.rateTax1,data.rateSvctax1);
  //总房价，累加净价, 服务费和所有的税
  let gross = [data.rateNet,data.rateSvc,data.rateTax1,data.rateTax2,data.rateTax3,data.rateTax4,data.rateTax5,
  data.rateSvctax1,data.rateSvctax2,data.rateSvctax3,data.rateSvctax4,data.rateSvctax5].reduce((pre,current) => {
    return Add(pre, current);
  }, 0);
  // let containAmt = data.extrapkgAmt;
  let charge = data.rateAmt;
  return {
    net,
    svc,
    totalTax,
    gross,
    // containAmt,
    charge
  };
}

/**
 * 计算小时房价格
 * 先减再打折
 * @export
 * @param {any} data
 */
// export function setBreakInHourRoom(data){
//   let discPcnt = data.discPcnt != null ? Divide(data.discPcnt, 100) : 1;
//   let discAmt = data.discAmt != null ? data.discAmt : 0;
//   let charge = Multiply(Add(data.setInputRate,discAmt),discPcnt);
//   return {
//     rateAmt: FormatMoney(charge)
//   };
// }

/**
 * 在确定服务费以及税额后，重新计算净额
 * @return {[type]} [description]
 */
function reCalcNet(charge,svc,tax1,tax2,tax3,tax4,tax5,xFlg){
  let net;
  let svcX = Multiply(svc,xFlg[0]);
  let tax1X = Multiply(tax1,xFlg[1]);
  let tax2X = Multiply(tax2,xFlg[2]);
  let tax3X = Multiply(tax3,xFlg[3]);
  let tax4X = Multiply(tax4,xFlg[4]);
  let tax5X = Multiply(tax5,xFlg[5]);

  net = Subtract(charge,Add(Add(Add(Add(Add(svcX,tax1X),tax2X),tax3X),tax4X),tax5X));

  return net;
}

/**
 * 计算总税额（含服务费税额）
 * @return {[type]} [description]
 */
function calcTax(net,taxPcnt,svcPcnt,svcTaxPcnt,r,s){
  return Round(Add(Divide(Multiply(net, taxPcnt), 100), Divide(Multiply(Divide(Multiply(net, svcPcnt), 100), svcTaxPcnt), 100)), r, s);
}

/**
 * 计算服务费税额
 * @return {[type]} [description]
 */
function calcSvcTax(net,svcPcnt,svcTaxPcnt,r,s){
  return Round(Divide(Multiply(Divide(Multiply(net,svcPcnt),100),svcTaxPcnt),100),r,s);
}

/**
 * 计算charge
 * @return {[type]} [description]
 */
function calcCharge(obj){
    let rate = toDouble(obj.rate); //房价
		let extrAmt = toDouble(obj.extrAmt); //额外收费,房含中flag为2的相加
		let discPcnt;
		if (null == obj.discPcnt) { //折扣
			discPcnt = 1;
		} else {
      discPcnt = obj.discPcnt >= 1 ? Divide(obj.discPcnt,100) : obj.discPcnt;
			discPcnt = toDouble(discPcnt);
		}
		let discAmt = toDouble(obj.discAmt); //折扣金额
		// charge = (Rate + ExtrAmt) * discPcnt + discAmt
		// add(div(mul(add(rate,extrAmt),discPcnt),100),discAmt)
    return Add(Multiply(Add(rate,extrAmt),discPcnt),discAmt);

}

/**
 * 根据收费标记获取被乘数
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
function getXflg(data){
  let xFlg = [];
	if (data.svcChargeFlg === "2") {
		// 服务费加收
		xFlg[0] = 0;
	} else {
		xFlg[0] = 1;
	}

	if (data.tax1ChargeFlg === "2") {
		// 税1加收
		xFlg[1] = 0;
	} else {
		xFlg[1] = 1;
	}

	if (data.tax2ChargeFlg === "2") {
		// 税2加收
		xFlg[2] = 0;
	} else {
		xFlg[2] = 1;
	}

	if (data.tax3ChargeFlg === "2") {
		// 税3加收
		xFlg[3] = 0;
	} else {
		xFlg[3] = 1;
	}

	if (data.tax4ChargeFlg === "2") {
		// 税4加收
		xFlg[4] = 0;
	} else {
		xFlg[4] = 1;
	}

	if (data.tax5ChargeFlg === "2") {
		// 税5加收
		xFlg[5] = 0;
	} else {
		xFlg[5] = 1;
	}

	return xFlg;
}

/**
	 *
	 * 	计算净额
	 *	charge
	 *	=
	 *	net
	 *	+	net 乘 svcPcnt / 100
	 *	+	net 乘 tax1Pcnt / 100
	 *	+	net 乘 tax2Pcnt / 100
	 *	+	net 乘 tax3Pcnt / 100
	 *	+	net 乘 tax4Pcnt / 100
	 *	+	net 乘 tax5Pcnt / 100
	 *	+	net 乘 svcPcnt / 100 乘 svctax1Pcnt / 100
	 *	+	net 乘 svcPcnt / 100 乘 svctax2Pcnt / 100
	 *	+	net 乘 svcPcnt / 100 乘 svctax3Pcnt / 100
	 *	+	net 乘 svcPcnt / 100 乘 svctax4Pcnt / 100
	 *	+	net 乘 svcPcnt / 100 乘 svctax5Pcnt / 100
	 * @param breakDto
	 * @param xFlg
	 * @return
	 */
function calcNet(data,xFlg){
    let charge = toDouble(data.charge);
		let svcPcnt = toDouble(data.svcRates);
		let tax1Pcnt = toDouble(data.tax1Rates);
		let tax2Pcnt = toDouble(data.tax2Rates);
		let tax3Pcnt = toDouble(data.tax3Rates);
		let tax4Pcnt = toDouble(data.tax4Rates);
		let tax5Pcnt = toDouble(data.tax5Rates);
		let svcTax1Pcnt = toDouble(data.svctax1Rates);
		let svcTax2Pcnt = toDouble(data.svctax2Rates);
		let svcTax3Pcnt = toDouble(data.svctax3Rates);
		let svcTax4Pcnt = toDouble(data.svctax4Rates);
		let svcTax5Pcnt = toDouble(data.svctax5Rates);
    let svcPcntOri = svcPcnt;
		// net
		// =
		// ((Rate + ExtrAmt) * discPcnt + discAmt)
		//  除以
		// (1 + svcPcnt/100 + tax1Pcnt/100 + tax2Pcnt/100 + tax3Pcnt/100 + tax4Pcnt/100 + tax5Pcnt/100 +
		// svcPcnt/100 * svctax1Pcnt/100 + svcPcnt/100 * svctax2Pcnt/100 + svcPcnt/100 * svctax3Pcnt/100 + svcPcnt/100 * svctax4Pcnt/100 + svcPcnt/100 * svctax5Pcnt/100)

		let totalPcnt;
		svcPcnt = Divide(Multiply(svcPcnt,xFlg[0]),100);
    svcPcntOri = Divide(svcPcntOri,100);
		tax1Pcnt = Divide(Multiply(tax1Pcnt,xFlg[1]),100);
		tax2Pcnt = Divide(Multiply(tax2Pcnt,xFlg[2]),100);
		tax3Pcnt = Divide(Multiply(tax3Pcnt,xFlg[3]),100);
		tax4Pcnt = Divide(Multiply(tax4Pcnt,xFlg[4]),100);
		tax5Pcnt = Divide(Multiply(tax5Pcnt,xFlg[5]),100);
		svcTax1Pcnt = Divide(Multiply(svcPcntOri,Multiply(svcTax1Pcnt,xFlg[1])),100);
		svcTax2Pcnt = Divide(Multiply(svcPcntOri,Multiply(svcTax2Pcnt,xFlg[2])),100);
		svcTax3Pcnt = Divide(Multiply(svcPcntOri,Multiply(svcTax3Pcnt,xFlg[3])),100);
		svcTax4Pcnt = Divide(Multiply(svcPcntOri,Multiply(svcTax4Pcnt,xFlg[4])),100);
		svcTax5Pcnt = Divide(Multiply(svcPcntOri,Multiply(svcTax5Pcnt,xFlg[5])),100);

		totalPcnt = Add(1,svcPcnt);
		totalPcnt = Add(totalPcnt,tax1Pcnt);
		totalPcnt = Add(totalPcnt,tax2Pcnt);
		totalPcnt = Add(totalPcnt,tax3Pcnt);
		totalPcnt = Add(totalPcnt,tax4Pcnt);
		totalPcnt = Add(totalPcnt,tax5Pcnt);
		totalPcnt = Add(totalPcnt,svcTax1Pcnt);
		totalPcnt = Add(totalPcnt,svcTax2Pcnt);
		totalPcnt = Add(totalPcnt,svcTax3Pcnt);
		totalPcnt = Add(totalPcnt,svcTax4Pcnt);
		totalPcnt = Add(totalPcnt,svcTax5Pcnt);

		let net = Divide(charge,totalPcnt);

		return net;
}

function toDouble(num){
  if(num == null){
    return 0;
  }
  return num * 1;
}
