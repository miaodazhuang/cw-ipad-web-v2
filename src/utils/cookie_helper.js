import cookies from 'js-cookie';

export const SetCookie = (name, value, {
  expires,
  secure,
  path= '/',
  domain= ''
}={}) => {
  return cookies.set(name,value,{expires,path,domain});

}

export const GetCookie = (name)=>{
  return cookies.get(name);

}

export const RemoveCookie= (name)=>{
  return cookies.remove(name);

}

export const GetCookieJSON=(name)=>{
  return cookies.getJSON(name);
}
