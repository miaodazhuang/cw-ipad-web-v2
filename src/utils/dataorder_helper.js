import _ from 'lodash';

/**
 * 排序数据方法
 * @param {[Array]}   DataSource    [需要排序的数据源]
 * @param {[Object]}  RowData       [要改变排序的数据 DataSource的一个子集]
 * @param {[String]}  operationType [操作类型 string :  up 向上    down 向下    top 置顶]
 * @param {[string]}  fieldname [需要比较的类型]
 * @param {[Array]}   return:DataSource 排序后的数据源
 */
export const OrberDataSource = (orgDataSource, RowData, operationType, fieldname) => {
  let DataSource = _.cloneDeep(orgDataSource);
  let currIndex = _.findIndex(DataSource, (value) => {
    return value[fieldname] === RowData[fieldname]
  });
  let targerIndex = 0;
  if (currIndex != -1) {
    if (operationType == 'up') { //向上 索引 -1
      targerIndex = currIndex === 0
        ? 0
        : _. subtract(currIndex, 1);
    } else if (operationType === 'down') { //向下索引 +1
    targerIndex =(_.add(currIndex,1) >= _. size(DataSource))
        ?(_.subtract(_.size(DataSource), 1))
        :(_.add(currIndex,1));
    } else if(operationType ==='top') { //置顶
      targerIndex = 0;
    }
    //取到被替换的目标对象
    let tarObj = DataSource[targerIndex];
    //设置目标对象为当前对象
    DataSource[targerIndex] = RowData;
    //设置当前对象为目标对象
    DataSource[currIndex] = tarObj;
  }
  return DataSource;
}
