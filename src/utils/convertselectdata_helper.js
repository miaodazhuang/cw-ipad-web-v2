import _ from 'lodash';

/**
* 基础转换方法 对获取到的数据源 进行整理转换为select组件使用的数据源对象
*
* @param {any} orgdata 原始数据
* @param {any} namefield 描述字段名称 （string|array）
* @param {any} valuefield 值字段名称
* @returns array
*/
export const baseconvert = (orgdata, namefield, valuefield) => {
  let _itemData = [];
  // 整理参数集合
  if (orgdata && _.isArray(orgdata)) {
    _.forEach(orgdata, (item, key) => {
      let _name = '';
      if (_.isArray(namefield) && namefield.length > 0) {
        let _nameArray = [];
        _.forEach(namefield, (itemNm) => {
          _nameArray.push(item[itemNm]);
        })
        _name = _nameArray.join(' ');
      } else {
        _name = item[namefield]
      }
      _itemData.push({ key: `${item[valuefield]}`, text: _name, value: `${item[valuefield]}`, "data-datasource": item })
    })
  }
  return _itemData;
}

/**
* 索引提升转换，提取原始数据中的字段作为该数据项的索引，以便提升查找速度
*
* @param {any} orgdata 原始数据
* @param {any} indexField 索引字段名称
* @returns array
*/
export const IndexConvert = (orgdata, indexField) => {
  // 如果原始数据为空则直接返回空对象
  if (!orgdata || !_.isArray(orgdata) || _.size(orgdata) === 0) {
    return {};
  }
  let _data = {};
  // 遍历原始数据
  _.forEach(orgdata, (item) => {
    if (_.has(item, indexField)) {
      // 如果原始数据中存在索引字段，则用索引字段的值作为数据对象的Key，并且把数据作为该Key的value进行存储
      _data[item[indexField]] = item;
    }
  })
  return _data;
}