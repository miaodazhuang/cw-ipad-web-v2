
/**
* Number格式化货币方法
*
* @param {any} number 需要格式化得数字
* @param {any} places 小数位
* @param {any} symbol 前缀
* @param {any} thousand 分位符 默认,
* @param {any} decimal 小数点 默认.
* @returns
*/
export const FormatMoney = (number, places = global.__CURRENCY__.scale, symbol, thousand, decimal) => {
  number = number || 0;
  places = !isNaN(places = Math.abs(places)) ? places : 2;
  symbol = symbol !== undefined
    ? symbol
    : "";
  thousand = "";
  //根据开关表进行判断是否千分号显示金额 '0'：否，'1'：是
  if (global.__OPTIONMAP__['3240-010-024'] && global.__OPTIONMAP__['3240-010-024'].option_str1 === '1') {
    thousand = ',';
  }
  decimal = decimal || ".";
  var negative = number < 0
    ? "-"
    : "",
    i = parseInt(number = Math.abs(+ number || 0).toFixed(places), 10) + "",
    j = (j = i.length) > 3
      ? j % 3
      : 0;
  return symbol + negative + (j
    ? i.substr(0, j) + thousand
    : "") + i
      .substr(j)
      .replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places
        ? decimal + Math.abs(number - i).toFixed(places).slice(2)
        : "");
}