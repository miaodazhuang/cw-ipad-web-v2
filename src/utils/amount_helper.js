/**
 * 获取应收金额,会返回一个新的应收金额
 *
 * @export
 * @param {any} order_detail 账单详情
 */
export function getConsumeAmount(order_detail) {
  //对 orderlist林的金额进行处理
  let _consume_amount = 0;
  _consume_amount += order_detail.pay_amount; //付款金额
  _consume_amount += order_detail.bill_amount; //消费金额
  if (order_detail.tax_item_flg1 === '2') {
    //税金1
    _consume_amount += order_detail.tax_amount1;
  }
  if (order_detail.tax_item_flg2 === '2') {
    //税金2
    _consume_amount += order_detail.tax_amount2;
  }
  if (order_detail.tax_item_flg3 === '2') {
    //税金3
    _consume_amount += order_detail.tax_amount3;
  }
  if (order_detail.tax_item_flg4 === '2') {
    //税金4
    _consume_amount += order_detail.tax_amount4;
  }
  if (order_detail.tax_item_flg5 === '2') {
    //税金5
    _consume_amount += order_detail.tax_amount5;
  }
  return _consume_amount;
}
/**
 * 计算两个数的和,解决小数点位数的BUG
 *
 * @export
 * @param {any} num1
 * @param {any} num2
 * @returns
 */
export function addNumber(num1, num2) {
  //第一个数的小数位数 第二个数的小数位数 乘以这个数以成为一个整数
  let sq1,
    sq2,
    m;
  try {
    sq1 = num1
      .toString()
      .split(".")[1]
      .length;

    sq2 = num2
      .toString()
      .split(".")[1]
      .length;

  } catch (e) {
    sq1 = 0;
    sq2 = 0;

  }
  //幂次方
  m = Math.pow(10, Math.max(sq1, sq2));
  return (num1 * m + num2 * m) / m;
}
/**
 * 计算两个数的差,解决小数点位数的BUG
 *
 * @export
 * @param {any} num1
 * @param {any} num2
 * @returns
 */
export function reduceNumber(num1, num2) {
  let sq1,
    sq2,
    m;
  try {
    sq1 = num1
      .toString()
      .split(".")[1]
      .length;

    sq2 = num2
      .toString()
      .split(".")[1]
      .length;

  } catch (e) {
    sq1 = 0;
    sq2 = 0;

  }
  m = Math.pow(10, Math.max(sq1, sq2));
  return (num1 * m - num2 * m) / m;
}
