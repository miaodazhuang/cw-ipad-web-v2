// export {Router, Route, match,RoutingContext,IndexRoute,Link,browserHistory as
// HistoryHandle } from 'dva/router'; export {Router as _Router, Route as
// _Route,match as _match,RoutingContext as _RoutingContext, IndexRoute as
// _IndexRoute,Link as _Link,hashHistory as _hashHistory } from 'dva/router';
// export {Router, Route, match,RoutingContext,IndexRoute,Link,memoryHistory as
// HistoryHandle } from 'react-router'; import {Router as _Router, Route as
// _Route,match as _match,RoutingContext as _RoutingContext, IndexRoute as
// _IndexRoute,Link as _Link,hashHistory as _hashHistory ,browserHistory as
// _browserHistory} from 'dva/router'; export const Router=_Router; export const
// Route=_Route; export const match=_match; export const
// RoutingContext=_RoutingContext; export const IndexRoute=_IndexRoute; export
// const Link=_Link; export const HistoryHandle=(window.__INITIAL__ &&
// window.__INITIAL__.NODE_ENV == 'production') ? _browserHistory : _hashHistory;

import {
  Router,
  Route,
  match,
  RoutingContext,
  IndexRoute,
  Link,
  browserHistory,
  hashHistory
} from 'dva/router';
export default {
  Router,
  Route,
  match,
  RoutingContext,
  IndexRoute,
  Link,
  HistoryHandle : (process.env && process.env.NODE_ENV == 'production')
    ? browserHistory
    : hashHistory
};
