
import { GetCookie } from 'Hyutilities/cookie_helper';
/**
 * 多语帮助文件
 * author:lxy
*/
/**
 * 通过登录是选择的多语言文件动态切语言
 * @param {多语资源对象} languageObject
 */
export const GetLanguage = (languageObject)=>{
  let languageConfig={};
  let language= GetCookie('lang');
  //console.log('GetLanguage_language:',language)
  //判断如果cookie中未存就用构建时的语言参数
  if(language===null || language==='null' ||!language){
    language=process.env.NODE_ENV_LANGUAGECODE;
  }
  languageConfig=languageObject[language];
  return languageConfig;
}