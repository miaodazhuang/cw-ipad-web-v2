/**
 * moment 格式化
*/
import _ from 'lodash';

/**
 *  日期格式化字符串方法
 *  根据开关表进行格式化日期如果开关表没有设置则返回oldformat的格式化字符串
 * @param {原格式化字符串} oldformat
 */
export const FormatMoment = (oldformat) => {
  let strFormat = _.toUpper(_.trim(oldformat));
  //根据开关表进行判断 日期格式（/，-）增加开关表配置，一共有18中显示样式
  if (global.__OPTIONMAP__['3130-010-015'] && global.__OPTIONMAP__['3130-010-015'].option_str1) {
    strFormat = global.__OPTIONMAP__['3130-010-015'].option_str1;
    strFormat = _.toUpper(strFormat);
    //判断是否只显示月日 MM-DD
    if (_.size(oldformat) === 5) {
      //年在开头
      if (strFormat.substring(0, 4) === 'YYYY') {
        strFormat = strFormat.substring(5, strFormat.length);
      } else if (strFormat.substring(3, 7) === 'YYYY') {
        //YYYY在中间
        strFormat = `${strFormat.substring(0, 3)}${strFormat.substring(8, 10)}`;
      } else {
        //YYYY在末尾
        strFormat = strFormat.substring(0, 5);
      }
    }

  }
  return strFormat;
}

//转换字符串日期为YYYY-MM-DD格式
export const TransBaseDate = (strdate) => {
  let rDate = strdate;
  if (_.size(_.trim(strdate)) === 10 && global.__OPTIONMAP__['3130-010-015'] && global.__OPTIONMAP__['3130-010-015'].option_str1) {
    let strFormat = global.__OPTIONMAP__['3130-010-015'].option_str1;
    strFormat = _.toUpper(strFormat);
    switch (strFormat) {
      case 'YYYY-DD-MM':
      case 'YYYY/DD/MM':
        rDate = `${rDate.substring(0,4)}-${rDate.substring(8,10)}-${rDate.substring(5,7)}`;
        break;
      case 'YYYY-MM-DD':
      case 'YYYY/MM/DD':
        rDate = `${rDate.substring(0,4)}-${rDate.substring(5,7)}-${rDate.substring(8,10)}`;
        break;
      case 'MM-DD-YYYY':
      case 'MM/DD/YYYY': //12-31-2011
        rDate = `${rDate.substring(6,10)}-${rDate.substring(0,2)}-${rDate.substring(3,5)}`;
        break;
      case 'MM-YYYY-DD':
      case 'MM/YYYY/DD': //12-2011-31
        rDate = `${rDate.substring(3,7)}-${rDate.substring(0,2)}-${rDate.substring(8,10)}`;
        break;
      case 'DD-YYYY-MM':
      case 'DD/YYYY/MM': //31-2011-12
        rDate = `${rDate.substring(3,7)}-${rDate.substring(8,10)}-${rDate.substring(0,2)}`;
        break;
      case 'DD-MM-YYYY':
      case 'DD/MM/YYYY': //31-12-2011
        rDate = `${rDate.substring(6,10)}-${rDate.substring(0,2)}-${rDate.substring(3,5)}`;
        break;
      default:
        break;
    }

  }
  return rDate;
}