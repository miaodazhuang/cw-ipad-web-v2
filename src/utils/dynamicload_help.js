import React, { Component } from 'react';
import RouterComponentHelp from './routercomponent_help';
import _ from 'lodash';
function registerModel(app, model) {
  const _index = _.findIndex(app._models, (item) => {
    return item.namespace === model.namespace;
  })
  if (_index === -1) {
    app.model(_.cloneDeep(model));
  }
}

function timeoutFunc(ret, ms = 100) {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, ms, ret);
  })
}

function asyncComponent(config) {
  const { resolve, app, title, isModal } = config;

  return class DynamicComponent extends Component {
    constructor(...args) {
      super(...args);
      this.LoadingComponent = config.LoadingComponent || (() => <div>loading...</div>);
      this.state = {
        AsyncComponent: null,
        Namespaces: null
      };
      this.load();
    }

    componentDidMount() {
      this.mounted = true;
    }

    componentWillUnmount() {
      this.mounted = false;
      if (_.size(this.state.Namespaces) > 0) {
        _.forEach(this.state.Namespaces, (item) => {
          if (_.findIndex(app._models, (itemModel) => itemModel.namespace === item) !== -1) {
            setTimeout(() => {
              app.unmodel(item);
            }, 100);
          }
        })
      }
    }

    load() {
      resolve().then(m => {
        const _component = m.component;
        const Namespaces = m.namespaces || [];
        const AsyncComponent = _component.default || _component;
        if (this.mounted && _.isFunction(this.setState)) {
          this.setState({ AsyncComponent, Namespaces });
        } else {
          this.state.AsyncComponent = AsyncComponent; // eslint-disable-line
          this.state.Namespaces = Namespaces; // eslint-disable-line
        }
      });
    }

    render() {
      const { AsyncComponent, Namespaces } = this.state;
      const { LoadingComponent } = this;
      if (AsyncComponent && !isModal) {
        return <AsyncComponent {...this.props} />
        // return (
        //   <RouterComponentHelp
        //     {...{ namespaces: Namespaces, app, title }}
        //     {...this.props}>
        //     <AsyncComponent {...this.props} />
        //   </RouterComponentHelp>
        // )
      } else if (AsyncComponent && isModal) {
        return (
          <AsyncComponent {...this.props} />
        )
      }

      return (
        <RouterComponentHelp
          {...{ namespaces: Namespaces, app, title }}
          {...this.props}>
          <LoadingComponent {...this.props} />
        </RouterComponentHelp>
      )
    }
  };
}

export default function dynamic(config) {
  const { app, models: resolveModels, component: resolveComponent } = config;
  return asyncComponent({
    resolve: config.resolve || function () {
      const models = typeof resolveModels === 'function'
        ? resolveModels()
        : [];
      const component = resolveComponent();
      return new Promise((resolve) => {
        Promise
          .all([
            ...models,
            component
          ])
          //延迟500毫秒
          .then(ret => timeoutFunc(ret))
          .then((ret) => {
            if (!models || !models.length) {
              return resolve({ component: ret[0], namespaces: [] });
            } else {
              const len = models.length;
              const namespaces = [];
              ret.slice(0, len).forEach((m) => {
                if (!Array.isArray(m)) {
                  m = [m];
                }
                m.map(_ => {
                  _ = _.default || _;
                  namespaces.push(_.namespace);
                  registerModel(app, _)
                });
                // const _m = m.default || m;
                // namespaces.push(_m.namespace);
                // registerModel(app, _m);
              });
              resolve({ component: ret[len], namespaces: namespaces });
            }
          });
      });
    },
    ...config
  });
}