import 'semantic-ui-css/semantic.min.css';
import dva from 'dva';
import _ from 'lodash';
import createHistory from 'history/createBrowserHistory';
import registerServiceWorker from './registerServiceWorker';
import Routers from './routers';
import EncryptClass from 'Hyutilities/encrypt_helper';
import { GetSessionItem } from 'Hyutilities/sessionstorage_helper';
import { packageKeyCodes } from 'Hyservices/login-service';
import moment from 'moment';
import 'moment/min/locales';
import './config';
import CommonlibAlert from './commonlib/alert';
import { GetCookie, SetCookie } from 'Hyutilities/cookie_helper';
import SystemModel from 'Hymodels/system-model';
import SystemRouterModel from 'Hymodels/system-router-model';
import SystemHelpModel from 'Hymodels/system-help-model';
import SystemPluginModel from 'Hymodels/system-plugin-model';
import SystemQueryModel from 'Hymodels/system-query-model';
import SystemPosModel from 'Hymodels/system-pos-model';
import 'HyHyless/index.less';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/mainindex';
import ja_jp from 'ja_jp/mainindex';
import zh_cn from 'zh_cn/mainindex';
import { largeLayout } from 'echarts/lib/layout/barGrid';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
let winlang = GetCookie('lang');
if (winlang === null || winlang === 'null' || !winlang) {
  winlang = process.env.NODE_ENV_LANGUAGECODE || 'zh_cn';
  //如果没有值就把默认值写入cookie
  SetCookie('lang', winlang);
}
console.log(`process.env.NODE_ENV:`, process.env.NODE_ENV);
if (process.env.NODE_ENV === "development") {
  if (winlang === "en_us") {
    require('en_us_less/style.less');
  } else if (winlang === "ja_jp") {
    require('ja_jp_less/style.less');
  } else if (winlang === "zh_cn") {
    require('zh_cn_less/style.less');
  }
}
// 整理语种
let lang = winlang;
lang = lang.split('_');
if (_.size(lang) === 2) {
  lang[1] = lang[1].toUpperCase();
}
lang = lang.join('-');

moment.locale(lang);
let resizeTimer = -1;
async function initApp() {
  // 获取浏览器可见区域宽高
  global.__DOCUMENTWIDTH__ = document.body.clientWidth;
  global.__DOCUMENTHEIGHT__ = document.body.clientHeight;
  //语种
  global.__LANGUAGE__ = winlang || process.env.NODE_ENV_LANGUAGECODE;
  // 路由数据
  let tabData = await GetSessionItem('tabData');
  // 页签数据
  let currentTabData = await GetSessionItem('currentTabData');

  //获取缓存的销售点
  let _salePoint = await GetSessionItem('salePoint');
  //获取销售点区域
  let _SystemPosModel = await GetSessionItem('SystemPosModel');
  // 如果操作员信息为空则根据缓存数据创建全局变量
  if (_.isNull(global.__USERINFO__)) {
    // 获取缓存的操作员登录信息
    const _userDataStr = await GetSessionItem('UserData');
    // 获取加密字符串
    const _encryptStr = await GetSessionItem('EncryptStr');

    // 用于创建用户加密类
    const _encryptClass = new EncryptClass(global.__USERDATAKEY__);
    // 根据秘钥创建登录数据
    const _encryptData = _encryptClass.Encrypt(_userDataStr || 'notLogin', true);

    // 如果操作员登录信息存在则为全局变量赋值
    if (_userDataStr && _encryptData.encryptData == _encryptStr) {
      // 根据用户登录信息字符串创建登录数据对象
      const _userData = JSON.parse(_userDataStr);
      // 集团信息
      global.__CHAININFO__ = _userData.ChainInfo;
      // 单位信息
      global.__UNITINFO__ = _userData.UnitInfo;
      // 所属单位信息
      global.__OUUNITINFO__ = _userData.OuUnitInfo;
      // 操作员信息
      global.__USERINFO__ = _userData.UserInfo;
      // 用户权限属性
      global.__USERPARAMS__ = [];
      // 开关表属性
      global.__OPTIONMAP__ = _userData.OptionMap;
      // 币种属性
      global.__CURRENCY__ = _userData.Currency;
      // 操作员菜单
      global.__USERMENU__ = _userData.UserMenus;
      // 用户权级
      global.__USERRIGHTCLS__ = _userData.UserRightCls;
      // 酒店业务日期
      global.__BUSINESSDT__ = _userData.BusinessDt;
      // 工作站号
      global.__WSNO__ = _userData.WsNo;
      // 数据 {}
      global.__SKUMAP__ = _userData.skuMap;
      // 当前酒店功能列表
      global.__FUNMAP__ = _userData.funMap;
      // 页面
      global.__VIEWSMAP__ = _userData.views;
      // 快捷键数据
      packageKeyCodes(_userData.shortKeyMap);
      // 如果操作员ID存在，则通过操作员ID创建参数加密类
      if (_userData.UserInfo.UserUid) {
        // 根据操作员ID创建加密对象实例
        global.__ENCRYPT__ = new EncryptClass(_userData.UserInfo.UserUid);
      }
      //设置销售点
      if (_salePoint) {
        global.__SALEPOINT__ = _salePoint;
      }

      global.__ISLOGIN__ = true;
    }
  }

  // 获取页面初始Model值
  let _initState = window.__INITIAL_STATE__ || {};

  // 初始化路由数据
  _initState["SystemRouterModel"] = {
    "tabData": tabData || [],
    "currentTabData": currentTabData || {}
  }
  if (_SystemPosModel) {
    // 初始化当前销售点区域
    _initState["SystemPosModel"] = {
      ..._SystemPosModel,
    }
  }

  const app = dva({
    history: createHistory(),
    initialState: _initState,
    onEffect: onEffect,
    // onStateChange: onStateChange
  });

  app.router(Routers);
  app.model(SystemModel);
  app.model(SystemRouterModel);
  app.model(SystemHelpModel);
  app.model(SystemPluginModel);
  app.model(SystemQueryModel);
  app.model(SystemPosModel);



  app.start('#root');

  registerServiceWorker();

}

// let prevState = {};
// function onStateChange(state) {
//   const diff = differ(prevState, state);
//   prevState = state;
//   console.log('diff-log', diff);




// }

/**
 * 增加对effect方法的预处理
 *
 * @param {any} effect
 * @param {any} { put, select }
 * @param {any} model
 * @param {any} actionType
 * @returns
 */
function onEffect(effect, { put, select, call }, model, actionType) {
  const filterNamespace = ["SystemModel", "SystemRouterModel"];
  return function* (...args) {

    const isSystemModel = _.includes(filterNamespace, model.namespace);
    const systemModel = yield select(({ SystemModel }) => SystemModel);
    if (!isSystemModel) {
      if (systemModel.fatchstate.statecode === 0 && !args[0].isSkip) {
        // 设置加载中状态
        yield put({
          type: "SystemModel/updateFatchState",
          statecode: 1,
          message: ""
        })
      }
    }

    // 执行function
    try {
      // 封装共同提醒逻辑
      if (model.namespace !== "SystemAlertModel" && !args[0].isCompleteAlert) {
        const currentModel = yield select((state) => state[model.namespace]);
        //const { alertIds } = yield select((state) => state["SystemAlertModel"])
        const commonAlertRes = yield call(CommonlibAlert.getAlertConfig, args, currentModel);
        //let isCompleteAlert = false;
        // && !_.includes(alertIds, commonAlertRes.alertData["alertId"])

        if (_.has(commonAlertRes, "alertData")) {
          yield put({
            ...commonAlertRes.alertData
          });
        }
        if (_.has(commonAlertRes, "intercept") && commonAlertRes.intercept) {
          yield put({
            type: "SystemModel/updateFatchState",
            statecode: 0,
            message: ""
          });
          return;
        }
      }

      yield effect(...args);
      if (!isSystemModel) {

        // 分析请求方式
        //const _effectType = args[0].type.toLowerCase();
        let postHandle = false;
        // if (_effectType.indexOf('save') > -1 ||
        //   _effectType.indexOf('post') > -1 ||
        //   _effectType.indexOf('commit') > -1) {
        //   postHandle = true;
        // }

        if (_.size(args) > 0 && (args[0].successmessage || postHandle)) {
          // 设置需要提示的信息
          yield put({
            type: "SystemModel/updateMessageState",
            statecode: 1,
            message: args[0].successmessage || languageConfig.successmessage
          })
        }
        if ((_.size(args) > 0 && !args[0].isSkip) || !args) {
          // 关闭loading状态
          yield put({
            type: "SystemModel/updateFatchState",
            statecode: 0,
            message: ""
          })
        }
      }

    } catch (e) {
      let _message = '';
      if (args[0].errormessage) {
        _message = args[0].errormessage;
      } else if (e.message) {
        _message = e.message.replace("Error:", "");
        const _findIndex = _message.indexOf(' at ');
        _message = _findIndex === -1 ? _message : _message.substring(0, _findIndex);
        // 如果是超时错误，则直接跳转到登录界面
        if (_.trim(_message) === '*#logout#*') {
          //注销操作 删除存储节点
          console.log("logout");
          window.location.href = '/';
          return;
        }
      }
      yield put({
        type: "SystemModel/updateMessageState",
        statecode: 3,
        message: _message,
      });
      yield put({
        type: "SystemModel/updateFatchState",
        statecode: 0,
        message: ""
      });
      return;
    }
  }
}

//大图小图需要嵌套的dispitem_usenm
//大图 roomstatus-big 一层， roomstatus-item 二层
//小图 roomstatus-small 一层， roomstatus-item 二层
function transferRoomConfigData(configData) {
  const dataTypValueMap = {
    "1": "data_svlu",
    "2": "data_ivlu",
    "3": "data_nvlu"
  }
  let styleData = [];
  const bigSmall = [
    { key: "10000", first: "roomstatus-big", second: "roomstatus-item", secSameLevel: ["roomstatus-selected", "roomstatus-V", "roomstatus-O", "roomstatus-fault", "roomstatus-arrival", "roomstatus-single-selected", "roomstatus-item:hover"] },
    { key: "10001", first: "roomstatus-small", second: "roomstatus-item", secSameLevel: ["roomstatus-selected", "roomstatus-V", "roomstatus-O", "roomstatus-fault", "roomstatus-arrival", "roomstatus-single-selected", "roomstatus-item:hover"] }
  ];
  bigSmall.forEach(size => {//大小图
    if (_.isArray(configData[size.key])) {
      configData[size.key].forEach(item => { //项目
        if (_.isArray(item.disAttrInfos)) {
          let selector = getClassSelector(item, size);
          let tempSelector = {
            className: selector,
            classValue: []
          };
          item.disAttrInfos.forEach(attr => {
            let value = attr[dataTypValueMap[attr.data_typ]];
            if (attr.data_unit) { //拼接单位 例如：px
              value = `${value}${attr.data_unit}`;
            }
            value = `${value}!important`;
            let attrValue = `${attr.dispitem_attr_usenm}:${value};`;
            tempSelector.classValue.push(attrValue);
          });
          if (item.status_flg === "0") { //隐藏
            tempSelector.classValue.push("visibility:hidden!important;");
          }
          if (tempSelector.classValue.length > 0) {
            //整理样式数据
            dealStyleData(tempSelector, styleData);
          }
        }
      });
    }
  });
  return styleData;
}

/**
 * 获取class的选择器
 */
function getClassSelector(item, size) {
  if (item.dispitem_usenm === size.first) { //第一个层级
    return `.${size.first}`;
  } else if (item.dispitem_usenm === size.second) { //第二个层级
    return `.${size.first} .${size.second}`;
  } else {
    let index = _.indexOf(size.secSameLevel, item.dispitem_usenm);
    if (index !== -1) { //第二个层级。并有同层
      return `.${size.first} .${size.second}.${item.dispitem_usenm}`;
    } else { //第三个层级
      let useNm = item.dispitem_usenm;
      if (item.item_typ === "EXT" && item.dispitem_key_id != undefined && item.dispitem_key_id != -1) {
        useNm = `${item.dispitem_usenm}-${item.dispitem_key_id}`;
      }
      return `.${size.first} .${size.second} .${useNm}`;
    }
  }
}

//整理样式数据
function dealStyleData(tempSelector, styleData) {
  //右侧图例中需要使用颜色的className
  const imageExampleClass = ["roomstatus-V", "roomstatus-O", "roomstatus-C", "roomstatus-D", "roomstatus-I", "roomstatus-T", "roomstatus-S", "roomstatus-B", "roomstatus-fault"];
  let selectorValue = tempSelector.classValue.join("");
  styleData.push(`${tempSelector.className} {${selectorValue}}`); //拼接样式数据

  //提取右侧图例中需要使用的颜色
  let exampleSelector = _.find(imageExampleClass, eve => tempSelector.className.indexOf(eve) !== -1);
  if (exampleSelector) {
    let exampleValue = _.find(tempSelector.classValue, eve => eve.indexOf("background-color") !== -1);
    let bigSmallSign = tempSelector.className.indexOf("roomstatus-big") !== -1 ? "roomstatus-example-big" : "roomstatus-example-small"; //区分图例和房态图真实的样式
    styleData.push(`.${bigSmallSign}.${exampleSelector} {${exampleValue}}`);
  }
}

/**
 * 注册自定义样式
 *
 * @param {*} styles 自定义样式
 */
function registerClassName(styles) {
  if (_.size(styles) === 0) {
    return;
  }
  var styleElement = document.getElementById('customStyles');
  if (!styleElement) {
    styleElement = document.createElement('style');
    styleElement.type = 'text/css';
    styleElement.id = 'customStyles';
    document.getElementsByTagName('head')[0].appendChild(styleElement);
  }

  _.forEach(styles, item => {
    styleElement.appendChild(document.createTextNode(item));
  });
}

initApp();

console.log(JSON.stringify(process.env));

/**
 * 设置浏览器事件，当浏览器尺寸变更后，刷新界面
 */
window.addEventListener('resize', (event) => {
  if (resizeTimer !== -1) {
    clearTimeout(resizeTimer);
  }
  resizeTimer = setTimeout(() => {
    let diffWidth = Math.abs(global.__DOCUMENTWIDTH__ - document.body.clientWidth);
    let diffHeight = Math.abs(global.__DOCUMENTHEIGHT__ - document.body.clientHeight);
    if (diffWidth > 10 || diffHeight > 80) {
      clearTimeout(resizeTimer);
      resizeTimer = -1;
      window.location.reload();
    }
  }, 500);
});


