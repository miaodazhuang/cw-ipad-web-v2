/**
 * pos-model
*/
import initState from './initstate';
const _model = {
  namespace: 'SystemPosModel',
  state: initState,
  subscriptions: {
    setup({ dispatch }) {
      dispatch({
        type: 'setTracksize',
      })
    }
  },
  effects: {

  },
  reducers: {
    /**
     * 更新posmodel属性
     *
     * @param {any} state
     * @param {any} action {pluginKey,openStatus}
     */
    changeStatusComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },



    /**
     * 设置滚动条宽度
     *
     * @param {*} state
     * @param {*} action
     */
    setTracksize(state, action) {
      //首先判断是移动端还是pc端，pc端固定10px;
      let tracksize = '10px';
      if (!IsPC()) {
        //判断dpr,保持滚动条宽度为2px
        const dpr = window.devicePixelRatio;
        if (dpr === 1) {
          tracksize = '2px';
        } else if (dpr === 2) {
          tracksize = '4px';
        } else if (dpr === 3) {
          tracksize = '6px';
        }
      }
      return {
        ...state,
        tracksize
      }
    }

  }
}
//判断是否是pc端
function IsPC() {
  var userAgentInfo = navigator.userAgent;
  var Agents = ["Android", "iPhone",
    "SymbianOS", "Windows Phone",
    "iPad", "iPod"];
  var flag = true;
  for (var v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      flag = false;
      break;
    }
  }
  return flag;
}


export default _model;