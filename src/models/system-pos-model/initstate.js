import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/systemposmodel';
import ja_jp from 'ja_jp/systemposmodel';
import zh_cn from 'zh_cn/systemposmodel';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default {
  //当前销售点汇总信息
  salesPointTotal: {
    text:"",
    value:"",
    "data-datasource": {
      name: "",
    }
  },
  //餐点信息
  period: {
    text:"",
    value:"1",
    "data-datasource": {
      name: languageConfig.productFlg_1,
    }
  },

  //顶部查询条件
  fastQuery:"",

}