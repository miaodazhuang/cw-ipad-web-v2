/**
 * 右侧插件model
*/
const _model = {
  namespace: 'SystemPluginModel',
  state: {
    // 插件编号
    "pluginKey": "-1",
    // 四叶草打开开关
    "openStatus": false,
    // 默认插件数据
    "defaultPluginData": [],
    // 插件参数
    "params": {},
    // 关闭四叶草的回调事件类型：命名空间/方法名称
    "callBackType": "",
    //每一步完成后的的回调事件类型：命名空间/方法名称
    "callBackCompletedType": "",
    // 回调参数
    "callBackParams": {}
  },
  effects: {

  },
  reducers: {
    /**
     * 变更四叶草状态
     *
     * pluginKey:插件编号
     * openStatus:展开状态
     * @param {any} state
     * @param {any} action {pluginKey,openStatus}
     */
    changeOpenStatus(state, action) {
      return {
        ...state,
        pluginKey: action.pluginKey,
        openStatus: action.openStatus,
        params: action.params || {},
        // 完成预授权回调方法
        callBackType: action.callBackType || "",
        callBackCompletedType: action.callBackCompletedType || "",
        // 回调参数
        callBackParams: action.callBackParams || {}
      }
    },
    /**
     * 整理默认插件数据
     *
     * @param {any} state
     * @param {any} action
     */
    updateDefaultPluginData(state, action) {
      const { defaultData } = action;
      return {
        ...state,
        defaultPluginData: defaultData
      }

    }
  }
}

export default _model;