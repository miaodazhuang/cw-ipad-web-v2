import _ from "lodash";
const _model = {
  namespace: 'SystemQueryModel',
  state: {
    quickQueryButton:{}
  },
  subscriptions: {
    historyListenEvent({ history, dispatch }) {

    }
  },
  effects: {
    /**
     *
     * 更新快捷查询按钮组
     * @param {*} params
     * @param {*} { select, call, put }
     */
    * updateQuickQueryButtonEffect(params, { select, call, put }) {
      let quickQueryButton = yield select(({ SystemQueryModel }) => SystemQueryModel.quickQueryButton);
      const url = yield select(({ SystemRouterModel }) => SystemRouterModel.currentTabData.match.url);
      let viewUrl = params.url || url.substring(8, 35);
      let keyButton;
      params.isSkip = true;
      if (params.closeKey) {
        keyButton = {};
        quickQueryButton[viewUrl] = _.omit(quickQueryButton[viewUrl], params.closeKeyString)
      } else {
        keyButton = {
          [params.key]: { //对应一类查询条件
            value: params.queryObj.value, //当前查询条件所对应的值
            text: params.queryObj.text, //button文本内容
            data: params.queryObj.data, //button上需要展示的其它文本集合
            status: params.queryObj.status,//查询条件是否有效
            search: params.queryObj.search, //是否执行搜索
            source: params.queryObj.source,//来源 value page, url
            excludeBaseConditions: params.queryObj.excludeBaseConditions, //排除当前页面基础查询条件标识
            excludeFastConditions: params.queryObj.excludeFastConditions, //排除当前页面快捷查询条件标识
          }
        };
      }
      let viewUrlObj = {};
      if (params.mutex) {
        //如果不同key之间是互斥的，则清空其它key值，只保留当前
        quickQueryButton[viewUrl] = {};
      }
      if (_.size(quickQueryButton[viewUrl]) > 0) {
        viewUrlObj = {
          ...quickQueryButton[viewUrl],
          ...keyButton
        }
      } else {
        viewUrlObj = keyButton;
      }
      quickQueryButton = {
        ...quickQueryButton,
        [viewUrl]: viewUrlObj
      };
      yield put({ type: 'updateQuickQueryButton', quickQueryButton: quickQueryButton })
      //执行回调
      if(params.cbDispatchTyp){
        params.isSkip = true;
        yield put({ type: params.cbDispatchTyp, ...params.cbParams })
      }
     },

    * clearUpViewQuickQueryButton(params, { select, call, put }) {
      yield put({
        type: 'clearUpViewQuickQueryButtonComplete',
        viewUrl: params.viewUrl
      })
      //执行回调
      if(params.cbDispatchObj){
        params.isSkip = true;
        yield put({
          type: params.cbDispatchObj.cbDispatchTyp,
          ...params.cbDispatchObj.cbParams
        })
      }
    }



  },
  reducers: {
    /**
     * 更新view对应的快捷查询按钮
     *
     * @param {*} state
     * @param {*} action
     */
    updateQuickQueryButton(state, action) {
      return {
        ...state,
        quickQueryButton: {
          ...action.quickQueryButton
        }
      }
    },
    /**
     * 更新view对应的快捷查询按钮
     *
     * @param {*} state
     * @param {*} action
     */
    clearUpViewQuickQueryButtonComplete(state, action) {
      return {
        ...state,
        quickQueryButton: {
          ..._.omit(state.quickQueryButton, `${action.viewUrl}`)
        }
      }
    },
  }
}

export default _model;