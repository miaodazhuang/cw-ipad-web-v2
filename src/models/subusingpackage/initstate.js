/**
 * systemName:
 * author:
 * remark:
 */
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subusingpackage';
import ja_jp from 'ja_jp/subusingpackage';
import zh_cn from 'zh_cn/subusingpackage';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default {
  acctNo:'',  //  客人编号
  guestNm: '', // 客人名
  balAmt: '', // 余额
  roomNum: '', // 房间号
  totalData:[],
  contentitem:[],
  totalSubmitData:[],
  dictData: {} // 所有信息
}