/**
 * systemName:
 * author:
 * remark:
 */
import initState from './initstate';
import _ from "lodash";
import { FormatMoney } from 'Hyutilities/money_helper';
import Ssubusingpackage from 'Hyservices/subusingpackage';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subusingpackage';
import ja_jp from 'ja_jp/subusingpackage';
import zh_cn from 'zh_cn/subusingpackage';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _Ssubusingpackage = new Ssubusingpackage();

const _model = {
  namespace: 'Msubusingpackage',
  state: { ...initState },
  subscriptions: {},
  effects: {
    * init(params, { select, call, put }) {
      const thisState  = _.cloneDeep(initState);
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      const queryParams = {
        roomNums:[params.roomNumber],
        posId: SystemPosModel.salesPointTotal.value,      // 销售点id
        productFlg: SystemPosModel.period.value,
        acctNo: params.acctNo
      }
      const resultData = yield call(_Ssubusingpackage.init, queryParams);
      thisState.dictData = resultData;
      let content = []; // 处理后的可用房含
      if (resultData) {
        thisState.acctNo = resultData.acctNo;
        thisState.guestNm = resultData.guestNm;
        thisState.balAmt = FormatMoney(resultData.balAmt);
        thisState.roomNum = params.roomNumber;
        thisState.acctStus = resultData.acctStus;
        if (resultData.packages && _.size(resultData.packages)>0) {
          content = _handlekind(resultData.packages);
        }
      }
      thisState.contentitem = content;
      yield put({
        type: 'initComplete',
        data: {
          ...thisState,
          totalData:[],
          totalSubmitData:[],
        }
      });
    },
    /**
     * 保存入账
     * @param {any} params
     * @param {any} {select, put, call}
     */
      * submitAllData(params, {select, put, call}) {
      // debugger
      const thisState = yield select(({ Msubusingpackage }) => Msubusingpackage);
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      const submitParams = {
        posId: SystemPosModel.salesPointTotal.value,      // 销售点id
        roomNum: params.roomNumber,
        acctNo: params.acctNo,
        packageInfo: thisState.totalSubmitData,
        productFlg: SystemPosModel.period.value
      }
      yield call(_Ssubusingpackage.submitPackagePad,submitParams);
      params.successmessage=languageConfig.successmessage;
      yield put({
        type: "SystemModel/updateModalWindowState"
      });
      if (params.cbDispatch) {
        yield put({
          type: params.cbDispatch
        });
      }
    }
  },
  reducers: {
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },
    submitData(state,action){
      return{
        ...state,
        ...action.param
      }
    },
  }
}

// 对当前房含数据进行处理
// "packageId":4,
//   "packageNm":"双早",
//   "packageCnt":1 //可用数量

function _handlekind(data) {
  let content = [];
  _.forEach(data, (node, i) => {
    content.push({
      "isHandleIcon": false,
      // 'textCBfunc':this._textCBfunc,
      "textData": [
        {
          "title": '',
          "value": node.packageNm,
        },
        {
          "title":'',
          "value": `${languageConfig.canUse}：${node.packageCnt}`
        },
        {
          "title": '',
          "value": node.price,
        },
        node]
    });
  });
  return content;
}

export default _model;
