/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
export default {
  //查询条件(同时提供给自动刷新方法使用)
  queryData: {

  },
  //房含列表数据
  data:[],
  //总数
  recordsTotal:0,

  //桌台id
  block_id:"",

  dicData: [],

  selectedFloor: 'all',//选中楼层
  selectedStatus: '1',//选中房含状态
  fastOpen: '',//房号
  //数量统计
  totalCount:[0, 0, 0, 0]
}