/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
import initState from './initstate';
import PathToRegexp from 'path-to-regexp';
import Spostableview from 'Hyservices/postableview';
import _ from 'lodash';
const _Spostableview = new Spostableview();

const _model = {
  namespace: 'Mpostableview',
  state: { ...initState },
  subscriptions: {
    historyListenEvent({ history, dispatch }) {
      return history.listen(async (history) => {
        const match = PathToRegexp("/main/:tabindex/postableview/:paramobject/:encryptstr").exec(history.pathname);
        if (match) {
          let routeParams = global.__COMPARISON__ && await global.__COMPARISON__(match);
          dispatch({
            type: "init",
            routeParams
          })
        }
      })
    }
  },
  effects: {
    /**
     * 初始化方法
     *
     * @param {*} params
     * @param {*} { select, call, put }
     */
    * init(params, { select, call, put }) {
      //销售点为空不进行操作

      if (global.__SALEPOINT__ === null) {
        yield put({
          type: "initComplete",
          data: {
            ...initState
          }
        })
      } else {
        const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
        const thisState = _.cloneDeep(initState);
        //深克隆当前对象
        let _thisState = _.cloneDeep(thisState);
        //查询当前缓存
         const cacheData = yield call(global.__GETCACHEDATA__, "Mpostableview");
        //查询参数
        _thisState.queryData = {
          posId: SystemPosModel.salesPointTotal.value,
          productFlg: SystemPosModel.period.value || "1",
          roomNums: [],
        };
        // //如果存在缓存则使用缓存参数
        if (cacheData) {
          _thisState.queryData = {
            ...cacheData.queryData,
            posId: SystemPosModel.salesPointTotal.value,
            productFlg: SystemPosModel.period.value || "1",
          }
        }

        let dictTyps = ['FLOOR'];
        //查询数据
        const { listData: { data = [] }, dicData } = yield call(_Spostableview.init, dictTyps, _thisState.queryData);
        // _thisState.totalCount[0] = data.length;
        _thisState.dicData = dicData;
        //【待使用】查询，显示所有在店房间中，可用房含数量大于零的房间
        let lastData = _.filter(data, item => item.sum_avl_cnt > 0 && item.acct_stus !== "OUT") || []
        _thisState.totalCount[1] = 0;
        _thisState.totalCount[2] = 0;
        _.forEach(data, item => {
          if (item) {
            _thisState.totalCount[1] += item.sum_avl_cnt;
            _thisState.totalCount[2] += item.sum_use_cnt
          }
        })
        // _thisState.totalCount[3] = (_.filter(data, item => item.sum_avl_cnt === 0 && item.sum_use_cnt === 0)||[]).length;
        _thisState.data = lastData;

        yield put({
          type: "initComplete",
          data: {
            ..._thisState
          }
        })
      }
    },


    /**
     * 查询列表
     *
     * @param {*} params
     * @param {*} { select, call, put }
     */
    * queryList(params, { select, call, put }) {
      const thisState = yield select(({ Mpostableview }) => Mpostableview)
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      const { selectedFloor, selectedStatus, source } = params;
      let _params = {
        posId: SystemPosModel.salesPointTotal.value,
        productFlg: SystemPosModel.period.value,
        roomNums: SystemPosModel.fastQuery?[SystemPosModel.fastQuery]:[],
      };
      if (source === 'floorItem') {
        _params = {
          ..._params,
          floorId: selectedFloor === 'all' ? '' : selectedFloor
        }
      } else {
        _params = {
          posId: SystemPosModel.salesPointTotal.value,
          productFlg: SystemPosModel.period.value,
          roomNums:  SystemPosModel.fastQuery?[SystemPosModel.fastQuery]:[],
          floorId: thisState.selectedFloor === 'all' ? '' : thisState.selectedFloor
        };
      }
      let totalCount = [0, 0, 0, 0];

      const { data } = yield call(_Spostableview.queryList, _params);

      _.forEach(data, item => {
        if (item) {

          totalCount[1] += item.sum_avl_cnt;
          totalCount[2] += item.sum_use_cnt
        }
      })
      let lastData = data;

      if (source === 'roomContainsStatus' || thisState.selectedStatus) {
        //如果是房含状态则过滤数据实现其中0，1,2,3分别是全部，待使用，已使用，无房含
        let currentStatus = selectedStatus || thisState.selectedStatus;
        if (currentStatus === '1') {
          //【待使用】查询，显示所有在店房间中，可用房含数量大于零的房间
          lastData = _.filter(data, item => item.sum_avl_cnt > 0 && item.acct_stus !== "OUT") || [];
        } else if (currentStatus === '2') {
          //【已使用】查询，显示所有在店房间中，可用房含数量等于零且已用房含数量大于零的房间
          lastData = _.filter(data, item => item.sum_avl_cnt === 0 && item.sum_use_cnt > 0);
        } else if (currentStatus === '3') {
          //无房含，查询所有可用已用等于0的
          lastData = _.filter(data, item => item.sum_avl_cnt === 0 && item.sum_use_cnt === 0);
        } else if (currentStatus === '4') {
          //已离待使用
          lastData = _.filter(data, item => item.sum_avl_cnt > 0 && item.acct_stus === "OUT");
        }
      }
      yield put({
        type: 'updateRootStatePropComplete',
        updateObj: {
          selectedFloor: source === 'floorItem' ? selectedFloor : thisState.selectedFloor,
          fastOpen: source === "floorItem" ? "" : thisState.fastOpen,
          selectedStatus: source === 'roomContainsStatus' ? selectedStatus : thisState.selectedStatus,
          data: lastData,
          totalCount: totalCount
        }
      })
    }

  },
  reducers: {
    /**
    * 初始化完成
    * @param {*} state
    * @param {*} action
    */
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },

    /**
    * 更新state的根节点属性值
    * @param {*} state
    * @param {*} action
    */
    updateRootStatePropComplete(state, action) {
      return {
        ...state,
        ...action.updateObj
      }
    },


  }
}

export default _model;