import { GetSessionItem, SetSessionItem } from 'Hyutilities/sessionstorage_helper';
import _ from "lodash";
import { PackageUrl } from 'Hyutilities/string_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/systemroutermodel';
import ja_jp from 'ja_jp/systemroutermodel';
import zh_cn from 'zh_cn/systemroutermodel';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

let replace = null;

function unmodel(app, namespaces) {
  // 注销来源model
  if (_.isArray(namespaces)) {
    _.forEach(namespaces, (namespace) => {
      app.unmodel(namespace);
    })
  } else {
    app.unmodel(namespaces);
  }
}

function filterState(state, excludeNamespaces) {
  const _reData = {};
  _.forEach(state, (item, key) => {
    if (!_.startsWith(key, 'System') && key !== 'routing' && key !== '@@dva' && !_.includes(excludeNamespaces, key)) {
      _reData[key] = item;
    }
  })
  return _reData;
}

async function checkViews(url, tabIndex) {
  const tmUrl = _.startsWith(url, "/") ? url : `/${url}`
  const urlArry = tmUrl.split('/');
  if (tmUrl) {
    let params = urlArry[2];
    let paramsEstr = urlArry[3];
    // 如果存在url参数，则把参数缓存到本地，解决大数据传参的问题
    if (_.trim(urlArry[2]) !== "0" && _.trim(urlArry[3]) !== "0") {
      let urlParamsData = await GetSessionItem("UrlParamsData");
      if (_.size(urlParamsData) === 0) urlParamsData = [];
      urlParamsData[tabIndex] = {
        ...urlParamsData[tabIndex],
        [urlArry[3]]: urlArry[2]
      }
      await SetSessionItem("UrlParamsData", urlParamsData);
      params = "0";
    }
    return `${urlArry[1]}/${params}/${paramsEstr}`;
  } else {
    throw languageConfig.message_3// "当前操作员无权限进入该界面";
  }
}


const _model = {
  namespace: 'SystemRouterModel',
  state: {
    currentTabData: {},
    tabData: [],
    childrenMenu: []
  },
  subscriptions: {
    historyListenEvent({ history, dispatch }) {
      replace = history.replace;
    }
  },
  effects: {
    // 整理路由数据
    * routerPackage(params, { put, call, select }) {
      const { app, namespaces, match, title } = params;
      let { currentTabData={}, tabData=[], unmodelNameSpaces } = yield select(state => state.SystemRouterModel);
      // 目标路由结构
      const targetmatch = PackageUrl(match);

      let sessionRouteData = yield call(GetSessionItem, "cacheData");
      if (!sessionRouteData) {
        sessionRouteData = [];
      }
      // 根据tabData截取缓存的数据
      if (_.size(tabData) < _.size(sessionRouteData)) {
        sessionRouteData = _.take(sessionRouteData || [], _.size(tabData));
      }

      let _findIndex = -1;
      let _sessionData = {};
      let _routerData = [];

      let currentTag = null;
      if (_.size(currentTabData) > 0 && currentTabData !== "closeTab") {
        currentTag = PackageUrl(currentTabData.match).routeTag;
      }
      //根据路由标识判断是否需要替换缓存数据
      const replaceRouteData = false;

      if (_.size(currentTabData) === 0) {
        // 第一次进入 判断如果当前页签长度小于传入的页签，则把数据提交到0页签中
        let _tabIndex = Number(targetmatch.params.tabIndex);
        if (_.size(tabData) <= _tabIndex) {
          _tabIndex = 0;
        }
        if (!_.has(tabData[_tabIndex], "routerData")) {
          tabData[_tabIndex] ={
            "routerData":[]
          };
        }
        tabData[_tabIndex]["routerData"].push({ title: title, url: encodeURI(targetmatch.url) });
      }  else if (targetmatch.params.tabIndex === currentTabData.match.params.tabIndex && !replaceRouteData) {
        // 当前所在路由结构
        const currentmatch = PackageUrl(currentTabData.match);

        // 在同页签下切换路由
        _findIndex = _.findIndex(tabData[targetmatch.params.tabIndex].routerData, (item) => {
          return item.url === encodeURI(targetmatch.url);
        })

        // 如过在tabData中找不到对应的路由地址数据，则向当前页签内增加一条记录
        if (_findIndex === -1) {
          tabData[targetmatch.params.tabIndex]
            .routerData
            .push({ title: title, url: encodeURI(targetmatch.url) });
          // 向缓存数据中根据页签索引添加缓存数据
          if (!_.has(sessionRouteData, Number(currentmatch.params.tabIndex))) {
            sessionRouteData[currentmatch.params.tabIndex] = {}
          }
          sessionRouteData[currentmatch.params.tabIndex][currentmatch.url] = yield select(state => {
            return filterState(state, namespaces);
          })

          yield call(SetSessionItem, 'cacheData', sessionRouteData);

        } else {
          _routerData = _.take(tabData[targetmatch.params.tabIndex].routerData, _findIndex);

          _.forEach(sessionRouteData[currentmatch.params.tabIndex], (item, index) => {
            if (index === targetmatch.url) {
              _sessionData[index] = item;
              return false;
            } else {
              _sessionData[index] = item;
            }
          })
          sessionRouteData[currentmatch.params.tabIndex] = _sessionData;

          yield call(SetSessionItem, 'cacheData', sessionRouteData);
          _routerData.push({ title: title, url: encodeURI(targetmatch.url) });
          tabData[targetmatch.params.tabIndex].routerData = _routerData;
        }
      } else if (targetmatch.params.tabIndex === currentTabData.match.params.tabIndex && replaceRouteData) {
        // 当前所在路由结构
        const currentmatch = PackageUrl(currentTabData.match);
        // 在同页签下切换同模块路由
        _findIndex = _.size(tabData[targetmatch.params.tabIndex].routerData || []) - 1;
        _routerData = _.take(tabData[targetmatch.params.tabIndex].routerData, _findIndex);
        _routerData.push({ title: title, url: encodeURI(targetmatch.url) });
        tabData[targetmatch.params.tabIndex].routerData = _routerData;

        _.forEach(sessionRouteData[currentmatch.params.tabIndex], (item, index) => {
          if (index === currentmatch.url) {
            return false;
          } else {
            _sessionData[index] = item;
          }
        })

        sessionRouteData[targetmatch.params.tabIndex] = _sessionData;
        yield call(SetSessionItem, 'cacheData', sessionRouteData);


      }

      const _currentTabData = {
        namespaces,
        match,
        title
      }

      // 根据目标页签索引整理table组件的定位数据，因为切换页签不需要整理，所以只需要在最终缓存页签数据时根据路由定义整理一次即可
      let tableLocation = _.has(global, "TABLECHECKEDOBJ") ? global.TABLECHECKEDOBJ : [];
      //let tempTableLocation = [];
      const routerUrls = _.map(tabData[targetmatch.params.tabIndex].routerData, item => item.url);
      if (tableLocation[targetmatch.params.tabIndex]) {
        let tempData = {};
        _.forEach(tableLocation[targetmatch.params.tabIndex], (item, index) => {
          if (_.includes(routerUrls, index)) {
            tempData[index] = item;
          }
        })
        tableLocation[targetmatch.params.tabIndex] = tempData;
      }
      global.TABLECHECKEDOBJ = tableLocation;

      // 根据目标页签索引整理scroll组件的滚动位置数据，因为切换页签不需要整理，所以只需要在最终缓存页签数据时根据路由定义整理一次即可
      let scrollLocation = _.has(global, "SCROLLPOSOBJ") ? global.SCROLLPOSOBJ : [];
      if (scrollLocation[targetmatch.params.tabIndex]) {
        let tempData = {};
        _.forEach(scrollLocation[targetmatch.params.tabIndex], (item, index) => {
          if (_.includes(routerUrls, index)) {
            tempData[index] = item;
          }
        })
        scrollLocation[targetmatch.params.tabIndex] = tempData;
      }
      global.SCROLLPOSOBJ = scrollLocation;

      yield call(SetSessionItem, 'tabData', tabData);
      yield call(SetSessionItem, 'currentTabData', _currentTabData)

      yield put({ type: "packageComplete", currentTabData: _currentTabData, tabData: tabData, unmodelNameSpaces: [] })
    },

    /**
     * 页面跳转
     *
     * replaceUrl:
     * 可选参数，如果传递了这个参数，则认为是需要路由替换的返回
     * 会从路由的根结点根据路由定义进行匹配，如果匹配到了，则进行路由替换并跳转
     * 同时会从被匹配的路由开始向后的缓存数据
     *
     * @param {any} params {url,replaceUrl}
     * @param {any} { select, put, call }
     */
    * push(params, { select, put, call }) {
      const SystemRouterModel = yield select(({ SystemRouterModel }) => SystemRouterModel);
      // 可选的路由参数
      const { replaceUrl = false } = params;
      // 获取tab页签数据和当前页签数据
      const { currentTabData, tabData } = SystemRouterModel;
      // 获取当前的路由索引
      const tabIndex = _.has(params, "tabIndex") ? params.tabIndex : currentTabData.match.params.tabIndex;
      // 根据页签获取当前页签下的路由集合
      const currentRouterData = tabData[tabIndex].routerData;
      // url校验
      const skuUrl = yield checkViews(params.url, tabIndex);
      const url = _.startsWith(skuUrl, '/') ? skuUrl : `/${skuUrl}`;

      // 如果replace属性为false，则直接进行路由跳转
      if (!replaceUrl || _.size(currentRouterData) === 0) {
        replace && replace(`/main/${tabIndex}${url}`);
        return;
      }

      // 如果replaceUrl属性为true，则根据路由规则进行路由替换
      // 获取缓存数据
      let sessionRouteData = yield call(GetSessionItem, "cacheData");
      // 整理目标url
      let targetUrlArr = skuUrl.split('/');
      targetUrlArr = _.dropRight(targetUrlArr, 2);
      const targetRouteUrl = targetUrlArr.join('/');

      // 根据目标路由定义遍历当前页签路由集合
      const replaceIndex = _.findIndex(currentRouterData, item => {
        if (_.startsWith(targetRouteUrl, "/")) {
          return _.startsWith(`/${item.url}`, targetRouteUrl);
        } else {
          return _.startsWith(item.url, targetRouteUrl);
        }
      })

      if (replaceIndex === -1) {
        replace && replace(`/main/${tabIndex}${url}`);
        return;
      }

      // 获取需要替换的路由
      const replaceOrgUrl = currentRouterData[replaceIndex].url;
      // 删除当前页签路由合集中该路由之后的路由数据（含该路由）
      const replaceRouterData = _.dropRight(currentRouterData, _.size(currentRouterData) - replaceIndex);
      // 更新页签数据
      tabData[tabIndex]["routerData"] = replaceRouterData;

      // 更新缓存数据
      const replaceCache = {};
      _.forEach(replaceRouterData, (item) => {
        if (item.url !== replaceOrgUrl) {
          replaceCache[item.url] = sessionRouteData[tabIndex][item.url];
        } else {
          return false;
        }
      })

      if (sessionRouteData) {
        sessionRouteData[tabIndex] = replaceCache;
        // 提交缓存数据
        yield call(SetSessionItem, 'cacheData', sessionRouteData);
      }

      // 更新tab数据
      yield put({
        type: "packageTabDataComplete",
        tabData
      })
      replace && replace(`/main/${tabIndex}${url}`);
    },
    /**
     * 返回上一个路由
     *
     * @param {any} params {encryptStr?}
     * @param {any} { select, put, call }
     */
    * back(params, { select, put, call }) {
      const SystemRouterModel = yield select(({ SystemRouterModel }) => SystemRouterModel);
      // 获取tab页签数据和当前页签数据
      const { tabData, currentTabData } = SystemRouterModel;
      // 获取当前的路由索引
      const tabIndex = currentTabData.match.params.tabIndex;
      // 根据页签获取当前页签下的路由集合
      const currentRouterData = tabData[tabIndex].routerData;
      // 获取当前页签下的路由合集长度
      const countRouterData = _.size(currentRouterData);

      // 如果路由合集长度等于0，则不做任何操作
      if (countRouterData > 0) {
        // 根据索引和路由地址清除table组件定位数据
        const currentUrl = decodeURI(currentRouterData[countRouterData - 1].url);
        if (global.TABLECHECKEDOBJ && global.TABLECHECKEDOBJ[tabIndex] && _.has(global.TABLECHECKEDOBJ[tabIndex], currentUrl)) {
          delete global.TABLECHECKEDOBJ[tabIndex][currentUrl];
        }
        if (global.SCROLLPOSOBJ && global.SCROLLPOSOBJ[tabIndex] && _.has(global.SCROLLPOSOBJ[tabIndex], currentUrl)) {
          delete global.SCROLLPOSOBJ[tabIndex][currentUrl];
        }

        const url = currentRouterData[countRouterData - 2].url;
        replace && replace(`/main/${tabIndex}/${url}`);
      }

    }

  },
  reducers: {
    initComplete(state, action) {
      return {
        currentTabData: {},
        tabData: _.size(state.tabData) > 0 ? [state.tabData[0]] : [],
        childrenMenu: []
      }
    },
    // 整理路由完成
    packageComplete(state, action) {
      return {
        ...state,
        currentTabData: action.currentTabData,
        tabData: action.tabData,
        unmodelNameSpaces: action.unmodelNameSpaces
      }
    },
    //更新tabData完成
    packageTabDataComplete(state, action) {
      return {
        ...state,
        currentTabData: {},
        tabData: action.tabData
      }
    },
    // 通过菜单跳转处理完成
    pushByMenuComplete(state, action) {
      return {
        ...state,
        tabData: action.tabData,
        childrenMenu: action.childrenMenu
      }
    },
    // 通过菜单跳转处理完成
    showSystemTitleComplete(state, action) {
      return {
        ...state,
        tabData: action.tabData,
      }
    },
  }
}

export default _model;