/**
 * systemName:
 * author:
 * remark:
 */
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/using_package';
import ja_jp from 'ja_jp/using_package';
import zh_cn from 'zh_cn/using_package';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default {
  routeParams: {},
  acct_no: "",
  alt_nm: "",
  blockKey: "",
  blockItemId: "",
  bal_amt: 0,
  room_num: "",
  pkg_list: [],
  memo:"",
  successmessage: languageConfig.successmessage,
}