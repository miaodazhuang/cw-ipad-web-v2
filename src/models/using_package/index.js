/**
 * systemName:使用房含
 * author:
 * remark:
 */
import initState from './initstate';
import _ from "lodash";
import { FormatMoney } from 'Hyutilities/money_helper';
import Susing_package from 'Hyservices/using_package';
const _Susing_package = new Susing_package();

const _model = {
  namespace: 'Musing_package',
  state: { ...initState },
  subscriptions: {},
  effects: {
    * init(params, { select, call, put }) {
      let thisState = _.cloneDeep(initState);
      const queryParams = {
        "acctNo": params.data.acct_no,
        "blockKey": params.data.block_key,
        "blockItemId": params.data.block_item_id,
      }
      thisState.routeParams = params;
      const { resultData } = yield call(_Susing_package.init, queryParams);
      if (resultData) {
        thisState.acct_no = resultData.acct_no;
        thisState.alt_nm = resultData.alt_nm;
        thisState.blockId = resultData.blockId
        thisState.blockKey = resultData.blockKey
        thisState.blockItemId = resultData.blockItemId
        thisState.bal_amt = FormatMoney(resultData.bal_amt);
        thisState.room_num = resultData.room_num;
        thisState.pkg_list = resultData.pkg_list;
        thisState.pseudo_flg = resultData.pseudo_flg||"";
        thisState.memo = resultData.memo||"";
      }
      yield put({
        type: 'initComplete',
        data: {
          ...thisState,
        }
      });
    },
    /**
     * 保存入账
     * @param {any} params
     * @param {any} {select, put, call}
     */
    * submitAllData(params, { select, put, call }) {
      // debugger
      const thisState = yield select(({ Musing_package }) => Musing_package);
      const submitParams = {
        "acctNo": thisState.acct_no,
        "blockKey": thisState.blockKey,
      }
      yield call(_Susing_package.closeBlock, submitParams);
      params.successmessage = thisState.successmessage;
      yield put({
        type: "SystemModel/updateModalWindowState"
      });
      if (thisState.routeParams.cbDispatch) {
        yield put({
          type: thisState.routeParams.cbDispatch,
          ...thisState.routeParams.cbParams
        })
      }
    }
  },
  reducers: {
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },
  }
}

export default _model;