export default {
  //悬浮菜单开关属性 0 关闭  1打开
  menuStatus: 0,
  // 异步调用状态
  fatchstate: {
    // 状态代码 0 未进行异步调用 1 正在进行异步调用
    statecode: 0,
    // 当statecode为1时，需要在加载中状态下显示的文字描述
    message: ""
  },
  // 提示消息状态
  messagestate: {
    // 状态代码 0 无消息 1 成功消息 2 警告消息 3 错误消息
    statecode: 0,
    // 需要提示的消息内容
    message: "",
    //消息编码
    messageCode:'',
  },
  // 弹出框状态
  modalwindowstate: {
    // 弹出框编码 -1 未弹出 否则参考编码表
    "key": "",
    //弹窗的全部编码
    "allkey": "",
    // 标题内容
    "title": "",
    // 自定义弹框行内样式
    "customContentStyle": {},
    // 自定义弹框样式
    "customClassName": "",
    // 需要传入弹框的props参数
    "params": {},
    // modal-size属性
    "size": "",
    //右上是否有关闭按钮
    "closeIcon": true,
  },
  // confirm弹框状态
  confirmstate: {
    // 显示状态
    "state": false,
    // 标题
    "title": "",
    // 消息
    "message": "",
    // 回调参数 组件在关闭|确认|其它操作后都会把该参数对象回传
    "cbParams": {},
    // 回调type 通过dispatch调用对应的命名空间和方法名称
    "dispatchTyp": "",
    //组件元素
    "elements":{},
    //组件值
    "formData":{},
  },
  //快捷查询按钮数据源
  quickQueryButton:{
  }

  //
}