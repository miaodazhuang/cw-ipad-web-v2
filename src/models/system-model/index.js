import _ from 'lodash';
import initState from './initstate';
import CommonService from 'Hyservices/common-service';
import { packageKeyCodes } from 'Hyservices/login-service';
// import { GetLanguage } from 'Hyutilities/language_helper';
// import en_us from 'en_us/components/businesscoms/hyhandlecom';
// import ja_jp from 'ja_jp/components/businesscoms/hyhandlecom';
// import zh_cn from 'zh_cn/components/businesscoms/hyhandlecom';
// let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _commonService = new CommonService();

const _model = {
  namespace: 'SystemModel',
  state: initState,
  subscriptions: {
    historyListenEvent({ history, dispatch }) {
      // 设置dispatch为全局变量
      if (!global.__DISPATCH__) {
        global.__DISPATCH__ = dispatch;
      }
      return history.listen(async (history) => {
        // 如果是登陆界面 禁用app下的键盘事件
        if (history.pathname === '/login') {
          global.__KEYMASTER__.setScope('login');
        } else {
          global.__KEYMASTER__.setScope('app');
        }
        // 根据pathname获取快捷键数据
        if (_.startsWith(history.pathname, "/main/")) {
          // 如果pathname以／main／开头,则进行处理
          const urlArry = history.pathname.split("/");
          if (_.size(urlArry) === 14) {
            // 0132-2010-A-010-L-T-001-L01
            const shortKeyParams = `${urlArry[3]}-${urlArry[4]}${urlArry[5]}-${urlArry[6]}-${urlArry[7]}-${urlArry[8]}-${urlArry[9]}-${urlArry[10]}`;
            // 调用快捷键处理方法
            dispatch({
              type: "getShortKeys",
              shortkeyCds: shortKeyParams
            })
          }
        }
      });
    },

    keyCodeEvent({ dispatch }) {
      global.__KEYMASTER__("alt+i", (e) => {
        const v = process.env.NODE_ENV_VERSION || "";
        if (v) {
          alert(`Client Version：${v}`);
        }
        e.preventDefault();
        return false;
      });
      // 登陆下增加回车快捷键
      global.__KEYMASTER__("enter", "login", (e) => {
        const loginDom = document.querySelectorAll('[name="login"]')[0];
        if (loginDom) {
          loginDom.click();
        }
      });

      //快速开台点菜
      global.__KEYMASTER__("alt+q", 'app', (e) => {
        //获取弹窗组件 判断再没有弹窗的情况下才可定位
        let modalsEle = document.getElementsByClassName("ui page modals")[0];
        const fastOpenDom = document.querySelectorAll('[name="fastOpen"]')[0];
        if (fastOpenDom && !modalsEle) {
          fastOpenDom.focus();
        }
      })

      //注销
      global.__KEYMASTER__("ctrl+alt+d", 'app', (e) => {
        const outDom = document.querySelectorAll('[name="out"]')[0];
        if (outDom) {
          outDom.click();
        }
      })


      global.__KEYMASTER__("enter", "app", (e) => {
        // e.stopPropagation();
        // e.preventDefault();
        //获取弹窗组件
        let modalsEle = document.getElementsByClassName("ui page modals")[0];
        //获取当前激活的元素
        const focus = document.activeElement;
        //如果当前激活元素为快速开台输入框 并且没有弹出窗
        if (focus.name === 'fastOpen' && !modalsEle) {
          focus.click();
        }
        return false;
      });


    },
    keepAlive({ dispatch }) {
      //判断是否是登陆状态,如果是登陆状态，半小时请求一次字典接口
      let timer = null;
      if(!global.__ISLOGIN__){
        timer && clearTimeout(timer)
      }else{
        timer = setInterval(() => {
          dispatch({
            type: "keepAlive"
          })
        }, 1800000);
      }
    }
  },
  effects: {
    /**
     * 根据模块编号获取快捷键
     *
     * @param {any} params
     * @param {any} { select, call, put }
     */
    * getShortKeys(params, { select, call, put }) {
      const { shortkeyCds } = params;
      const reData = yield call(_commonService.searchShortKey, [shortkeyCds]);
      // 更新默认插件数据
      yield put({
        type: "SystemPluginModel/updateDefaultPluginData",
        defaultData: reData || []
      })
      // 快捷键数据
      packageKeyCodes(reData);

    },



    /**
     * 更新弹窗属性
     *
     * @param {*} param
     * @param {*} { select, call, put }
     */
    * updateModalWindowState(param, { select, call, put }) {
      // 查询当前页面state数据
      let {
        key = "",
        allkey = "",
        title = "",
        customContentStyle = "",
        customClassName = "",
        params = null,
        size = null,
        closeIcon = true,
        className = null
      } = param;
      // //计时器暂停开启
      // let stopTimer = true;
      // //关闭窗口
      // if (key === "") {
      //   stopTimer = false;
      // }
      // yield put({
      //   type: "SystemPosModel/changeStatusComplete",
      //   data: { stopTimer: stopTimer },
      // })


      yield put({
        type: "updateModalWindowStateComplete",
        data: {
          key,
          allkey,
          title,
          customContentStyle,
          customClassName,
          params,
          size,
          closeIcon,
          className
        }
      })
    },
    //防止session过期，每半个小时调用一次
    * keepAlive(param, { select, call, put }) {
      yield call(_commonService.keepAlive)
    }

  },
  reducers: {
    /**
     * 变更扩展菜单状态
     *
     * @param {any} state
     * @param {any} action
     * @returns
     */
    updateMenuStatusComplete(state, action) {
      return {
        ...state,
        menuStatus: action.menuStatus
      }
    },
    /**
     * 变更fatch状态
     *
     * @param {any} state
     * @param {any} action
     * @returns
     */
    updateFatchState(state, action) {
      return {
        ...state,
        fatchstate: {
          ...state.fatchstate,
          statecode: action.statecode,
          message: action.message
        }
      }
    },
    /**
     * 变更消息状态
     *
     * @param {any} state
     * @param {any} action
     * @returns
     */
    updateMessageState(state, action) {
      return {
        ...state,
        messagestate: {
          ...state.messagestate,
          statecode: action.statecode,
          message: action.message,
          messageCode: action.messageCode,
        }
      }
    },
    /**
     * 变更弹框状态
     *
     * @param {any} state
     * @param {any} action
     * @returns
     */
    updateModalWindowStateComplete(state, action) {
      return {
        ...state,
        "modalwindowstate": {
          "key": action.data.key || "",
          "allkey": action.data.key || "",
          "title": action.data.title || "",
          "customContentStyle": action.data.customContentStyle || null,
          "customClassName": action.data.customClassName || "",
          "params": action.data.params || null,
          "size": action.data.size || null,
          "closeIcon": action.data.closeIcon,
          "className": action.data.className || null
        }
      }
    },
    /**
     * 确认框更新事件
     * title:标题 默认：系统消息
     * message:提示信息
     * cbParams:需要回传的参数对象
     * dispatchTyp:需要会调时的Type字符串 回传参数：{confirm:确认状态（true/false）,cbParams:回传参数对象（object）}
     *
     * @param {any} state
     * @param {any} action
     * @returns
     */
    updateConfirmState(state, action) {
      return {
        ...state,
        confirmstate: {
          "state": action.state || false,
          "title": action.title || "",
          "message": action.message || "",
          "cbParams": action.cbParams || "",
          "dispatchTyp": action.dispatchTyp || "",
          "formData": action.formData,
          "elements": action.elements,
        }
      }
    },


    //更新组件内部数据
    updateConfirmStateData(state, action) {
      return {
        ...state,
        confirmstate: {
          ...state.confirmstate,
          formData: action.formData
        }
      }
    },
  }
}

function changeIndex(inputs, findIndex) {
  if (findIndex === inputs.length - 1) {
    changeIndex(inputs, -1)
  } else {
    if (!_.isNull(inputs[findIndex + 1].getAttribute("disabled")) || !_.isNull(inputs[findIndex + 1].getAttribute("readonly")) || inputs[findIndex + 1].parentNode.getAttribute("aria-disabled")) {
      findIndex++;
      changeIndex(inputs, findIndex)
    } else {
      inputs[findIndex + 1].focus();
    }
  }
}

function getFirstIndex(inputs, findIndex) {
  if (!_.isNull(inputs[findIndex].getAttribute("disabled")) || !_.isNull(inputs[findIndex].getAttribute("readonly")) || inputs[findIndex].parentNode.getAttribute("aria-disabled")) {
    if (findIndex < inputs.length) {
      findIndex++;
      getFirstIndex(inputs, findIndex)
    } else {
      return
    }
  } else {
    inputs[findIndex].focus();
  }
}

const hasAttr = function (el, name) {
  var attr = el.getAttributeNode && el.getAttributeNode(name);
  return attr ? attr.specified : false
}

const addEvent = function (obj, type, callback) {
  if (obj.addEventListener) {
    obj.addEventListener(type, callback, false);
  }
  else if (obj.attachEvent) {
    obj.attachEvent("on" + type, callback);
  }
}

const done = function (el, fn) {
  if (!hasAttr(el, "`tabindex`")) {
    el.tabIndex = -1;
  }
  addEvent(el, "focus", function (e) {
    fn.call(el, (e || window.event));
  });
}

export default _model;