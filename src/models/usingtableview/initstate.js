/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/usingtableview';
import ja_jp from 'ja_jp/usingtableview';
import zh_cn from 'zh_cn/usingtableview';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default {
  //查询条件(同时提供给自动刷新方法使用)
  queryData: {
    "salesId": "", //销售点id
    "useDt": global.__BUSINESSDT__,//日期
    "periodFlg": "1", //房含属性 早中晚餐
    "periodId": "",//餐点时间
    "blockTypId": "",// 桌台类 型id
    "fastQuery": "", //房号/姓名
    "groupTyp": "1",// 1餐点 2同来人
    "memoFlg": "" //只展示有备注
  },
  //数据
  data: [],
  //桌台
  blockData: [
    {
      "block_id": -1,
      "block_drpt": languageConfig.all,
      "free_quant": 0,
      "use_quant": 0,
      "total_quant": 0
    },
  ],
  //
  seq: [],
  //字典
  dicData: [],

  block_id: "",

  periodIdData: {
    text: languageConfig.all,
    value: "",
    "data-datasource": {
      name: languageConfig.all,
    }
  }


}