/**
 * systemName:
 * author:
 * remark:
 */
import initState from './initstate';
import PathToRegexp from 'path-to-regexp';
import _ from 'lodash';
import Susingtableview from 'Hyservices/usingtableview';
const _Susingtableview = new Susingtableview();

const _model = {
  namespace: 'Musingtableview',
  state: { ...initState },
  subscriptions: {
    historyListenEvent({ history, dispatch }) {
      return history.listen(async (history) => {
        const match = PathToRegexp("/main/:tabindex/usingtableview/:paramobject/:encryptstr").exec(history.pathname);
        if (match) {
          let routeParams = global.__COMPARISON__ && await global.__COMPARISON__(match);
          dispatch({
            type: "init",
            routeParams
          })
        }
      })
    }
  },
  effects: {
    /**
  * 初始化方法
  *
  * @param {*} params
  * @param {*} { select, call, put }
  */
    * init(params, { select, call, put }) {
      //销售点为空不进行操作

      if (global.__SALEPOINT__ === null) {
        yield put({
          type: "initComplete",
          data: {
            ...initState
          }
        })
      } else {
        const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
        const thisState = _.cloneDeep(initState);
        //深克隆当前对象
        let _thisState = _.cloneDeep(thisState);
        //查询当前缓存
        // const cacheData = yield call(global.__GETCACHEDATA__, "Mpostableview");
        //查询参数
        _thisState.queryData = {
          ..._thisState.queryData,
          salesId: SystemPosModel.salesPointTotal.value,
          periodFlg: SystemPosModel.period.value,
          fastQuery: SystemPosModel.fastQuery,
        };

        let dictTyps = [''];
        //查询数据
        const { data = [], blockData = [], seq = [] } = yield call(_Susingtableview.init, dictTyps, _thisState.queryData);
        _thisState.data = data;
        _thisState.blockData = blockData;
        _thisState.seq = seq;

        yield put({
          type: "initComplete",
          data: {
            ..._thisState
          }
        })
      }
    },


    /**
     * 查询列表
     *
     * @param {*} params
     * @param {*} { select, call, put }
     */
    * queryList(params, { select, call, put }) {
      const thisState = yield select(({ Musingtableview }) => Musingtableview)
      let _thisState = _.cloneDeep(thisState)
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      _thisState.queryData = {
        ..._thisState.queryData,
        ...params,
        fastQuery: SystemPosModel.fastQuery
      }
      _thisState.queryData = _.omit(_thisState.queryData, ["type", "successmessage"])
      //判断是否为选中用餐时间窗口返回 如果是则更改数据
      if (_.has(params, "periodIdData")) {
        _thisState.queryData.periodId = params.periodIdData.value;
        _thisState.periodIdData = params.periodIdData;
        //选择了用餐时间 只能选中餐点
        // if (params.periodIdData.value === "") {
        //   _thisState.queryData.groupTyp = "1";
        // }
      }

      if (_.has(params, "blockTypId")) {
        _thisState.block_id = params.blockTypId;
      }

      let _params = {
        ..._thisState.queryData,
        salesId: SystemPosModel.salesPointTotal.value,
        periodFlg: SystemPosModel.period.value,
      };
      params = _.omit(params, "successmessage");


      let { data, seq, blockData } = yield call(_Susingtableview.queryList, _params);

      _thisState.data = data;
      _thisState.seq = seq;
      _thisState.blockData = blockData;

      yield put({
        type: "initComplete",
        data: _thisState
      })

    }
  },
  reducers: {
    /**
    * 初始化完成
    * @param {*} state
    * @param {*} action
    */
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },

    /**
    * 更新state的根节点属性值
    * @param {*} state
    * @param {*} action
    */
    updateRootStatePropComplete(state, action) {
      return {
        ...state,
        ...action.updateObj
      }
    },
  }
}

export default _model;