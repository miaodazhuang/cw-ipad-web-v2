/**
 * systemName: 无可用房含
 * author: zhanghg
 * remark:
 */
import initState from './initstate';
import Ssubaddpackage from 'Hyservices/subaddpackage';
import _ from 'lodash';
import { FormatMoney } from 'Hyutilities/money_helper';
const _Ssubaddpackage = new Ssubaddpackage();

const _model = {
  namespace: 'Msubaddpackage',
  state: initState,
  subscriptions: {},
  effects: {
    * init(params, { select, call, put }) {
      let _thisState = _.cloneDeep(initState);
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      _thisState.roomNumber = params.roomNumber;
      _thisState.blockItemId = params.blockItemId||"";
      _thisState.blockKey = params.blockKey||"";
      _thisState.block_Id = params.blockId||"";
      _thisState.resName = params.resName;
      _thisState.pseudo_flg = params.pseudo_flg||"";//NML  正常房 PF 普通假房  POS 销售点假房
      _thisState.surplus = FormatMoney(params.surplus);

      const paramdata = {
        //global.__BUSINESSDT__ 酒店业务日期
        'frDt': global.__BUSINESSDT__,
        'toDt': global.__BUSINESSDT__,
        'productFlg': SystemPosModel.period.value,
        'posId': SystemPosModel.salesPointTotal.value, //销售点ID
        'posFlg': '1',
      }
      let { data, price } = yield call(_Ssubaddpackage.init, paramdata);
      let datas = [];
      _.forEach(data.resultData, item => {
        datas.push({
          packageNm: item.packageNm,
          price: item.listPackageDayInfo[0].price,
          packageId: item.packageId,
          audittrncdId: item.audittrncdId,
        })
      });
      let contentItem = handleAddpackData(datas);
      let pricedata = [];
      _.forEach(price.responseData.PARA_TRNCODE, (item) => {
        pricedata.push({
          key: item.param_id,
          text: item.param_drpt,
          value: item.param_id,
          trn_typ: item.trn_typ
        })
      });

      let blockIds = [];
      let submmitData = [];
      yield put({
        type: 'initComplete',
        data: {
          ..._thisState,
          contentItem: contentItem,
          packageData: pricedata,
          blockIds,
          submmitData
        }
      })
    },

    * choseType(params, { select, call, put }) {
      let trncd_id = params.value;
      const data = yield call(_Ssubaddpackage.getDictionaryCCARDTYP, { "trncd_id": trncd_id })
      let result = data.CCARDTYP;
      yield put({
        type: 'changetype',
        typeData: result
      })
    },


    * submitData(params, { select, call, put }) {
      const thisState = yield select(({ Msubaddpackage }) => Msubaddpackage);
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      let pkgDetails = [];
      _.forEach(thisState.submmitData, (item) => {
        pkgDetails.push({
          quantity: item.goodsQuant,
          pkgId: item.packageId,
        })
      });

      let item = _.find(thisState.typeData, (i) => { return i.value === _.head(thisState.typeIds) })
      let obj = {
        arNo: "",
        corpNo: "",
      }
      if (item) {
        obj.arNo = item["data-datasource"].ar_no;
        obj.corpNo = item["data-datasource"].corp_no;
      }

      let paramdata = {
        acctNo: params.acctNo, //账号
        payTrncdId: _.size(thisState.blockIds) !== 0 ? thisState.blockIds[0] : null, //付款交易代码
        postFlg: _.size(thisState.blockIds) !== 0 ? 1 : 0,
        posId: SystemPosModel.salesPointTotal.value, //销售点ID
        trnCmnt: SystemPosModel.salesPointTotal["data-datasource"].name, //当前销售点的名称
        pkgDetails,
        ccardtypId: _.size(thisState.typeIds) !== 0 ? thisState.typeIds[0] : null,
        productFlg: SystemPosModel.period.value,
        posFlg: '1',
        ...obj
      };

      if(SystemPosModel.salesPointTotal["data-datasource"].sales_typ==="2"){
        paramdata.blockKey=thisState.blockKey;
        paramdata.blockItemId=thisState.blockItemId;
        paramdata.periodId=SystemPosModel.period.value;
        paramdata.blockId=thisState.block_Id;
        yield call(_Ssubaddpackage.openBlockTrn, paramdata);
      }else{
        yield call(_Ssubaddpackage.noUsePkg, paramdata);
      }
      yield put({
        type: 'SystemModel/updateModalWindowState'
      });
      if (params.cbDispatch) {
        yield put({
          type: params.cbDispatch,
          ...params.cbParams
        })
      }
    },

    * calcGoods(params, { select, call, put }) {
      const thisState = yield select(({ Msubaddpackage }) => Msubaddpackage);
      let _thisState = _.cloneDeep(thisState);

      if (_.head(_thisState.blockIds) === params.data.value) {
        _thisState.allAmt = 0;
        _thisState.blockIds = [];
        _thisState.typeData = [];
        _thisState.typeIds = "";
      } else {
        if (params.data.trn_typ === "890" || params.data.trn_typ === "891") {
          const data = yield call(_Ssubaddpackage.getDictionaryCCARDTYP, { "trncd_id": params.data.value })
          _thisState.typeData = data.CCARDTYP;
        } else {
          _thisState.typeData = [];
          _thisState.typeIds = "";
        }
        let _params = {
          goodsTrns: []
        };
        _.forEach(thisState.submmitData, (item) => {
          _params.goodsTrns.push({
            goodsQuant: item.goodsQuant,
            goodsDrpt: item.goodsDrpt,
            salePrice: item.price,
            trncdId: item.audittrncdId,
          })
        });
        const data = yield call(_Ssubaddpackage.calcGoods, _params);
        _thisState.allAmt = data.allAmt || 0;
        _thisState.blockIds[0] = params.data.value;
      }
      yield put({
        type: 'initComplete',
        data: _thisState
      })

    },

  },
  reducers: {
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },
    updateFormData(state, action) {
      return {
        ...state,
        blockIds: action.blockIds
      }
    },
    changeSubmit(state, action) {
      return {
        ...state,
        ...action.param
      }
    },
    changetype(state, action) {
      return {
        ...state,
        typeData: action.typeData
      }
    },
    updateBank(state, action) {
      return {
        ...state,
        typeIds: action.typeIds
      }
    },
    delchoseType(state, action) {
      return {
        ...state,
        typeData: [],
        typeIds: []
      }
    }
  }
}

function handleAddpackData(data) {
  let content = [];
  _.forEach(data, (item) => {
    content.push({
      "isHandleIcon": false,
      "textData": [
        {
          title: '',
          value: item.packageNm,
        },
        {
          title: '',
          value: item.price
        },
        item
      ]
    });
  });
  return content;
}

export default _model;
