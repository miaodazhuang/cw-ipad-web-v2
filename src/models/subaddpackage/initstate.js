/**
 * systemName: 无可用房号
 * author:
 * remark:
 */
export default {
  contentItem: [],
  submmitData:[], // 提交数据
  //右侧的数组
  packageData:[],
  //右侧默认选中的数组
  blockIds: [],
  //点击信用卡的银行类型
  typeData: [],
  //点击银行卡
  typeIds: [],
  //房号
  roomNumber: 0,
  //客人姓名
  resName: '',
  //余额
  surplus: 0,
  //支付金额
  allAmt:0,
}
