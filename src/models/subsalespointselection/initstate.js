/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 */
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subsalespointselection';
import ja_jp from 'ja_jp/subsalespointselection';
import zh_cn from 'zh_cn/subsalespointselection';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default {
  //参数
  routeParams: {},
  //数据源
  data: [],
  //选中项
  checked: {
    sales_id: -1
  },

  //餐点信息   属性 1早餐 3午茶 2晚餐
  periodData: [
    {
      text: languageConfig.productFlg_1, value: "1",
      "data-datasource": {
        name: languageConfig.productFlg_1,
      }
    },
    {
      name: languageConfig.productFlg_3, value: "3",
      "data-datasource": {
        name: languageConfig.productFlg_3,
      }
    },
    {
      name: languageConfig.productFlg_2, value: "2",
      "data-datasource": {
        name: languageConfig.productFlg_2,
      }
    },
  ]

}