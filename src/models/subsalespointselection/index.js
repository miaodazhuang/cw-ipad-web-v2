/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 * params:{
 *  pageType:"",           string productFlg:选择餐别 默认为选择销售点
 *  cbDisPatch: 'xxx/xxx', string 回调方法
 *  cbParams: {},          object 回调参数
 * }
 */
import { SetSessionItem } from 'Hyutilities/sessionstorage_helper';
import _ from 'lodash';
import initState from './initstate';
import Ssubsalespointselection from 'Hyservices/subsalespointselection';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/subsalespointselection';
import ja_jp from 'ja_jp/subsalespointselection';
import zh_cn from 'zh_cn/subsalespointselection';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _Ssubsalespointselection = new Ssubsalespointselection();

const _model = {
  namespace: 'Msubsalespointselection',
  state: { ...initState },
  subscriptions: {},
  effects: {
    /**
     * 初始化
     *
     * @param {*} params
     * @param {*} { select, call, put }
     */
    * init(params, { select, call, put }) {
      //解构参数
      let { cbDisPatch = "", cbParams = {}, pageType = "" } = params;
      //深拷贝当前state对象
      let _initState = _.cloneDeep(initState);
      _initState.routeParams = {
        pageType,
        cbDisPatch,
        cbParams
      }
      const state = yield select((state) => state);
      const { SystemPosModel } = state;
      //                     销售点            房含属性
      let dictype = ["PARA_POS_SALES", "GRP_FIN_PRODUCT.PRODUCT_FLG"];
      let pageTypeParams = {
        pageLength: 100,
        pageStart: 1,
        periodFlg: SystemPosModel.period && SystemPosModel.period.value,
        salesId: SystemPosModel.salesPointTotal && SystemPosModel.salesPointTotal.value,
        validFlg: "1"
      }
      let dictionaryData = yield call(_Ssubsalespointselection.init, dictype, pageType, pageTypeParams);
      _initState.dictionaryData = dictionaryData;
      let data = [];
      if (pageType === "salesPointTotal") {
        data = dictionaryData["PARA_POS_SALES"];
        if (SystemPosModel.salesPointTotal && SystemPosModel.salesPointTotal.value) {
          _initState.checked = {
            ...SystemPosModel.salesPointTotal
          }
        } else {
          //默认选中第一个
          if (_.size(data) !== 0) {
            let item = _.head(data);
            _initState.checked = {
              ...item
            }
          }
        }
      } else if (pageType === "productFlg") {
        //处理餐点数据  判断是否为排餐  “1” 自助餐   “2” 排餐
        if (SystemPosModel.salesPointTotal["data-datasource"].sales_typ === "2") {
          //房含属性
          let PRODUCT_FLG = [];
          _.forEach(SystemPosModel.salesPointTotal["data-datasource"].period_flgs, i => {
            _.forEach(dictionaryData["GRP_FIN_PRODUCT.PRODUCT_FLG"], k => {
              if (k.value === i) {
                PRODUCT_FLG.push(k);
              }
            })
          })
          data = PRODUCT_FLG;
        } else {
          data = _initState.periodData;
        }
        if (SystemPosModel.period && SystemPosModel.period.value) {
          let item = _.find(data, (i) => { return i.value === SystemPosModel.period.value });
          _initState.checked = {
            ...item
          }
        } else {
          //默认选中第一个
          if (_.size(data) !== 0) {
            let item = _.head(data);
            _initState.checked = {
              ...item
            }
          }
        }
      } else if (pageType === "periodIdData") { //用餐时间
        let { period_id = "" } = params;
        data = dictionaryData["PERIOD"] || [];
        data.unshift({
          text: languageConfig.all,
          value: "",
          "data-datasource": {
            name: languageConfig.all
          }
        })
        let item = _.find(data, (i) => { return i.value === period_id });
        if (item) {
          _initState.checked = {
            ...item
          }
        } else {
          _initState.checked = {
            text: languageConfig.all,
            value: "",
            "data-datasource": {
              name: languageConfig.all
            }
          }
        }

      }
      _initState.data = data;
      yield put({
        type: "initComplete",
        data: {
          ..._initState
        }
      })
    },


    /**
     * 切换销售点
     *
     * @param {*} params
     * @param {*} {select, call, put}
     */
    * saveInitSales(params, { select, call, put }) {
      const state = yield select((state) => state);
      const { Msubsalespointselection, SystemPosModel } = state;
      let thisState = _.cloneDeep(Msubsalespointselection);
      //选择的销售点id
      let _systemPosModelData = {};
      let _encryptStr = global.__ENCRYPT__.Encrypt(escape(JSON.stringify({
      })));
      //选择餐点
      if (thisState.routeParams.pageType === 'productFlg') {
        _systemPosModelData = {
          ...SystemPosModel,
          period: {
            ...thisState.checked
          },
        }
        //缓存posmodel数据
        yield call(SetSessionItem, "SystemPosModel", _systemPosModelData);

        yield put({
          type: "SystemPosModel/changeStatusComplete",
          data: {
            ..._systemPosModelData
          }
        })

        //"1” 自助餐 “2” 排餐
        if (SystemPosModel.salesPointTotal["data-datasource"]["sales_typ"] === "1") {
          yield put({ type: "SystemRouterModel/push", url: `/postableview/${_encryptStr.orgData}/${_encryptStr.encryptData}` })
        } else {
          yield put({ type: "SystemRouterModel/push", url: `/usingtableview/${_encryptStr.orgData}/${_encryptStr.encryptData}` })
        }
      } else if (thisState.routeParams.pageType === 'salesPointTotal') {
        _systemPosModelData = {
          ...SystemPosModel,
          salesPointTotal: {
            ...thisState.checked
          },
        }
        yield put({
          type: "SystemPosModel/changeStatusComplete",
          data: {
            ..._systemPosModelData
          }
        })
        //选择销售点
        let _salePoint = { ...thisState.checked };
        //需要更新的posModel数据
        //缓存销售点信息
        yield call(SetSessionItem, "salePoint", _salePoint);
        //更新pos 的汇总信息 和 餐点信息
        global.__SALEPOINT__ = {
          ...thisState.checked
        }

        //缓存posmodel数据
        yield call(SetSessionItem, "SystemPosModel", _systemPosModelData);
        //"1” 自助餐 “2” 排餐
        if (thisState.checked["data-datasource"]["sales_typ"] === "1") {
          yield put({ type: "SystemRouterModel/push", url: `/postableview/${_encryptStr.orgData}/${_encryptStr.encryptData}` })
        } else {
          yield put({ type: "SystemRouterModel/push", url: `/usingtableview/${_encryptStr.orgData}/${_encryptStr.encryptData}` })
        }
      } else if (thisState.routeParams.pageType === 'periodIdData') {
        yield put({
          type: "Musingtableview/queryList",
          periodIdData: thisState.checked
        })
      }

      //关闭当前窗口
      yield put({ type: "SystemModel/updateModalWindowState" });
    },

  },
  reducers: {
    /**
     * 初始化完成
     *
     * @param {*} state
     * @param {*} action
     * @returns
     */
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },

    /**
    * 更新state的根节点属性值
    * @param {*} state
    * @param {*} action
    */
    updateRootStatePropComplete(state, action) {
      return {
        ...state,
        ...action.updateObj
      }
    },
  }
}

export default _model;