/**
 * systemName:开台
 * author:
 * remark:
 */
import initState from './initstate';
import _ from "lodash";
import Susing_opentable from 'Hyservices/using_opentable';
import initstate from './initstate';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/using_opentable';
import ja_jp from 'ja_jp/using_opentable';
import zh_cn from 'zh_cn/using_opentable';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _Susing_opentable = new Susing_opentable();

const _model = {
  namespace: 'Musing_opentable',
  state: { ...initState },
  subscriptions: {},
  effects: {
    * init(params, { select, call, put }) {

      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      let _thisState = _.cloneDeep(initstate);
      _thisState.routeParams = { ...params };
      let _params = {
        "fontAcctNo": SystemPosModel["salesPointTotal"]["data-datasource"].font_acct_no || "",
      }
      let { data } = yield call(_Susing_opentable.init, _params);
      _thisState.data = data;
      _thisState.checked = _.size(data) !== 0 ? _.head(data) || {} : {};
      yield put({
        type: "initComplete",
        data: _thisState
      })
    },

    * queryAcctForBlock(params, { select, call, put }) {
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      const thisState = yield select(({ Musing_opentable }) => Musing_opentable);
      let _thisState = _.cloneDeep(thisState);
      let _params = {
        "fontAcctNo": SystemPosModel["salesPointTotal"]["data-datasource"].font_acct_no || "",
        "fastQuery": thisState.formData.fastQuery || ""
      }
      let { data = [] } = yield call(_Susing_opentable.queryAcctForBlock, _params);
      _thisState.data = data;
      _thisState.checked = _.size(data) !== 0 ? _.head(data) || {} : {};
      yield put({
        type: "initComplete",
        data: _thisState
      })
    },

    //开台
    * openBlock(params, { select, call, put }) {
      const SystemPosModel = yield select(({ SystemPosModel }) => SystemPosModel);
      const thisState = yield select(({ Musing_opentable }) => Musing_opentable);
      let _params = {
        acctNo: thisState.checked.acct_no,
        salesId: SystemPosModel["salesPointTotal"].value,
        periodId: thisState.routeParams.data.period_id,
        blockItems: [{
          blockTypId: thisState.routeParams.data.block_id,
          blockQuant: thisState.formData.blockQuant,
        }]
      }
      const { resultData } = yield call(_Susing_opentable.openBlock, _params);

      yield put({ type: "SystemModel/updateModalWindowState" })

      yield put({
        type: "SystemModel/updateModalWindowState",
        key: 'subsubaddpackage',
        title: languageConfig.title,
        size: 'large',
        params: {
          roomNumber: thisState.checked.room_num,
          resName: thisState.checked.alt_nm,
          surplus: thisState.checked.bal_amt,
          acctNo: thisState.checked.acct_no,
          blockItemId: resultData.blockItemId,
          blockKey: resultData.blockKey,
          blockId: thisState.routeParams.data.block_id,
          pseudo_flg: thisState.checked.pseudo_flg,
          cbDispatch: thisState.routeParams.cbDispatch,
        },
      })

    },

  },
  reducers: {
    initComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },



    /**
* 更新state的根节点属性值
* @param {*} state
* @param {*} action
*/
    updateRootStatePropComplete(state, action) {
      return {
        ...state,
        ...action.updateObj
      }
    },

    /**
 * 更新表单数据
 * @param {*} state
 * @param {*} action {fieldName,fieldValue}
 */
    updateBaseFormData(state, action) {
      return {
        ...state,
        formData: {
          ...state.formData,
          ...action.formData
        }
      }
    },


  }
}

export default _model;