/**
 * systemName:开台
 * author:
 * remark:
 */
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/using_opentable';
import ja_jp from 'ja_jp/using_opentable';
import zh_cn from 'zh_cn/using_opentable';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default {
  routeParams: {
    data: {},
  },

  element: {
    blockQuant: {//桌台数
      "type": "input",
      "width": 6,
      "interactionOptions": {
        "isMust": true,
      },
      "componentOptions": {
        "text": languageConfig.blockQuant
      }
    },

    fastQuery: {//在店客人
      "type": "input",
      "width": 6,
      "componentOptions": {
        "text": languageConfig.fastQuery,
        placeholder: languageConfig.placeholder,//"房号/姓名",
      }
    }
  },

  formData: {
    "blockQuant": "1",
    "fontAcctNo": "",
    "fastQuery": "",
  },

  //账户信息
  data: [],

  //选中信息
  checked: {
    acct_no: "",
  },
}