export default {
  // 弹窗Ifram Url
  iframeUrl: '',
  // 页面Ifram Url
  baseIframeUrl: '',
  // 弹出框状态
  modalwindowstate: {
    // 是否显示
    "show": false,
    // 需要传入弹框的props参数
    "params": {},
    // modal-size属性
    "size": "",
  },
}