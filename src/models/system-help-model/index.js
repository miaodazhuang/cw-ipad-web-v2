/**
 * @file 帮助中心 弹窗
 */
import initState from './initstate';
import CommonService from 'Hyservices/common-service';
const _commonService = new CommonService();

const _model = {
  namespace: 'SystemHelpModel',
  state: initState,
  effects: {
    // 更新状态
    * updateHelpModalState(params, { select, call, put }) {
        let _iframeUrl = ''
        const { view_id } = params.params || {}
        if (view_id) {
          const data = yield call(_commonService.getHelpFrame, { view_id, language_cd: process.env.NODE_ENV_LANGUAGECODE })
          if (data.length) {
            _iframeUrl = data[0].url
          }
        }
        const modalwindowstate = {
          // 是否显示
          "show": params.show || false,
          // 需要传入弹框的props参数
          "params": params.params || {},
          // modal-size属性
          "size": params.size,
        }

        yield put({
          type: 'updateState',
          modalwindowstate,
          iframeUrl: _iframeUrl,
        })
    },
  },
  reducers: {
    // 更新状态
    updateState(state, action) {
      return {
        ...state,
        ...action,
      }
    }
  }
}

export default _model;