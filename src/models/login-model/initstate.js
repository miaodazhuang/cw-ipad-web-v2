import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/login';
import ja_jp from 'ja_jp/login';
import zh_cn from 'zh_cn/login';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
export default {


  // 登录库类型 0正式库
  dbType: "0",
  // 是否记住登录信息
  isRemember: true,


  element: {
    groupnum: {
      "type": "input",
      "className": "groupinput",
      "width": 16,
      "interactionOptions": {
        "isMust": true,
        "enableChange":true,
      },
      "componentOptions": {
        "maxlength": 20,
        "className": "text-input",
        "text": languageConfig.groupnum,
        'placeholder': languageConfig.placeholder_groupnum
      }
    },

    htlnum: {
      "type": "input",
      'className': 'groupinput',
      "width": 16,
      "interactionOptions": {
        "isMust": true,
        "enableChange":true,
      },
      "componentOptions": {
        "className": "text-input",
        "maxlength": 20,
        "text": languageConfig.htlnum,
        'placeholder': languageConfig.placeholder_htlnum
      }
    },

    accountCode: {
      "type": "input",
      'className': 'groupinput',
      "width": 16,
      "interactionOptions": {
        "isMust": true,
        "enableChange":true,
      },
      "componentOptions": {
        "className": "text-input",
        "text": languageConfig.accountCode,
        'placeholder': languageConfig.placeholder_accountCode
      }
    },

    accountPassword: {
      "type": "input",
      'className': 'groupinput',
      "width": 16,
      "interactionOptions": {
        "isMust": true,
        "enableChange":true,
      },
      "componentOptions": {
        type: "password",
        "className": "text-input",
        "text": languageConfig.accountPassword,
        'placeholder': languageConfig.placeholder_accountPassword
      }
    },

    language: {
      "type": "select",
      "width": 16,
      'className': 'grouplanguage',
      "interactionOptions": {
        "isMust": true,
      },
      "componentOptions": {
        "text": languageConfig.language,
        "selection": true,
        "options": [
          { key: 0, text: languageConfig.language1, value: "zh_cn" },
          //{ key: 1, text: "English", value: "en_us" },
          { key: 2, text: languageConfig.language2, value: "ja_jp" }
        ]
      }
    },
  },

  // 登录表单数据
  formData: {
    // 集团代码
    groupnum: "",
    // 酒店代码
    htlnum: "",
    // 用户账户
    accountCode: "",
    // 用户密码
    accountPassword: "",
    //语言
    language: process.env.NODE_ENV_LANGUAGECODE?process.env.NODE_ENV_LANGUAGECODE:'zh_cn',
  },
}