/** 登陆 */
import initState from './initstate';
import base64 from 'js-base64';
import PathToRegexp from 'path-to-regexp';
import _ from 'lodash';
import { GetCookie } from 'Hyutilities/cookie_helper';
import { RemoveSessionItem } from 'Hyutilities/sessionstorage_helper';
import { login } from 'Hyservices/login-service';
import { SetCookie } from 'Hyutilities/cookie_helper';

let replace = null;
const _model = {
  namespace: 'LoginModel',
  state: initState,
  subscriptions: {
    historyListenEvent({ history, dispatch }) {
      replace = history.replace;
      return history.listen(async (history) => {
        const match = PathToRegexp("/login").exec(history.pathname);
        if (match) {
          dispatch({
            type: "init"
          })
        }
      })
    }
  },
  effects: {
    /**
     * 初始化登录
     *
     * @param {any} params
     * @param {any} { select, put, call }
     */
    * init(params, { select, put, call }) {
      //debugger
      // 清空路由model中的数据
      yield put({
        type: "SystemRouterModel/initComplete"
      })
      // 获取缓存的登录信息
      const groupnum = GetCookie("ChainCd") || "";
      const htlnum = GetCookie("UnitCd") || "";
      const accountCode = GetCookie('AccountCode') || ''
      const dbType = Number(GetCookie("DbType") || 0);
      const language = GetCookie('lang') || process.env.NODE_ENV_LANGUAGECODE;
      // 调用初始化完成事件
      yield put({
        type: "initComplete",
        formData: { groupnum, htlnum, dbType, language, accountCode }
      })
    },
    /**
     * 登录事件
     * @param {*} params
     * @param {*} param1
     */
    * login(params, { select, put, call }) {
      const thisState = yield select(({ LoginModel }) => LoginModel);
      const { groupnum, htlnum, accountCode, accountPassword } = thisState.formData;
      // 清空缓存数据
      yield call(RemoveSessionItem, "DictionaryData");
      yield call(RemoveSessionItem, "EncryptStr");
      yield call(RemoveSessionItem, "UserData");
      yield call(RemoveSessionItem, "salePoint");
      yield call(RemoveSessionItem, "cacheData");
      yield call(RemoveSessionItem, "tabData");
      yield call(RemoveSessionItem, "currentTabData");

      let winlang = GetCookie('lang');
      if (winlang === null || winlang === 'null' || !winlang) {
        winlang = process.env.NODE_ENV_LANGUAGECODE || 'zh_cn';
        //如果没有值就把默认值写入cookie
        SetCookie('lang', winlang);
      }

      const loginInfo = {
        isRemember: thisState.isRemember ? 7 : '',
        paramsdata: {
          chainCd: groupnum.toUpperCase(),
          unitCd: htlnum.toUpperCase(),
          systemInfo: base64.Base64.encode(`${accountCode},${accountPassword}`),
          dbType: thisState.dbType,
          sysID: 'IPAD-POS',
          accountCode: accountCode,
          languageCd: winlang.replace('_', '-') || process.env.NODE_ENV_LANGUAGECODE.replace('_', '-') || "zh-cn"
        }
      }
      let loginData = yield call(login, loginInfo);
      if (loginData) {

        if(loginData.unitFlg!=="1"){
          yield put({
            type: "SystemModel/updateMessageState",
            statecode: 2,
            message: '施設コードでログインしてください。'
          })
        }else{ 
          window.location.replace("/main/0/inittable/0/0");
        }
      }
    },
  },
  reducers: {
    /**
     * 完成登录表单初始化
     * @param {*} state
     * @param {*} action
     */
    initComplete(state, action) {
      // 结构登录表单数据
      const { groupnum, htlnum, dbType, language, accountCode, accountPassword = '' } = action.formData;
      SetCookie('lang', language);
      return {
        ...state,
        formData: {
          ...state.formData,
          groupnum,
          htlnum,
          dbType,
          language,
          accountCode,
          accountPassword,
        },
      }
    },
    /**
     * 更新登录表单数据
     * @param {*} state
     * @param {*} action {fieldName,fieldValue}
     */
    updateFormData(state, action) {
      if (_.has(action, "formData")) {
        let formData = Object.assign({}, state.formData, action.formData);
        if (_.has(formData, 'language')) {
          SetCookie('lang', formData.language);
        }
        return {
          ...state,
          formData: formData
        }
      } else {
        return {
          ...state,
          formData: {
            ...state.formData,
            [action.fieldName]: action.fieldValue
          }
        }
      }
    },

    updateLanguageComplete(state, action) {
      return {
        ...state,
        ...action.data
      }
    },

    /**
    * 更新state的根节点属性值
    * @param {*} state
    * @param {*} action
    */
    updateRootStatePropComplete(state, action) {
      return {
        ...state,
        ...action.updateObj
      }
    },

  }
}


export default _model;