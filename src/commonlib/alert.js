import { FetchHandle } from 'Hyutilities/request_helper';
import _ from 'lodash';

const alertConfig = {
  //预定提醒
  "M01322010A010LT00201": {
    // 预订类型
    "type": "YD",
    // 是否拦截effect方法
    "intercept": true,
    // effectType
    "submitAcctDataByCheckin": {
      // 动态参数配置
      "dynamicParamsPath": {
        "acctNo": {
          // 参数来源state：从当前model中获取否则从参数中获取
          "from": "state",
          // 参数路径
          "path": "routeParams.id"
        }
      },
      // 静态参数
      "definition": {
        // 预订提醒
        "alertTyps": ["CI"],
        "getFirst": "1"
      },
      // 接口地址
      "func": "/bs/3220010/RsvAccountAlert/getRsvAccountAlertByTyp",
      // 提醒消息路径配置
      "alertData": {
        "alertNote": "memo",
        "alertId": "alertId",
        "color": "alertColor",
        "preNo": "acctNo",
        "accName": "accompanyNms",
        "timeStart": "arrDt",
        "timeEnd": "dptDt",
        "alertTypDept": "alertTypDept"
      }
    },
  },
  //收银提醒
  "M01324010A010LT00101": {
    // 预订类型
    "type": "YD",
    // 是否拦截effect方法
    "intercept": true,
    //结账退房 待结结账退房 提前退房结账
    "showAlter": {
      // 动态参数配置
      "dynamicParamsPath": {
        "acctNo": {
          // 参数来源state：从当前model中获取否则从参数中获取
          "from": "state",
          // 参数路径
          "path": "accountDetail.acctNo"
        }
      },
      // 静态参数
      "definition": {
        // 预订提醒
        "alertTyps": ["CO"],
        "getFirst": "1"
      },
      // 接口地址
      "func": "/bs/3220010/RsvAccountAlert/getRsvAccountAlertByTyp",
      // 提醒消息路径配置
      "alertData": {
        "alertNote": "memo",
        "alertId": "alertId",
        "color": "alertColor",
        "preNo": "acctNo",
        "accName": "accompanyNms",
        "timeStart": "arrDt",
        "timeEnd": "dptDt",
        "alertTypDept": "alertTypDept"
      }
    },
    // effectType 过房费 提前过房费 同来人结账
    "openConfirm": {
      // 动态参数配置
      "dynamicParamsPath": {
        "acctNo": {
          // 参数来源state：从当前model中获取否则从参数中获取
          "from": "state",
          // 参数路径
          "path": "accountDetail.acctNo"
        }
      },
      // 静态参数
      "definition": {
        // 预订提醒
        "alertTyps": ["CO"],
        "getFirst": "1"
      },
      // 接口地址
      "func": "/bs/3220010/RsvAccountAlert/getRsvAccountAlertByTyp",
      // 提醒消息路径配置
      "alertData": {
        "alertNote": "memo",
        "alertId": "alertId",
        "color": "alertColor",
        "preNo": "acctNo",
        "accName": "accompanyNms",
        "timeStart": "arrDt",
        "timeEnd": "dptDt",
        "alertTypDept": "alertTypDept"
      }
    },

    // effectType 加收全半日房费
    "openConfirm_DP": {
      // 动态参数配置
      "dynamicParamsPath": {
        "acctNo": {
          // 参数来源state：从当前model中获取否则从参数中获取
          "from": "state",
          // 参数路径
          "path": "accountDetail.acctNo"
        }
      },
      // 静态参数
      "definition": {
        // 预订提醒
        "alertTyps": ["DP"],
        "getFirst": "1"
      },
      // 接口地址
      "func": "/bs/3220010/RsvAccountAlert/getRsvAccountAlertByTyp",
      // 提醒消息路径配置
      "alertData": {
        "alertNote": "memo",
        "alertId": "alertId",
        "color": "alertColor",
        "preNo": "acctNo",
        "accName": "accompanyNms",
        "timeStart": "arrDt",
        "timeEnd": "dptDt",
        "alertTypDept": "alertTypDept"
      }
    },

  },
}

async function getAlertConfig(effectArgs, state) {
  const types = effectArgs[0].type.split('/');
  if (_.has(alertConfig, types[0]) && _.has(alertConfig[types[0]], types[1])) {
    // 如果存在初始化定义，则根据配置信息获取接口参数
    const {
      dynamicParamsPath, // 动态参数定义
      definition, // 静态参数定义
      func // 接口地址
    } = alertConfig[types[0]][types[1]];
    let params = {};
    // 获取动态参数参数
    _.forEach(dynamicParamsPath, (item, index) => {
      params[index] = item.from === "state" ? _.get(state, item.path) : _.get(effectArgs, item.path);
    })
    // 拼接静态参数
    params = _.assign({}, params, definition);

    // 调用接口获取提醒消息
    const { data } = await FetchHandle(func, 'post', params);
    const alertData = {};
    if (_.size(data) > 0) {
      _.forEach(alertConfig[types[0]][types[1]]["alertData"], (item, index) => {
        alertData[index] = _.get(data[0], item);
      })
      //alertData["alertTypDept"] = alertConfig[types[0]].type;
      alertData["alertTye"] = alertConfig[types[0]].type;
      alertData["effectArgs"] = effectArgs[0];
      alertData["active"] = true;
      alertData["isSkip"] = true;
      alertData["type"] = "SystemAlertModel/updateAlertState";

      return {
        intercept: true,
        alertData
      }
    } else {
      return {
        intercept: false
      }
    }

  } else {
    return {
      intercept: false
    }
  }
}

export default {
  getAlertConfig
}