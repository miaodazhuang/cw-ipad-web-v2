import dynamic from 'Hyutilities/dynamicload_help';

export default (app) => {
  return dynamic({
    app: app,
    title: '首页',
    models: () => [
      import("Hymodels/login-model")
    ],
    component: () => import('./component')
  });
}