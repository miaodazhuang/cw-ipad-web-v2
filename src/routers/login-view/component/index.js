import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SetSessionItem } from 'Hyutilities/sessionstorage_helper';
import { SetCookie } from 'Hyutilities/cookie_helper';
import HyButtonCom from 'Hycomponents/featurecoms/hybuttoncom';
import HyFormCom from 'Hycomponents/featurecoms/hyformcom';
import { BaseComponent } from 'Hyutilities/component_helper';
import { connect } from 'dva';
import HyLoadCom from 'Hycomponents/businesscoms/hyloadcom';
import HyMessageCom from 'Hycomponents/businesscoms/hymessagecom';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/login';
import ja_jp from 'ja_jp/login';
import zh_cn from 'zh_cn/login';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const packageConfig = require('../../../../package.json');

@BaseComponent
class LoginView extends Component {


  static defaultProps = {
  }

  static propTypes = {
    element: PropTypes.object,
    formData: PropTypes.object,
    isRemember: PropTypes.bool,
    dbType: PropTypes.string,
  }
  // componentWillMount() {}
  componentDidMount() {
    this.props.dispatch({
      type: 'Loginmodel/init',
      params: {},
    })
  }

  render() {
    return (
      <div className='LoginView-loginpage'>
        <div className="loginpage-container">
          <div className="loginpage-public-image"></div>
          <div className={'hyhotelbaselogincom loginpage-login'}>
            {/* <div className={`logo ${process.env.NODE_ENV_APP_VERSION !== 'business' ? 'zh_cn' : 'ja_jp'}`} /> */}
            <div className={`logo ja_jp`} />
            <h1>{languageConfig.versionDesc}</h1>
            <HyFormCom
              className="label_words_4 flex"
              ref={c => this.formCom = c}
              key="formCom"
              name='formCom'
              formData={this.props.formData}
              onBlur={this.onBlur}
              onChange={this.onChange}
              elements={this.props.element} />
            <div className="login-form-item actions">
              <div className='left-div'>
                <div
                  className={`action-item remember-width remember ${this.props.isRemember ? 'active' : ''}`}
                  onClick={this._onChangeRememberEvent}
                >
                  {languageConfig.isRemember}
                </div>
              </div>
              <div className='right-div'>
                <div
                  className={`action-item library ${`${this.props.dbType}` === '0' ? 'active' : ''}`}
                  onClick={() => this._onDBTypeChange('0')}
                >
                  {languageConfig.production}
                </div>
                <div
                  className={`action-item library ${`${this.props.dbType}` === '1' ? 'active' : ''}`}
                  onClick={() => this._onDBTypeChange('1')}
                >
                  {languageConfig.practice}
                </div>
              </div>
            </div>
            <div className="login-form-item login-btn">
              <HyButtonCom onClick={this._loginEvent} name='login' >{languageConfig.login}</HyButtonCom>
            </div>
          </div>
        </div>
        <div className='copyright'>{`${languageConfig.copyright} ${packageConfig.Version || ''}`}</div>
        <HyLoadCom />
        <HyMessageCom />
      </div>
    );
  }

  /**
   * 切换数据库
   *
   * @memberof LoginView
   */
  _onDBTypeChange = (data) => {
    this.props.dispatch({
      type: "LoginModel/updateRootStatePropComplete",
      updateObj: {
        dbType: data
      }
    })
  }

  /**
   * 是否记住登陆信息
   *
   * @memberof LoginView
   */
  _onChangeRememberEvent = (event, data) => {
    this.props.dispatch({
      type: "LoginModel/updateRootStatePropComplete",
      updateObj: {
        isRemember: !this.props.isRemember
      }
    })
  }

  /**
   * 更新表单值
   *
   * @memberof LoginView
   */
  onBlur = (event, data) => {
    this.props.dispatch({
      type: "LoginModel/updateFormData",
      formData: {
        ...data.formData,
      }
    })
  }



  /**
   *更改语言
  */
  onChange = (event, data) => {
    if (data.eventData.fieldname === "language") {
      global.__LANGUAGE__ = data.eventData.value;
      SetCookie('lang', data.eventData.value);
      setLang(data.eventData.value);
    }else{
      this.props.dispatch({
        type: "LoginModel/updateFormData",
        formData: {
          ...data.formData,
        }
      })
    }
  }



  /**
   * 登陆事件
   *
   * @memberof LoginView
   */
  _loginEvent = (event, data) => {
    if (data.name === 'login') {
      const submitData = this.formCom.GetValue();
      // 如果表单验证失败，则直接返回
      if (submitData.error) {
        return;
      }
      // 提交表单数据
      this.props.dispatch({
        type: "LoginModel/login",
      })
    }
  }
}
/**
 * 设置缓存语种
 *
 * @param {*} lang
 */
async function setLang(lang) {
  await SetSessionItem('lang', lang);
  window.location.reload();
}

function mapStateToProps({ LoginModel }) {
  return {
    element: LoginModel.element,
    formData: LoginModel.formData,
    isRemember: LoginModel.isRemember,
    dbType: LoginModel.dbType,
  };
}

export default connect(mapStateToProps)(LoginView);
