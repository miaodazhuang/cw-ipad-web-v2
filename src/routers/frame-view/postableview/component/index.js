/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
import React, { Component } from 'react';
import ReactDOM from "react-dom";
import PropTypes from 'prop-types';
import HyRightOperCom from 'Hycomponents/businesscoms/hyrightopercom';
import HyTableItemCom from 'Hycomponents/featurecoms/hytableitemcom';
import HyScrollCom from 'Hycomponents/featurecoms/hyscrollcom';
import { connect } from 'dva';
import { Menu } from 'semantic-ui-react';
import _ from 'lodash';
import moment from 'moment';
import { GetLanguage } from 'Hyutilities/language_helper';
import { FormatMoney } from 'Hyutilities/money_helper';
import en_us from 'en_us/postableview';
import ja_jp from 'ja_jp/postableview';
import zh_cn from 'zh_cn/postableview';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
//定义房含使用情况常亮
const ROOM_CONTAINING_STATUS = [
  {key: '0', text: languageConfig.all, value: '0'},
  {key: '1', text: languageConfig.toBeUsed, value: '1'},
  {key: '2', text: languageConfig.used, value: '2'},
  {key: '3', text: languageConfig.noContaining, value: '3'},
  {key: '4', text: languageConfig.leaveToUsed, value: '4'},
]

class Vpostableview extends Component {
  /**
   * 默认props属性
   */
  static defaultProps = {
    data: []
  }

  state = {
    showScroll: false
  }

  /**
   * props属性定义
   */
  static propTypes = {
    data: PropTypes.array,
    queryData: PropTypes.object,
  }

  componentDidMount() {
    //计算桌台容器高度
    // this._calcContainerHeight();
  }



  render() {
    const { totalCount } = this.props;
    return (
      <div className='Mpostableview'>
        <div className='roomContainsStatus'>
          <div className='roomContainsStatus-menu'>
            {this.getRoomContainsStatus()}
          </div>
          <div className='roomContainsStatus-total'>
            <span>{`${languageConfig.toBeUsed}: ${totalCount[1]}`}</span>
            <span>{`${languageConfig.used}: ${totalCount[2]}`}</span>
          </div>
        </div>

        <div className='tableDiv-wrap'>
          <div className='floor'>
            {this.getFloor()}
          </div>
          <div className='postableview-scroll'>
            <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} onScroll={this._onScroll} showVeriticalTrack={this.state.showScroll}>
              <div className='tableDiv' id="tableDivContainer" ref="tableDivContainer">
                {this.getRoomCom()}
              </div>
            </HyScrollCom>
          </div>
        </div>
      </div>
    );
  };


  /**
   * 滚动事件
   *
   * @memberof Vpostableview
   */
  _onScroll = (e, data) =>{
    if(!this.state.showScroll){
      this.setState({
        showScroll: true
      },()=>{
        setTimeout(()=>{
          this.setState({
            showScroll: false
          })
        },1000)
      })
    }
  }

  /**
  * 计算容器高度
  */
  _calcContainerHeight = () => {
    const ViewHeight = (document.documentElement.clientHeight - 104);
    this.refs.tableDivContainer.style.height = `${ViewHeight}px`;
  }

  /**
   * 获取楼层按钮
   *
   * @memberof Vpostableview
   */
  getFloor = () =>{
    const { FLOOR = [] } = this.props.dicData;
    const { selectedFloor } = this.props;
    let allFloor = [{key: 'all', value: 'all', text: languageConfig.all, 'data-datasource':{param_drpt: languageConfig.all}}, ...FLOOR];
    return (
      _.map(allFloor, item =>{
        let selected = selectedFloor === item.value? 'selecte-floor': '';
        return <div className={`floor-item text-ellipsis ${selected}`} onClick={this.onClickFloorItem.bind(this, item)}>{item['data-datasource'].param_drpt}</div>
      })
    )
  }

  /**
   * 点击左侧按钮
   *
   * @memberof Vpostableview
   */
  onClickFloorItem = (data) =>{
    this.props.dispatch({
      type: 'Mpostableview/queryList',
      source: 'floorItem',
      selectedFloor: data.value
    })
  }

  /**
   * 生成房含状态
   *
   * @memberof Vpostableview
   */
  getRoomContainsStatus = () => {
    const { selectedStatus } = this.props;
    //获取当前统计的数量
    return _.map(ROOM_CONTAINING_STATUS, (item, index) =>{
      let selected = item.value === selectedStatus? 'selectedItem': '';
      return <div className={`AreaDIV ${selected}`} onClick={(event) => { this._onClickRoomContainsStatus(event, { selectedStatus: item.value }) }} ><span className='text-ellipsis'>{item.text}</span></div>
    })
  }

  /**
   * 生成桌台
   *
   * @memberof Vpostableview
   */
  getRoomCom = () => {
    let coms = [];
    _.forEach(this.props.data, (i, index) => {
      //显示内容
      let titleArray = [`${i.room_num}`];//房号
      titleArray.push(`${i.alt_nm}`)//客人姓名
      if(_.find(i.package_list, item => item.avl_cnt !== 0 || item.use_cnt !== 0)){
        _.forEach(i.package_list, item =>{
          if(!_.isNil(item.package_id) && (item.avl_cnt !==0 || item.use_cnt !==0)){
            titleArray.push(`${item.package_nm}` )
            titleArray.push(`${languageConfig.canUse}:${item.avl_cnt} ${languageConfig.hadUse}:${item.use_cnt}` )
          }
        })
      }
      let block_stus = i.sum_avl_cnt > 0? '1': '0';
      let checked = false;
      coms.push(<HyTableItemCom
        key={`black-${index}`}
        showtopSum={false}
        data={i}
        titleArray={titleArray}
        blackStatus={`${block_stus}`}
        className=''
        checked={checked}
        onClick={this._onClickRoom}
      ></HyTableItemCom>);
    })
    return coms;
  }




  /**
   * 选择区域
   *
   * @memberof Vpostableview
   */
  _onClickRoomContainsStatus = (event, data) => {
    this.props.dispatch({
      type: 'Mpostableview/queryList',
      source: 'roomContainsStatus',
      selectedStatus: data.selectedStatus
    })
  }

  /**
   * 房间点击事件
   *
   * @memberof Vpostableview
   */
  _onClickRoom = (event, data) => {
    const { room_num, alt_nm, bal_amt, acct_no, sum_avl_cnt} = data;
    let key,size;
    //判断是否有房含，打开不同弹窗
    if(sum_avl_cnt > 0){
      //如果剩余数量大于0
      key = 'subusingpackage';
      size = 'large';
    }else{
      key = 'subsubaddpackage';
      size = 'large';
    }
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: key,
      title: languageConfig.useRoomContains,
      size: size,
      params: {
        roomNumber: room_num,
        resName: alt_nm,
        surplus: bal_amt,
        acctNo: acct_no,
        cbDispatch: "Mpostableview/queryList",
        cbParams: {
          successmessage: languageConfig.successmessage
        }
      },
    })
  }

}

function mapStateToProps({ Mpostableview, SystemPosModel }) {
  return {
    data: Mpostableview.data,
    totalCount: Mpostableview.totalCount,
    queryData: Mpostableview.queryData,
    block_id: Mpostableview.block_id,
    recordsTotal: Mpostableview.recordsTotal,
    dicData: Mpostableview.dicData,
    selectedFloor: Mpostableview.selectedFloor,
    selectedStatus: Mpostableview.selectedStatus,
    period: SystemPosModel.period,
    tracksize: SystemPosModel.tracksize
  };
}

export default connect(mapStateToProps)(Vpostableview);
