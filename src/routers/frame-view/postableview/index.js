import dynamic from 'Hyutilities/dynamicload_help';
export default (app) => {
  return dynamic({
    app: app,
    title: "postableview",
    models: () => [
      import("Hymodels/postableview")
    ],
    component: () => import('./component')
  });
}