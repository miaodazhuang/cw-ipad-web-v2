/**
 * systemName:
 * author:
 * remark:
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HyTableItemCom from 'Hycomponents/featurecoms/hytableitemcom';
import HyScrollCom from 'Hycomponents/featurecoms/hyscrollcom';
import { connect } from 'dva';
import { Radio, Label } from 'semantic-ui-react'
import _ from 'lodash';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/usingtableview';
import ja_jp from 'ja_jp/usingtableview';
import zh_cn from 'zh_cn/usingtableview';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

class Vusingtableview extends Component {

  /**
   * 默认props属性
   */
  static defaultProps = {
    data: []
  }

  state = {
    showScroll: false
  }

  /**
   * props属性定义
   */
  static propTypes = {
    data: PropTypes.array,
    queryData: PropTypes.object,
  }

  componentDidMount() {
    //计算桌台容器高度
    // this._calcContainerHeight();
    //disabled={this.props.queryData.periodId === "" ? true : false}
  }


  render() {
    return (
      <div className='Musingtableview'>
        <div className='roomContainsStatus'>
          <div className='roomContainsStatus-menu'>
            {this.getContainsStatus()}
          </div>
          <div className='roomContainsStatus-total'>
            <Label className='label-memoFlg'>
              {`${languageConfig.memoFlg}:`}
            </Label>
            <Radio
              name="memoFlg"
              className='memoFlg'
              toggle
              corner='left'
              value='0'
              checked={this.props.queryData.memoFlg === '1'}
              onChange={this.handleChange}
            />
            <Radio
              className='groupTyp'
              label={languageConfig.groupTyp_1}
              name='radioGroup1'
              checked={this.props.queryData.groupTyp === '1'}
              onChange={this.handleChange}
            />
            <Radio
              className='groupTyp'
              label={languageConfig.groupTyp_2}
              name='radioGroup2'
              checked={this.props.queryData.groupTyp === '2'}
              onChange={this.handleChange}
            />
          </div>
        </div>

        <div className='tableDiv-wrap'>
          <div className='floor'>
            {this.getFloor()}
          </div>
          <div className='postableview-scroll'>
            <HyScrollCom key='0' tracksize={this.props.tracksize || '10px'} onScroll={this._onScroll} showVeriticalTrack={this.state.showScroll}>
              <div className='tableDiv' id="tableDivContainer" ref="tableDivContainer">
                {this.getRoomCom()}
              </div>
            </HyScrollCom>
          </div>
        </div>
      </div>
    );
  };


  //选择右侧数据
  handleChange = (event, data) => {
    let _params = {
      ...this.props.queryData
    }
    if (data.name === "memoFlg") {
      _params.memoFlg = data.checked ? "1" : "";
    } else if (data.name === 'radioGroup1') {
      _params.groupTyp = data.checked ? "1" : "2"
    } else if (data.name === "radioGroup2") {
      _params.groupTyp = data.checked ? "2" : "1"
    }
    this.props.dispatch({
      type: "Musingtableview/queryList",
      ..._params
    })
  }




  /**
 * 滚动事件
 *
 * @memberof Vpostableview
 */
  _onScroll = (e, data) => {
    if (!this.state.showScroll) {
      this.setState({
        showScroll: true
      }, () => {
        setTimeout(() => {
          this.setState({
            showScroll: false
          })
        }, 1000)
      })
    }
  }

  /**
  * 计算容器高度
  */
  _calcContainerHeight = () => {
    const ViewHeight = (document.documentElement.clientHeight - 104);
    this.refs.tableDivContainer.style.height = `${ViewHeight}px`;
  }

  /**
   * 获取楼层按钮
   *
   * @memberof Vpostableview
   */
  getFloor = () => {
    return (
      _.map(this.props.blockData, item => {
        let selected = `${this.props.block_id}` === `${item.block_id}` ? 'selecte-floor' : '';
        return <div className={`floor-item  ${selected}`} onClick={this.onClickFloorItem.bind(this, item)}>
          <span className='span-first-child text-ellipsis'>
            {item.block_drpt}
          </span>
          <span className='span-last-child text-ellipsis'>
            {`${languageConfig.can}${item.free_quant}/${item.total_quant}`}
          </span>
        </div>
      })
    )
  }

  /**
   * 点击左侧按钮
   *
   * @memberof Vpostableview
   */
  onClickFloorItem = (data) => {
    this.props.dispatch({
      type: 'Musingtableview/queryList',
      blockTypId: data.block_id
    })
  }

  /**
   * 生成房含状态
   *
   * @memberof Vpostableview
   */
  getContainsStatus = () => {
    //获取当前统计的数量
    return (
      <div className={`AreaDIV  selectedItem `}
        onClick={(event) => { this._onClickContainsStatus(event, { period_id: this.props.periodIdData.value }) }} >
        <span className='text-ellipsis span-1'>{this.props.periodIdData.text}</span><span className='span-2'>{'▼'}</span>
      </div>
    )
  }



  /**
   * 选择区域
   *
   * @memberof Vpostableview
   */
  _onClickContainsStatus = (event, data) => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: "subsalespointselection",
      title: languageConfig.select,
      size: "large",
      params: {
        pageType: "periodIdData",//选择餐点
        period_id: this.props.queryData.periodId
      }
    })
  }





  /**
   * 生成桌台
   *
   * @memberof Vpostableview
   */
  getRoomCom = () => {
    let coms = [];
    _.forEach(this.props.seq, i => {
      let _array = _.get(this.props.data, i);
      if (_.size(_array) !== 0) {
        let comItems = [];
        let sum = 0, can = 0;
        //循环生成每个item
        _.forEach(_array, (k, index) => {
          //0 空台  1占用
          let block_stus = k.acct_no ? '1' : '2';
          sum = sum + k.block_quant;
          if (!k.acct_no) {
            can = can + k.block_quant;
          }
          //显示房号 和 桌台X数量
          let _specialContent = <div className="itemComs">
            <div className='title-Div text-ellipsis left'>{k.room_num || languageConfig.null}</div>
            <div className='title-Div text-ellipsis right'>{`${k.block_drpt}X${k.block_quant}`}</div>
          </div>;
          let packageString = ["", ""];
          if (_.has(k, "pkg_list")) {
            _.forEach(k.pkg_list, j => {
              packageString.push(`${j.pkg_nm || ""} ${j.pkg_quant || ""}`)
            })
          }
          let titleArray = [
            "",//站位使用不显示
            k.alt_nm || "",
            ...packageString
          ];
          comItems.push(<HyTableItemCom
            key={`black-${index}`}
            usingWidth={true}
            showtopSum={false}
            data={k}
            specialIndex={0}
            specialContent={_specialContent}
            titleArray={titleArray}
            blackStatus={`${block_stus}`}
            className=''
            checked={false}
            onClick={this._onClickPackage}
          ></HyTableItemCom>);
        })

        coms.push(<div className='seq-row'>
          <div className='seq-title'>{`${i} (${languageConfig.can}${can}/${sum})`}</div>
          <div className='seq-comItems'>{comItems}</div>
        </div>)
      }
    })

    return coms;
  }





  /**
   * 点击事件
   *
   * @memberof Vpostableview
   */
  _onClickPackage = (event, data) => {
    let title = "";
    let key = "";
    let size = "";
    if (_.has(data, "acct_no")) {
      //使用房含
      key = "using_package";
      title = languageConfig.using_package;
      size = "large";
    } else {
      //开台
      key = "using_opentable";
      title = languageConfig.using_openpackage;
      size = "small";
    }
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: key,
      title: title,
      size: size,
      params: {
        data: data,
        cbDispatch: "Musingtableview/queryList",
        cbParams: {
          successmessage: languageConfig.successmessage
        }
      },
    })
  }


}

function mapStateToProps({ Musingtableview, SystemPosModel }) {
  return {
    data: Musingtableview.data,
    queryData: Musingtableview.queryData,
    dicData: Musingtableview.dicData,
    block_id: Musingtableview.block_id,
    blockData: Musingtableview.blockData,
    seq: Musingtableview.seq,
    periodId: Musingtableview.periodId,
    periodIdData: Musingtableview.periodIdData,
    period: SystemPosModel.period,
  };
}

export default connect(mapStateToProps)(Vusingtableview);