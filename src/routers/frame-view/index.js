/**
 * 框架
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, Route, Switch } from 'dva/router';
import { connect } from 'dva';
import HyTopCom from 'Hycomponents/businesscoms/hytopcom';
// 加载中状态组件
import HyLoadCom from 'Hycomponents/businesscoms/hyloadcom';
import HyMessageCom from 'Hycomponents/businesscoms/hymessagecom';
import HySubWindowCom from 'Hycomponents/subwindows';
import HyConfirmCom from 'Hycomponents/businesscoms/hyconfirmcom';
import HyPopHelpCom from 'Hycomponents/businesscoms/hypophelpcom';
import HyAlertCom from 'Hycomponents/businesscoms/hyalertcom';


//桌台列表
import Vpostableview from './postableview';
import Vusingtableview from './usingtableview';
import Inittable from './inittable';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/frame-view';
import ja_jp from 'ja_jp/frame-view';
import zh_cn from 'zh_cn/frame-view';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });


//#region 业务路由

//#endregion 业务路由结束

class FrameView extends Component {

  static childContextTypes = {
    app: PropTypes.object.isRequired,
    dynamic: PropTypes.func,
    history: PropTypes.object,
    match: PropTypes.object
  }

  getChildContext = () => {
    return {
      app: this.props.app,
      dynamic: this.props.dynamic,
      history: this.props.history,
      match: this.props.match
    }
  }

  componentDidMount() {
    //判断是否已经选择了销售点,未选择直接弹出销售点选择列表
    if (global.__SALEPOINT__ === null) {
      this._openSalesPointSelection();
    }

  }

  componentDidCatch(error, info) {
    //alert("javascript Rendering error！");
    global.__LOGOUT__();
    console.error(error, info);
  }

  render() {
    const { match } = this.props;
    return (
      <div className="frame-view">
        <div className="top-segment">
          <HyTopCom {...this.props}></HyTopCom>
        </div>
        <Switch>
          <Route path={`${match.path}/inittable/:paramobject/:encryptstr`} component={Inittable(this.props.app)} />
          <Route path={`${match.path}/postableview/:paramobject/:encryptstr`} component={Vpostableview(this.props.app)} />
          <Route path={`${match.path}/usingtableview/:paramobject/:encryptstr`} component={Vusingtableview(this.props.app)} />
        </Switch>
        <HyMessageCom />
        <HySubWindowCom />
        <HyPopHelpCom />
        <HyConfirmCom />
        <HyAlertCom />
        <HyLoadCom />
      </div>
    );
  }


  /**
   * 弹出销售点选择列表
   *
   * @memberof FrameView
   */
  _openSalesPointSelection = () => {
    this.props.dispatch({
      type: "SystemModel/updateModalWindowState",
      key: "subsalespointselection",
      title: languageConfig.subsalespointselection,
      size: "large",
      closeIcon: false,
      params: {
        pageType: "salesPointTotal",
      }
    })
  }
}
export default withRouter(connect()(FrameView));