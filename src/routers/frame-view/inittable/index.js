import dynamic from 'Hyutilities/dynamicload_help';
export default (app) => {
  return dynamic({
    app: app,
    title: "inittable",
    models: () => [],
    component: () => import('./component')
  });
}