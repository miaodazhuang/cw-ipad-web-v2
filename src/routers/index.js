import React from 'react';
import PropTypes from 'prop-types';
import { routerRedux, Route, Switch, Redirect } from 'dva/router';
import dynamic from '../utils/dynamicload_help';
import FrameView from './frame-view';
import LoginView from './login-view';
import { GetCookie } from 'Hyutilities/cookie_helper';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/frame-view';
import ja_jp from 'ja_jp/frame-view';
import zh_cn from 'zh_cn/frame-view';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const { ConnectedRouter } = routerRedux;

let winlang = GetCookie('lang');
if (winlang === null || winlang === 'null' || !winlang) {
  winlang = process.env.NODE_ENV_LANGUAGECODE || 'zh_cn';
}



const Routers = ({ history, app }) => {
  //设置网站标题
  window.document.title = languageConfig.title;
  //生成环境下添加多语下的样式文件
  if (process.env.NODE_ENV === "production") {
    const link = document.createElement('link');
    link.rel = 'stylesheet';
    link.href = `/${process.env.NODE_ENV_VERSION}/static/css/theme-${winlang}.css`;
    document.head.appendChild(link);
  }
  return (
    <ConnectedRouter history={history} >
      <Switch>
        <Route path="/login" component={LoginView(app)} />
        <Route path="/main/:tabIndex" component={(props) => <FrameView app={app} dynamic={dynamic} {...props} />} />
        <Redirect to={{ pathname: '/login' }} />
      </Switch>
    </ConnectedRouter>
  )
}

Routers.propTypes = {
  history: PropTypes.object,
  app: PropTypes.object,
}

export default Routers;
