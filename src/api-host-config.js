import _ from 'lodash';

// 通过环境变量判断当前是否为灰度环境，如果是使用huiduapi否则使用pmsapi
const devApiHost = process.env.NODE_ENV_DAH || "10.12.27.5";//13.253


const windowMapAip = {
  "PMSAPI": _.has(window, '__PMSAPI__') && _.trim(window.__PMSAPI__) !== "" ? window.__PMSAPI__ : "pmsapi.jwcloudpms.com/3160001RA-1.0.0-SNAPSHOT",
  "POSAPI": _.has(window, '__POSAPI__') && _.trim(window.__POSAPI__) !== "" ? window.__POSAPI__ : "posapi.jwcloudpms.com/3161001RA-1.0.0-SNAPSHOT",
  "LPSAPI": _.has(window, '__LPSAPI__') && _.trim(window.__LPSAPI__) !== "" ? window.__LPSAPI__ : "lpsapi.jwcloudpms.com/3162001RA-1.0.0-SNAPSHOT",
  "CRMAPI": _.has(window, '__CRMAPI__') && _.trim(window.__CRMAPI__) !== "" ? window.__CRMAPI__ : "crmapi.jwcloudpms.com/3163001RA-1.0.0-SNAPSHOT",
  "CRSAPI": _.has(window, '__CRMAPI__') && _.trim(window.__CRMAPI__) !== "" ? window.__CRSAPI__ : "crsapi.jwcloudpms.com/3164001RA-1.0.0-SNAPSHOT",
  "EBSAPI": _.has(window, '__EBSAPI__') && _.trim(window.__EBSAPI__) !== "" ? window.__EBSAPI__ : "ebsapi.jwcloudpms.com/3165001RA-1.0.0-SNAPSHOT",
  "SCMAPI": _.has(window, '__SCMAPI__') && _.trim(window.__SCMAPI__) !== "" ? window.__SCMAPI__ : "scmapi.jwcloudpms.com/3166001RA-1.0.0-SNAPSHOT",
  "RPTAPI": _.has(window, '__RPTAPI__') && _.trim(window.__RPTAPI__) !== "" ? window.__RPTAPI__ : "rptapi.jwcloudpms.com/1160001RA-1.0.0-SNAPSHOT",
  "IIRAPI": _.has(window, '__IIRAPI__') && _.trim(window.__IIRAPI__) !== "" ? window.__IIRAPI__ : "iirapi.jwcloudpms.com/1161001RA-1.0.0-SNAPSHOT",
  "GFCAPI": _.has(window, '__GFCAPI__') && _.trim(window.__GFCAPI__) !== "" ? window.__GFCAPI__ : "gfcapi.jwcloudpms.com/3167001RA-1.0.0-SNAPSHOT",
  "GRPAPI": _.has(window, '__GRPAPI__') && _.trim(window.__GRPAPI__) !== "" ? window.__GRPAPI__ : "grpapi.jwcloudpms.com/3168001RA-1.0.0-SNAPSHOT",
  "CMSAPI": _.has(window, '__CMSAPI__') && _.trim(window.__CMSAPI__) !== "" ? window.__CMSAPI__ : "cmsapi.jwcloudpms.com/1162001RA-1.0.0-SNAPSHOT",
  "LOGAPI": _.has(window, '__LOGAPI__') && _.trim(window.__LOGAPI__) !== "" ? window.__LOGAPI__ : "logapi.jwcloudpms.com/3261001RA-1.0.0-SNAPSHOT",
  "OPAPI": _.has(window, '__OPAPI__') && _.trim(window.__OPAPI__) !== "" ? window.__OPAPI__ : "opapi.jwcloudpms.com/3153001O-1.0.0-SNAPSHOT",
}

const hostMapIp = {
  "pmsapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__PMSAPI__') && _.trim(window.__PMSAPI__) !== "")
      ? windowMapAip["PMSAPI"]
      : `${devApiHost}:9091/`,
  "posapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__POSAPI__') && _.trim(window.__POSAPI__) !== "")
      ? windowMapAip["POSAPI"]
      : `${devApiHost}:9093/`,
  "lpsapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__LPSAPI__') && _.trim(window.__LPSAPI__) !== "")
      ? windowMapAip["LPSAPI"]
      : `${devApiHost}:9093/`,
  "crmapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__CRMAPI__') && _.trim(window.__CRMAPI__) !== "")
      ? windowMapAip["CRMAPI"]
      : `${devApiHost}:9093/`,
  "crsapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__CRMAPI__') && _.trim(window.__CRMAPI__) !== "")
      ? windowMapAip["CRSAPI"]
      : `${devApiHost}:9093/`,
  "ebsapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__EBSAPI__') && _.trim(window.__EBSAPI__) !== "")
      ? windowMapAip["EBSAPI"]
      : `${devApiHost}:9093/`,
  "scmapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__SCMAPI__') && _.trim(window.__SCMAPI__) !== "")
      ? windowMapAip["SCMAPI"]
      : `${devApiHost}:9093/`,
  "rptapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__RPTAPI__') && _.trim(window.__RPTAPI__) !== "")
      ? windowMapAip["RPTAPI"]
      : `${devApiHost}:9094/`,
  "iirapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__IIRAPI__') && _.trim(window.__IIRAPI__) !== "")
      ? windowMapAip["IIRAPI"]
      : `${devApiHost}:9093/`,
  "gfcapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__GFCAPI__') && _.trim(window.__GFCAPI__) !== "")
      ? windowMapAip["GFCAPI"]
      : `${devApiHost}:9093/`,
  "grpapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__GRPAPI__') && _.trim(window.__GRPAPI__) !== "")
      ? windowMapAip["GRPAPI"]
      : `${devApiHost}:9091/`,
  "cmsapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__CMSAPI__') && _.trim(window.__CMSAPI__) !== "")
      ? windowMapAip["CMSAPI"]
      : `${devApiHost}:9093/`,
  "logapi.jwcloudpms.com":
    (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__LOGAPI__') && _.trim(window.__LOGAPI__) !== "")
      ? windowMapAip["LOGAPI"]
      : `${devApiHost}:9093/`,
  "opapi.jwcloudpms.com":
      (process.env.PUBLIC_NETWORK === "true" && process.env.NODE_ENV === "production") || (_.has(window, '__OPAPI__') && _.trim(window.__OPAPI__) !== "")
        ? windowMapAip["OPAPI"]
        : `${devApiHost}:9092/`,
}

const proApiHost = {
  "3520": "crmapi.jwcloudpms.com",
  "3510": "crmapi.jwcloudpms.com",
  "3540": "lpsapi.jwcloudpms.com",
  "3550": "crsapi.jwcloudpms.com",
  "3560": "ebsapi.jwcloudpms.com",
  "3150": "grpapi.jwcloudpms.com",
  "3130": "grpapi.jwcloudpms.com",
  "3140": "grpapi.jwcloudpms.com",
  "3160": "grpapi.jwcloudpms.com",
  "3104": "grpapi.jwcloudpms.com",
  "3250": "pmsapi.jwcloudpms.com",
  "3280": "pmsapi.jwcloudpms.com",
  "3290": "pmsapi.jwcloudpms.com",
  "3210": "pmsapi.jwcloudpms.com",
  "3220": "pmsapi.jwcloudpms.com",
  "3240": "pmsapi.jwcloudpms.com",
  "3270": "pmsapi.jwcloudpms.com",
  "3410": "pmsapi.jwcloudpms.com",
  "3310": "posapi.jwcloudpms.com",
  "3260": "gfcapi.jwcloudpms.com",
  "1310": "cmsapi.jwcloudpms.com",
  "3102": "logapi.jwcloudpms.com",
  "7220": "opapi.jwcloudpms.com",
}

export const getApiHost = (url) => {
  // 判断请求的地址，是否为／开头，如果不是添加上／
  let _url = url;
  if (!_.startsWith(_url, '/')) {
    _url = `/${_url}`;
  }

  // 查找请求的url中／bs／的位置
  _url = _url.replace('/bs/', "");

  const urlKey = _url.substring(0, 4);

  const httpStr = `${process.env.HTTPSAPI}` === "true" ? "https" : "http";

  if (_.has(proApiHost, urlKey)) {
    return `${httpStr}://${hostMapIp[proApiHost[urlKey]]}${url}`;
  } else {
    return `${httpStr}://${hostMapIp["grpapi.jwcloudpms.com"]}${url}`;
  }

}