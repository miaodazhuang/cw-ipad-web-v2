/**
 * systemName:开台
 * author:
 * remark:
 */
// 公共的ajax帮助库
import { FetchHandle } from 'Hyutilities/request_helper';

export default class Susing_opentable {
  //#region 业务方法

  init = async (params) => {
    return await FetchHandle('/bs/3220010/PosTableblock/queryAcctForBlock', 'get', params);
  }

  /**
 "fontAcctNo":"销售点对应账户",
  "fastQuery":""
* 查询
*/
  queryAcctForBlock = async (params) => {
    return await FetchHandle('/bs/3220010/PosTableblock/queryAcctForBlock', 'get', params);
  }


  /**
 "fontAcctNo":"销售点对应账户",
  "fastQuery":""
* 开台
*/
  openBlock = async (params) => {
    return await FetchHandle('/bs/3220010/PosTableblockItem/openBlock', 'post', params);
  }
  //#endregion
}