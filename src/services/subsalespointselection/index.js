/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 */
// 公共的ajax帮助库
import { FetchHandle } from 'Hyutilities/request_helper';
import _ from 'lodash';
import { baseconvert } from 'Hyutilities/convertselectdata_helper';
import CommonService from 'Hyservices/common-service';
const _commonService = new CommonService();

export default class Ssubsalespointselection {
  //#region 业务方法
  /**
 * 初始化方法
 *
 * @memberof Ssubsalespointselection
 */
  init = async (params, pageType, pageTypeParams) => {
    let resultData = {};
    if (pageType === "periodIdData") {
      let { data } = await FetchHandle('/bs/3310010/PosPeriod/listGetConfigPeriod', 'get', pageTypeParams);
      // 整理参数集合
      if (_.size(data) !== 0) {
        let _itemData = baseconvert(data, ["start_tm"], "period_id");
        resultData['PERIOD'] = _itemData;
      }

    } else {
      resultData = await _commonService.queryDictionary(params)
    }
    //查询销售点列表
    return resultData;
  }


  /**
   * 切换销售点
   *
   * @memberof Ssubsalespointselection
   */
  saveInitSales = async (params) => {
    //切换销售点
    await FetchHandle('/bs/3310010/PosSales/saveInitSales', 'post', params);
    //获取当前站点信息
    const { resultData = {} } = await FetchHandle('/bs/3310010/PosPeriod/listGetPeriodNow', 'get', params);
    //查询底部当前销售点汇总信息
    const { data = [] } = await FetchHandle('/bs/3310010/PosBlock/ListGetTotal', 'get', {
      pageLength: 20,
      pageStart: 1,
      ...params
    })
    let pointTotal = {};
    if (_.size(data) !== 0) {
      pointTotal = { ..._.head(data) };
    }
    return {
      periodNow: resultData,
      pointTotal: pointTotal,
    }

  }

  //#endregion
}