/**
 * systemName:
 * author:
 * remark:
 */
// 公共的ajax帮助库
import { baseconvert } from 'Hyutilities/convertselectdata_helper';
import { FetchHandle } from 'Hyutilities/request_helper';
export default class Ssubaddpackage {
  //#region 业务方法
  //#endregion
  init = async (params) => {
    const resultData = await Promise.all([
      FetchHandle('/bs/3130010/GrpRsvRateCommon/queryGrpRsvRatePackageListInfo', 'get', params),
      FetchHandle("/bs/Dictionary/getDictionary", "get", {
        reqData: {
          PARA_TRNCODE: { PARA_TRNCODE: { trn_typs: "800,810,899,890,891" } }
        }
      })
    ])
    return { data: resultData[0] || [], price: resultData[1] || [] }
  }

  ccardtype = async (params) => {
    return await FetchHandle('/bs/Dictionary/getDictionary', 'get',
      { reqData: { CCARDTYP: { CCARDTYP: {} } } })
  }

  noUsePkg = async (params) => {
    return await FetchHandle('/bs/3240010/HptFinTransOpr/noUsePkg', 'post', params)
  }


  openBlockTrn = async (params) => {
    return await FetchHandle('/bs/3220010/PosTableblockItem/openBlockTrn', 'post', params)
  }



  /**
 * 通过入账代码获取电子钱和银行卡类型
 *
 * @param {"trncd_id":8}
 * @memberof CommonService
 */
  getDictionaryCCARDTYP = async (params) => {
    // 获取系统返回的共通参数
    const _reVal = await FetchHandle(`/bs/3130010/CmmBaseParam/queryListByTrncdId`, 'get', params);
    let _reDicData = {};
    // 整理参数集合
    if (_reVal && _reVal.data) {
      let _itemData = baseconvert(_reVal.data, ["param_cd", "param_drpt"], "param_id");
      _reDicData['CCARDTYP'] = _itemData;
    }
    return _reDicData;
  }


    //计算税后价格
    calcGoods = async (params) => {
      return FetchHandle("/bs/3220010/RsvAccount/calcGoods", 'get', params);
    }




}
