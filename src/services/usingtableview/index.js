/**
 * systemName:
 * author:
 * remark:
 */
// 公共的ajax帮助库
import { FetchHandle } from 'Hyutilities/request_helper';
import _ from 'lodash';
import CommonService from 'Hyservices/common-service';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/usingtableview';
import ja_jp from 'ja_jp/usingtableview';
import zh_cn from 'zh_cn/usingtableview';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });
const _commonService = new CommonService();

export default class Susingtableview {
  //#region 业务方法

  init = async (dictTyps, listParams) => {
    const result = await Promise.all([
      _commonService.queryDictionary(dictTyps),
      this.queryList(listParams)
    ])
    return { data: result[1].data, seq: result[1].seq, blockData: result[1].blockData, dicData: result[0] };
  }

  /**
   * 查询早餐列表
   * {
      "salesId": "21024", 销售点id
      "useDt":"2020-12-07", 日期
      "periodFlg":"1", 房含属性
      "periodId":"21008",餐点id 时间
      "blockTypId":12 桌台类 型id
      "groupTyp":"1" 1餐点 2同来人
      "fastQuery":"123" // 模糊搜索
      "memoFlg":"1" 只展示有备注
  }
   * @memberof Spostableview
   */
  queryList = async (params) => {
    let _result = await FetchHandle("/bs/3220010/PosTableblock/queryPosBlockMap", 'get', params);
    let resultData = {
      data: {},
      seq: [],
      blockData: []
    };
    if (_.size(_result.resultData) !== 0) {
      resultData = _result.resultData;
    }
    //计算汇总
    let _total_quant = 0, _free_quant = 0;
    let _blockData = _.cloneDeep(resultData.blockData);
    _.forEach(resultData.blockData, i => {
      _total_quant = _total_quant + i.total_quant;
      _free_quant = _free_quant + i.free_quant;
    })
    _blockData.unshift({
      "block_id": "",
      "block_drpt": languageConfig.all,
      "free_quant": _free_quant,
      "use_quant": 0,
      "total_quant": _total_quant
    })

    return { data: resultData.data || {}, seq: resultData.seq || [], blockData: _blockData || [] };
  }

  //#endregion
}