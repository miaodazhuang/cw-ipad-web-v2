import _ from 'lodash';
import { FetchHandle } from 'Hyutilities/request_helper';
import { SetSessionItem } from 'Hyutilities/sessionstorage_helper';
import { SetCookie, RemoveCookie } from 'Hyutilities/cookie_helper';
import { IndexConvert } from 'Hyutilities/convertselectdata_helper';
import EncryptClass from 'Hyutilities/encrypt_helper';

import moment from 'moment';


export async function login(params) {
  // 调用登录接口
  const _reloginData = await FetchHandle('/login', 'post', params.paramsdata);
  // 创建操作员填写的登录数据
  const _userData = {
    unitCd: params.paramsdata.unitCd,
    chainCd: params.paramsdata.chainCd,
    dbType: params.paramsdata.dbType,
    accountCode: params.paramsdata.accountCode,
    isRemember: params.isRemember,
  }
  return await packageLoginData(_reloginData, _userData);
}

/**
 * 整理登录数据
 *
 * @param {any} loginData 获取的异步数据
 * @param {any} userData 操作员输入的登录信息
 * @param {any} isHandleCookie 是否进行cookie操作
 * @returns
 */
async function packageLoginData(loginData, userData, isHandleCookie) {

  if (userData.isRemember) {
    // 保存酒店代码
    SetCookie('UnitCd', userData.unitCd, { expires: userData.isRemember });
    // 保存集团代码
    SetCookie('ChainCd', userData.chainCd, { expires: userData.isRemember });
    // 设置登录库
    SetCookie('DbType', userData.dbType, { expires: userData.isRemember });
    // 保存语言
    SetCookie('lang', global.__LANGUAGE__, { expires: userData.isRemember });
    //账号
    SetCookie('AccountCode', userData.accountCode, { expires: userData.isRemember });
    //密码不存储 存在安全问题
  } else {
    RemoveCookie('AccountCode');
  }
  // 创建登录信息对象
  const _userData = {
    // 单位信息
    "UnitInfo": {
      // 设置用户集团用户标识 取unitUidExcu属性（执行酒店-当前登录酒店信息）
      "UnitFlg": loginData.unitUidExcu.unitFlg,
      // 酒店ID
      "UnitUid": loginData.unitUidExcu.unitUid,
      // 酒店名称
      "UnitNm": loginData.unitUidExcu.unitNm,
      // 酒店代码
      "UnitCd": userData.unitCd,
      // 是否有上级单位
      "ParentUnitFlg": loginData.parentUnitFlg
    },
    // 所属单位信息
    "OuUnitInfo": {
      // 当前操作员所属单位信息
      "UnitFlg": loginData.unitUidOu.unitFlg,
      // 酒店ID
      "UnitUid": loginData.unitUidOu.unitUid,
      // 酒店名称
      "UnitNm": loginData.unitUidOu.unitNm,
    },
    // 集团信息
    "ChainInfo": {
      // 集团代码
      "ChainCd": userData.chainCd,
      // 集团名称
      "ChainNm": loginData.chainNm,
      // 集团ID
      "ChainUid": loginData.chainUid,
      // 集团类型
      "ChainFlg": loginData.chainFlg
    },
    // 操作员信息
    "UserInfo": {
      // 操作员ID
      "UserUid": loginData.userUid,
      //操作员代码
      "UserCd":userData.accountCode,
      // 操作员姓名
      "UserNm": loginData.userNm,
      // 用户Session KEY
      "JW_DATA": loginData.responseCommonDto.sessionKey
    },
    // 用户菜单
    "UserMenus": loginData.userMenus,
    // 用户权限属性
    "UserParams": IndexConvert(loginData.userPerms, 'code'),
    // 开关表属性
    "OptionMap": loginData.optionMap,
    // sku数据
    "skuMap": loginData.skuMap,
    // 用户权限属性
    "funMap": IndexConvert(loginData.funCode, 'code'),
    // 用户权级
    "UserRightCls": loginData.userRightCls,
    // 币种属性
    "Currency": loginData.currency,
    // 酒店业务日期
    "BusinessDt": loginData.businessDt,
    // 通用快捷键
    "shortKeyMap": loginData.shortKeyMap,
    // 界面sku
    "views": loginData.views,
    // 工作站号
    "WsNo": loginData.wsNo,
  }

  // 用于创建用户加密类
  const _encryptClass = new EncryptClass(global.__USERDATAKEY__);
  // 根据秘钥创建登录数据
  const _encryptData = _encryptClass.Encrypt(JSON.stringify(_userData), true);

  // 缓存登录数据
  await SetSessionItem('UserData', _encryptData.orgData);
  await SetSessionItem('EncryptStr', _encryptData.encryptData);

  // 集团信息
  global.__CHAININFO__ = _userData.ChainInfo;
  // 单位信息
  global.__UNITINFO__ = _userData.UnitInfo;
  // 所属单位信息
  global.__OUUNITINFO__ = _userData.OuUnitInfo;
  // 操作员信息
  global.__USERINFO__ = _userData.UserInfo;
  // 用户权限属性
  global.__USERPARAMS__ = _userData.UserParams;
  // 开关表属性
  global.__OPTIONMAP__ = _userData.OptionMap;
  // 币种属性
  global.__CURRENCY__ = _userData.Currency;
  // 操作员菜单
  global.__USERMENU__ = _userData.UserMenus;
  // 用户权级
  global.__USERRIGHTCLS__ = _userData.UserRightCls;
  // 酒店业务日期
  global.__BUSINESSDT__ = moment(moment(_userData.BusinessDt).format('L')).valueOf();
  // 工作站号
  global.__WSNO__ = _userData.WsNo;
  // 实例化加密类
  global.__ENCRYPT__ = new EncryptClass(_userData.UserInfo.UserUid);
  // 数据 {}
  global.__SKUMAP__ = _userData.skuMap;
  // 页面
  global.__VIEWSMAP__ = _userData.views;
  // 快捷键数据
  packageKeyCodes(_userData.shortKeyMap);

  // 获取token
  await getToken(userData.isRemember);

  return loginData;
}



/**
 * 获取token
 *
 * @export
 */
export async function getToken(_isRemember) {
  const _retokenData = await FetchHandle('/bs/BsToken/getToken', 'post', null, 'Component').catch(e => {
    throw e
  });
  SetCookie('JW_TOKEN', JSON.stringify({ 'Component': _retokenData.responseCommonDto.token }), { expires: _isRemember });
}

/**
 * 整理快捷键数据
 *
 * @export
 * @param {any} keyCodeData
 */
export async function packageKeyCodes(keyCodeData) {

  const keys = {};

  if (!keyCodeData || _.size(keyCodeData) === 0) {
    return {};
  }

  _.forEach(keyCodeData, item => {
    global.__SHORTKEYMAP__[item.shortkey_cd] = _.trim(item.shortkey).toLowerCase();
  })

  return keys;
}
