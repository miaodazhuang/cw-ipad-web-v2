import _ from 'lodash';
import { FetchHandle } from 'Hyutilities/request_helper';
import { SetSessionItem, GetSessionItem } from 'Hyutilities/sessionstorage_helper';
import { baseconvert } from 'Hyutilities/convertselectdata_helper';

export default class CommonService {
  /**
   * 查询共通参数
   *
   * @memberof CommonService
   */
  queryDictionary = async (params, controlParams = null) => {
    // 获取缓存的字典数据
    let _dicData = await GetSessionItem("DictionaryData") || {};

    // 需要返回的字典数据
    let _reDicData = {};

    // 整理查询参数
    let reqData = {};
    _.forEach(params, (item, index) => {
      let _item = _.cloneDeep(item);
      let addunit = false;
      if (_.findIndex(_item, (i) => { return i === '*' }) !== -1) {
        addunit = true;
        _item = _item.substring(0, (_.size(_item) - 1));
      }
      const dicName = `${_item}_${controlParams}`;
      if (_.has(_dicData, dicName)) {
        // 如果缓存的字典数据中存在该Key值，则使用缓存数据做为返回值
        _reDicData[_item] = _dicData[dicName];
      } else {
        // 否则进行查询参数拼接
        reqData[_item] = {
          [_item]: {
            "dicFlg": controlParams ? 1 : null,
            "unit_uid": addunit ? global.__UNITINFO__.UnitUid : null
          }
        };
      }
    });

    let _reVal = null;
    if (_.size(reqData) > 0) {
      // 获取系统返回的共通参数
      _reVal = await FetchHandle(`/bs/Dictionary/getDictionary`, 'get', { reqData });
    }

    // 整理参数集合
    if (_reVal && _reVal.responseData) {
      _.forEach(_reVal.responseData, (item, key) => {
        //如果是国家代码，如中国是  CHN 中国 0086 添加国家代码(存放在param_str10属性上)
        let _itemData = baseconvert(item, ["param_cd", "param_drpt", key === 'COUNTRY' ? "param_str10" : ""], "param_id");
        // 设置需要返回的值
        _reDicData[key] = _itemData;
        // 更新缓存的数据
        _dicData[`${key}_${controlParams}`] = _itemData;
      })
    }

    // 更新浏览器缓存
    await SetSessionItem("DictionaryData", _dicData);

    return _reDicData;
  }


  /**
 * 本单位下查询共通参数
 *
 * @memberof CommonService
 */
  queryDictionaryUnitUid = async (params, unitUid, controlParams = null, ) => {
    // 需要返回的字典数据
    let _reDicData = {};
    // 整理查询参数
    let reqData = {};
    _.forEach(params, (item, index) => {
      // 否则进行查询参数拼接
      reqData[item] = {
        [item]: {
          "dicFlg": controlParams ? 1 : null,
          "unit_uid": unitUid,
        }
      };
    });
    let _reVal = null;
    if (_.size(reqData) > 0) {
      // 获取系统返回的共通参数
      _reVal = await FetchHandle(`/bs/Dictionary/getDictionary`, 'get', { reqData });
    }
    // 整理参数集合
    if (_reVal && _reVal.responseData) {
      _.forEach(_reVal.responseData, (item, key) => {
        //如果是国家代码，如中国是  CHN 中国 0086 添加国家代码(存放在param_str10属性上)
        let _itemData = baseconvert(item, ["param_cd", "param_drpt", key === 'COUNTRY' ? "param_str10" : "", 'memo'], "param_id");
        // 设置需要返回的值
        _reDicData[key] = _itemData;
      })
    }
    return _reDicData;
  }

  /**
   * 根据类型和父级id查询子集数据
   *
   * @param {any} params [{key,typ,paramgrpId}]
   * @memberof CommonService
   */
  getDictionaryGrp = async (params, controlParams = null) => {
    // 整理查询参数
    let reqData = {};
    _.forEach(params, (item, index) => {
      reqData[item.key] = {
        [item.typ]: {
          "paramgrpId": item.paramgrpId,
          "dicFlg": controlParams ? 1 : null
        }
      }
    });
    // 获取系统返回的共通参数
    const _reVal = await FetchHandle(`/bs/Dictionary/getDictionary`, 'get', { reqData });
    let _reDicData = {};
    // 整理参数集合
    if (_reVal && _reVal.responseData) {
      _.forEach(_reVal.responseData, (item, key) => {
        let _itemData = baseconvert(item, ["param_cd", "param_drpt"], "param_id");
        _reDicData[key] = _itemData;
      })
    }
    return _reDicData;
  }

  /**
   * 根据类型名称查询管控字段
   * @param {*} tableNm
   */
  searchControlField = async (tableNm) => {
    const { resultData } = await FetchHandle('/bs/3140010/GrpCmmCtrlParam/queryCtrlParam', 'get', { paramTyp: tableNm });
    return resultData || {};
  }

  /**
   * 根据业务模块查询快捷键
   *
   * @memberof CommonService
   */
  searchShortKey = async (shortkeyCds) => {
    const { data } = await FetchHandle('/bs/3130010/PlfCmmShortkey/getPlfCmmShortkey', 'get', { shortkeyCds });
    return data || [];
  }

  /**
   * 根据路由ID获取帮助页面
   *
   * @memberof CommonService
   * @param {Object} view_id 页面ID（不传或空则返回主页） language_id 语言Id（不传默认系统当前语言）
   */
  getHelpFrame = async (params) => {
    const { data } = await FetchHandle('/bs/3170010/CmsHelp/getHelpPage', 'get', params);
    return data || [];
  }

  /**
   * 刷新字典参数
   *
   * @memberof CommonService
   * @param {Object} view_id 页面ID（不传或空则返回主页） language_id 语言Id（不传默认系统当前语言）
   */
  refreshDictionary = async (params, controlParams = null) => {
    // 获取缓存的字典数据
    let _dicData = await GetSessionItem("DictionaryData") || {};

    // 需要返回的字典数据
    let _reDicData = {};

    // 整理查询参数
    let reqData = {};
    _.forEach(params, (item, index) => {
      let _item = _.cloneDeep(item);
      let addunit = false;
      if (_.findIndex(_item, (i) => { return i === '*' }) !== -1) {
        addunit = true;
        _item = _item.substring(0, (_.size(_item) - 1));
      }
      reqData[_item] = {
        [_item]: {
          "dicFlg": controlParams ? 1 : null,
          "unit_uid": addunit ? global.__UNITINFO__.UnitUid : null
        }
      };
    });

    let _reVal = null;
    if (_.size(reqData) > 0) {
      // 获取系统返回的共通参数
      _reVal = await FetchHandle(`/bs/Dictionary/getDictionary`, 'get', { reqData });
    }

    // 整理参数集合
    if (_reVal && _reVal.responseData) {
      _.forEach(_reVal.responseData, (item, key) => {
        //如果是国家代码，如中国是  CHN 中国 0086 添加国家代码(存放在param_str10属性上)
        let _itemData = baseconvert(item, ["param_cd", "param_drpt", key === 'COUNTRY' ? "param_str10" : ""], "param_id");
        // 设置需要返回的值
        _reDicData[key] = _itemData;
        // 更新缓存的数据
        _dicData[`${key}_${controlParams}`] = _itemData;
      })
    }

    // 更新浏览器缓存
    await SetSessionItem("DictionaryData", _dicData);

    return _reDicData;
  }

  /**
   * 刷新方法
   *
   * @memberof CommonService
   */
  refresh = async (params) => {
    //查询底部当前销售点汇总信息
    const { data = [] } = await FetchHandle('/bs/3310010/PosBlock/ListGetTotal', 'get', {
      pageLength: 20,
      pageStart: 1,
      salesId: params.salesId,
    })

    let pointTotal = {};
    if (_.size(data) !== 0) {
      pointTotal = { ..._.head(data) };
    }
    //查询菜谱
    let {
      cookbook_id = "",//菜单id
      cookbook_nm = "",//菜单名称
      cookbook_typs = [],//菜单大类小类
      menus = [] //菜单数据
    } = await FetchHandle('/bs/3130030/PosCookbook/getPosCookbook', 'get', params);

    //查询沽清菜品
    const cleardata = await FetchHandle('/bs/3310010/PosClear/savePosGetClearMenus', "post", {
      cookbook_id: cookbook_id,
      ...params
    });


    //循环判断当前菜品是否已经被沽清  如果被沽清了则添加沽清标志
    _.forEach(menus, (i) => {
      //循环菜品规格
      _.forEach(i.prices, (p) => {
        let item = _.find(cleardata.menus, (k) => { return k.menu_id === i.menu_id && k.menu_price_id === p.menu_price_id && k.menu_spec_id === p.menu_spec_id });
        if (item) {
          //当前菜品规格添加上沽清标志
          p.CLEAR = true;
        } else {
          p.CLEAR = false;
        }
      })
    })

    let cookBook = {
      cookBookId: cookbook_id,
      cookBookNm: cookbook_nm,
      cookBookMenu: menus,
      cookBookTyp: _.filter(cookbook_typs, (i) => { return i.cookbook_typ_grade === 1 }),
      cookBookTypSub: _.filter(cookbook_typs, (i) => { return i.cookbook_typ_grade === 2 }),
    };


    return { pointTotal: pointTotal, cookBook: cookBook };
  }

  /**
   * 餐饮清机换市功能
   * @param {any} params
   */
  savePosWorkChange = async(params) => {
    return await FetchHandle('/bs/3310010/PosWork/savePosWorkChange', 'post', params);
  }

  keepAlive = async(params) => {
    return await FetchHandle(`/bs/Dictionary/getDictionary`, 'get', { "reqData": { "FLOOR": { "FLOOR": { } } } });
  }


}