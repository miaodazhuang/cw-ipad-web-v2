/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
// 公共的ajax帮助库
import { FetchHandle } from 'Hyutilities/request_helper';
import CommonService from 'Hyservices/common-service';
import { Promise } from 'es6-promise';

const _commonService = new CommonService();
export default class Spostableview {
  //#region 业务方法

  init = async (dictTyps, listParams) => {
    const result = await Promise.all([
      _commonService.queryDictionary(dictTyps),
      this.queryList(listParams)
    ])
    return { listData: result[1], dicData: result[0] };
  }

  /**
   * 查询早餐列表
   * @memberof Spostableview
   */
  queryList = async (params) => {
    const { resultData=[] } = await FetchHandle("/bs/3240010/HptFinBreakfast/queryAcctBreakfastListPos", 'get', params);
    return { data: resultData };
  }

  //#endregion
}