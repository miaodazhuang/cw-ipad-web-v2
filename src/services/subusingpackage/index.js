/**
 * systemName:
 * author:
 * remark:
 */
// 公共的ajax帮助库
import {FetchHandle} from 'Hyutilities/request_helper';
import CommonService from 'Hyservices/common-service';
const _commonService = new CommonService();

export default class Ssubusingpackage {
  /**
   * 查询可用房含列表数据
   */
  init = async (params) => {
    return  await FetchHandle('/bs/3240010/HptFinBreakfast/getAcctBreakfastPad', 'get', params);
  }

  /**
   * 使用房含
   */
  submitPackagePad = async (params) => {
    return  await FetchHandle('/bs/3240010/HptFinTransOpr/usePackagePad', 'post', params);
  }
}