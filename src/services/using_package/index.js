/**
 * systemName:使用房含
 * author:
 * remark:
 */
// 公共的ajax帮助库
import {FetchHandle} from 'Hyutilities/request_helper';
export default class Susing_package {
  //#region 业务方法
    /**
   * 查询可用房含列表数据
   */
  init = async (params) => {
    return  await FetchHandle('/bs/3220010/PosTableblock/queryPosBlockInfo', 'get', params);
  }


    /**
   * 关台
   */
  closeBlock = async (params) => {
    return  await FetchHandle('/bs/3220010/PosTableblockItem/closeBlock', 'post', params);
  }

  //#endregion
}