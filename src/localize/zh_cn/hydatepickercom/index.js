// 集团首页
export default {
  'showmessage': '请输入正确的日期格式,例如',
  'minErrmessage':'所填日期小于最小日期',
  'maxErrmessage':'所填日期大于最大日期',
  'Sunday':'日',
  'Monday':'一',
  'Tuesday':'二',
  'Wednesday':'三',
  'Thursday':'四',
  'Friday':'五',
  'Saturday':'六',
  'pricedateErrmessage':'终止日期必须大于起始日期!',
  'noBreakfast':'无早',
  'noeBreakfast':'单早',
  'twoBreakfast':'双早',
  'breakfast':'早',
  'year':'年',
  'month':'月',
  today:'今天'
}