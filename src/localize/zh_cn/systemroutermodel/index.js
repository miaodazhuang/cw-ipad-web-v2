export default {
  message_0:'M0000016 : 菜单地址异常',
  message_1:'M0000015 : 没有权限访问该地址',
  message_2:'M0000014 : 地址无效',
  message_3:'M0000013 : 当前操作员无权限进入该界面',
}