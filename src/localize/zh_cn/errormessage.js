const _languageConfig = {
	"FW0001" : "This vendor cannot be found. Check if the vendor code is correct or if the plug-in is not installed.",
}


/**
 * 根据key获取对应的文字信息
 * @param {string} key 语言配置识别号
 */
export function GetLanguage(key) {
	if (_languageConfig) {
		const _languageText = _languageConfig[key];
		return _languageText
			? _languageText
			: `Error Code ${key}，unknown error!`;
	} else {
		return `Error Code ${key}，unknown error!`;
	}
}
