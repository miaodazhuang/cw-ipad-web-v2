// 集团首页
export default {
  'Sunday':'日',
  'Monday':'一',
  'Tuesday':'二',
  'Wednesday':'三',
  'Thursday':'四',
  'Friday':'五',
  'Saturday':'六',
  'year':'年',
  'month':'月',
  'close':'关',
  'sum':'金额',
  'percentage':'百分比',
  'day':'天',
  'more':'更多',
}