/**
 * systemName:
 * author:
 * remark:
 */
export default {
  //标题
  title: "",
  roomNumber: "房号",
  roomNumberPF: "假房房号",
  resName: "客人姓名",
  surplus: "余额",
  usetotal: "合计金额",
  sure: "确定",
  cancel: "取消",
  household: "挂房帐",
  paid: "现付",
  choiceroom: "请选择房含项目",
  paytype: "请选择支付方式",
  creditypt: "请选择信用卡类型",
  successmessage:"操作成功",
  payMoney:"支付金额",
  pseudo_flg_waring:"支払方式を選択してからOKボタンを押してください。",
}
