export default {
    'save': '保存',
    'cancel': '取消',
    'invalid': '无效',
    'moveUpward': '上移',
    'moveDown': '下移',
    'moveToTheTop': '移到上方',
    'moveToTheBottom': '移到下方',
  }