export default {
  'datevalidcheck': '终止日期必须大于起始日期！',
  'breakerCnt0': '无早',
  'breakerCnt1': '单早',
  'breakerCnt2': '双早',
  'breakerCnt3': '早',
  'Sunday':'日',
  'Monday':'一',
  'Tuesday':'二',
  'Wednesday':'三',
  'Thursday':'四',
  'Friday':'五',
  'Saturday':'六',
}
