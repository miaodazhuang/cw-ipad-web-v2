/**
 * confirm组件
 */
export default {
  'cancelButton':'取消',
  'confirmButton':'确认',
  'header':'系统消息',
  'content':'确认进行操作？'
}