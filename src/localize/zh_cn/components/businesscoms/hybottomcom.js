// 底部组件
export default {
 bill_amount: "用餐金额",
 block_num: "桌台",
 empty_num: "空台",
 open_num: "占用",
 psn_num: "用餐人数",
 nextrefreshtime:"下次刷新时间",
 service_rate:'默认服务费率',
 discount_rate:'默认折扣率',
 tax_rate:'默认税率',

}