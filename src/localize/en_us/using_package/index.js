/**
 * systemName:
 * author:
 * remark:
 */
export default {
  title: "PKG利用",
  title_keyong: "利用可能",
  givePay: 'Posting',
  alt_nm: "ゲスト名",
  bal_amt: "未払残高",
  room_num: "客室番号",
  pkg_list: "PKGアイテム",
  save: "OK",
  cancel: "キャンセル",
  successmessage: "確定",
  roomNumberPF: "場所Dummyアカウント",
  memo:"座席備考",
}