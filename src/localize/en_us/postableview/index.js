/**
 * systemName:
 * author:
 * remark:
 */
export default {
  //标题
  title: "房含列表",
  all:'全部',
  toBeUsed: '待使用',
  used: '已使用',
  noContaining: '无房含',
  leaveToUsed: '已离待使用',
  canUse: '可用',
  hadUse: '已用',
  useRoomContains: '使用房含',
  successmessage:'操作成功',
}