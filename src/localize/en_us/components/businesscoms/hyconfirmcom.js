/**
 * confirm组件
 */
export default {
  'cancelButton':'Cancel',
  'confirmButton':'OK',
  'header':'System Message',
  'content':'Are you sure to operate?'
}