/**
 * 菜单多语言
 */
export default {
  'helpInfo':'Help',
  'userInfo':'User ',
  'updatePassWord':'Change Password',
  'refurbishInfo':'Refresh Parameter',
  'out':'Log Off',
  'selectClass':'Change Shift',
  'selectHotel':'Change Hotel ',
  'updatepw':'Change Password',
  'noWSNM':'No Workstation',
  'gzz':'Work Station ',
}
