export default {
  'datevalidcheck': 'End date must be later than start date!',
  'breakerCnt0': 'No Breakfast',
  'breakerCnt1': 'Single Breakfast',
  'breakerCnt2': 'Double Breakfast',
  'breakerCnt3': 'Breakfast',
  'Sunday':'Sunday',
  'Monday':'Monday',
  'Tuesday':'Tuesday',
  'Wednesday':'Wednesday',
  'Thursday':'Thursday',
  'Friday':'Friday',
  'Saturday':'Saturday',
}
