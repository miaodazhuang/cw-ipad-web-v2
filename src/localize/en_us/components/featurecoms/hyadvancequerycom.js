export default {
  advanceQuery: "More",
  reset: "One Key Reset",
  expand: 'Expand',
  shrink: 'Fold',
}