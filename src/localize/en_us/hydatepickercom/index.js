// 集团首页
export default {
  'showmessage': 'Please input date in the correct format, e.g. ',
  'minErrmessage':'The date cant be earlier than the earliest date',
  'maxErrmessage':'The date cant be later than the latest date',
  'Sunday':'Day',
  'Monday':'One',
  'Tuesday':'Two',
  'Wednesday':'Three',
  'Thursday':'Four',
  'Friday':'Five',
  'Saturday':'Six',
  'pricedateErrmessage':'End date must be later than start date!',
  'noBreakfast':'No Breakfast',
  'noeBreakfast':'Single Breakfast',
  'twoBreakfast':'Double Breakfast',
  'breakfast':'Breakfast',
  'year':'Year',
  'month':'Month',
  today:'Today'
}