/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 */
export default {
  //标题
  title: "销售点选择",
  wring:'请选择销售点!',
  save:'确定',
  cancel:'取消',
  out:'注销',
  wring_1:'请选择',
  productFlg_1:"早餐",
  productFlg_3:"午餐",
  productFlg_2:"晚餐",
}