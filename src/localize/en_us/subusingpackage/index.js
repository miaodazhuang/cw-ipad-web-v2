/**
 * systemName:
 * author:
 * remark:
 */
export default {
  roomNo: '房号',
  name: '客人姓名',
  balance: '余额',
  availableRoomFlag: '可用房含',
  sure: '确定',
  cancel: '取消',
  package_select_message: '请选择可用房含',
  package_success_message: '使用房含成功！',
  canUse: '可用',
  givePay: '挂帐',
  useRoomContains: '使用房含',
  successmessage:'操作成功',
  extraPackage: '加收',
  package: '房含'
}
