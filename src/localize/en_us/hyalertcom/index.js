export default {
  'KS':'Profile Reminder',
  'KH':'Customer Reminder',
  'YD':'Reservation Reminder',
  'HY':'Member Reminder',
  'inout':'Arrival and Departure Date',
  'to':'To'
}