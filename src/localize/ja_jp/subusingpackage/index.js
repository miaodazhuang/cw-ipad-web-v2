/**
 * systemName:
 * author:
 * remark:
 */
export default {
  roomNo: '客室番号',
  name: 'ゲスト名',
  balance: '未払残高',
  availableRoomFlag: '利用可能PKG',
  sure: 'OK',
  cancel: 'キャンセル',
  package_select_message: '利用可能PKG項目を選択してください\'',
  package_success_message: 'PKG利用完了！',
  canUse: '利用可能',
  givePay: 'Posting',
  useRoomContains: 'PKG利用',
  successmessage:'確定',
  extraPackage: '追加',
  package: 'PKGアイテム'
}
