/**
 * systemName:桌态图
 * author:lxy
 * remark:
 */
export default {
  //标题
  title: "テーブルステータスマップ",
  all: 'すべて',
  can: "残",
  memoFlg: "座席備考",
  groupTyp_1: "食事時間",
  groupTyp_2: "同行者",
  select: "食事時間選択",
  using_package: "PKG利用",
  using_openpackage: "テーブル設定",
  successmessage: '確定',
  null:" 空き",
}
