/**
 * systemName:销售点选择列表
 * author:lxy
 * remark:
 */
export default {
  //标题
  title: "POS選択",
  wring:'POSを選択してください!',
  save:'OK',
  cancel:'キャンセル',
  out:'ログオフ',
  wring_1:'種別選択',
  productFlg_1: "朝食",
  productFlg_3: "昼食",
  productFlg_2: "夕食",
  all:'すべて',
}