/**
 * systemName:
 * author:
 * remark:
 */
export default {
  //标题
  title: "テーブル設定",
  blockQuant: "テーブル数",
  fastQuery: "滞在ゲスト",
  queryBtn: '検索',
  waring: "１つのアカウントを選択してください。",
  save: "入店",
  cancel: "キャンセル",
  placeholder:"客室番号/ゲスト名",
  notitle:"検索条件に合致する滞在中のゲスト・客室番号が見つかりません。",
}