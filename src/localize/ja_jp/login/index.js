export default {
  'versionDesc': "料飲受付システム",
  'placeholder_groupnum': 'グループコードを入力してください',
  'placeholder_htlnum': '施設コードを入力してください',
  'placeholder_accountCode': 'ユーザーIDを入力してください',
  'placeholder_accountPassword': 'パスワードを入力してください',
  'isRemember':'ログイン情報保存',
  'practice':'Training',
  'production':'Product',
  'copyright':'',
  'login':'ログイン',
  'language': '言語',
  'languageTitle':'言語',
  'language1':"簡体中国語",
  'language2':"日本語",
  'accountPassword':'パスワード',
  'accountCode':'ユーザーID',
  'htlnum':'施設コード',
  'groupnum':'Group Code',

}