/**
 * systemName:
 * author:
 * remark:
 */
export default {
  //标题
  title: "PKGリスト",
  all:'すべて',
  toBeUsed: '未使用',
  used: '使用済',
  noContaining: 'PKG無し',
  leaveToUsed: 'C/O済PKG利用',
  canUse: '利用可能',
  hadUse: '利用済',
  useRoomContains: 'PKG利用',
  successmessage:'確定',
}
