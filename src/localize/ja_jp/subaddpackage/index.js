/**
 * systemName:
 * author:
 * remark:
 */
export default {
  //标题
  title: "",
  roomNumber: "客室番号",
  roomNumberPF: "場所Dummyアカウント",
  resName: "ゲスト名",
  surplus: "残高",
  usetotal: "合計金額",
  sure: "OK",
  cancel: "キャンセル",
  household: "客室Posting",
  paid: "即支払",
  choiceroom: "PKGアイテムを選択してください",
  paytype: "支払方式を選択してください",
  creditypt: "クレジットカードタイプを選択してください",
  successmessage:"確定",
  payMoney:"支払金額",
  pseudo_flg_waring:"支払方式を選択してからOKボタンを押してください。",
}
