/**
 * systemName:
 * author:
 * remark:
 */
import initState from './initstate';
import S__namespace__ from 'Hyservices/__namespace__';
const _S__namespace__ = new S__namespace__();

const _model = {
  namespace: 'M__namespace__',
  state: { ...initState },
  subscriptions: {},
  effects: {
    * init(params, { select, call, put }) { }
  },
  reducers: {
    initComplete(state, action) {
      return {
        ...state
      }
    }
  }
}

export default _model;