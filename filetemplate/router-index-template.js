import dynamic from 'Hyutilities/dynamicload_help';
import { GetLanguage } from 'Hyutilities/language_helper';
import en_us from 'en_us/__namespace__';
import ja_jp from 'ja_jp/__namespace__';
import zh_cn from 'zh_cn/__namespace__';
let languageConfig = GetLanguage({ 'en_us': en_us, 'ja_jp': ja_jp, 'zh_cn': zh_cn });

export default (app) => {
  return dynamic({
    app: app,
    title: languageConfig.title,
    models: () => [
      import("Hymodels/__namespace__")
    ],
    component: () => import('./component')
  });
}