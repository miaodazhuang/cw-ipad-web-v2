var spawn = require('child_process').spawn;
var program = require('commander');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');


program
  .version('0.1.0')
  .option('-n, --namespace [value]', 'Namespace 命名空间')
  .option('-v, --view', 'view 模块')
  .option('-s, --subwindow', 'subwindow 模块')
  .option('-p, --public', 'public 模块')
  .parse(process.argv);

  console.log(program.argv);
  

if (!program.namespace) {
  console.error("namespace 必须输入");
  throw new Error("namespace 必须输入");
}

// 创建需要创建的目录对象
const files = {
  localize: path.join(process.cwd(), `/src/localize/`),
  model: path.join(process.cwd(), `/src/models/${program.namespace}`),
  service: path.join(process.cwd(), `/src/services/${program.namespace}`)
}

if (program.view) {
  files["comdir"] = path.join(process.cwd(), `/src/routers/frame-view/${program.namespace}`);
  files["comfile"] = path.join(process.cwd(), `/src/routers/frame-view/${program.namespace}/component`);
  files["comtemplate"] = path.join(process.cwd(), "/filetemplate/router-template.js");
  files["comdirtemplate"] = path.join(process.cwd(), "/filetemplate/router-index-template.js");
} else if (program.subwindow) {
  files["comdir"] = path.join(process.cwd(), `/src/components/subwindows/${program.namespace}`);
  files["comfile"] = path.join(process.cwd(), `/src/components/subwindows/${program.namespace}/component`);
  files["comtemplate"] = path.join(process.cwd(), "/filetemplate/subwin-template.js");
  files["comdirtemplate"] = path.join(process.cwd(), "/filetemplate/subwin-index-template.js");
} else if (program.public) {
  files["comdir"] = path.join(process.cwd(), `/src/components/plugin/${program.namespace}`);
  files["comfile"] = path.join(process.cwd(), `/src/components/plugin/${program.namespace}/component`);
  files["comtemplate"] = path.join(process.cwd(), "/filetemplate/plugin-template.js");
  files["comdirtemplate"] = path.join(process.cwd(), "/filetemplate/public-index-template.js");
}

//#region 工具方法
/**
* 读取文件
*
* @param {any} filename
* @param {any} encoding
* @returns
*/
async function readFile(filename, encoding) {
  return new Promise(function (resolve, reject) {
    fs.readFile(filename, encoding, function (err, res) {
      if (err) {
        return reject(err);
      }
      resolve(res);
    })
  })
}

/**
* 写入文件
*
* @param {any} filename
* @param {any} data
* @param {any} encoding
* @returns
*/
async function writeFile(filename, data, encoding) {
  return new Promise(function (resolve, reject) {
    fs.writeFile(filename, data, encoding, function (err) {
      if (err) {
        return reject(err);
      }
      resolve();
    })
  })
}

/**
 * 判断文件是否存在
 *
 * @param {*} filename
 * @returns
 */
async function existsFile(filename) {
  return new Promise(function (resolve, reject) {
    fs.exists(filename, function (err, res) {
      if (err) {
        return reject(err);
      }
      resolve(res);
    })
  })
}
//#endregion

async function CreateNamespace() {
  console.info(`开始添加业务模块:${program.namespace}`);
  try {
    console.info(`开始处理语种文件`);
    //#region 处理语种文件
    const localizeDirs = {
      "zh_cn": `${files.localize}/zh_cn/${program.namespace}`,
      "en_us": `${files.localize}/en_us/${program.namespace}`,
      "ja_jp": `${files.localize}/ja_jp/${program.namespace}`
    }
    let localizeFile = await readFile(path.join(process.cwd(), "/filetemplate/localize-template.js"), 'utf-8');

    // 中文
    if (!await existsFile(localizeDirs.zh_cn)) {
      fs.mkdirSync(localizeDirs.zh_cn);
    }
    if (!await existsFile(`${localizeDirs.zh_cn}/index.js`)) {
      await writeFile(`${localizeDirs.zh_cn}/index.js`, localizeFile, 'utf-8');
    }

    // 英文
    if (!await existsFile(localizeDirs.en_us)) {
      fs.mkdirSync(localizeDirs.en_us);
    }
    if (!await existsFile(`${localizeDirs.en_us}/index.js`)) {
      await writeFile(`${localizeDirs.en_us}/index.js`, localizeFile, 'utf-8');
    }

    // 日文
    if (!await existsFile(localizeDirs.ja_jp)) {
      fs.mkdirSync(localizeDirs.ja_jp);
    }
    if (!await existsFile(`${localizeDirs.ja_jp}/index.js`)) {
      await writeFile(`${localizeDirs.ja_jp}/index.js`, localizeFile, 'utf-8');
    }
    //#endregion

    console.info(`开始处理model文件`);
    //#region 处理model文件
    let modelFile = await readFile(path.join(process.cwd(), "/filetemplate/model-template.js"), 'utf-8');
    let modelInitStateFile = await readFile(path.join(process.cwd(), "/filetemplate/model-initstate-template.js"), "utf-8");
    const modelIndex = `${files.model}/index.js`;
    const modelInitState = `${files.model}/initstate.js`;
    if (!await existsFile(files.model)) {
      fs.mkdirSync(files.model);
    }
    if (!await existsFile(modelIndex)) {
      modelFile = modelFile.replace(/\__namespace__/g, program.namespace);
      await writeFile(modelIndex, modelFile, 'utf-8');
    }
    if (!await existsFile(modelInitState)) {
      modelInitStateFile = modelInitStateFile.replace(/\__namespace__/g, program.namespace);
      await writeFile(modelInitState, modelInitStateFile, 'utf-8');
    }
    //#endregion

    console.info(`开始处理service文件`);
    //#region 处理services文件
    let serviceFile = await readFile(path.join(process.cwd(), "/filetemplate/service-template.js"), "utf-8");
    let serviceIndex = `${files.service}/index.js`;
    if (!await existsFile(files.service)) {
      fs.mkdirSync(files.service);
    }
    if (!await existsFile(serviceIndex)) {
      serviceFile = serviceFile.replace(/\__namespace__/g, program.namespace);
      await writeFile(serviceIndex, serviceFile, 'utf-8');
    }
    //#endregion

    console.info(`开始处理视图文件`);
    //#region 处理view subwin public
    let comFile = await readFile(files.comtemplate, "utf-8");
    let comDirFile = await readFile(files.comdirtemplate, "utf-8");
    let comIndex = `${files.comfile}/index.js`;
    let comDirIndex = `${files.comdir}/index.js`;
    if (!await existsFile(files.comdir)) {
      fs.mkdirSync(files.comdir);
    }
    if (!await existsFile(files.comfile)) {
      fs.mkdirSync(files.comfile);
    }
    if (!await existsFile(comIndex)) {
      comFile = comFile.replace(/\__namespace__/g, program.namespace);
      await writeFile(comIndex, comFile, 'utf-8');
    }
    if (!await existsFile(comDirIndex)) {
      comDirFile = comDirFile.replace(/\__namespace__/g, program.namespace);
      await writeFile(comDirIndex, comDirFile, 'utf-8');
    }
    //#endregion
  } catch (e) {
    console.log('e', e);
  }
}

CreateNamespace();