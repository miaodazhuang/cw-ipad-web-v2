const express = require("express");
const http = require("http");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const fs = require("fs");
const FileStreamRotator = require("file-stream-rotator");
const uuid = require("node-uuid");

// http路由配置
const routerHandle = require("./server-routes/http-routes");

const cookieSecret = "zhaoxiang";
const app = express();
const port = normalizePort(process.env.PORT || '3000');
// const sslport = normalizePort(process.env.SSLPORT || '3050');

// =日志==============================
// 日志目录
// var logDirectory = __dirname + '/log'
// // 判断是否存在目录，如果不存在则创建
// fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)
// var accessLogStream = FileStreamRotator.getStream({
//     date_format: 'YYYYMMDD',
//     filename: logDirectory + '/access-%DATE%.log',
//     frequency: 'daily',
//     verbose: true
// })
// logger.token('id', function getId(req) {
//     return req.id
// })
// 创建日志
// app.use(logger(':id :method :url :status :response-time :remote-addr :user-agent ', { stream: accessLogStream }));

// =设置视图目录==============================
app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'html');
app.set('view engine', 'html');

// =应用中间件===============================
// app.use(logger('dev'));
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser(cookieSecret));
app.use(express.static(path.join(__dirname, '/build')));

// =设置路由============================================
routerHandle(app);

// =错误请求===================================================
// 处理404请求
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// 处理500请求

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    console.log('err', err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// 生成请求编号
function assignId(req, res, next) {
    req.id = uuid.v4()
    next()
}

function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        return val;
    }
    if (port >= 0) {
        return port;
    }

    return false;
}


let httpServer = http.createServer(app);
//创建http服务器
httpServer.listen(port, function () {
    console.log('HTTP Server is running port on: ', port);
});