const http = require('http');
const url = require('url');
const base64 = require('js-base64');
module.exports = function (app){
  // 报表路由定义
  app.get('/report/*/:reportUrl', function (req, res, next) {
    try {
      // 如果请求的url是pdf则去掉扩展名
      let _url = req.params.reportUrl.replace('.pdf', '');
      let urlStr = base64.Base64.decode(_url);
      let urlObj = url.parse(urlStr);
      let options = {
        hostname: urlObj.hostname,
        port: urlObj.port,
        method: "GET",
        path: urlObj.path
      };
      var sreq = http.request(options, function (sres) {
        sres.pipe(res);
      });
      sreq.end();
    }
    catch (e) {
      res.send("internal error" + e);
    }
  });
    // 所有的get操作都指向到index.html页面
  app.get(/^((?!\.js).)+$/, function (req, res, next) {
    res.sendfile("./views/index.html");
  });
}